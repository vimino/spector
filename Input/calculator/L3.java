// Import necessary Libraries
import java.util.Scanner;

class SuperCalculator {
	public static void main(String args[])
	{
		// Operation (on) and Operands (od)
		char oper = ' ';
		double first = 0.0, second = 0.0;

		// Prepare a scanner to read the input
		Scanner in = new Scanner(System.in);

		// Show the program title
		System.out.println("Calculator");

		// Repeat until the user chooses to exit (x)
		do {
			oper = ' ';
			first = 0.0;
			second = 0.0;

			// Show the Menu
			System.out.println();
			System.out.println("+) Add");
			System.out.println("-) Subtract");
			System.out.println("*) Multiply");
			System.out.println("/) Divide");
			System.out.println("x) Exit");
			System.out.println();

			// Ask for the users choice
			System.out.print("Choice:\t\t");

			// Read the input (a single character)
			String t;
			try {
				t = in.nextLine();
				oper = t.charAt(0);
			} catch(Exception x) {
				oper = ' ';
			}

			if(oper == '+' || oper == '-' || oper == '/' || oper == '*') {
				// Ask for and read the operands if the user chose an operation
				System.out.print("Operand 1:\t");
				first = in.nextDouble();
				System.out.print("Operand 2:\t");
				second = in.nextDouble();
				in.nextLine();
			} else if(oper == 'x' || oper == 'X') {
				// Do nothing if the user wants to exit
			} else {
				// Give an error if its not a valid operation
				System.out.println("Unknown Operation!");
			}

			// Execute operation and report result
			switch(oper) {
				case '+': // Add
					System.out.println("Result:\t\t"+(first+second));
					break;

				case '-': // Subtract
					System.out.println("Result:\t\t"+(first-second));
					break;

				case '/': // Divide
					if(second == 0.0) { // Stop divisions by zero
						System.out.println("You can't divide by zero.");
					} else {
						System.out.println("Result:\t\t"+(first/second));
					}
					break;

				case '*': // Multiply
					System.out.println("Result:\t\t"+(first*second));
					break;
			}
		} while(oper != 'x');
	}
}
