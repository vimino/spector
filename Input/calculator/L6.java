// Import necessary Libraries
import java.util.Scanner;

class Calculator {
	public static void main(String args[])
	{
		// Operation (on) and Operands (od)
		String on = " ";
		float od1 = 0.0f, od2 = 0.0f;

		// Prepare a scanner to read the input
		Scanner input = new Scanner(System.in);

		// Show the program title
		System.out.println("Calculator");

		// Repeat until the user chooses to exit (x)
		do {
			on = " ";
			od1 = 0.0f;
			od2 = 0.0f;

			// Show the Menu
			System.out.println();
			System.out.println("+) Add");
			System.out.println("-) Subtract");
			System.out.println("*) Multiply");
			System.out.println("/) Divide");
			System.out.println("x) Exit");
			System.out.println();

			// Ask for the users choice
			System.out.print("Choice:\t\t");

			// Read the input (a single character)
			try {
				on = input.nextLine();
			} catch(Exception x) {
				on = " ";
			}

			if(on.charAt(0) == '+' || on.charAt(0) == '-' || on.charAt(0) == '/' || on.charAt(0) == '*') {
				// Ask for and read the operands if the user chose an operation
				System.out.print("Operand 1:\t");
				od1 = input.nextFloat();
				System.out.print("Operand 2:\t");
				od2 = input.nextFloat();
				input.nextLine();
			} else if(on.charAt(0) == 'x' || on.charAt(0) == 'X') {
				// Do nothing if the user wants to exit
			} else {
				// Give an error if its not a valid operation
				System.out.println("Unknown Operation!");
			}

			// Execute operation and report result
			switch(on.charAt(0)) {
				case '+': // Add
					System.out.println("Result:\t\t"+(od1+od2));
					break;

				case '-': // Subtract
					System.out.println("Result:\t\t"+(od1-od2));
					break;

				case '/': // Divide
					if(od2 == 0.0f) { // Stop divisions by zero
						System.out.println("You can't divide by zero.");
					} else {
						System.out.println("Result:\t\t"+(od1/od2));
					}
					break;

				case '*': // Multiply
					System.out.println("Result:\t\t"+(od1*od2));
					break;
			}
		} while(on.charAt(0) != 'x');
	}
}
