// Import necessary Libraries
import java.util.Scanner;

class Calculator {
	public static void main(String args[])
	{
		// Operation (on) and Operands (od)
		char on = ' ';
		double od1 = 0.0, od2 = 0.0;

		// Prepare a scanner to read the input
		Scanner input = new Scanner(System.in);

		// Show the program title
		System.out.println("Calculator");

		// Repeat until the user chooses to exit (x)
		do {
			on = ' ';
			od1 = 0.0;
			od2 = 0.0;

			// Show the Menu
			System.out.println();
			System.out.println("+) Add");
			System.out.println("-) Subtract");
			System.out.println("*) Multiply");
			System.out.println("/) Divide");
			System.out.println("x) Exit");
			System.out.println();

			// Ask for the users choice
			System.out.print("Choice:\t\t");

			// Read the input (a single character)
			String temp;
			try {
				temp = input.nextLine();
				on = temp.charAt(0);
			} catch(Exception x) {
				on = ' ';
			}

			if(on == '/' || on == '*' || on == '+' || on == '-') {
				// Ask for and read the operands if the user chose an operation
				System.out.print("Operand 1:\t");
				od1 = input.nextDouble();
				System.out.print("Operand 2:\t");
				od2 = input.nextDouble();
				input.nextLine();
			} else if(on == 'X' || on == 'x') {
				// Do nothing if the user wants to exit
			} else {
				// Give an error if its not a valid operation
				System.out.println("Unknown Operation!");
			}

			// Execute operation and report result
			switch(on) {
				case '+': // Add
					System.out.println("Result:\t\t"+(od2+od1));
					break;

				case '-': // Subtract
					System.out.println("Result:\t\t"+(-od2+od1));
					break;

				case '/': // Divide
					if(0.0 == od2) { // Stop divisions by zero
						System.out.println("You can't divide by zero.");
					} else {
						System.out.println("Result:\t\t"+(od1/od2));
					}
					break;

				case '*': // Multiply
					System.out.println("Result:\t\t"+(od2*od1));
					break;
			}
		} while('x' != on);
	}
}
