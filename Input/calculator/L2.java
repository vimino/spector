import java.util.Scanner;

class Calculator {
	public static void main(String args[])
	{
		/* Operation and operands */
		char on = ' ';
		double od1 = 0.0, od2 = 0.0;

		Scanner input = new Scanner(System.in);

		System.out.println("Calculator");

		/* Repeat until exit is choosen */
		do {
			on = ' ';
			od1 = 0.0;
			od2 = 0.0;

			System.out.println();
			System.out.println("+) Add");
			System.out.println("-) Subtract");
			System.out.println("*) Multiply");
			System.out.println("/) Divide");
			System.out.println("x) Exit");
			System.out.println();

			System.out.print("Choice:\t\t");

			/* Read choice */
			String temp;
			try {
				temp = input.nextLine();
				on = temp.charAt(0);
			} catch(Exception x) {
				on = ' ';
			}

			if(on == '+' || on == '-' || on == '/' || on == '*') {
				/* Get operands */
				System.out.print("Operand 1:\t");
				od1 = input.nextDouble();
				System.out.print("Operand 2:\t");
				od2 = input.nextDouble();
				input.nextLine();
			} else if(on == 'x' || on == 'X') {
			} else {
				System.out.println("Unknown Operation!");
			}

			/* Do the math */
			switch(on) {
				case '+': // Add
					System.out.println("Result:\t\t"+(od1+od2));
					break;

				case '-': // Subtract
					System.out.println("Result:\t\t"+(od1-od2));
					break;

				case '/': // Divide
					if(od2 == 0.0) { // Stop divisions by zero
						System.out.println("You can't divide by zero.");
					} else {
						System.out.println("Result:\t\t"+(od1/od2));
					}
					break;

				case '*': // Multiply
					System.out.println("Result:\t\t"+(od1*od2));
					break;
			}
		} while(on != 'x');
	}
}
