// Import necessary Libraries
import java.util.Scanner;

class Calculator {
	public static double add(double op1, double op2) { return op1+op2; }
	public static double sub(double op1, double op2) { return op1-op2; }
	public static double mul(double op1, double op2) { return op1*op2; }
	public static double div(double op1, double op2) { return op1/op2; }

	public static char readChar() {
		Scanner input = new Scanner(System.in);
		String temp;
		temp = input.nextLine();
		return temp.charAt(0);
	}

	public static double readDouble() {
		Scanner input = new Scanner(System.in);
		double temp;
		temp = input.nextDouble();
		return temp;
	}

	public static void printMenu() {
		System.out.println();
		System.out.println("+) Add");
		System.out.println("-) Subtract");
		System.out.println("*) Multiply");
		System.out.println("/) Divide");
		System.out.println("x) Exit");
		System.out.println();
	}

	public static void main(String args[])
	{
		// Operation (on) and Operands (od)
		char on = ' ';
		double od1 = 0.0, od2 = 0.0;

		// Show the program title
		System.out.println("Calculator");

		// Repeat until the user chooses to exit (x)
		do {
			on = ' ';
			od1 = 0.0;
			od2 = 0.0;

			// Show the Menu
			printMenu();

			// Ask for the users choice
			System.out.print("Choice:\t\t");

			// Read the input (a single character)
			on = readChar();

			if(on == '+' || on == '-' || on == '/' || on == '*') {
				// Ask for and read the operands if the user chose an operation
				System.out.print("Operand 1:\t");
				od1 = readDouble();
				System.out.print("Operand 2:\t");
				od2 = readDouble();
			} else if(on == 'x' || on == 'X') {
				// Do nothing if the user wants to exit
			} else {
				// Give an error if its not a valid operation
				System.out.println("Unknown Operation!");
			}

			// Execute operation and report result
			switch(on) {
				case '+': // Add
					System.out.println("Result:\t\t"+add(od1,od2));
					break;

				case '-': // Subtract
					System.out.println("Result:\t\t"+sub(od1,od2));
					break;

				case '/': // Divide
					if(od2 == 0.0) { // Stop divisions by zero
						System.out.println("You can't divide by zero.");
					} else {
						System.out.println("Result:\t\t"+div(od1,od2));
					}
					break;

				case '*': // Multiply
					System.out.println("Result:\t\t"+mul(od1,od2));
					break;
			}
		} while(on != 'x');
	}
}
