class AllIn2 {
	static void greet(String name) {
		System.out.println("Hello " + name);
	}

	public static void main(String[] args) {
		int y = 5;
		while(y*2 == 10) {
			greet("John");
			break;
		}
	}
}
