import java.util.Scanner;

class Game21Matches {
	// Global scanner to read the input
	public static Scanner input = new Scanner(System.in);

	public static int CompTurn(int f, char e, int d)
	{
		/* Number of matches that the Comp. will take */
		int r = 1;

		/* Computer Turn */
		if (e == 'n')
		{
			/* If all the matches are left, the Computer takes 4 matches */
			if (f == 21)
				r = 4;
			else
			{
				if (f > 11)
					r = 5-d + ((f-1) % 5);
				else
					r = 5-d;
			}
		}
	
		/* Never take more than 4 matches */
		if (r > 4)
		   r = 4;

		/* If it is not the First Player, use the following algorithm */
		if (e == 'y')
			r = 5-d;

		/* If there are only 5 matches or less, the Computer makes the smart choice */
		if ((f <= 5) && (f>1))
			r = f-1;

		/* Return the number of matches taken */
		return r;
	}

	public static int PlayTurn(int f)
	{
		/* Number of matches that the Play. will take */
		int j = 1;

		/* Is the play valid ? */
		int ok = 0;

		do {
			System.out.print("How many Matches will you take (1 to 4) : ");

			j = input.nextInt();
			input.nextLine();

			/* Control matches taken */
			if(j < 1)
				System.out.println("\nNumber too Low!\n");
			else if(j > 4)
				System.out.println("\nNumber too High!\n");
			else if((f < 4) && (j > f)) 
				System.out.println("\nThere aren't that many matches left!\n");
			else
				ok = 1;
		} while(ok == 0);

		/* Returns the number of Matches that the Player takes */
		return j;
	}

	public static void Game(int Type)
	{
		/* Number of matches left */
		int f = 21;

		/* Type of game */
		char e = ' ';

		/* Whose turn is it ? */
		int v = 1;

		/* Number of matches taken */
		int d = 0;

		if(Type == 1)
		{
			do {
				System.out.print("\nDo you want to be the First Player (y or n) : ");
				String temp;
				try {
					temp = input.nextLine();
					e = temp.charAt(0);
				} catch(Exception x) {
					e = ' ';
				}

				if ((e != 'y') && (e != 'n'))
					System.out.println("\nInvalid Choice !");
			} while ((e != 'y') && (e != 'n'));
		}

		while(f > 1)
		{
			System.out.println("\nMatches : "+f);

			System.out.println("Player Turn : "+v);

			/* Who plays ? */
			if(Type == 1)
			{
				if (((e == 'n') && (v == 1)) || ((e == 'y') && (v != 1)))
				{
					d = CompTurn(f,e,d);

					System.out.println("How many matches do you want to take (1 to 4) : "+d);
				}
				else
				{
					d = PlayTurn(f);
				}
			}
			else
			{
				d = PlayTurn(f);
			}

			f -= d;

			/* If no one won, repeat */
			if(f > 1)
			{
				v++;
				if (v > 2)
					v = 1;
			}
		}

		System.out.println("\nThe Winner is Player "+v+"!\n");
	}

	public static void main(String[] args)
	{
		int op = 0;

		System.out.println("Game of the 21 Matches");
		System.out.println("\nRules:");
		System.out.println("\tThere are 21 Matches.");
		System.out.println("\tEach Player can take 1 to 4 Matches in his turn.");
		System.out.println("\tThe Player that takes the last Match loses.");

		do {
			System.out.println("\n1) Play, Human versus Computer");
			System.out.println("2) Play, Human versus Human");
			System.out.println("0) Exit\n");

			System.out.print("Choice: ");
			op = input.nextInt();
			input.nextLine();

			switch(op)
			{
			case 0:
				/* Quit game */
				break;
			case 1:
				/* Human versus Computer game */
				Game(op);
				break;
			case 2:
				/* Human versus Human game */
				Game(op);
				break;
			default:
				System.out.println("\nInvalid Number !");
			}
		} while(op != 0);
	}
}
