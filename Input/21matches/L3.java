/* Library(s) used by the Program */
import java.util.Scanner;

class G21Matches {
	// Global scanner to read the input
	public static Scanner in = new Scanner(System.in);

	public static int CoTu(int left, char first, int taken)
	{ /* Function that handles a Computer turn and returns his choice */
		/* Saves the number of matches that the Comp. will take */
		int play = 1;

		/* If its the Comp. turn, he uses the following algorithm */
		if (first == 'n')
		{
			/* If all the matches are left, the Computer takes 4 matches */
			if (left == 21)
				play = 4;
			else
			{
				if (left > 11)
					play = 5-taken + ((left-1) % 5);
				else
					play = 5-taken;
			}
		}
	
		/* Ensures that the Computer never takes more than 4 matches */
		if (play > 4)
		   play = 4;

		/* If it is not the First Player, use the following algorithm */
		if (first == 'y')
			play = 5-taken;

		/* If there are only 5 matches or less, the Computer makes the smart choice */
		if ((left <= 5) && (left>1))
			play = left-1;

		/* Return the number of matches that the Comp. takes */
		return play;
	}

	public static int PlTu(int left)
	{ /* Function that handles a Player turn and returns his choice */
		/* Player Play */
		int play = 1;

		/* Is a valid Play (0 = No) */
		int ok = 0;

		do {
			/* Asks how many Matches the Player wants to take ? */
			System.out.print("How many Matches will you take (1 to 4) : ");

			/* Gets the number of Matches that the Player wants to take */
			play = in.nextInt();
			in.nextLine();

			/* Verifies if the value is valid */
			if(play < 1)
				System.out.println("\nNumber too Low!\n");
			else if(play > 4)
				System.out.println("\nNumber too High!\n");
			else if((left < 4) && (play > left)) 
				System.out.println("\nThere aren't that many matches left!\n");
			else
				ok = 1;
		} while(ok == 0);

		/* Returns the number of Matches that the Player takes */
		return play;
	}

	public static void Ga(int Type)
	{ /* Game Loop, ends when there are no matches left */
		/* Says how many matches are left */
		int left = 21;

		/* Says if Game is versus Player or Computer */
		char first = ' ';

		/* Says whose turn it is (Player number) */
		int turn = 1;

		/* Number of matches taken */
		int taken = 0;

		/* On games versus Computer, ask who starts */
		if(Type == 1)
		{
			do {
				/* Ask and get the choice of the Player */
				System.out.print("\nDo you want to be the First Player (y or n) : ");
				String temp;
				try {
					temp = in.nextLine();
					first = temp.charAt(0);
				} catch(Exception x) {
					first = ' ';
				}

				/* Warn the Player if the choice is invalid */
				if ((first != 'y') && (first != 'n'))
					System.out.println("\nInvalid Choice !");
			} while ((first != 'y') && (first != 'n'));
		}

		/* Main Game Loop */
		while(left > 1)
		{
			/* Show matches left */
			System.out.println("\nMatches : "+left);

			/* Say whose turn it is */
			System.out.println("Player Turn : "+turn);

			/* Who should play, according to Game type */
			if(Type == 1)
			{ /* Hum. versus Comp. */
				/* See who should play */
				if (((first == 'n') && (turn == 1)) || ((first == 'y') && (turn != 1)))
				{ /* Comp. turn */
					/* Run the CoTu function and get his play */
					taken = CoTu(left,first,taken);

					/* Shows the question and the Comp. choice */
					System.out.println("How many matches do you want to take (1 to 4) : "+taken);
				}
				else
				{ /* Its the Hum. turn */
					/* Run the PlayTurn function and get the Play. choice */
					taken = PlTu(left);
				}
			}
			else
			{ /* Human versus Human */
				/* Run the PlayTurn function and get the Player choice  */
				taken = PlTu(left);
			}

			/* Take matches from the total */
			left -= taken;

			/* If no one won, Go to next turn */
			if(left > 1)
			{
				turn++;
				if (turn > 2)
					turn = 1;
			}
		}

		/* Show who Won */
		System.out.println("\nThe Winner is Player "+turn+"!\n");
	}

	public static void main(String[] args)
	{ /* Main Loop,the Menu is a Loop that only stops when the User asks to Exit */
		/* Player Option */
		int op = 0;

		/* Presentation of the Game and its Rules */
		System.out.println("Game of the 21 Matches");
		System.out.println("\nRules:");
		System.out.println("\tThere are 21 Matches.");
		System.out.println("\tEach Player can take 1 to 4 Matches in his turn.");
		System.out.println("\tThe Player that takes the last Match loses.");

		/* Beginning of the Main Loop */
		do {
			/* Initial Menu */
			System.out.println("\n1) Play, Human versus Computer");
			System.out.println("2) Play, Human versus Human");
			System.out.println("0) Exit\n");

			/* Get and Interpret the Choice */
			System.out.print("Choice: ");
			op = in.nextInt();
			in.nextLine();

			switch(op)
			{
			case 0:
				/* Exit the Game */
				break;
			case 1:
				/* Run the Game */
				Ga(op);
				break;
			case 2:
				/* Run the Game */
				Ga(op);
				break;
			default:
				/* Tell the User that he made an Invalid Choice */
				System.out.println("\nInvalid Number !");
			}
		} while(op != 0);
	}
}
