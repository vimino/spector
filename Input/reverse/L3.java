// (Argument) String Reverser
public class esreveR {
	// This method joins an Array of Strings into a String
    private static String a2s(String[] arr, boolean sep) {
		// Start with an empty result string
        String res = "";

		// For each element in the array
        for(String ele : arr) {
			// If we want to separate the elements and we already have one
            if(sep && res.length() > 0) {
				// Add a white space
                res = res + " ";
            }

			// Add the element to the result string
            res = res + ele;
        }

		// Return the result string
        return res;
    }

	// This method joins an Array of Chars into a String
    private static String a2s(char[] arr, boolean sep) {
		// Start with an empty result string
        String res = "";

		// For each element in the array
        for(char ele : arr) {
			// If we want to separate the elements and we already have one
            if(sep && res.length() > 0) {
				// Add a white space
                res = res + " ";
            }

			// Add the element to the result string
            res = res + ele;
        }

		// Return the result string
        return res;
    }

	// This method recieves the arguments, joins them, reverses them and shows the reverse
    public static void main(String[] ars) {
		// Start by joining all the Arguments into a String, separating them with white spaces
        String ori = a2s(ars, true);

		// Get the number of arguments
        int siz = ori.length();
		// Make an array the same size as the joined String
        char[] arr = new char[siz];

		// For each character, add the opposing character from the original string
        for(int i = 0; i < siz; i++) {
            arr[i] = ori.charAt(siz - i - 1);
        }

		// Create the reverse string by joining the array of reversed characters
        String rev = a2s(arr, false);

		// Show the reversed string
        System.out.println(rev);
    }
}
