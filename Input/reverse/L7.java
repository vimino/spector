// (Argument) String Reverser
public class Reverse {
	// This method joins an Array of Chars into a String
    private static String array2string(char[] array, boolean separate) {
		// Start with an empty result string
        String result = "";

		// For each element in the array
        for(char element : array) {
			// If we want to separate the elements and we already have one
            if(separate && result.length() > 0) {
				// Add a white space
                result = result + " ";
            }

			// Add the element to the result string
            result = result + element;
        }

		// Return the result string
        return result;
    }

	// This method recieves the arguments, joins them, reverses them and shows the reverse
    public static void main(String[] args) {
        String original = array2string(args, true);
		// Start by joining all the Arguments into a String, separating them with white spaces

        int size = original.length();
		// Get the number of arguments

        char[] array = new char[size];
		// Make an array the same size as the joined String

        for(int i = 0; i < size; i++) {
            array[i] = original.charAt(size - i - 1);
        }
		// For each character, add the opposing character from the original string

        String reverse = array2string(array, false);
		// Create the reverse string by joining the array of reversed characters

        System.out.println(reverse);
		// Show the reversed string
    }

	// This method joins an Array of Strings into a String
    private static String array2string(String[] array, boolean separate) {
        String result = "";
		// Start with an empty result string

        for(String element : array) {
            if(separate && result.length() > 0) {
                result = result + " ";
				// Add a white space
            }
			// If we want to separate the elements and we already have one

            result = result + element;
			// Add the element to the result string
        }
		// For each element in the array

        return result;
		// Return the result string
    }
}
