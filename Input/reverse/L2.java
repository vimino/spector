// Class that Reverses Arguments
public class Reverse {
	// Join an Array of Strings into a String
    private static String array2string(String[] array, boolean separate) {
        String result = "";

        for(String element : array) {
            if(separate && result.length() > 0) {
                result = result + " ";
            }

            result = result + element;
        }

        return result;
    }

	// Join an Array of Chars into a String
    private static String array2string(char[] array, boolean separate) {
        String result = "";

        for(char element : array) {
            if(separate && result.length() > 0) {
                result = result + " ";
            }

            result = result + element;
        }

        return result;
    }

	// Main method
    public static void main(String[] args) {
        String original = array2string(args, true);

        int size = original.length();
        char[] array = new char[size];

        for(int i = 0; i < size; i++) {
            array[i] = original.charAt(size - i - 1);
        }

        String reverse = array2string(array, false);

        System.out.println(reverse);
    }
}
