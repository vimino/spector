// (Argument) String Reverser
public class Reverse {
	// This method joins an Array of Strings into a String
    private static String array2string(String[] array, boolean separate) {
		// Start with an empty result string
        String result = "";

		// For each element in the array
        for(String element : array) {
			// If we want to separate the elements and we already have one
            if(separate && result.length() > 0) {
				// Add a white space
                result = result + " ";
            }

			// Add the element to the result string
            result = result + element;
        }

		// Return the result string
        return result;
    }

	// This method recieves the arguments, joins them, reverses them and shows the reverse
    public static void main(String[] args) {
		// Start by joining all the Arguments into a String, separating them with white spaces
        String original = array2string(args, true);

		// Get the number of arguments
        int size = original.length();
		// Make an array the same size as the joined String
        char[] array = new char[size];

		// For each character, add the opposing character from the original string
        for(int i = 0; i < size; i++) {
            array[i] = original.charAt(size - i - 1);
        }

		// Create the reverse string by joining the array of reversed characters
		String result = "";

		// For each element in the array
        for(char element : array) {
			// If we want to separate the elements and we already have one
            if(false && result.length() > 0) {
				// Add a white space
                result = result + " ";
            }

			// Add the element to the result string
            result = result + element;
        }

		String reverse = result;

		// Show the reversed string
        System.out.println(reverse);
    }
}
