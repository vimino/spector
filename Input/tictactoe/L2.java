import java.util.Scanner;

public class TicTacToe {
    private static char[] piece = new char[2];
    private static byte[][] grid = new byte[3][3];
    private static Scanner input = null;

    // clear board
    private static void clearGrid() {
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                grid[i][j] = 0;
            }
        }
    }

    // print board
    public static void printGrid() {
        for(int i = 0; i < 3; i++) {
            System.out.print("|");
            for(int j = 0; j < 3; j++) {
                if(grid[j][i] == 0) {
                    System.out.print(" ");
                } else {
                    System.out.print(piece[grid[j][i]-1]);
                }
                System.out.print("|");
            }

            System.out.println(" |" + (i*3+1) + "|" + (i*3+2) + "|" + (i*3+3) + "|");
        }
        System.out.println();
    }

    // read number
    public static int readInt() {
        int in = 0;

        while(input.hasNext()) {
            if(input.hasNextInt()) {
                in = input.nextInt();
                input.nextLine();
                break;
            } else {
                input.nextLine();
            }
        }

        return in;
    }

    // read line
    public static void readLine() {
        input.nextLine();
    }

    // player move
    public static boolean yourMove() {
        boolean done = false;

        while(!done) {
            // get choice
            System.out.print("Position: ");
            int pos = readInt(), i = 0, j = 0;

            // check choice
            if(pos == 0) {
                System.out.println();
                System.out.println("You chose to quit (0).");
                System.out.println();
                return true;
            } else if(pos < 0 || pos > 9) {
                System.out.println("That place is invalid, choose another one.");
                continue;
            }

            // calculate coordinates
            pos = pos - 1;
            i = pos/3;
            j = pos - i*3;

            // check if position is valid
            if(grid[j][i] != 0) {
                System.out.println("There is a piece there, choose another place.");
            } else {
                grid[j][i] = 1;
                done = true;
            }

            System.out.println();
        }

        return false;
    }

    // computer move
    public static void myMove() {
        int pos = 0;

        // calculate a position
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                if(grid[j][i] == 0) {
                    grid[j][i] = 2;
                    pos = j + (i*3) + 1;
                    break;
                }
            }
            if(pos != 0) { break; }
        }

        // show the position
        System.out.println("Position: " + pos);
        System.out.println();
    }

    // executes a play
    public static void play(boolean PlayerStarts) {
        clearGrid();
        printGrid();

        boolean yourTurn = PlayerStarts, over = false, winner = false;

        while(!over) {
            if(yourTurn) {
                System.out.println("Turn: Player");

                over = yourMove();
                if(over) { return; }
                yourTurn = false;
            } else {
                System.out.println("Turn: Computer");

                myMove();
                yourTurn = true;
            }

            printGrid();

            // check for win
            if(grid[0][0] != 0 && grid[0][0] == grid[1][0] && grid[1][0] == grid[2][0]) { // upper horizontal
                over = true;
                if(grid[0][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(grid[0][1] != 0 && grid[0][1] == grid[1][1] && grid[1][1] == grid[2][1]) { // middle horizontal
                over = true;
                if(grid[0][1] == 1) { winner = true; }
                else { winner = false; }
            } else if(grid[0][2] != 0 && grid[0][2] == grid[1][2] && grid[1][2] == grid[2][2]) { // lower horizontal
                over = true;
                if(grid[0][2] == 1) { winner = true; }
                else { winner = false; }
            } else if(grid[0][0] != 0 && grid[0][0] == grid[0][1] && grid[0][1] == grid[0][2]) { // left vertical
                over = true;
                if(grid[0][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(grid[1][0] != 0 && grid[1][0] == grid[1][1] && grid[1][1] == grid[1][2]) { // middle vertical
                over = true;
                if(grid[1][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(grid[2][0] != 0 && grid[2][0] == grid[2][1] && grid[2][1] == grid[2][2]) { // right vertical
                over = true;
                if(grid[2][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(grid[0][0] != 0 && grid[0][0] == grid[1][1] && grid[1][1] == grid[2][2]) { // \ diagonal
                over = true;
                if(grid[0][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(grid[2][0] != 0 && grid[2][0] == grid[1][1] && grid[1][1] == grid[0][2]) { // / diagonal
                over = true;
                if(grid[2][0] == 1) { winner = true; }
                else { winner = false; }
            } else {
                // check if there is no space left
                int empty = 0;
                for(int i = 0; i < 3; i++) {
                    for(int j = 0; j < 3; j++) {
                        if(grid[i][j] == 0) {
                            empty++;
                        }
                    }
                }

                // check for tie
                if(empty == 0) {
                    System.out.println("Game Over: It's a tie!");
                    System.out.print("Press a key to continue ...");
                    readLine();

                    System.out.println();
                    return;
                }
            }
        }

        // announce winner
        System.out.print("Game Over: ");
        if(winner) { System.out.println("You win!"); }
        else { System.out.println("You lose."); }

        System.out.print("Press a key to continue ...");
        readLine();

        System.out.println();
    }

    public static void main(String[] args) {
        // set the characters
        piece[0] = 'X';
        piece[1] = 'O';

        // clear board
        clearGrid();

        // make scanner
        input = new Scanner(System.in);

        // loop until exit
        boolean exit = false;
        while(!exit) {
			// show menu
            System.out.println("Tic-Tac-Toe");
            System.out.println("Rules: Each player chooses a spot in a 3x3 grid, whoever gets a line from one side to the other wins.");
            System.out.println();
            System.out.println("Menu:");
            System.out.println("  1 - Start with Player");
            System.out.println("  2 - Start with Computer");
            System.out.println("  0 - Exit");
            System.out.println();
            System.out.print("Choice: ");

            // read option
            int option = readInt();
            System.out.println();

            // do option
            if(option == 1) {
                play(true);
            } else if(option == 2) {
                play(false);
            } else if(option == 0) {
                exit = true;
            }
        }
    }
}
