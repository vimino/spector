import java.util.Scanner;

public class TicTacToe {
    private static char[] pie = new char[2];
    private static byte[][] board = new byte[3][3];
    private static Scanner inp = null;

    // Clear the grid
    private static void clearBoard() {
        for(int x = 0; x < 3; x++) {
            for(int y = 0; y < 3; y++) {
                board[x][y] = 0;
            }
        }
    }

    // Print the grid with the necessary pieces
    public static void printBoard() {
        for(int x = 0; x < 3; x++) {
            System.out.print("|");
            for(int y = 0; y < 3; y++) {
                if(board[y][x] == 0) {
                    System.out.print(" ");
                } else {
                    System.out.print(pie[board[y][x]-1]);
                }
                System.out.print("|");
            }

            System.out.println(" |" + (x*3+1) + "|" + (x*3+2) + "|" + (x*3+3) + "|");
        }
        System.out.println();
    }

    // Read an integer (number)
    public static int readInt() {
        int in = 0;

        // Get a number then clean the rest of the line
        while(inp.hasNext()) {
            if(inp.hasNextInt()) {
                in = inp.nextInt();
                inp.nextLine();
                break;
            } else {
                inp.nextLine();
            }
        }

        return in;
    }

    // Read a line (used to wait)
    public static void readLine() {
        inp.nextLine();
    }

    // Ask for the player to state his play and execute it
    public static boolean yourMove() {
        boolean done = false;

        while(!done) {
            // Get the position
            System.out.print("Position: ");
            int pos = readInt(), x = 0, y = 0;

            // Check if the position is valid
            if(pos == 0) {
                System.out.println();
                System.out.println("You chose to quit (0).");
                System.out.println();
                return true;
            } else if(pos < 0 || pos > 9) {
                System.out.println("That place is invalid, choose another one.");
                continue;
            }

            // Get the positions coordinates
            pos = pos - 1;
            x = pos/3;
            y = pos - x*3;

            // Check if the position is taken or place a piece
            if(board[y][x] != 0) {
                System.out.println("There is a piece there, choose another place.");
            } else {
                board[y][x] = 1;
                done = true;
            }

            System.out.println();
        }

        return false;
    }

    // Calculate a play and execute it
    public static void myMove() {
        int pos = 0;

        // Decide on a position and set the piece
        for(int x = 0; x < 3; x++) {
            for(int y = 0; y < 3; y++) {
                if(board[y][x] == 0) {
                    board[y][x] = 2;
                    pos = y + (x*3) + 1;
                    break;
                }
            }
            if(pos != 0) { break; }
        }

        // Say what position that was
        System.out.println("Position: " + pos);
        System.out.println();
    }

    // Plays a game of tic-tac-toe and returns whether the player or computer won
    public static void play(boolean PlayerStarts) {
        clearBoard();
        printBoard();

        boolean yourTurn = PlayerStarts, over = false, winner = false;

        while(!over) {
            // Let the player/computer make his move
            if(yourTurn) {
                System.out.println("Turn: Player");

                over = yourMove();
                if(over) { return; }
                yourTurn = false;
            } else {
                System.out.println("Turn: Computer");

                myMove();
                yourTurn = true;
            }

            // Show the current grid
            printBoard();

            // Check if anyone won
            if(board[0][0] != 0 && board[0][0] == board[1][0] && board[1][0] == board[2][0]) { // upper horizontal
                over = true;
                if(board[0][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(board[0][1] != 0 && board[0][1] == board[1][1] && board[1][1] == board[2][1]) { // middle horizontal
                over = true;
                if(board[0][1] == 1) { winner = true; }
                else { winner = false; }
            } else if(board[0][2] != 0 && board[0][2] == board[1][2] && board[1][2] == board[2][2]) { // lower horizontal
                over = true;
                if(board[0][2] == 1) { winner = true; }
                else { winner = false; }
            } else if(board[0][0] != 0 && board[0][0] == board[0][1] && board[0][1] == board[0][2]) { // left vertical
                over = true;
                if(board[0][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(board[1][0] != 0 && board[1][0] == board[1][1] && board[1][1] == board[1][2]) { // middle vertical
                over = true;
                if(board[1][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(board[2][0] != 0 && board[2][0] == board[2][1] && board[2][1] == board[2][2]) { // right vertical
                over = true;
                if(board[2][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(board[0][0] != 0 && board[0][0] == board[1][1] && board[1][1] == board[2][2]) { // \ diagonal
                over = true;
                if(board[0][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(board[2][0] != 0 && board[2][0] == board[1][1] && board[1][1] == board[0][2]) { // / diagonal
                over = true;
                if(board[2][0] == 1) { winner = true; }
                else { winner = false; }
            } else {
                // Check if there are positions left
                int emp = 0;
                for(int x = 0; x < 3; x++) {
                    for(int y = 0; y < 3; y++) {
                        if(board[x][y] == 0) {
                            emp++;
                        }
                    }
                }

                // If there are no positions left just say its a tie and wait to exit
                if(emp == 0) {
                    System.out.println("Game Over: It's a tie!");
                    System.out.print("Press a key to continue ...");
                    readLine();

                    System.out.println();
                    return;
                }
            }
        }

        // Say who won and wait to exit
        System.out.print("Game Over: ");
        if(winner) { System.out.println("You win!"); }
        else { System.out.println("You lose."); }

        System.out.print("Press a key to continue ...");
        readLine();

        System.out.println();
    }

    public static void main(String[] args) {
        // Set the piece characters
        pie[0] = 'X';
        pie[1] = 'O';

        // Clear the grid
        clearBoard();

        // Make a new Scanner
        inp = new Scanner(System.in);

        // Loop the menu until the player chooses to exit
        boolean exi = false;
        while(!exi) {
            System.out.println("Tic-Tac-Toe");
            System.out.println("Rules: Each player chooses a spot in a 3x3 grid, whoever gets a line from one side to the other wins.");
            System.out.println();
            System.out.println("Menu:");
            System.out.println("  1 - Start with Player");
            System.out.println("  2 - Start with Computer");
            System.out.println("  0 - Exit");
            System.out.println();
            System.out.print("Choice: ");

            // Read the option
            int opt = readInt();
            System.out.println();

            // Execute the option
            if(opt == 1) {
                play(true);
            } else if(opt == 2) {
                play(false);
            } else if(opt == 0) {
                exi = true;
            }
        }
    }
}
