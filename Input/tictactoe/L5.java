import java.util.Scanner;

public class TicTacToe {
    private static char[] piece = new char[2];
    private static byte[][] grid = new byte[3][3];
    private static Scanner input = null;

    // Clear the grid
    private static void clearGrid() {
        for(int i = 0; 3 > i; i++) {
            for(int j = 0; 3 > j; j++) {
                grid[i][j] = 0;
            }
        }
    }

    // Print the grid with the necessary pieces
    public static void printGrid() {
        for(int i = 0; 3 > i; i++) {
            System.out.print("|");
            for(int j = 0; 3 > j; j++) {
                if(grid[j][i] == 0) {
                    System.out.print(" ");
                } else {
                    System.out.print(piece[grid[j][i]-1]);
                }
                System.out.print("|");
            }

            System.out.println(" |" + (1+3*i) + "|" + (i*3+2) + "|" + (3+3*i) + "|");
        }
        System.out.println();
    }

    // Read an integer (number)
    public static int readInt() {
        int in = 0;

        // Get a number then clean the rest of the line
        while(input.hasNext()) {
            if(input.hasNextInt()) {
                in = input.nextInt();
                input.nextLine();
                break;
            } else {
                input.nextLine();
            }
        }

        return in;
    }

    // Read a line (used to wait)
    public static void readLine() {
        input.nextLine();
    }

    // Ask for the player to state his play and execute it
    public static boolean yourMove() {
        boolean done = false;

        while(!done) {
            // Get the position
            System.out.print("Position: ");
            int pos = readInt(), i = 0, j = 0;

            // Check if the position is valid
            if(0 == pos) {
                System.out.println();
                System.out.println("You chose to quit (0).");
                System.out.println();
                return true;
            } else if(0 > pos || 9 < pos) {
                System.out.println("That place is invalid, choose another one.");
                continue;
            }

            // Get the positions coordinates
            pos = pos - 1;
            i = pos/3;
            j = pos - 3*i;

            // Check if the position is taken or place a piece
            if(0 != grid[j][i]) {
                System.out.println("There is a piece there, choose another place.");
            } else {
                grid[j][i] = 1;
                done = true;
            }

            System.out.println();
        }

        return false;
    }

    // Calculate a play and execute it
    public static void myMove() {
        int pos = 0;

        // Decide on a position and set the piece
        for(int i = 0; 3 > i; i++) {
            for(int j = 0; 3 > j; j++) {
                if(grid[j][i] == 0) {
                    grid[j][i] = 2;
                    pos = j + (i*3) + 1;
                    break;
                }
            }
            if(pos != 0) { break; }
        }

        // Say what position that was
        System.out.println("Position: " + pos);
        System.out.println();
    }

    // Plays a game of tic-tac-toe and returns whether the player or computer won
    public static void play(boolean PlayerStarts) {
        clearGrid();
        printGrid();

        boolean yourTurn = PlayerStarts, over = false, winner = false;

        while(!over) {
            // Let the player/computer make his move
            if(yourTurn) {
                System.out.println("Turn: Player");

                over = yourMove();
                if(over) { return; }
                yourTurn = false;
            } else {
                System.out.println("Turn: Computer");

                myMove();
                yourTurn = true;
            }

            // Show the current grid
            printGrid();

            // Check if anyone won
            if(0 != grid[0][0] && grid[1][0] == grid[2][0] && grid[0][0] == grid[1][0]) { // upper horizontal
                over = true;
                if(grid[0][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(0 != grid[0][1] && grid[1][1] == grid[2][1] && grid[0][1] == grid[1][1]) { // middle horizontal
                over = true;
                if(grid[0][1] == 1) { winner = true; }
                else { winner = false; }
            } else if(0 != grid[0][2] && grid[1][2] == grid[2][2] && grid[0][2] == grid[1][2]) { // lower horizontal
                over = true;
                if(grid[0][2] == 1) { winner = true; }
                else { winner = false; }
            } else if(0 != grid[0][0] && grid[0][1] == grid[0][2] && grid[0][0] == grid[0][1]) { // left vertical
                over = true;
                if(grid[0][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(0 != grid[1][0] && grid[1][1] == grid[1][2] && grid[1][0] == grid[1][1]) { // middle vertical
                over = true;
                if(grid[1][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(0 != grid[2][0] && grid[2][1] == grid[2][2] && grid[2][0] == grid[2][1]) { // right vertical
                over = true;
                if(grid[2][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(0 != grid[0][0] && grid[1][1] == grid[2][2] && grid[0][0] == grid[1][1]) { // \ diagonal
                over = true;
                if(grid[0][0] == 1) { winner = true; }
                else { winner = false; }
            } else if(0 != grid[2][0] && grid[1][1] == grid[0][2] && grid[2][0] == grid[1][1]) { // / diagonal
                over = true;
                if(grid[2][0] == 1) { winner = true; }
                else { winner = false; }
            } else {
                // Check if there are positions left
                int empty = 0;
                for(int i = 0; 3 > i; i++) {
                    for(int j = 0; 3 > j; j++) {
                        if(grid[i][j] == 0) {
                            empty++;
                        }
                    }
                }

                // If there are no positions left just say its a tie and wait to exit
                if(0 == empty) {
                    System.out.println("Game Over: It's a tie!");
                    System.out.print("Press a key to continue ...");
                    readLine();

                    System.out.println();
                    return;
                }
            }
        }

        // Say who won and wait to exit
        System.out.print("Game Over: ");
        if(winner) { System.out.println("You win!"); }
        else { System.out.println("You lose."); }

        System.out.print("Press a key to continue ...");
        readLine();

        System.out.println();
    }

    public static void main(String[] args) {
        // Set the piece characters
        piece[0] = 'X';
        piece[1] = 'O';

        // Clear the grid
        clearGrid();

        // Make a new Scanner
        input = new Scanner(System.in);

        // Loop the menu until the player chooses to exit
        boolean exit = false;
        while(!exit) {
            System.out.println("Tic-Tac-Toe");
            System.out.println("Rules: Each player chooses a spot in a 3x3 grid, whoever gets a line from one side to the other wins.");
            System.out.println();
            System.out.println("Menu:");
            System.out.println("  1 - Start with Player");
            System.out.println("  2 - Start with Computer");
            System.out.println("  0 - Exit");
            System.out.println();
            System.out.print("Choice: ");

            // Read the option
            int option = readInt();
            System.out.println();

            // Execute the option
            if(1 == option) {
                play(true);
            } else if(2 == option) {
                play(false);
            } else if(0 == option) {
                exit = true;
            }
        }
    }
}
