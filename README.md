# Spector
### A source code plagiarism detector.
This tool was created to detect plagiarism in source code, in an *Academic Environment*. However, after having developed it, we noticed that we focused on **similarity** detection which resulted in the creation of false positives when faced with simple cases (like a group of students doing the same exercise).

### **[Download][file]**

### Compilation and Use

This tool comes in a Netbeans 8.0.2 project and will produce a **dist** folder when built. That folder will then have the required components of the tool (Spector.jar and lib/antlr-...-complete.jar).

The tool can be used from a command line with ```java -jar dist/Spector.jar <arguments>``` or with the included **spector.sh** script (kept outside the *dist* folder) with ```./spector.sh <arguments>```.

### Version

Latest version: 1.1.6

The version has a major, minor and revision values. Note that the minor and revision values are reset only when the major version is increased.

### Todo

- Improve the existing Java grammar and the detection algorithms,
- Add grammars to support additional languages,
- Build a plagiarism detector with care to avoid false positives,
- Build a duplicate detector to help optimize code by identifying repetitions,
- Produce a GUI (Graphical User Interface) that integrates the tool(s) and shows its results.

### License

Copyright &copy; 2015, Vítor T. Martins
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

[logo]:https://gitlab.com/vimino/spector/raw/master/logo.gif
[file]:https://gitlab.com/vimino/spector/raw/master/Spector.zip
