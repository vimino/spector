package spector;

/*
Copyright (c) 2015, Vítor T. Martins
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import lang.Suspect;
import org.antlr.runtime.tree.CommonTree;

public class Comparison {
    private Suspect first;
    private Suspect second;
    public enum ResultType {ExactCopy, SimilarIdentifiers, SimilarExpressions, SimilarConditionals, SuspiciousCalls};
    private ArrayList<Double> measures;

    private HashMap<String, ArrayList<CommonTree>> IOM_A, IOM_B;
    private HashMap<String, ArrayList<String>> I_Equivalences, I_Copies;

    public void addIdentifierMaps(HashMap<String, ArrayList<CommonTree>> a, HashMap<String, ArrayList<CommonTree>> b) {
        IOM_A = a;
        IOM_B = b;
    }

    public void addIdentifierEquivalences(HashMap<String, ArrayList<String>> equivalences) {
        I_Equivalences = equivalences;
    }

    public void addIdentifierCopies(HashMap<String, ArrayList<String>> copies) {
        I_Copies = copies;
    }

    public HashMap<String, ArrayList<String>> getIdentifierEquivalences() {
        return I_Equivalences;
    }

    public HashMap<String, ArrayList<String>> getIdentifierCopies() {
        return I_Copies;
    }

    public boolean hasIdentifierMatches() {
        if(I_Equivalences == null || I_Equivalences.isEmpty()) {
            return false;
        }
        return true;
    }

    public HashMap<String, ArrayList<String>> getIdentifierMatches() {
        HashMap<String, ArrayList<String>> match = new HashMap<String, ArrayList<String>>();

        for(String equi : I_Equivalences.keySet()) {
            for(String copy : I_Copies.keySet()) {
                if(equi.equals(copy)) { // There is a copy so add that instead
                    match.put(copy, I_Copies.get(copy));
                }
            }

            if(!match.containsKey(equi)) { // If we didn't add this already
                match.put(equi, I_Equivalences.get(equi));
            }
        }

        return match;
    }

    public boolean areIdentifiersEquivalent(String a, String b) {
        return (I_Equivalences.containsKey(a) && I_Equivalences.get(a).contains(b));
    }

    public boolean areIdentifiersCopies(String a, String b) {
        return (I_Copies.containsKey(a) && I_Copies.get(a).contains(b));
    }

    private HashMap<String, ArrayList<CommonTree>> EOM_A, EOM_B;
    private HashMap<String, ArrayList<String>> E_Equivalences, E_Copies;

    public void addExpressionMaps(HashMap<String, ArrayList<CommonTree>> a, HashMap<String, ArrayList<CommonTree>> b) {
        EOM_A = a;
        EOM_B = b;
    }

    public void addExpressionEquivalences(HashMap<String, ArrayList<String>> equivalences) {
        E_Equivalences = equivalences;
    }

    public void addExpressionCopies(HashMap<String, ArrayList<String>> copies) {
        E_Copies = copies;
    }

    public HashMap<String, ArrayList<String>> getExpressionEquivalences() {
        return E_Equivalences;
    }

    public HashMap<String, ArrayList<String>> getExpressionCopies() {
        return E_Copies;
    }

    public boolean hasExpressionMatches() {
        if(E_Equivalences == null || E_Equivalences.isEmpty()) {
            return false;
        }
        return true;
    }

    public HashMap<String, ArrayList<String>> getExpressionMatches() {
        HashMap<String, ArrayList<String>> match = new HashMap<String, ArrayList<String>>();

        for(String equi : E_Equivalences.keySet()) {
            for(String copy : E_Copies.keySet()) {
                if(equi.equals(copy)) { // There is a copy so add that instead
                    match.put(copy, E_Copies.get(copy));
                }
            }

            if(!match.containsKey(equi)) { // If we didn't add this already
                match.put(equi, E_Equivalences.get(equi));
            }
        }

        return match;
    }

    public boolean areExpressionsEquivalent(String a, String b) {
        return (E_Equivalences.containsKey(a) && E_Equivalences.get(a).contains(b));
    }

    public boolean areExpressionsCopies(String a, String b) {
        return (E_Copies.containsKey(a) && E_Copies.get(a).contains(b));
    }

    private HashMap<CommonTree, CommonTree> CCM_A, CCM_B;
    private HashMap<CommonTree, ArrayList<CommonTree>> C_Equivalences, C_Copies;

    public void addConditionalMaps(HashMap<CommonTree, CommonTree> a, HashMap<CommonTree, CommonTree> b) {
        CCM_A = a;
        CCM_B = b;
    }

    public void addConditionalEquivalences(HashMap<CommonTree, ArrayList<CommonTree>> equivalences) {
        C_Equivalences = equivalences;
    }

    public void addConditionalCopies(HashMap<CommonTree, ArrayList<CommonTree>> copies) {
        C_Copies = copies;
    }

    public HashMap<CommonTree, ArrayList<CommonTree>> getConditionalEquivalences() {
        return C_Equivalences;
    }

    public HashMap<CommonTree, ArrayList<CommonTree>> getConditionalCopies() {
        return C_Copies;
    }

    public boolean hasConditionalMatches() {
        if(C_Equivalences == null || C_Equivalences.isEmpty()) {
            return false;
        }
        return true;
    }

    public HashMap<CommonTree, ArrayList<CommonTree>> getConditionalMatches() {
        HashMap<CommonTree, ArrayList<CommonTree>> match = new HashMap<CommonTree, ArrayList<CommonTree>>();

        if(C_Equivalences == null || C_Equivalences.isEmpty()) {
            return match;
        }

        for(CommonTree equi : C_Equivalences.keySet()) {
            if(equi == null) { continue; }

            for(CommonTree copy : C_Copies.keySet()) {
                if(copy == null) { continue; }
                if(equi.equals(copy)) { // There is a copy so add that instead
                    match.put(copy, C_Copies.get(copy));
                }
            }

            if(!match.containsKey(equi)) { // If we didn't add this already
                match.put(equi, C_Equivalences.get(equi));
            }
        }

        return match;
    }

    public boolean areConditionalsEquivalent(CommonTree a, CommonTree b) {
        return (C_Equivalences.containsKey(a) && C_Equivalences.get(a).contains(b));
    }

    public boolean areConditionalsCopies(CommonTree a, CommonTree b) {
        return (C_Copies.containsKey(a) && C_Copies.get(a).contains(b));
    }

    private HashMap<CommonTree, ArrayList<CommonTree>> BCM_A, BCM_B;
    private HashMap<CommonTree, ArrayList<CommonTree>> B_Equivalences, B_Copies;

    public void addBlockMaps(HashMap<CommonTree, ArrayList<CommonTree>> a, HashMap<CommonTree, ArrayList<CommonTree>> b) {
        BCM_A = a;
        BCM_B = b;
    }

    public void addBlockEquivalences(HashMap<CommonTree, ArrayList<CommonTree>> equivalences) {
        B_Equivalences = equivalences;
    }

    public void addBlockCopies(HashMap<CommonTree, ArrayList<CommonTree>> copies) {
        B_Copies = copies;
    }

    public HashMap<CommonTree, ArrayList<CommonTree>> getBlockEquivalences() {
        return B_Equivalences;
    }

    public HashMap<CommonTree, ArrayList<CommonTree>> getBlockCopies() {
        return B_Copies;
    }

    public boolean hasBlockMatches() {
        if(C_Equivalences == null || C_Equivalences.isEmpty()) {
            return false;
        }
        return true;
    }

    public HashMap<CommonTree, ArrayList<CommonTree>> getBlockMatches() {
        HashMap<CommonTree, ArrayList<CommonTree>> match = new HashMap<CommonTree, ArrayList<CommonTree>>();

        if(C_Equivalences == null || C_Equivalences.isEmpty()) { return match; }

        for(CommonTree equi : C_Equivalences.keySet()) {
            if(equi == null) { continue; }

            for(CommonTree copy : C_Copies.keySet()) {
                if(copy == null) { continue; }

                if(equi.equals(copy)) { // There is a copy so add that instead
                    match.put(copy, C_Copies.get(copy));
                }
            }

            if(!match.containsKey(equi)) { // If we didn't add this already
                match.put(equi, C_Equivalences.get(equi));
            }
        }

        return match;
    }

    public boolean areBlocksEquivalent(CommonTree a, CommonTree b) {
        return (B_Equivalences.containsKey(a) && B_Equivalences.get(a).contains(b));
    }

    public boolean areBlocksCopies(CommonTree a, CommonTree b) {
        return (B_Copies.containsKey(a) && B_Copies.get(a).contains(b));
    }

    public Comparison() {
        first = null;
        second = null;
        measures = new ArrayList<Double>();
        for(ResultType type : ResultType.values()) {
            measures.add(0.0);
        }

        IOM_A = null;
        IOM_B = null;
        I_Equivalences = null;
        I_Copies = null;

        EOM_A = null;
        EOM_B = null;
        E_Equivalences = null;
        E_Copies = null;

        CCM_A = null;
        CCM_B = null;
        C_Equivalences = null;
        C_Copies = null;

        ////
    }

    public Comparison(Suspect a, Suspect b) {
        try {
            if(a == null || b == null) {
                 throw new NullPointerException();
            }

            this.first = a;
            this.second = b;

            measures = new ArrayList<Double>();
            for(ResultType type : ResultType.values()) {
                measures.add(0.0);
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(a != null && b != null) {
                System.out.print("(" + a.getName() + ", " + b.getName() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }
    }

    public Suspect getFirst() {
        return first;
    }

    public void setFirst(Suspect first) {
        this.first = first;
    }

    public Suspect getSecond() {
        return second;
    }

    public void setSecond(Suspect second) {
        this.second = second;
    }

    public double getMeasure(ResultType which) {
        return measures.get(which.ordinal());
    }

    public ArrayList<Double> getMeasures() {
        return measures;
    }

    public void setMeasure(ResultType which, double value) {
        measures.set(which.ordinal(), value);
    }

    public void setMeasures(ArrayList<Double> results) {
        measures.addAll(results);
    }

    public double getTotal() {
        double total = 0.0;
        int count = 0;

        total = measures.get(ResultType.ExactCopy.ordinal());
        if(total > 0.0) {
            return total;
        }

        total = 0.0;
        for(ResultType rt : ResultType.values()) {
            double value = measures.get(rt.ordinal());
            if(value > 0.0) {
                count += 1;
                total += value;
            }
        }

        total /= count;

        return total;
    }
}