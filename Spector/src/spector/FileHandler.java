package spector;

/*
Copyright (c) 2015, Vítor T. Martins
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * A file handler that handles several operations upon files and directories
 * @version 1.0
 * @author  Vítor T. Martins
 */
public class FileHandler {
    private final static boolean debug = false;      // Toggle debugging information for this module

    /**
     * Check if a path leads to a file or directory
     * @param   path    string with a path
     * @return  whether the input leads to a file or directory
     */
    public static boolean isValid (String path) {
        if (path == null) {
            return false;
        }

        try {
            File file = new File(path);
            return isValid(file);
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println("(" + path + ") @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return false;
    }

    /**
     * Check if a path leads to a file or directory
     * @param   path    string with a path
     * @return  whether the input leads to a file or directory
     */
    public static boolean isValid (File file) {
        if (file == null) {
            return false;
        }

        try {
            if (file.isFile()) {
                if (debug) {
                    System.out.println("FileHandler.isValid("+file.getName()+"): Path leads to a file.");
                }

                return true;
            } else if(file.isDirectory()) {
                if (debug) {
                    System.out.println("FileHandler.isValid("+file.getName()+"): Path leads to a directory.");
                }

                return true;
            } else {
                if (debug) {
                    System.out.println("FileHandler.isValid("+file.getName()+"): Path doesn't lead to a file or directory.");
                }

                return false;
            }
        } catch (Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(file != null) {
                System.out.print("(" + file.getName() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    /**
     * Lists all files inside the path
     * @param   path    path to a file or directory
     * @param   recursive whether the search is recursive
     * @return  linked list with the files that were found
     */
    public static ArrayList<File> listFiles (String path) {
        return listFiles(path, false);
    }

    /**
     * Lists all files inside the path
     * @param   path    path to a file or directory
     * @param   recursive whether the search is recursive
     * @return  linked list with the files that were found
     */
    public static ArrayList<File> listFiles (String path, boolean recursive) {
        File input = null;
        ArrayList<File> list = new ArrayList<File>();

        if (path == null) {
            if (debug) {
                System.out.println("FileHandler.listFiles(null): Null path.");
            }

            return list;
        }

        try {
            input = new File(path);
            if (input == null) {
                if (debug) {
                    System.out.println("FileHandler.listFiles("+path+"): Unable to create a File object.");

                    return list;
                }
            }

            list.addAll(listFiles(input, recursive));
        } catch (Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println("(" + path + ", " + recursive +") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return new ArrayList<File>();
        }

        if (debug) {
            System.out.println("FileHandler.listFiles("+input.getPath()+"): Found "+list.size()+" files.");
        }

        return list;
    }

    /**
     * Lists all files under a file
     * @param   file      path to a file or directory
     * @param   recursive whether the search is recursive
     * @return  linked list with the files that were found
     */
    public static ArrayList<File> listFiles (File file) {
        return listFiles(file, false);
    }

    /**
     * Lists all files under a file
     * @param   file      path to a file or directory
     * @param   recursive whether the search is recursive
     * @return  linked list with the files that were found
     */
    public static ArrayList<File> listFiles (File file, boolean recursive) {
        ArrayList<File> list = new ArrayList<File>();

        if (file == null) {
            if (debug) {
                System.out.println("FileHandler.listFiles(null): Null file.");
            }

            return list;
        }

        try {
            if (file.isFile()) {
                list.add(file);
            } else if (file.isDirectory()) {
                for (File f : file.listFiles()) {
                    list.addAll(listChildFiles(f, recursive));
                }
            } else {
                if (debug) {
                    System.out.println("FileHandler.listFiles("+file.getName()+"): Path doesn't lead to a file or directory.");
                }
            }
        } catch (Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");

            if(file != null) {
                System.out.print(file.getName());
            } else {
                System.out.print("null");
            }

            System.out.println(", " + recursive + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return new ArrayList<File>();
        }

        if (debug) {
            System.out.println("FileHandler.listFiles("+file.getName()+"): Found "+list.size()+" files.");
        }

        return list;
    }

    /**
     * Lists all files under a file
     * @param   file      path to a file or directory
     * @param   recursive whether the search is recursive
     * @return  linked list with the files that were found
     */
    public static ArrayList<File> listChildFiles (File file, boolean recursive) {
        ArrayList<File> list = new ArrayList<File>();

        if (file == null) {
            return list;
        }

        try {
            if (file.isFile()) {
                list.add(file);
            } else if (recursive && file.isDirectory()) {
                for (File f : file.listFiles()) {
                    list.addAll(listChildFiles(f, recursive));
                }
            } else {
                if (debug) {
                    System.out.println("FileHandler.listFiles("+file.getName()+"): Path doesn't lead to a file or directory.");
                }
            }
        } catch (Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");

            if(file != null) {
                System.out.print(file.getName());
            } else {
                System.out.print("null");
            }

            System.out.println(", " + recursive + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return new ArrayList<File>();
        }

        if (debug) {
            System.out.println("FileHandler.listFiles("+file.getName()+"): Found "+list.size()+" files.");
        }

        return list;
    }

    /**
     * Lists all submissions under a folder
     * @param   file      path to a file or directory
     * @param   recursive whether the search is recursive
     * @return  linked list with the files that were found
     */
    public static ArrayList<File> listSubmissions(String path) {
        return listSubmissions(path, false);
    }

    /**
     * Lists all submissions under a folder
     * @param   file      path to a file or directory
     * @param   recursive whether the search is recursive
     * @return  linked list with the files that were found
     */
    public static ArrayList<File> listSubmissions(String path, boolean recursive) {
        ArrayList<File> list = new ArrayList<File>();
        try {
            File file = new File(path);
            if(file == null) {
                return list;
            }
            return listSubmissions(file, recursive);
        } catch(Exception x) {
            return list;
        }
    }

    /**
     * Lists all submissions under a folder
     * @param   file      path to a file or directory
     * @param   recursive whether the search is recursive
     * @return  linked list with the files that were found
     */
    public static ArrayList<File> listSubmissions(File file) {
        return listSubmissions(file, false);
    }

    /**
     * Lists all submissions under a folder
     * @param   file      path to a file or directory
     * @param   recursive whether the search is recursive
     * @return  linked list with the files that were found
     */
    public static ArrayList<File> listSubmissions(File file, boolean recursive) {
        ArrayList<File> list = new ArrayList<File>();

        if (file == null) {
            if (debug) {
                System.out.println("FileHandler.listSubmissions(null): Null file.");
            }

            return list;
        }

        try {
            if (file.isDirectory()) {
                for (File child : file.listFiles()) {
                    if(child.isDirectory()) {
                        for(File grandchild : child.listFiles()) {
                            list.addAll(listChildFiles(grandchild, recursive));
                        }
                    }
                }
            } else {
                if (debug) {
                    System.out.println("FileHandler.listSubmissions("+file.getName()+"): Path doesn't lead to a file or directory.");
                }
            }
        } catch (Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");

            if(file != null) {
                System.out.print(file.getName());
            } else {
                System.out.print("null");
            }

            System.out.println(", " + recursive + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return new ArrayList<File>();
        }

        if (debug) {
            System.out.println("FileHandler.listSubmissions("+file.getName()+"): Found "+list.size()+" files.");
        }

        return list;
    }

    /**
     * Create a file at the specified path
     * @param   path        target path
     * @param   contents    contents of the file
     * @return  reports the success of the file's creation
     */
    public static boolean saveFile (String path, String contents) {
        File file = null;
        String name = null;

        try {
            file = new File(path);
            if (file == null) {
                if (debug) {
                    System.out.println("FileHandler.saveFile("+name+"): Null path.");
                }

                return false;
            } else {
                name = file.getName();
            }

            if (!file.exists()) {
                // Create the file
                boolean created = file.createNewFile();
                if (!created) {
                    if (debug) {
                        System.out.println("FileHandler.saveFile("+name+"): Unable to create the file.");
                    }

                    return false;
                }

            } else {
                //throw new Exception("File already exists.");
            }

            // Write the contents in the file
            if (contents != null && !contents.isEmpty()) {
                FileWriter fw = new FileWriter(path);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(contents);
                bw.close();
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println("(" + path + ") @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return false;
    }

    /**
     * Attempts to produce .png files from .dot files
     * Warning: Only works on systems that have the "dot" shell script
     * @param  path    The path leading to the dot file
     * @return Whether the operation was successful
     */
    public static boolean produceDOT(String path) {
        if (debug) {
            System.out.print("produceDOT("+path+")");
        }
        int exit = 1;
        try {
            //System.out.println("dot " + path + " -T" + " png" + " -O");
            Process p = new ProcessBuilder("dot", path, "-T", "png", "-O").start();
            p.waitFor();
            exit = p.exitValue();
        } catch(Exception x) {
            if (debug) {
                System.out.println(": Failed");
            }

            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println("(" + path + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }

        // Report the result
        if(exit == 0) {
            if (debug) {
                System.out.println(": Succeeded");
            }
            return true;
        } else {
            if (debug) {
                System.out.println(": Failed");
            }
            return false;
        }
    }

    /**
     * Removes the extension from a file name
     * @param  name The file name (with or without an extension)
     * @return The file name without its extensions
     */
    public static String cutExtension(String name) {
        return name.replaceFirst("[.][^.]+", "");
    }
}
