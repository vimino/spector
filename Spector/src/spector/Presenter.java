package spector;

/*
Copyright (c) 2015, Vítor T. Martins
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import java.io.File;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

public class Presenter {
    private File output;
    private ArrayList<Comparison> comparisons;
    private DecimalFormat df = new DecimalFormat("0.###");

    public Presenter() {
        comparisons = new ArrayList<Comparison>();
    }

    public boolean addComparison(Comparison c) {
        if(c == null) {
            System.out.println();
            return false;
        }

        comparisons.add(c);
        return true;
    }

    public int countComparisons() {
        return comparisons.size();
    }

    public boolean sort(int order) {
        /* order
            0 = alphanumeric (do not do anything)
            1 = ascending
            -1 = descending
        */

        if(comparisons.size() < 2 || order == 0) { return false; }
        if(order != -1 && order != 1) { return false; }

        try {
            Comparator<Comparison> sorter = null;

            if(order == 1) {
                sorter = new Comparator<Comparison>() {
                    @Override
                    public int compare(Comparison a, Comparison b) {
                        return Double.compare(a.getTotal(), b.getTotal());
                    }
                };
            } else if(order == -1) {
                sorter = new Comparator<Comparison>() {
                    @Override
                    public int compare(Comparison a, Comparison b) {
                        return Double.compare(b.getTotal(), a.getTotal());
                    }
                };
            }

            if(sorter == null) { return false; }
            Collections.sort(comparisons, sorter);

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    public void printSummary() {
        for(Comparison comp : comparisons) {
            String total = df.format(comp.getTotal());

            System.out.println(comp.getFirst().getFile().getName() + " - " + comp.getSecond().getFile().getName() + " : " + total + "%");
        }
    }

    public void printFullSummary() {
        for(Comparison comp : comparisons) {
            int count = 0;

            ArrayList<String> meastr = new ArrayList<String>();
            for(Double value : comp.getMeasures()) {
                meastr.add(df.format(value));
                if(value == 0.0) { continue; }
                count++;
            }

            String total = df.format(comp.getTotal());

            System.out.print(comp.getFirst().getFile().getName() + " - " + comp.getSecond().getFile().getName() + " : (");

            Iterator<String> it = meastr.iterator();
            while(it.hasNext()) {
                String measure = it.next();
                System.out.print(measure);

                if(it.hasNext()) {
                    System.out.print("+");
                }
            }

            System.out.println(")/" + count + " = " + total + "%");
        }
    }

    public void printMatches() {
        for(Comparison c : comparisons) {
            System.out.println();
            System.out.println("Matches for " + c.getFirst().getName() + " - " + c.getSecond().getName());

            if(c.hasIdentifierMatches()) {
                System.out.println();
                System.out.println("Identifier(s):");
                System.out.println(c.getIdentifierMatches());
            }

            if(c.hasExpressionMatches()) {
                System.out.println();
                System.out.println("Expression element(s):");
                System.out.println(c.getExpressionMatches());
            }

            if(c.hasConditionalMatches()) {
                System.out.println();
                System.out.println("Conditional(s):");
                System.out.println(c.getConditionalMatches());
            }

            if(c.hasBlockMatches()) {
                System.out.println();
                System.out.println("Block(s):");
                System.out.println(c.getBlockMatches());
            }
        }
    }

    public void setOutput(String path) {
        output = new File(path);
    }

    public void setOutput(File file) {
        output = file;
    }

    public String roundValue(double measure) {
        DecimalFormat df = new DecimalFormat("###.###");
        return df.format(measure);
    }

    public String buildMeter(String value) {
        try {
            String meter = "<div style=\"width:" + value + "%;background:";
            double v = Double.parseDouble(value);
            if(v < 20) {
                meter += "#f44";
            } else if(v > 20 && v < 40) {
                meter += "#fa4";
            } else if(v > 40 && v < 60) {
                meter += "#ff4";
            } else if(v > 60 && v < 80) {
                meter += "#af4";
            } else if(v > 80) {
                meter += "#4ff";
            }
            meter += ";\"></div>";

            return meter;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println("(" + value + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return "";
        }
    }

    public boolean buildHTML() {
        try {
            if(comparisons.isEmpty()) {
                System.out.println("Presenter.buildPresentation: No comparisons to present.");
                return true;
            }

            if(output == null) {
                output = new File("results");
            }

            // Build a folder to store the output
            if(!output.exists()) {
                if(output.mkdir() == false) {
                    throw new Exception("Unable to create the output directory.");
                }
            } else if(!output.isDirectory()) {
                throw new Exception("A file exists with the output directorys name.");
            }

            // Create a summary.html file in the folder  // >>> PrintWriter will do this for us
            /*File summary = new File(outdir.getAbsolutePath()+"/summary.html");
            if(summary.exists()) {
                if(summary.delete() == false) {
                    throw new Exception("Unable to delete the summary.html file.");
                }
            }

            if(summary.createNewFile() == false) {
                throw new Exception("Unable to create a summary.html file.");
            }
            */

            String fullpath = output.getAbsolutePath()+File.separator+"summary.html";
            PrintWriter writer = new PrintWriter(fullpath, "UTF-8");

            // Append the HTML header
            writer.println("<!DOCTYPE html>");
            writer.println("<html lang=\"en\">");
            writer.println("\t<head>");
            writer.println("\t\t<meta charset=\"utf-8\" />");
            writer.println("\t\t<style type=\"text/css\">");
            writer.println("\t\t\t*{padding:0ex;margin:0;}");
            writer.println("\t\t\tbody{text-align:center;}");
            writer.println("\t\t\th2{font-size:200%;padding:.4ex;}");
            writer.println("\t\t\ttable{margin:0 auto;}");
            writer.println("\t\t\ttd{padding:.4ex;}");
            writer.println("\t\t\tdiv.meter{background:#444;border:2px solid #444;border-radius:.6ex;width:6em;}");
            writer.println("\t\t\tdiv.meter div{height:1ex;border-radius:.4ex;}");
            writer.println("\t\t\tspan{font-size:80%}");
            writer.println("\t\t</style>");
            writer.println("\t</head>");
            writer.println();
            writer.println("\t<body>");
            writer.println("<h2>Spector</h2>");
            writer.println("\t\t<table>");
            writer.println("\t\t\t<tr>");
            writer.println("\t\t\t\t<td>First File</td>");
            writer.println("\t\t\t\t<td>Second File</td>");
            writer.println("\t\t\t\t<td>Similarity</td>");
            writer.println("\t\t\t</tr>");

            // Generate the HTML body
            for(Comparison c : comparisons) {
                writer.println("\t\t\t<tr>");
                writer.println("\t\t\t\t<td>"+c.getFirst().getFile().getName()+"</td>");
                writer.println("\t\t\t\t<td>"+c.getSecond().getFile().getName()+"</td>");
                writer.println("\t\t\t\t<td>");
                String value = roundValue(c.getTotal());
                writer.println("\t\t\t\t\t<div class=\"meter\">");
                writer.println("\t\t\t\t\t\t" + buildMeter(value));
                writer.println("\t\t\t\t\t</div>");
                writer.println("\t\t\t\t\t<span>" + value + "%</span>");
                writer.println("\t\t\t\t</td>");
                writer.println("\t\t\t</tr>");
            }

            // Append the HTML tail
            writer.println("\t\t</table>");
            writer.println("\t</body>");
            writer.println("</html>");
            writer.close();

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }
}
