package spector;

/*
Copyright (c) 2015, Vítor T. Martins
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import lang.Suspect;
import lang.TokenGroup;
import lang.TokenType;
import org.antlr.runtime.tree.CommonTree;

/**
 * An AST inspector
 * @version 1.0
 * @author  Vítor T. Martins
 */
public class Inspector {
    // Global variables
    private static boolean debug = false;      // Toggle debugging information for this module
    private static Comparison result = null;   // The container of all the results from this comparison

    private static DecimalFormat df = new DecimalFormat("0.###");

    // Settings with default values
    private static double threshold = 0.0;     // Threshold that controls the strictness of the comparison

///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Getters and setters

    public static void setDebug(boolean value) {
        debug = value;
    }

    public static boolean getDebug() {
        return debug;
    }

    public static void setThreshold(double value) {
        threshold = value;
    }

    public static double getThreshold() {
        return threshold;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Auxiliary Functions

    public static double sumMeasures(double[] measure) {
        double sum = 0.0;
        for(double value: measure) {
            sum += value;
        }
        return sum;
    }

    /**
     * Divide two numbers and avoid dividing by zero or less
     * @param   a The first operand
     * @param   b The second operand
     * @return  The division of a by b or just a if b is zero or less
     */
    public static double safeDiv(double a, double b) {
        double c = 0.0;
        if(b <= 0.0) { c = a; }
        else { c = a / b; }
        return c;
    }

    public static void printMapValues(HashMap<CommonTree, String> map) {
        if(map == null || map.size() == 0) {
            System.out.println("{}");
            return;
        }

        System.out.print("{");

        Iterator<String> it = map.values().iterator();
        while(it.hasNext()) {
            Object current = it.next();

            if(current == null) { System.out.print("null"); }
            else { System.out.print(current.toString()); }

            if(it.hasNext()) { System.out.print(", "); }
        }

        System.out.println("}");
    }

    public static double sumMeasures(ArrayList<Double> measure) {
        ArrayList<Double> validMeasures = new ArrayList<Double>();

        double total = 0.0;
        int count = 0;

        for(double current : measure) {
            if(current > 0.0) {
                validMeasures.add(current);
            }
        }

        Iterator<Double> it = validMeasures.iterator();

        if(debug) { System.out.print("("); }
        while(it.hasNext()) {
            double current = it.next();

            if(debug) {
                if(df == null) {
                    System.out.print(current);
                } else {
                    System.out.print(df.format(current));
                }

                if(it.hasNext()) {
                    System.out.print(" + ");
                }
            }

            total += current;
            ++count;
        }
        if(debug) { System.out.print(") / " + count); }

        return (total / count);
    }


    // specific function to get names
    public static String getIdentifier(CommonTree node) {
        if(node == null || node.getChildCount() < 1) {
            return null;
        }

        String identifier = "";
        int type = node.getType();
        CommonTree nameHolder = null;

        if(Spector.nexus.inGroup(type, TokenGroup.Call)) {
            // get the first child (has the name)
            nameHolder = (CommonTree) node.getChildren().get(0);
        } else if(Spector.nexus.inGroup(type, TokenGroup.Block)) {
            // get the parent (has the name)
            nameHolder = (CommonTree) node.getParent();
        } else if(Spector.nexus.inGroup(type, TokenGroup.Expression)) {
            // get the parent (has the name)
            nameHolder = (CommonTree) node.getParent().getParent();
        }

        if(nameHolder == null) {
            return null;
        }

        // get the full sequence from the name holder
        identifier += getFullSequence(nameHolder);

        return identifier;
    }

    public static void printMap(HashMap<CommonTree, ArrayList<CommonTree>> map) {
        if(map == null || map.size() == 0) {
            System.out.println("{}");
            return;
        }

        System.out.println("{");

        for(CommonTree node : map.keySet()) {
            if(node == null) {
                System.out.print("null");
            } else {
                String identifier = getIdentifier(node);
                System.out.print("\t\t" + node.getText());
                if(identifier != null) {
                    System.out.print(" (" + identifier + ") : ");
                } else {
                    System.out.print(" : ");
                }
            }

            ArrayList<CommonTree> places = map.get(node);
            System.out.print("[");
            Iterator<CommonTree> it = places.iterator();
            while(it.hasNext()) {
                CommonTree place = it.next();
                String name = getIdentifier(place);
                if(name == null) {
                    System.out.print(place);
                } else {
                    System.out.print(place + " (" + name + ")");
                }

                if(it.hasNext()) { System.out.print(", "); }
            }
            System.out.println("]");
        }
        System.out.println("\t}");
    }

    public static void printBlockMap(HashMap<CommonTree, ArrayList<CommonTree>> map, HashMap<CommonTree, String> BNM_A, HashMap<CommonTree, String> BNM_B) {
        if(map == null || map.size() == 0 || BNM_A == null || BNM_A.size() == 0 || BNM_B == null || BNM_B.size() == 0) {
            System.out.println("{}");
            return;
        }

        System.out.println("{");

        for(CommonTree block : map.keySet()) {
            System.out.print("\t\t");
            if(block == null) {
                System.out.print("null");
            } else {
                System.out.print(BNM_A.get(block) + " : ");
            }

            ArrayList<CommonTree> matches = map.get(block);
            System.out.print("[");
            Iterator<CommonTree> it = matches.iterator();
            while(it.hasNext()) {
                CommonTree match = it.next();
                System.out.print(BNM_B.get(match));
                if(it.hasNext()) { System.out.print(", "); }
            }
            System.out.println("]");
        }
        System.out.println("\t}");
    }

    public static void printStringToStringMap(HashMap<String, ArrayList<String>> map) {
        System.out.print("{");

        if(map != null && map.size() > 0) {
            Iterator<String> sit = map.keySet().iterator();

            while(sit.hasNext()) {
                String str = sit.next();
                if(str == null) {
                    System.out.print("null");
                    continue;
                } else {
                    System.out.print(str);
                }

                ArrayList<String> nodes = map.get(str);
                System.out.print(":[");
                if(nodes != null && nodes.size() > 0) {
                    Iterator<String> nit = nodes.iterator();

                    while(nit.hasNext()) {
                        String node = nit.next();
                        if(node == null) {
                            System.out.print("null");
                            continue;
                        }

                        System.out.print(node);

                        if(nit.hasNext()) { System.out.print(", "); }
                    }
                }
                System.out.print("]");

                if(sit.hasNext()) { System.out.print(", "); }
            }
        }

        System.out.println("}");
    }

    public static void printStringToNodeMap(HashMap<String, ArrayList<CommonTree>> map) {
        System.out.print("{");

        if(map != null && map.size() > 0) {
            Iterator<String> sit = map.keySet().iterator();

            while(sit.hasNext()) {
                String str = sit.next();
                if(str == null) {
                    System.out.print("null");
                    continue;
                } else {
                    System.out.print(str);
                }

                ArrayList<CommonTree> nodes = map.get(str);
                System.out.print(":[");
                if(nodes != null && nodes.size() > 0) {
                    Iterator<CommonTree> nit = nodes.iterator();

                    while(nit.hasNext()) {
                        CommonTree node = nit.next();
                        if(node == null) {
                            System.out.print("null");
                            continue;
                        }

                        CommonTree parent = (CommonTree) node.getParent();
                        if(parent == null) {
                            System.out.print("null");
                            continue;
                        }

                        String partxt = parent.getText();
                        if(partxt == null) {
                            System.out.print("null");
                        } else {
                            System.out.print(partxt);
                        }

                        if(nit.hasNext()) { System.out.print(", "); }
                    }
                }
                System.out.print("]");

                if(sit.hasNext()) { System.out.print(", "); }
            }
        }

        System.out.println("}");
    }

    public static String getFullExpression(CommonTree expression) {
        String expstr = "";

        if(expression == null) {
            return expstr;
        }

        try {
            String text = expression.getText();
            int type = expression.getType();

            boolean ignore = Spector.nexus.isIgnored(type);

            ArrayList<CommonTree> children = (ArrayList<CommonTree>) expression.getChildren();
            if(children == null) {
                if(!ignore) {
                    expstr += text;
                }
            } else {
                if(expression.getChildCount() == 2) {
                    expstr += getFullExpression(children.get(0)) + ", ";
                    if(!ignore) {
                        expstr += text + ", ";
                    }
                    expstr += getFullExpression(children.get(1));
                } else if(expression.getChildCount() == 1) {
                    if(!ignore) {
                        expstr += text + ", ";
                    }
                    expstr += getFullExpression(children.get(0));
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");

            if(expression == null) {
                System.out.print("null");
            } else {
                System.out.print(expression);
            }

            System.out.println(") @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return expstr;
    }

    public static void printNodeToExpressionMap(HashMap<CommonTree, CommonTree> map) {
        System.out.print("{");

        if(map != null && map.size() > 0) {
            Iterator<CommonTree> pit = map.keySet().iterator();

            while(pit.hasNext()) {
                CommonTree node = pit.next();
                if(node == null) {
                    System.out.print("null");
                    continue;
                } else {
                    System.out.print(node);
                }

                CommonTree expression = map.get(node);
                System.out.print(":[" + getFullExpression(expression) + "]");

                if(pit.hasNext()) { System.out.print(", "); }
            }
        }

        System.out.println("}");
    }

    public static void printBlockMap(HashMap<CommonTree, String> map) {
        if(map == null || map.size() == 0) {
            System.out.println("{}");
            return;
        }

        Iterator<CommonTree> it = map.keySet().iterator();

        System.out.print("{");
        while(it.hasNext()) {
            CommonTree node = it.next();

            if(node == null) {
                System.out.print("null");
            } else {
                String name = map.get(node);
                System.out.print(node.getText() + " (" + name + ")");
            }

            if(it.hasNext()) {
                System.out.print(", ");
            } else {
                System.out.println("}");
            }
        }
    }

    /**
     * Matches two numbers, the first one being affected by the threshold
     * @param a The number affected by a threshold
     * @param b The number to compare to
     * @return  Whether the numbers match given the threshold
     */
    public static boolean matchWithThreshold(double a, double b) {
        double mina = a * (1.0-threshold);
        double maxa = a * (1.0+threshold);

        boolean match = false;
        if(a == b) {
            match = true;
        } else if(b >= mina && b <= maxa) {
            match = true;
        }

        if(debug) {
            //System.out.println("matchWithThreshold("+a+","+b+") <=> "+mina+" <= "+b+" <= "+maxa+" ? "+match);
        }

        return match;
    }

    public static boolean matchWithThreshold(HashMap<Integer, Integer> a, HashMap<Integer, Integer> b) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        for(int type : a.keySet()) {
            if(!b.containsKey(type)) {
                return false;
            }

            double countA = a.get(type);
            double countB = b.get(type);

            double mina = countA * (1.0-threshold);
            double maxa = countA * (1.0+threshold);

            if(countA == countB) {
                continue;
            } else if(countB >= mina && countB <= maxa) {
                continue;
            } else {
                return false;
            }
        }

        return true;
    }

    public static HashMap<Integer, Integer> addNodeCountMaps(HashMap<Integer, Integer> a, HashMap<Integer, Integer> b) {
        if(a == null || b == null) {
            return new HashMap<Integer, Integer>();
        }

        HashMap<Integer, Integer> merge = (HashMap<Integer, Integer>) a.clone();

        for(int type : b.keySet()) {
            int current = 0;
            int extra = b.get(type);

            if(merge.containsKey(type)) {
                current = merge.get(type);
            }

            merge.put(type, (current + extra));
        }

        return merge;
    }

    /**
     * Recursive function that counts the number of tokens inside a CommonTree
     * @param   ct  An ANTLR Abstract Syntax Tree
     * @return  Number of nodes
     */
    public static int countNodes(CommonTree ct) {
        return countNodes(ct, false);
    }

    public static int countNodes(CommonTree node, boolean any) {
        int count = 0;

        if(node == null) {
            return 1;
        }

        try {
            int type = node.getType();

            if(any || !Spector.nexus.isIgnored(type)) {
                count++;
            }

            if(node.getChildCount() > 0) {
                for(CommonTree n : (List<CommonTree>) node.getChildren()) {
                    count += countNodes(n, any);
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(node != null) {
                System.out.print("(" + node.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        /*if(debug) { System.out.println("Inspector.countNodes(" + node + "): " + count); }*/
        return count;
    }

    /**
     * Method that counts the number of tokens inside a Source tree
     * @param   s   A Suspect object
     * @return  Number of nodes
     */
    public static int countNodes(Suspect s) {
        return countNodes(s.getTree());
    }

    public static int countNodes(Suspect s, boolean any) {
        return countNodes(s.getTree(), any);
    }

    // Warning: This is heavy

    public static CommonTree getCalledBlock(CommonTree call, HashMap<CommonTree, String> blocks) {
        CommonTree called = null;

        String calling = getIdentifier(call);

        for(CommonTree block : blocks.keySet()) {
            if(block == null) { continue; }
            String name = blocks.get(block);

            if(calling == null || name == null) {
                if(calling == null && name == null) {
                    return block;
                } else {
                    return null;
                }
            }

            if(calling.equals(name)) {
                return block;
            }
        }

        return null; // must be a call to an external method
    }

    public static int countBlockNodes(CommonTree node, HashMap<CommonTree, String> bnm, HashMap<CommonTree, ArrayList<CommonTree>> bcm) {
        return countBlockNodes(node, bnm, bcm, false);
    }

    public static int countBlockNodes(CommonTree node, HashMap<CommonTree, String> bnm, HashMap<CommonTree, ArrayList<CommonTree>> bcm, boolean any) {
        int count = 0;

        if(bnm == null || bcm == null) {
            return 0;
        }

        if(node == null) {
            return 1;
        }

        try {
            String text = node.getText();
            int type = node.getType();


            if(any || !Spector.nexus.isIgnored(type)) {
                count++;
            }

            // Every call needs to be followed to count the target blocks nodes
            if(Spector.nexus.hasType(type, TokenType.Call)) {
                CommonTree calling = getCalledBlock(node, bnm);

                if(calling != null) { // not calling an external method
                    count--; // remove this call node
                    count += countBlockNodes(calling, bnm, bcm, any);
                }
            }

            if(node.getChildCount() > 0) {
                for(CommonTree child : (List<CommonTree>) node.getChildren()) {
                    count += countBlockNodes(child, bnm, bcm, any);
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(node != null) {
                System.out.print("(" + node.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return count;
    }

    /**
     * Matches the number of nodes between two Suspects
     * @param a The first Suspect
     * @param b The second Suspect
     * @return  Whether the Suspects have the same number of nodes
     */
    public static boolean matchNodes(Suspect a, Suspect b) {
        return matchNodes(a, b, false);
    }

    public static boolean matchNodes(Suspect a, Suspect b, boolean any) {
        int an = countNodes(a, any);
        int bn = countNodes(b, any);

        if(an == bn) {
            return true;
        }

        return false;
    }

    public static boolean matchBlockChildrenByType(ArrayList<CommonTree> a, ArrayList<CommonTree> b) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        if(a.size() != b.size()) {
            return false;
        }

        try {
            HashMap<Integer, Integer> nbtA = countDirectNodesByType(a);
            HashMap<Integer, Integer> nbtB = countDirectNodesByType(b);

            for(int typeA : nbtA.keySet()) {
                for(int typeB : nbtB.keySet()) {
                    if(typeA == typeB) {
                        if(nbtA.get(typeA) != nbtB.get(typeA)) {
                            return false;
                        }
                    }
                }
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    /**
     * Recursive method that counts the number of tokens inside a CommonTree of specific types
     * @param   ct  An ANTLR Abstract Syntax Tree
     * @return  A Type,Number map
     */
    public static HashMap<Integer, Integer> countNodesByType(CommonTree ct) {
        HashMap<Integer, Integer> nbt = new HashMap<>(); // Nodes By Type

        if(ct == null) {
            return nbt;
        }

        try {
            // Add the root node
            int rootType = ct.getType();
            if(!Spector.nexus.isIgnored(rootType)) {
                if(nbt.containsKey(rootType)) {
                    nbt.put(rootType, nbt.get(rootType)+1);
                } else {
                    nbt.put(rootType, 1);
                }
            }

            if(ct.getChildCount() > 0) {
                for(CommonTree n : (List<CommonTree>) ct.getChildren()) {
                    HashMap<Integer, Integer> childNodes = countNodesByType(n);
                    if(childNodes.size() > 0) {
                        nbt.putAll(childNodes);
                    }
                }
            }

            return nbt;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(ct != null) {
                System.out.print("(" + ct.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    public static HashMap<Integer, Integer> countDirectNodesByType(ArrayList<CommonTree> list) {
        return countDirectNodesByType(list, false);
    }

    public static HashMap<Integer, Integer> countDirectNodesByType(ArrayList<CommonTree> list, boolean any) {
        HashMap<Integer, Integer> nbt = new HashMap<>(); // Nodes By Type

        if(list == null) {
            return nbt;
        }

        try {
            for(CommonTree node : list) {
                if(node == null) { continue; }
                int type = node.getType();
                if(!any && Spector.nexus.isIgnored(type)) {
                    continue;
                }

                if(nbt.containsKey(type)) {
                    nbt.put(type, nbt.get(type) + 1);
                } else {
                    nbt.put(type, 1);
                }
            }

            return nbt;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    public static HashMap<Integer, Integer> countNodesByTypeWithoutCalls(CommonTree ct) {
        HashMap<Integer, Integer> nbt = new HashMap<>(); // Nodes By Type

        if(ct == null) {
            return nbt;
        }

        try {
            // Add the root node
            int rootType = ct.getType();

            if(Spector.nexus.hasType(rootType, TokenType.Call)) {
                return nbt;
            }

            if(!Spector.nexus.isIgnored(rootType)) {
                if(nbt.containsKey(rootType)) {
                    nbt.put(rootType, nbt.get(rootType)+1);
                } else {
                    nbt.put(rootType, 1);
                }
            }

            if(ct.getChildCount() > 0) {
                for(CommonTree n : (List<CommonTree>) ct.getChildren()) {
                    HashMap<Integer, Integer> childNodes = countNodesByTypeWithoutCalls(n);
                    if(childNodes.size() > 0) {
                        nbt.putAll(childNodes);
                    }
                }
            }

            return nbt;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(ct != null) {
                System.out.print(ct.getText());
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    /**
     * Recursive method that counts the number of tokens inside a CommonTree of specific types
     * @param   s  A Suspect object
     * @return  A Type,Number map
     */
    public static HashMap<Integer, Integer> countNodesByType(ArrayList<CommonTree> ocs) {
        HashMap<Integer, Integer> nbt = new HashMap<Integer, Integer>(); // Nodes By Type

        try {
            for(CommonTree ct : ocs) {
                HashMap<Integer, Integer> cnbt = countNodesByType(ct);

                for(int type : cnbt.keySet()) {
                    if(nbt.containsKey(type)) {
                        nbt.put(type, nbt.get(type) + cnbt.get(type));
                    } else {
                        nbt.put(type, cnbt.get(type));
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return nbt;
    }

    /**
     * Recursive method that counts the number of tokens inside a CommonTree of specific types
     * @param   s  A Suspect object
     * @return  A Type,Number map
     */
    public static HashMap<Integer, Integer> countNodesByType(Suspect s) {
        return countNodesByType(s.getTree());
    }

    public static boolean matchNodesByType(CommonTree a, CommonTree b, boolean lookAtParents, boolean lookAtChildren) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        try {
            int aType = a.getType();
            int bType = b.getType();

            if(aType != bType) {
                return false;
            }

            if(lookAtParents) {
                CommonTree fatherA = (CommonTree) a.getParent();
                CommonTree fatherB = (CommonTree) b.getParent();

                //System.out.println(fatherA + " = " + fatherB + " ?");

                if(fatherA == null || fatherB == null) {
                    if(fatherA != null || fatherB != null) {
                        return false;
                    }
                } else if(fatherA != null && fatherB != null) {
                        int afType = fatherA.getType();
                        int bfType = fatherB.getType();

                        if(afType != bfType) {
                            return false;
                        } else {
                            return true;
                        }
                }
            }

            if(lookAtChildren) {
                HashMap<Integer, Integer> nbtA = countNodesByType(a);
                HashMap<Integer, Integer> nbtB = countNodesByType(b);

                //System.out.println(nbtA + " = " + nbtB + " ?");

                if(nbtA == null || nbtB == null) {
                    if(nbtA == null && nbtB == null) {
                        return true;
                    } else {
                        return false;
                    }
                }

                for(int type : nbtA.keySet()) {
                    if(nbtB.containsKey(type) == false) {
                        return false;
                    }

                    int countA = nbtA.get(type);
                    int countB = nbtB.get(type);

                    if(countA != countB) {
                        return false;
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(a != null && b != null) {
                System.out.print("(" + a.getText() + ", " + b.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }

        return true;
    }

    public static boolean matchNodesByType(CommonTree a, CommonTree b) {
        return matchNodesByType(a, b, true, true);
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static boolean matchNodesByType(ArrayList<CommonTree> a, ArrayList<CommonTree> b) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        boolean gotMatch = false;

        for(CommonTree at : a) {
            gotMatch = false;
            for(CommonTree bt : b) {
                if(matchNodesByType(at, bt)) {
                    gotMatch = true;
                }
            }
            if(!gotMatch) {
                return false;
            }
        }

        return true;
    }

    public static boolean matchNodesByType(Suspect a, Suspect b) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        CommonTree treeA = a.getTree();
        CommonTree treeB = b.getTree();

        return matchNodesByType(treeA, treeB);
    }

    /**
     * A comparison that checks if two CommonTrees have the same type or are in the same group
     * @param   a       The first CommonTree
     * @param   b       The second CommonTree
     * @param   group   The Token group
     * @return Whether the CommonTrees have the same type or are in the same group
     */
    public static boolean matchNodesByTypeOrGroup(CommonTree a, CommonTree b, TokenGroup group) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        try {
            int aType = a.getType();
            int bType = b.getType();

            if(!Spector.nexus.inGroup(aType, group) || !Spector.nexus.inGroup(bType, group)) {
                if(aType != bType) {
                    return false;
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");

            if(a != null && b != null) {
                System.out.print(a.getText() + ", " + b.getText());
            }

            System.out.println(group + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }

        return true;
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static boolean matchNodesByTypeOrGroup(ArrayList<CommonTree> a, ArrayList<CommonTree> b, TokenGroup group) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        boolean gotMatch = false;

        for(CommonTree at : a) {
            gotMatch = false;
            for(CommonTree bt : b) {
                if(matchNodesByTypeOrGroup(at, bt, group)) {
                    gotMatch = true;
                }
            }
            if(!gotMatch) {
                return false;
            }
        }

        return true;
    }

    public static boolean matchNodesByTypeOrGroup(Suspect a, Suspect b, TokenGroup group) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        CommonTree treeA = a.getTree();
        CommonTree treeB = b.getTree();

        return matchNodesByTypeOrGroup(treeA, treeB, group);
    }

    public static boolean matchSimilarNodes(String a, ArrayList<CommonTree> ocsA, String b, ArrayList<CommonTree> ocsB) {
        return matchSimilarNodes(a, ocsA, b, ocsB, false);
    }

    public static boolean matchSimilarNodes(String a, ArrayList<CommonTree> ocsA, String b, ArrayList<CommonTree> ocsB, boolean orderIndependent) {
        if(ocsA == null || ocsB == null) {
            if(ocsA == null && ocsB == null) {
                return true;
            } else {
                return false;
            }
        }

        for(CommonTree ocA : ocsA) {
            ArrayList<CommonTree> temp = (ArrayList<CommonTree>) ocsB.clone();

            for(CommonTree ocB : temp) {
                // Check if they are both null (root)

                if(ocA == null || ocB == null) {
                    if(ocA == null && ocB == null) {
                        ocsB.remove(ocB);
                        break;
                    } else {
                        if(orderIndependent) {
                            continue;
                        } else {
                            return false;
                        }
                    }
                }

                // Check if they have the same type of parent

                // parents
                CommonTree pA = null;
                CommonTree pB = null;

                if(Spector.nexus.hasType(ocA.getType(), TokenType.Identifier)) {
                    pA = (CommonTree) ocA.getParent();
                } else {
                    pA = ocA;
                }

                if(Spector.nexus.hasType(ocB.getType(), TokenType.Identifier)) {
                    pB = (CommonTree) ocB.getParent();
                } else {
                    pB = ocB;
                }

                if(pA == null || pB == null) {
                    if(pA != null || pB != null) {
                        if(orderIndependent) {
                            continue;
                        } else {
                            return false;
                        }
                    }
                }

                ArrayList<CommonTree> nsA = null, nsB = null;

                // parent types
                int ptA = pA.getType();
                int ptB = pB.getType();

                if(ptA != ptB) {
                    if(orderIndependent) {
                        continue;
                    } else {
                        return false;
                    }
                }

                // Check if they have similar neighbors

                // neighbors
                nsA = (ArrayList<CommonTree>) pA.getChildren();
                nsB = (ArrayList<CommonTree>) pB.getChildren();

                if(nsA == null || nsB == null) {
                    if(nsA == null && nsB == null) {
                        break;
                    } else {
                        if(orderIndependent) {
                            continue;
                        } else {
                            return false;
                        }
                    }
                }

                for(CommonTree nA : nsA) {
                    if(nA != null && nA.equals(ocA)) {
                        continue;
                    }

                    for(CommonTree nB : nsB) {
                        if(nB != null && nB.equals(ocB)) {
                            continue;
                        }

                        if(nA == null || nB == null) {
                            if(nA == null && nB == null) {
                                break;
                            } else {
                                if(orderIndependent) {
                                    continue;
                                } else {
                                    return false;
                                }
                            }
                        }

                        int tA = nA.getType();
                        int tB = nB.getType();

                        if(Spector.nexus.hasType(tA, TokenType.Identifier) || Spector.nexus.hasType(tB, TokenType.Identifier)) {
                            break;
                        } else if(tA != tB) {
                            if(orderIndependent) {
                                continue;
                            } else {
                                return false;
                            }
                        }

                        String sA = nA.getText();
                        String sB = nB.getText();

                        if(sA == null || sB == null) {
                            if(sA == null && sB == null) {
                                break;
                            } else {
                                if(orderIndependent) {
                                    continue;
                                } else {
                                    return false;
                                }
                            }
                        }

                        if(sA.equals(sB)) {
                            break;
                        } else if(!orderIndependent) {
                            return false;
                        }
                    }
                }

                ocsB.remove(ocB);
                break;
            }
        }

        if(ocsB.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if two Nodes (and their children) have the same content
     * @param a First CommonTree
     * @param b Second CommonTree
     * @return Whether the CommonTrees are equivalent
     */
    public static boolean matchNodesByExactContent(CommonTree a, CommonTree b) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        try {
            // Are the contents of the nodes equal ?

            String aText = a.getText();
            String bText = b.getText();

            if(aText == null || bText == null) {
                if(aText == null && bText == null) {
                    return true;
                } else {
                    return false;
                }
            }

            if(!aText.equals(bText)) {
                return false;
            }

            // Are the contents of the children nodes equal ?

            List<CommonTree> aChildren = (List<CommonTree>) a.getChildren();
            List<CommonTree> bChildren = (List<CommonTree>) b.getChildren();

            // If the either list of Children is NULL
            if(aChildren == null || bChildren == null) {
                // Make sure that they are both NULL
                if(aChildren == null && bChildren == null) {
                    return true;
                } else {
                    return false;
                }
            }

            // Copy the list of children in the second CommonTree
            ArrayList<CommonTree> temp = new ArrayList<CommonTree>(bChildren);
            boolean match = true;

            // For every Child node A and B
            for(CommonTree ca : aChildren) {
                CommonTree cb = temp.remove(0); //temp.removeFirst();

                if(!matchNodesByExactContent(ca, cb)) {
                    return false;
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(a != null && b != null) {
                System.out.print("(" + a.getText() + ", " + b.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }

        return true;
    }

    public static boolean matchNodesByExactContent(Suspect a, Suspect b) {
        return matchNodesByExactContent(a.getTree(), b.getTree());
    }

   /**
     * Compare two CommonTrees
     * @param a First CommonTree
     * @param b Second CommonTree
     * @return Whether the CommonTrees are equivalent
     */
    public static boolean matchNodesByContent(CommonTree a, CommonTree b) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }
        try {
            // Are the contents of the nodes equal ?

            String aText = a.getText();
            String bText = b.getText();
            if(aText == null || bText == null) {
                if(aText == null && bText == null) {
                    return true;
                } else {
                    return false;
                }
            }

            if(!aText.equals(bText)) {
                return false;
            }

            // Are the contents of the children nodes equal ?

            List<CommonTree> aChildren = (List<CommonTree>) a.getChildren();
            List<CommonTree> bChildren = (List<CommonTree>) b.getChildren();

            // If the either list of Children is NULL
            if(aChildren == null || bChildren == null) {
                // Make sure that they are both NULL
                if(aChildren == null && bChildren == null) {
                    return true;
                } else {
                    return false;
                }
            }

            // Prepare a map for the copies (we don't want duplicate nodes matching)
            HashMap<CommonTree, CommonTree> copies = new HashMap<CommonTree, CommonTree>();

            int size = 0;

            // For every Child nodes A and B
            for(CommonTree ca : aChildren) {
                for(CommonTree cb : bChildren) {
                    // If the child from B wasn't matched before
                    // And it matches the child from A
                    if(!copies.containsValue(cb) && matchNodesByExactContent(ca, cb) == true) {
                        // Add it to the list of copies
                        copies.put(ca, cb);
                        ++size;
                    }
                }
            }

            if(size != aChildren.size()) {
                return false;
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(a != null && b != null) {
                System.out.print("(" + a.getText() + ", " + b.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }

        return true;
    }

    /**
     * Compares two lists of Occurrence nodes
     * @param a First list of Occurrence Nodes
     * @param b Second list of Occurrence Nodes
     * @return Whether the lists are equivalent
     */
    public static boolean matchNodesByContent(ArrayList<CommonTree> a, ArrayList<CommonTree> b) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        boolean gotMatch = false;

        for(CommonTree at : a) {
            gotMatch = false;
            for(CommonTree bt : b) {
                if(matchNodesByContent(at, bt)) {
                    gotMatch = true;
                }
            }
            if(!gotMatch) {
                return false;
            }
        }

        return true;
    }

    /**
     * Compares two Suspects CommonTrees
     * @param a First Suspect
     * @param b Second Suspect
     * @return Whether the CommonTrees are equivalent
     */
    public static boolean matchNodesByContent(Suspect a, Suspect b) {
        return matchNodesByContent(a.getTree(), b.getTree());
    }

    public static boolean matchExpressionContents(CommonTree a, CommonTree b) {
        try {
            if(a == null || b == null) {
                if(a == null && b == null) {
                    return true;
                } else {
                    return false;
                }
            }

            // Get node types
            int typeA = a.getType();
            int typeB = b.getType();

            // If it isn't ignored, let's compare the types
            if(!Spector.nexus.isIgnored(typeA) && !Spector.nexus.isIgnored(typeB)) {
                // Check if its a pair of identifiers

                boolean aIsIdentifier = Spector.nexus.inGroup(typeA, TokenGroup.Identifier);
                boolean bIsIdentifier = Spector.nexus.inGroup(typeB, TokenGroup.Identifier);

                if(aIsIdentifier || bIsIdentifier) {
                    if(!aIsIdentifier || !bIsIdentifier) {
                        return false;
                    }
                } else {
                    String textA = a.getText();
                    String textB = b.getText();

                    if(textA == null || textB == null) {
                        if(textA != null || textB != null) {
                            return false;
                        }
                    } else {
                        if(!textA.equals(textB)) {
                            return false;
                        }
                    }
                }
            }

            // Are the contents of the children nodes equal ?

            ArrayList<CommonTree> childrenA = (ArrayList<CommonTree>) a.getChildren();
            ArrayList<CommonTree> childrenB = (ArrayList<CommonTree>) b.getChildren();

            // If the either list of Children is NULL
            if(childrenA == null || childrenB == null) {
                // Make sure that they are both NULL
                if(childrenA == null && childrenB == null) {
                    return true;
                } else {
                    return false;
                }
            }

            // Copy the list of children in the second CommonTree
            ArrayList<CommonTree> childrenBCopy;
            boolean found = false;

            // For every Child nodes A and B
            for(CommonTree ca : childrenA) {
                childrenBCopy = (ArrayList<CommonTree>) childrenB.clone();
                found = false;

                for(CommonTree cb : childrenBCopy) {
                    if(!matchExpressionContents(ca, cb)) {
                        continue;
                    } else {
                        childrenBCopy.remove(cb);
                        found = true;
                        break;
                    }
                }

                if(found == false) {
                    return false;
                }
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(a != null && b != null) {
                System.out.print("(" + a.getText() + ", " + b.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    public static boolean matchCaseExpressionContents(CommonTree a, CommonTree b) {
        try {
            if(a == null || b == null) {
                if(a == null && b == null) {
                    return true;
                } else {
                    return false;
                }
            }

            // Get the parents and figure out who is under a case
            CommonTree parentA = (CommonTree) a.getParent();
            CommonTree parentB = (CommonTree) b.getParent();

            boolean aIsCase = false;
            boolean bIsCase = false;

            if(parentA != null && parentB != null) {
                int parentTypeA = parentA.getType();
                int parentTypeB = parentB.getType();

                if(Spector.nexus.inGroup(parentTypeA, TokenGroup.Case)) {
                    aIsCase = true;
                    parentA = getParentCondition(a);
                }

                if(Spector.nexus.inGroup(parentTypeB, TokenGroup.Case)) {
                    bIsCase = true;
                    parentB = getParentCondition(b);
                }
            }

            if(aIsCase && bIsCase) {
                return matchCaseExpressionContents(a, b, parentA, parentB);
            } else if(aIsCase) {
                return matchCaseExpressionContents(a, b, parentA, null);
            } else if(bIsCase) {
                return matchCaseExpressionContents(a, b, null, parentB);
            }

            return false;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(a != null && b != null) {
                System.out.print("(" + a.getText() + ", " + b.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    // Assumes an expression will start with a single node with children
    public static CommonTree getExpressionHead(CommonTree node) {
        try {
            if(node == null) {
                return null;
            }

            int nodeType = node.getType();
            List<CommonTree> children = (List<CommonTree>) node.getChildren();

            if(Spector.nexus.inGroup(nodeType, TokenGroup.Expression)) {
                if(node.getChildCount() == 1) {
                    Iterator<CommonTree> it = children.iterator();
                    return it.next();
                } else {
                    return null;
                }
            }

            for(CommonTree child : children) {
                return getExpressionHead(child);
            }

            return null;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(node != null) {
                System.out.print("(" + node.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    public static boolean matchCaseExpressionContents(CommonTree conditionA, CommonTree conditionB, CommonTree switchConditionA, CommonTree switchConditionB) {
        try {
            if(conditionA == null || conditionB == null) {
                if(conditionA == null && conditionB == null) {
                    return true;
                } else {
                    return false;
                }
            }

            if(switchConditionA == null && switchConditionB == null) {
                return false; // This must never happen
            }

            CommonTree headA = getExpressionHead(conditionA);
            CommonTree headB = getExpressionHead(conditionB);
            CommonTree parentHeadA = getExpressionHead(switchConditionA);
            CommonTree parentHeadB = getExpressionHead(switchConditionB);

            boolean aIsCase = false;
            boolean bIsCase = false;

            // if A is not a case it should start with an ==
            if(parentHeadA == null) {
                if(Spector.nexus.hasType(headA.getType(), TokenType.Equals)) {
                    aIsCase = true;
                } else {
                    return false;
                }
            }

            // if B is not a case it should start with an ==
            if(parentHeadB == null) {
                if(Spector.nexus.hasType(headB.getType(), TokenType.Equals)) {
                    bIsCase = true;
                } else {
                    return false;
                }
            }

            ArrayList<CommonTree> childrenA = null;
            ArrayList<CommonTree> childrenB = null;

            if(aIsCase) {
                childrenA = (ArrayList<CommonTree>) headA.getChildren();
            } else {
                childrenA = new ArrayList<CommonTree>();
                childrenA.add(headA);
                childrenA.add(parentHeadA);
            }

            if(bIsCase) {
                childrenB = (ArrayList<CommonTree>) headB.getChildren();
            } else {
                childrenB = new ArrayList<CommonTree>();
                childrenB.add(headB);
                childrenB.add(parentHeadB);
            }

            ArrayList<CommonTree> childrenBCopy = (ArrayList<CommonTree>) childrenB.clone();

            for(CommonTree childA : childrenA) {
                for(CommonTree childB : childrenBCopy) {
                    if(childA.getText().equals(childB.getText())) {
                        childrenB.remove(childB);
                    }
                }
            }

            return childrenB.isEmpty();
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(conditionA != null && switchConditionA != null && conditionB != null && switchConditionB != null) {
                System.out.print("(" + conditionA.getText() + ", " + switchConditionA.getText() + ", " + conditionB.getText() + ", " + switchConditionB.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    public static boolean checkNodesForEquivalence(String idA, CommonTree parentA, String idB, CommonTree parentB) {
        try {
            // Is either identifier null ?
            if(idA == null || idB == null) {
                // Are they both ?
                if(idA == null && idB == null) {
                    return true;
                } else {
                    return false;
                }
            }

            // Is either node null ?
            if(parentA == null || parentB == null) {
                // Are they both ?
                if(parentA == null && parentB == null) {
                    return true;
                } else {
                    return false;
                }
            }

            ArrayList<CommonTree> childrenA = (ArrayList<CommonTree>) parentA.getChildren();
            ArrayList<CommonTree> childrenB = (ArrayList<CommonTree>) parentB.getChildren();

            // Is either list null ?
            if(childrenA == null || childrenB == null) {
                // Are they both ?
                if(childrenA == null && childrenB == null) {
                    return true;
                } else {
                    return false;
                }
            }

            // For each pair of children
            for(CommonTree ca : childrenA) {
                if(ca == null || Spector.nexus.hasType(ca.getType(), TokenType.Identifier)) { continue; }

                String aText = ca.getText();

                for(CommonTree cb : childrenB) {
                    if(cb == null || Spector.nexus.hasType(cb.getType(), TokenType.Identifier)) { continue; }

                    String bText = cb.getText();

                    // Is either content null ?
                    if(aText == null || bText == null) {
                        // Are they both null ?
                        if(aText == null && bText == null) {
                            return true;
                        } else {
                            continue;
                        }
                    }

                    // Do the contents match ?
                    if(aText.equals(bText)) {
                        return true;
                    }
                }
            }

        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(parentA != null && parentB != null) {
                System.out.print("(" + idA + ", " + parentA.getText() + ", " + idB + parentB.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            System.out.println();
        }

        return false;
    }
    public static boolean checkNodesForEquivalence(String idA, ArrayList<CommonTree> a, String idB, ArrayList<CommonTree> b) {
        if(idA == null || idB == null) {
            if(idA == null && idB == null) {
                return true;
            } else {
                return false;
            }
        }

        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        for(CommonTree at : a) {
            for(CommonTree bt : b) {
                if(checkNodesForEquivalence(idA, at, idB, bt)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean checkNodesForSimilarity(String idA, CommonTree parentA, String idB, CommonTree parentB) {
        try {
            // Is either identifier null ?
            if(idA == null || idB == null) {
                // Are they both ?
                if(idA == null && idB == null) {
                    return true;
                } else {
                    return false;
                }
            }

            // Are the ids the same ?
            if(!idA.equals(idB)) {
                return false;
            }

            // Is either node null ?
            if(parentA == null || parentB == null) {
                // Are they both ?
                if(parentA == null && parentB == null) {
                    return true;
                } else {
                    return false;
                }
            }

            ArrayList<CommonTree> childrenA = (ArrayList<CommonTree>) parentA.getChildren();
            ArrayList<CommonTree> childrenB = (ArrayList<CommonTree>) parentB.getChildren();

            // Is either list null ?
            if(childrenA == null || childrenB == null) {
                // Are they both ?
                if(childrenA == null && childrenB == null) {
                    return true;
                } else {
                    return false;
                }
            }

            // For each pair of children
            for(CommonTree ca : childrenA) {
                if(ca == null) { continue; }

                String aText = ca.getText();

                for(CommonTree cb : childrenB) {
                    if(cb == null) { continue; }

                    String bText = cb.getText();

                    // Is either content null ?
                    if(aText == null || bText == null) {
                        // Are they both null ?
                        if(aText == null && bText == null) {
                            return true;
                        } else {
                            continue;
                        }
                    }

                    // Do the contents match ?
                    if(aText.equals(bText)) {
                        return true;
                    }
                }
            }

        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(parentA != null && parentB != null) {
                System.out.print("(" + idA + ", " + parentA.getText() + ", " + idB + parentB.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            System.out.println();
        }

        return false;
    }

    /**
     * Compares two lists of Occurrence nodes
     * @param a First list of Occurrence Nodes
     * @param b Second list of Occurrence Nodes
     * @return Whether the lists are equivalent
     */
    public static boolean checkNodesForSimilarity(String idA, ArrayList<CommonTree> a, String idB, ArrayList<CommonTree> b) {
        if(idA == null || idB == null) {
            if(idA == null && idB == null) {
                return true;
            } else {
                return false;
            }
        }

        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        for(CommonTree at : a) {
            for(CommonTree bt : b) {
                if(checkNodesForSimilarity(idA, at, idB, bt)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *
     * @param root
     * @return
     */
    public static String getFullSequence(CommonTree root) {
        if(root == null) {
            return null;
        }

        String sequence = "";
        String rootText = root.getText();

        if(!rootText.equals(".")) {
            return rootText;
        } else {
            List<CommonTree> children = (List<CommonTree>)root.getChildren();
            boolean first = true;
            for(CommonTree child : children) {
                int childType = child.getType();
                if(!Spector.nexus.inGroup(childType, TokenGroup.Sequence)) {
                    String childText = child.getText();
                    if(!first) { sequence += "."; }
                    if(childText != null) {
                        sequence += childText;
                    } else {
                        sequence += "null";
                    }
                    if(first) { first = false; }
                } else {
                    sequence += "." + getFullSequence(child);
                }
            }
            return sequence;
        }
    }

    /**
     *
     * @param tree
     * @param group
     * @return
     */
    public static HashMap<String, ArrayList<CommonTree>> filterByGroup(CommonTree tree, TokenGroup group) {
        HashMap<String, ArrayList<CommonTree>> idnet = new HashMap<>();

        if(tree == null) {
            return idnet;
        }

        try {
            int rootType = tree.getType();
            String rootText = tree.getText();
            ArrayList<CommonTree> ocs = null;
            boolean amSequence = false;

            if(Spector.nexus.inGroup(rootType, TokenGroup.Sequence)) {
                rootText = getFullSequence(tree);
                amSequence = true;
            }

            if(Spector.nexus.inGroup(rootType, group)) {
                if(idnet.containsKey(rootText)) {
                    ocs = idnet.get(rootText);
                    ocs.add(tree);
                } else {
                    ocs = new ArrayList<>();
                    ocs.add(tree);
                    idnet.put(rootText, ocs);
                }
            }

            //if(debug) { System.out.println("filterByGroup("+tree+","+group+"): "+rootType+"?"+Spector.nexus.inGroup(rootType, group)); }

            if(!amSequence && tree.getChildCount() > 0) {
                for(CommonTree child : (List<CommonTree>) tree.getChildren()) {
                    HashMap<String, ArrayList<CommonTree>> childids = filterByGroup(child, group);

                    if(childids != null) {
                        for(String id : childids.keySet()) {
                            if(idnet.containsKey(id)) {
                                idnet.get(id).addAll(childids.get(id));
                            } else {
                                idnet.put(id, childids.get(id));
                            }
                        }
                    }
                }
            }
            return idnet;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");

            if(tree != null) {
                System.out.print(tree.getText());
            }

            System.out.println(group + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    public static boolean matchChildrenByType(ArrayList<CommonTree> a, ArrayList<CommonTree> b) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            }
        }

        try {
            for(CommonTree ctA : a) {
                for(CommonTree ctB : b) {
                    if(ctA == null || ctB == null) {
                        if(ctA != null || ctB != null) {
                            return false;
                        }
                    } else {

                    }
                }
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    public static HashMap<CommonTree, String> getNodesByGroup(CommonTree node, TokenGroup group) {
        HashMap<CommonTree, String> map = new HashMap<CommonTree, String>();

        if(node == null) {
            return map;
        }

        try {
            int myType = node.getType();
            String myText = node.getText();
            boolean amSequence = false;

            if(Spector.nexus.inGroup(myType, TokenGroup.Sequence)) {
                myText = getFullSequence(node);
                amSequence = true;
            }

            if(Spector.nexus.inGroup(myType, group)) {
                map.put(node, myText);
            }

            if(!amSequence && node.getChildCount() > 0) {
                for(CommonTree child : (List<CommonTree>) node.getChildren()) {
                    HashMap<CommonTree, String> children = getNodesByGroup(child, group);

                    if(children != null) {
                        map.putAll(children);
                    }
                }
            }

            return map;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");

            if(node != null) {
                System.out.print("(" + node.getText() + ")");
            }

            System.out.println(group + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    public static HashMap<CommonTree, String> getNodesByGroup(Suspect suspect, TokenGroup group) {
        if(suspect == null) {
            return new HashMap<CommonTree, String>();
        }

        CommonTree tree = suspect.getTree();

        if(tree == null) {
            return new HashMap<CommonTree, String>();
        }

        return getNodesByGroup(tree, group);
    }

    public static HashMap<CommonTree, String> getBlockNodes(CommonTree node) {
        HashMap<CommonTree, String> map = new HashMap<CommonTree, String>();

        if(node == null) {
            return map;
        }

        try {
            int myType = node.getType();
            String myText = node.getText();
            boolean amSequence = false;

            if(Spector.nexus.inGroup(myType, TokenGroup.Sequence)) {
                myText = getFullSequence(node);
                amSequence = true;
            }

            if(Spector.nexus.inGroup(myType, TokenGroup.Block)) {
                map.put(node, getIdentifier(node));
            }

            if(!amSequence && node.getChildCount() > 0) {
                for(CommonTree child : (List<CommonTree>) node.getChildren()) {
                    HashMap<CommonTree, String> children = getBlockNodes(child);

                    if(children != null) {
                        map.putAll(children);
                    }
                }
            }

            return map;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(node != null) {
                System.out.print("(" + node.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return new HashMap<CommonTree, String>();
        }
    }

    public static HashMap<CommonTree, String> getBlockNodes(Suspect suspect) {
        if(suspect == null) {
            return new HashMap<CommonTree, String>();
        }

        CommonTree tree = suspect.getTree();

        if(tree == null) {
            return new HashMap<CommonTree, String>();
        }

        return getBlockNodes(tree);
    }

    public static ArrayList<CommonTree> getChildren(CommonTree node, HashMap<CommonTree, String> bnm) {
        return getChildren(node, bnm, false, false);
    }

    public static ArrayList<CommonTree> getChildren(CommonTree node, HashMap<CommonTree, String> bnm, boolean any, boolean noIdentifier) {
        ArrayList<CommonTree> children = new ArrayList<CommonTree>();
        if(node == null) {
            return children;
        }

        ArrayList<CommonTree> list = (ArrayList<CommonTree>) node.getChildren();
        if(list == null) {
            return children;
        }

        try {
            for(CommonTree child : list) {
                if(child == null) { continue; }

                int type = child.getType();
                String text = child.getText();

                if(Spector.nexus.inGroup(type, TokenGroup.Identifier) && noIdentifier) { continue; }

                boolean isIgnored = Spector.nexus.isIgnored(type);
                boolean isSequence = Spector.nexus.inGroup(type, TokenGroup.Sequence);
                boolean isCall = Spector.nexus.hasType(type, TokenType.Call);

                if(isSequence) {
                    text = getIdentifier(node);
                }

                if(isCall) {
                    // If we find a call we want the block its pointing at
                    CommonTree called = findBlockByName(bnm, getIdentifier(child));

                    if(called == null) {
                        if(any || !isIgnored) {
                            children.add(child); // add the call of an external method
                        }
                        children.addAll(getChildren(child, bnm, any, true));
                    } else {
                        // ignore the call node
                        children.addAll(getChildren(called, bnm, any, noIdentifier)); // add the called block
                    }
                } else {
                    if(isSequence) {
                        children.add(child);
                    } else {
                        if(any || !isIgnored) {
                            children.add(child);
                        }

                        children.addAll(getChildren(child, bnm, any, noIdentifier));
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return children;
    }

    public static HashMap<CommonTree, ArrayList<CommonTree>> mapBlocksToChildren(HashMap<CommonTree, String> bnm) {
        return mapBlocksToChildren(bnm, false);
    }

    public static HashMap<CommonTree, ArrayList<CommonTree>> mapBlocksToChildren(HashMap<CommonTree, String> bnm, boolean any) {
        HashMap<CommonTree, ArrayList<CommonTree>> bcm = new HashMap<CommonTree, ArrayList<CommonTree>>();

        if(bnm == null) {
            return bcm;
        }

        try {
            for(CommonTree block : bnm.keySet()) {
                ArrayList<CommonTree> children = null;

                if(bcm.containsKey(block)) {
                    children = bcm.get(block);
                } else {
                    children = new ArrayList<CommonTree>();
                }

                children.addAll(getChildren(block, bnm, any, false));
                bcm.put(block, children);
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return bcm;
    }

    public static HashMap<CommonTree, ArrayList<CommonTree>> getCallNodes(CommonTree node) {
        HashMap<CommonTree, ArrayList<CommonTree>> map = new HashMap<CommonTree, ArrayList<CommonTree>>();

        if(node == null) {
            return map;
        }

        try {
            int myType = node.getType();
            boolean amSequence = false;

            if(Spector.nexus.inGroup(myType, TokenGroup.Sequence)) {
                amSequence = true;
            }

            if(Spector.nexus.inGroup(myType, TokenGroup.Call)) {
                map.put(node, new ArrayList<CommonTree>());
            }

            if(!amSequence && node.getChildCount() > 0) {
                for(CommonTree child : (List<CommonTree>) node.getChildren()) {
                    HashMap<CommonTree, ArrayList<CommonTree>> children = getCallNodes(child);

                    if(children != null) {
                        map.putAll(children);
                    }
                }
            }

            return map;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(node != null) {
                System.out.print("(" + node.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return new HashMap<CommonTree, ArrayList<CommonTree>>();
        }
    }

    public static HashMap<CommonTree, ArrayList<CommonTree>> getCallNodes(Suspect suspect) {
        if(suspect == null) {
            return new HashMap<CommonTree, ArrayList<CommonTree>>();
        }

        CommonTree tree = suspect.getTree();

        if(tree == null) {
            return new HashMap<CommonTree, ArrayList<CommonTree>>();
        }

        return getCallNodes(tree);
    }

    public static HashMap<CommonTree, ArrayList<CommonTree>> mapNodeToNodeByGroup(HashMap<CommonTree, CommonTree> mapA, HashMap<CommonTree, CommonTree> mapB, TokenGroup group) {
        HashMap<CommonTree, ArrayList<CommonTree>> map = new HashMap<CommonTree, ArrayList<CommonTree>>();

        try {
            for(CommonTree a : mapA.keySet()) {
                if(a == null || !Spector.nexus.inGroup(a.getType(), group)) {
                    continue;
                }

                for(CommonTree b : mapB.keySet()) {
                    if(b == null || !Spector.nexus.inGroup(b.getType(), group)) {
                        continue;
                    }

                    ArrayList<CommonTree> list = null;
                    if(map.containsKey(a)) {
                        list = map.get(a);
                    } else {
                        list = new ArrayList<CommonTree>();
                    }

                    list.add(b);
                    map.put(a, list);
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");
            System.out.println(group + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }

        return map;
    }

    /*
    public static HashMap<CommonTree, ArrayList<CommonTree>> mapNodesByGroup(CommonTree a, CommonTree b, TokenGroup group) {
        HashMap<CommonTree, ArrayList<CommonTree>> map = new HashMap<CommonTree, ArrayList<CommonTree>>();

        if(a == null || b == null) {
            return map;
        }

        try {
            HashMap<CommonTree, String> aNodes = getNodesByGroup(a, group);
            HashMap<CommonTree, String> bNodes = getNodesByGroup(b, group);

            return mapNodesByGroup(aNodes, bNodes);
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");

            if(a != null && b != null) {
                System.out.print(a.getText() + ", " + b.getText() + ", ");
            }

            System.out.println(group + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }
    */

    public static HashMap<CommonTree, ArrayList<CommonTree>> mapNodesByGroup(HashMap<CommonTree, String> CNM_A, HashMap<CommonTree, String> CNM_B) {
        HashMap<CommonTree, ArrayList<CommonTree>> map = new HashMap<CommonTree, ArrayList<CommonTree>>();

        if(CNM_A == null || CNM_B == null) {
            return map;
        }

        try {
            for(CommonTree nodeA : CNM_A.keySet()) {
                for(CommonTree nodeB : CNM_B.keySet()) {
                    if(map.containsKey(nodeA)) {
                        ArrayList<CommonTree> list = map.get(nodeA);
                        list.add(nodeB);
                    } else {
                        ArrayList<CommonTree> list = new ArrayList<CommonTree>();
                        list.add(nodeB);
                        map.put(nodeA, list);
                    }
                }
            }

            return map;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    public static CommonTree getParentBlock(CommonTree node) {
        if(node == null) {
            return null;
        }

        try {
            CommonTree parent = (CommonTree) node.getParent();

            while(parent.getParent() != null && !Spector.nexus.inGroup(parent.getType(), TokenGroup.Block)) {
                parent = (CommonTree) parent.getParent();
            }

            if(parent == null || !Spector.nexus.inGroup(parent.getType(), TokenGroup.Block)) {
                return null;
            }

            return parent;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(node != null) {
                System.out.print("(" + node.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    public static boolean pairCallsWithBlocks(HashMap<CommonTree, String> blockMap, HashMap<CommonTree, ArrayList<CommonTree>> callMap) {
        if(blockMap == null || blockMap.isEmpty() || callMap == null || callMap.isEmpty()) {
            return false;
        }

        try {
            for(CommonTree call : callMap.keySet()) {
                if(call == null || !callMap.containsKey(call)) {
                    continue;
                }

                ArrayList<CommonTree> blockList = callMap.get(call);
                //System.out.println("#1.1: " + blockList);

                // get the block where this call is and add it to the list
                CommonTree parent = (CommonTree) call.getParent();
                if(parent == null) {
                    continue;
                }
                //System.out.println("#1.2: " + parent);
                CommonTree parentBlock = getParentBlock(parent);
                if(parentBlock == null) {
                    continue;
                }
                //System.out.println("#1.3: " + parentBlock);

                CommonTree called;
                for(CommonTree block : blockMap.keySet()) {
                    if(block == null) {
                        continue;
                    }

                    String name = blockMap.get(block);

                    if(name == null) {
                        continue;
                    }

                    String calling = getIdentifier(call);

                    if(name.equals(calling)) {
                        blockList.add(block);
                        break;
                    }
                }

                blockList.add(parentBlock);
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    public static boolean filterCalls(HashMap<CommonTree, ArrayList<CommonTree>> callMap) {
        if(callMap == null || callMap.isEmpty()) {
            return false;
        }

        try {
            HashMap<CommonTree, ArrayList<CommonTree>> cloneCallMap = (HashMap<CommonTree, ArrayList<CommonTree>>) callMap.clone();
            for(CommonTree call : cloneCallMap.keySet()) {
                if(call == null) {
                    callMap.remove(call);
                    continue;
                }

                ArrayList<CommonTree> blockList = callMap.get(call);

                if(blockList == null) {
                    continue;
                }

                if(blockList.size() < 2) {
                    callMap.remove(call);
                }
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    public static boolean filterDuplicateCalls(HashMap<CommonTree, ArrayList<CommonTree>> callMapA, HashMap<CommonTree, ArrayList<CommonTree>> callMapB) {
        if(callMapA == null || callMapA.size() < 2 || callMapB == null|| callMapB.size() < 2) {
            return false;
        }

        try {
            HashMap<CommonTree, ArrayList<CommonTree>> cloneCallMapA = (HashMap<CommonTree, ArrayList<CommonTree>>) callMapA.clone();
            HashMap<CommonTree, ArrayList<CommonTree>> cloneCallMapB = (HashMap<CommonTree, ArrayList<CommonTree>>) callMapB.clone();

            for(CommonTree callA : cloneCallMapA.keySet()) {
                if(callA == null) {
                    continue;
                }

                for(CommonTree callB : cloneCallMapB.keySet()) {
                    if(callB == null) {
                        continue;
                    }

                    CommonTree parentA = cloneCallMapA.get(callA).get(0);
                    CommonTree parentB = cloneCallMapB.get(callB).get(0);

                    String parentNameA = getIdentifier(parentA);
                    String parentNameB = getIdentifier(parentB);

                    /*
                    CommonTree blockA = cloneCallMapA.get(callA).get(1);
                    CommonTree blockB = cloneCallMapB.get(callB).get(1);

                    String nameA = getIdentifier(blockA);
                    String nameB = getIdentifier(blockB);
                    */

                    String nameA = getIdentifier(callA);
                    String nameB = getIdentifier(callB);

                    // under the same block and calling the same method ?
                    if(parentNameA.equals(parentNameB) && nameA.equals(nameB)) {
                        callMapA.remove(callA);
                        callMapB.remove(callB);
                    }
                }
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    public static HashMap<CommonTree, ArrayList<CommonTree>> mapBlocks(
            HashMap<CommonTree, String> BNM_A,
            HashMap<CommonTree, String> BNM_B,
            HashMap<CommonTree, ArrayList<CommonTree>> BCM_A,
            HashMap<CommonTree, ArrayList<CommonTree>> BCM_B
    ) {
        return mapBlocks(BNM_A, BNM_B, BCM_A, BCM_B, false);
    }

    public static HashMap<CommonTree, ArrayList<CommonTree>> mapBlocks(
            HashMap<CommonTree, String> BNM_A,
            HashMap<CommonTree, String> BNM_B,
            HashMap<CommonTree, ArrayList<CommonTree>> BCM_A,
            HashMap<CommonTree, ArrayList<CommonTree>> BCM_B,
            boolean any
    ) {
        HashMap<CommonTree, ArrayList<CommonTree>> map = new HashMap<CommonTree, ArrayList<CommonTree>>();

        if(BCM_A == null || BCM_B == null) {
            return map;
        }

        try {
            for(CommonTree bA : BCM_A.keySet()) {
                for(CommonTree bB : BCM_B.keySet()) {
                    if(bA == null || bB == null) {
                        if(bA == null && bB == null) {
                            continue;
                        } else {
                            return map;
                        }
                    }

                    int cA = countBlockNodes(bA, BNM_A, BCM_A, any);
                    int cB = countBlockNodes(bB, BNM_B, BCM_B, any);

                    //System.out.println(getIdentifier(bA) + ":" + cA + " = " + getIdentifier(bB) + ":" + cB);

                    if(matchWithThreshold(cA, cB)) {
                        ArrayList<CommonTree> list = null;

                        if(map.containsKey(bA)) {
                            list = map.get(bA);
                        } else {
                            list = new ArrayList<CommonTree>();
                        }

                        list.add(bB);
                        map.put(bA, list);
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return map;
    }

    // adds and filters calls by the number of nodes to a map of candidates
    public static boolean addCallsToCandidates(
            HashMap<CommonTree, ArrayList<CommonTree>> candidates,
            HashMap<CommonTree, ArrayList<CommonTree>> CBM_A,
            HashMap<CommonTree, String> BNM_A,
            HashMap<CommonTree, String> BNM_B) {
        if(candidates == null || CBM_A == null || BNM_A == null || BNM_B == null) {
            return false;
        }

        try {
            for(CommonTree call : CBM_A.keySet()) {
                CommonTree called = CBM_A.get(call).get(0);
                CommonTree callerA = CBM_A.get(call).get(1);

                String name = getNameFromBlockList(BNM_A, callerA);
                if(name == null) {
                    continue;
                }
                CommonTree callerB = findBlockByName(BNM_B, name);

                // nc = node count
                int ncC = countNodes(call); // the nodes from the call
                int ncD = countNodes(called); // the nodes inside the called method
                int ncA = countNodes(callerA); // the nodes inside the block with the call
                int ncB = countNodes(callerB); // the nodes inside the block with the same name as A

                //System.out.println("#3; (" + ncD + " + " + ncA + ") = (" + ncB + " + " + ncC + ") ?");

                // if the block with a call has a close number of nodes
                if(matchWithThreshold((ncA + ncD), (ncB + ncC))) {
                    ArrayList<CommonTree> list = new ArrayList<CommonTree>();
                    list.add(called);
                    list.add(callerA);
                    list.add(callerB);
                    candidates.put(call, list);
                }
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    // a candidate has: call node : [called node, calling node, node in other source with the same name as the calling node]
    public static boolean filterCandidatesByNodeType(HashMap<CommonTree, ArrayList<CommonTree>> candidates) {
        if(candidates == null) {
            return false;
        }

        HashMap<CommonTree, ArrayList<CommonTree>> candidatesClone = (HashMap<CommonTree, ArrayList<CommonTree>>) candidates.clone();

        try {
            for(CommonTree candidate : candidatesClone.keySet()) {
                ArrayList<CommonTree> blocks = candidatesClone.get(candidate);

                if(blocks.size() < 3) {
                    continue;
                }

                CommonTree called = blocks.get(0);
                CommonTree callerA = blocks.get(1);

                CommonTree callerB = blocks.get(2); // + call = called + callerA

                HashMap<Integer, Integer> ntcA = countNodesByType(callerA); // the types of the method with the call
                HashMap<Integer, Integer> ntcB = countNodesByType(callerB); // the types of the method without a call
                HashMap<Integer, Integer> ntcC = countNodesByType(candidate); // the types of the call
                HashMap<Integer, Integer> ntcD = countNodesByType(called); // the types of what was called

                /*
                // take a call out of the callers node count
                int callType = Spector.nexus.getType(TokenType.Call);
                if(ntcA.containsKey(callType)) { ntcA.put(callType, ntcA.get(callType) - 1); }

                int identifierType = Spector.nexus.getType(TokenType.Identifier);
                if(ntcA.containsKey(identifierType)) { ntcA.put(identifierType, ntcA.get(identifierType) - 1); }

                int argumentsType = Spector.nexus.getType(TokenType.Arguments);
                if(ntcA.containsKey(argumentsType)) { ntcA.put(argumentsType, ntcA.get(argumentsType) - 1); }
                */

                HashMap<Integer, Integer> a = addNodeCountMaps(ntcA, ntcD);
                HashMap<Integer, Integer> b = addNodeCountMaps(ntcA, ntcD);
                //System.out.println("#1:\n\t" + a +"\n\t" + b);
                //System.out.println("#1:\n\t(" + ntcA + " + " + ntcD + "\n\t (" + ntcB + " + " + ntcC + ")");

                // remove all keys that don't have an equal number of
                if(!matchWithThreshold(a, b)) {
                    candidates.remove(candidate);
                    break;
                }
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    // a candidate has: call node : [called node, calling node, node in other source with the same name as the calling node]
    public static boolean filterCandidatesByContent(HashMap<CommonTree, ArrayList<CommonTree>> candidates) {
        if(candidates == null) {
            return false;
        }

        HashMap<CommonTree, ArrayList<CommonTree>> candidatesClone = (HashMap<CommonTree, ArrayList<CommonTree>>) candidates.clone();

        try {
            for(CommonTree candidate : candidatesClone.keySet()) {
                ArrayList<CommonTree> blocks = candidatesClone.get(candidate);

                if(blocks.size() != 3) {
                    continue;
                }

                CommonTree called = blocks.get(0);
                ArrayList<CommonTree> calledChildren = (ArrayList<CommonTree>) called.getChildren();
                CommonTree callerA = blocks.get(1);
                ArrayList<CommonTree> callerAChildren = (ArrayList<CommonTree>) callerA.getChildren();
                CommonTree fakeA = new CommonTree();
                fakeA.addChildren(calledChildren);
                fakeA.addChildren(callerAChildren);

                //CommonTree call = candidate;
                CommonTree callerB = blocks.get(2); // + call = called + callerA
                ArrayList<CommonTree> callerBChildren = (ArrayList<CommonTree>) callerB.getChildren();
                CommonTree parent = (CommonTree) candidate.getParent();
                CommonTree fakeB = new CommonTree();
                fakeB.addChildren(callerBChildren);
                fakeB.addChild(parent);

                //System.out.println(fakeA.toStringTree() + "\n" + fakeB.toStringTree());

                if(matchByUnsortedContents(fakeA, fakeB)) {
                    candidates.remove(candidate);
                }
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    public static CommonTree getCondition(CommonTree parent) {
        if(parent == null) {
            return null;
        }

        try {
            ArrayList<CommonTree> children = (ArrayList<CommonTree>) parent.getChildren();

            for(CommonTree child : children) {
                if(Spector.nexus.inGroup(child.getType(), TokenGroup.Condition)) {
                    return child;
                }
            }

            return null;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(parent != null && parent.getText() != null) {
                System.out.print("(" + parent.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    public static CommonTree getParentCondition(CommonTree node) {
        if(node == null) {
            return null;
        }

        try {
            CommonTree parent = (CommonTree) node.getParent();

            // Parent is not root
            while(parent != null) {
                parent = (CommonTree) parent.getParent();
                List<CommonTree> children = (List<CommonTree>) parent.getChildren();

                if(children == null) {
                    break;
                }

                for(CommonTree child : children) {
                    if(Spector.nexus.inGroup(child.getType(), TokenGroup.Condition)) {
                        return child;
                    }
                }
            }

            return null;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(node != null) {
                System.out.print("(" + node.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    public static void filterByConditionCount(HashMap<CommonTree, ArrayList<CommonTree>> list) {
        try {
            HashMap<CommonTree, ArrayList<CommonTree>> copyList = (HashMap<CommonTree, ArrayList<CommonTree>>) list.clone();

            for(CommonTree a : copyList.keySet()) {
                ArrayList<CommonTree> values = copyList.get(a);
                ArrayList<CommonTree> copyValues = (ArrayList<CommonTree>) values.clone();

                for(CommonTree b : copyValues) {
                    CommonTree conditionA = getCondition(a);
                    CommonTree conditionB = getCondition(b);

                    if(conditionA == null || conditionB == null) {
                        return;
                    }

                    // node count
                    int ncA = countNodes(conditionA);
                    int ncB = countNodes(conditionB);

                    if(Spector.nexus.inGroup(a.getType(), TokenGroup.Case)) {
                        ncA += 2; // Add 2 nodes (Expression, ==)
                    }

                    if(Spector.nexus.inGroup(b.getType(), TokenGroup.Case)) {
                        ncB += 2; // Add 2 nodes(Expression, ==)
                    }

                    if(!matchWithThreshold(ncA, ncB)) {
                        values.remove(b);
                        if(values.isEmpty()) {
                            list.remove(a);
                        }
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }
    }

    public static void filterByConditionTypeCount(HashMap<CommonTree, ArrayList<CommonTree>> list) {
        try {
            HashMap<CommonTree, ArrayList<CommonTree>> copyList = (HashMap<CommonTree, ArrayList<CommonTree>>) list.clone();

            for(CommonTree a : copyList.keySet()) {
                if(a == null) {
                    continue;
                }

                ArrayList<CommonTree> values = copyList.get(a);
                ArrayList<CommonTree> copyValues = (ArrayList<CommonTree>) values.clone();

                for(CommonTree b : copyValues) {
                    if(b == null) {
                        continue;
                    }

                    CommonTree conditionA = getCondition(a);
                    CommonTree conditionB = getCondition(b);

                    if(conditionA == null || conditionB == null) {
                        return;
                    }

                    // node type count
                    HashMap<Integer, Integer> ntcA = countNodesByType(conditionA);
                    HashMap<Integer, Integer> ntcB = countNodesByType(conditionB);

                    if(Spector.nexus.inGroup(a.getType(), TokenGroup.Case)) {
                        CommonTree pcA = getParentCondition(a);
                        if(pcA == null) {
                            continue; // This shouldn't happen since a Case must be under a Switch
                        }

                        HashMap<Integer, Integer> subConditionA = countNodesByType(pcA);
                        for(Integer tA : subConditionA.keySet()) {
                            int extra = subConditionA.get(tA);
                            if(ntcA.containsKey(tA)) {
                                int value = ntcA.get(tA) + extra;
                                ntcA.put(tA, value);
                            } else {
                                ntcA.put(tA, extra);
                            }
                        }

                        int eqeq = Spector.nexus.getType(TokenType.Equals);
                        if(ntcA.containsKey(eqeq)) {
                            int value = ntcA.get(eqeq) + 1;
                            ntcA.put(eqeq, value);
                        } else {
                            ntcA.put(eqeq, 1);
                        }
                    }

                    if(Spector.nexus.inGroup(b.getType(), TokenGroup.Case)) {
                        CommonTree pcB = getParentCondition(b);
                        if(pcB == null) {
                            continue; // This shouldn't happen since a Case is under a Switch
                        }

                        HashMap<Integer, Integer> subConditionB = countNodesByType(pcB);
                        for(Integer tB : subConditionB.keySet()) {
                            int extra = subConditionB.get(tB);
                            if(ntcB.containsKey(tB)) {
                                int value = ntcB.get(tB) + extra;
                                ntcB.put(tB, value);
                            } else {
                                ntcB.put(tB, extra);
                            }
                        }

                        int eqeq = Spector.nexus.getType(TokenType.Equals);
                        if(ntcB.containsKey(eqeq)) {
                            int value = ntcB.get(eqeq) + 1;
                            ntcB.put(eqeq, value);
                        } else {
                            ntcB.put(eqeq, 1);
                        }
                    }

                    if(!matchWithThreshold(ntcA, ntcB)) {
                        values.remove(b);
                        if(values.isEmpty()) {
                            list.remove(a);
                        }
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }
    }

    public static void filterByConditionContents(HashMap<CommonTree, ArrayList<CommonTree>> list) {
        try {
            if(list == null) {
                return;
            }

            HashMap<CommonTree, ArrayList<CommonTree>> copyList = (HashMap<CommonTree, ArrayList<CommonTree>>) list.clone();

            for(CommonTree a : copyList.keySet()) {
                if(a == null || Spector.nexus.hasType(a.getType(), TokenType.Identifier)) {
                    continue;
                }

                ArrayList<CommonTree> values = copyList.get(a);
                ArrayList<CommonTree> copyValues = (ArrayList<CommonTree>) values.clone();

                for(CommonTree b : copyValues) {
                    if(b == null || Spector.nexus.hasType(b.getType(), TokenType.Identifier)) {
                        continue;
                    }

                    CommonTree conditionA = getCondition(a);
                    CommonTree conditionB = getCondition(b);

                    if(conditionA == null || conditionB == null) {
                        return;
                    }

                    CommonTree parentA = (CommonTree) conditionA.getParent();
                    CommonTree parentB = (CommonTree) conditionB.getParent();

                    int parentTypeA = parentA.getType();
                    int parentTypeB = parentB.getType();

                    if(Spector.nexus.inGroup(parentTypeA, TokenGroup.Case) || Spector.nexus.inGroup(parentTypeB, TokenGroup.Case)) {
                        if(!matchCaseExpressionContents(conditionA, conditionB)) {
                            values.remove(b);
                            if(values.isEmpty()) {
                                list.remove(a);
                            }
                        }
                    } else {
                        if(!matchExpressionContents(conditionA, conditionB)) {
                            values.remove(b);
                            if(values.isEmpty()) {
                                list.remove(a);
                            }
                        }
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }
    }

    public static boolean filterByExactContents(HashMap<CommonTree, ArrayList<CommonTree>> list) {
        try {
            if(list == null) {
                return false;
            }

            HashMap<CommonTree, ArrayList<CommonTree>> copyList = (HashMap<CommonTree, ArrayList<CommonTree>>) list.clone();

            for(CommonTree a : copyList.keySet()) {
                ArrayList<CommonTree> values = copyList.get(a);
                ArrayList<CommonTree> copyValues = (ArrayList<CommonTree>) values.clone();

                for(CommonTree b : copyValues) {
                    CommonTree conditionA = getCondition(a);
                    CommonTree conditionB = getCondition(b);

                    if(conditionA == null || conditionB == null) {
                        return false;
                    }

                    if(!matchNodesByExactContent(conditionA, conditionB)) {
                        values.remove(b);
                        if(values.isEmpty()) {
                            list.remove(a);
                        }
                    }
                }
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    public static HashMap<CommonTree, ArrayList<CommonTree>> getCallsAndBlocks(CommonTree node) {
        HashMap<CommonTree, ArrayList<CommonTree>> map = new HashMap<CommonTree, ArrayList<CommonTree>>();

        try {
            if(node == null) {
                return map;
            }

            int type = node.getType();
            boolean amCall = Spector.nexus.inGroup(type, TokenGroup.Call);
            boolean amBlock = Spector.nexus.inGroup(type, TokenGroup.Block);

            if(amCall || amBlock) {
                ArrayList<CommonTree> list = null;

                if(map.containsKey(node)) {
                    list = map.get(node);
                } else {
                    list = new ArrayList<CommonTree>();
                }

                // a call will have 2 blocks associated, where it is and what it is calling
                if(amCall) {
                    list.add((CommonTree) node.getParent());
                }

                map.put(node, list);
            }

            // check the children recursively
            List<CommonTree> children = (List<CommonTree>) node.getChildren();
            if(children == null) {
                return map;
            }

            for(CommonTree child : children) {
                if(child == null) {
                    continue;
                }

                HashMap<CommonTree, ArrayList<CommonTree>> childMap = getCallsAndBlocks(child);

                for(CommonTree childKey : childMap.keySet()) {
                    if(childKey == null) {
                        continue;
                    }

                    if(map.containsKey(childKey)) {
                        ArrayList<CommonTree> currentList = map.get(childKey);
                        ArrayList<CommonTree> childList = childMap.get(childKey);

                        for(CommonTree childNode : childList) {
                            if(childNode == null) {
                                continue;
                            }

                            if(!currentList.contains(childNode)) {
                                currentList.add(childNode);
                            }
                        }
                    } else {
                        map.put(childKey, childMap.get(childKey));
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return new HashMap<CommonTree, ArrayList<CommonTree>>();
        }

        return map;
    }

    public static String getNameFromBlockList(HashMap<CommonTree, String> blockList, CommonTree node) {
        if(blockList == null || blockList.isEmpty()) {
            return null;
        }

        try {
            for(CommonTree block : blockList.keySet()) {
                if(block.equals(node)) {
                    return blockList.get(block);
                }
            }

            return null;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(node != null) {
                System.out.print("(" + node.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    public static CommonTree findBlockByName(HashMap<CommonTree, String> blockList, String name) {
        if(blockList == null) {
            return null;
        }

        for(CommonTree block : blockList.keySet()) {
            if(block == null) {
                continue;
            }

            String blockName = blockList.get(block);

            if(blockName == null) {
                continue;
            }

            if(name.equals(blockName)) {
                return block;
            }
        }

        return null;
    }

    // matches too fake CommonTrees of nodes that are actually just a bunch of nodes
    public static boolean matchByUnsortedContents(CommonTree a, CommonTree b) {
        try {
            if(a == null || b == null) {
                if(a == null && b == null) {
                    return true;
                } else {
                    return false;
                }
            }

            //System.out.println("#1: " + a + " | " + b);

            int typeA = a.getType();
            int typeB = b.getType();

            if(typeA != typeB) {
                return false;
            }

            String textA = a.getText();
            String textB = b.getText();

            if(Spector.nexus.inGroup(typeA, TokenGroup.Sequence)) {
                textA = getFullSequence(a);
                textB = getFullSequence(b);

                if(textA.equals(textB)) {
                    return true;
                } else {
                    return false;
                }
            }

            if(textA == null || textB == null) {
                if(textA != null || textB != null) {
                    return false;
                }
            } else if(!textA.equals(textB)) {
                return false;
            }

            ArrayList<CommonTree> childrenA = (ArrayList<CommonTree>) a.getChildren();
            ArrayList<CommonTree> childrenB = (ArrayList<CommonTree>) b.getChildren();

            if(childrenA == null || childrenB == null) {
                if(childrenA == null && childrenB == null) {
                    return true;
                } else {
                    return false;
                }
            }

            if(childrenA.isEmpty() || childrenB.isEmpty()) {
                if(childrenA.isEmpty() && childrenB.isEmpty()) {
                    return true;
                } else {
                    return false;
                }
            }

            for(CommonTree childA : childrenA) {
                ArrayList<CommonTree> copyChildrenB = (ArrayList<CommonTree>) childrenB.clone();

                for(CommonTree childB : copyChildrenB) {
                    if(childA == null || childB == null) {
                        if(childA == null && childB == null) {
                            return true;
                        } else {
                            continue;
                        }
                    }

                    int childTypeA = childA.getType();
                    int childTypeB = childB.getType();

                    if(childTypeA != childTypeB) {
                        continue;
                    }

                    //System.out.println("#2: " + childA + " (" + childTypeA + ") = " + childB + " (" + childTypeB + ") ?");

                    String childTextA = childA.getText();
                    String childTextB = childB.getText();

                    if(Spector.nexus.inGroup(childTypeA, TokenGroup.Sequence)) {
                        childTextA = getFullSequence(childA);
                        childTextB = getFullSequence(childB);

                        if(childTextA.equals(childTextB)) {
                            //System.out.println("#3: " + textA + " = " + textB);
                            childrenB.remove(childB);
                            return true;
                        } else {
                            continue;
                        }
                    }

                    if(childTextA == null || childTextB == null) {
                        if(childTextA == null && childTextB == null) {
                            childrenB.remove(childB);
                        } else {
                            continue;
                        }
                    } else if(childTextA.equals(textB)) {
                        //System.out.println("#4: " + textA + " = " + textB);
                        childrenB.remove(childB);
                    }

                    matchByUnsortedContents(childA, childB);
                }
            }

            if(childrenB.isEmpty()) {
                return true;
            } else {
                //System.out.println("#9: " + childrenB);
                return false;
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(a != null && b != null) {
                System.out.print("(" + a.getText() + ", " + b.getText() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    // matches too fake CommonTrees of nodes that are actually just a bunch of nodes
    public static int matchByUnsortedContents(ArrayList<CommonTree> a, ArrayList<CommonTree> b) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return 1;
            } else {
                return 0;
            }
        }

        int matches = 0;

        try {
            ArrayList<CommonTree> copyB = (ArrayList<CommonTree>) b.clone();

            for(CommonTree childA : a) {
                if(childA == null) {
                    continue;
                }

                for(CommonTree childB : copyB) {
                    if(childB == null || !b.contains(childB)) {
                        continue;
                    }

                    int typeB = childB.getType();

                    String textA = childA.getText();
                    String textB = childB.getText();

                    if(textA == null || textB == null) {
                        /*
                        if(textA == null && textB == null) {
                            //matches += 1;
                            //b.remove(childB);
                        }
                        */
                        continue;
                    }

                    if(textA.equals(textB)) {
                        matches += 1;
                        b.remove(childB);
                    }

                    matchByUnsortedContents(childA, childB);
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0;
        }

        return matches;
    }

    /**
     * Maps the children of nodes of a type
     * @param   node    The node
     * @param   group   The group of target node types (will skip these)
     * @param   parent  The parent of the expression (for skipping purposes)
     * @return  A map that associates the children of target nodes to their occurrences
     */
    public static HashMap<String, ArrayList<CommonTree>> mapChildrenOfGroup(CommonTree node, TokenGroup group, boolean underGroup) {
        HashMap<String, ArrayList<CommonTree>> map = new HashMap<String, ArrayList<CommonTree>>();
        if(node == null) { return map; }

        try {
            int type = node.getType();
            String text = node.getText();

            // Is this node a sequence ?
            boolean amSequence = Spector.nexus.inGroup(type, TokenGroup.Sequence);
            if(amSequence) {
                text = getFullSequence(node);
            }

            boolean amCall = Spector.nexus.inGroup(type, TokenGroup.Call);
            if(amCall) {
                text = getIdentifier(node);
            }

            // Is this node in the Group ?
            boolean amTarget = Spector.nexus.inGroup(type, group);

            // If its a target, the following nodes are under the group and must be added
            if(amTarget) {
                underGroup = true;
            } else if(underGroup && !Spector.nexus.isIgnored(type)) {
                ArrayList<CommonTree> ocs = null;
                if(map.containsKey(text)) {
                    ocs = map.get(text);
                    ocs.add(node);
                } else {
                    ocs = new ArrayList<CommonTree>();
                    ocs.add(node);
                }
                map.put(text, ocs);

                amTarget = true;
            }

            if(!amSequence && node.getChildCount() > 0) {
                for(CommonTree child : (List<CommonTree>) node.getChildren()) {
                    HashMap<String, ArrayList<CommonTree>> childmap = null;

                    if(child == null) { continue; }
                    if(amCall) {
                        if(Spector.nexus.inGroup(child.getType(), TokenGroup.Identifier)) {
                            continue;
                        }
                    }

                    childmap = mapChildrenOfGroup(child, group, underGroup);
                    if(childmap != null) {
                        for(String key : childmap.keySet()) {
                            if(map.containsKey(key)) {
                                map.get(key).addAll(childmap.get(key));
                            } else {
                                map.put(key, childmap.get(key));
                            }
                        }
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return map;
    }

    public static HashMap<String, ArrayList<CommonTree>> mapChildrenOfGroup(CommonTree node, TokenGroup group) {
        return mapChildrenOfGroup(node, group, false);
    }

    public static HashMap<String, ArrayList<CommonTree>> mapChildrenOfGroup(CommonTree node) {
        return mapChildrenOfGroup(node, null, true);
    }

    public static boolean matchChildrenByContent(ArrayList<CommonTree> a, ArrayList<CommonTree> b) {
        if(a == null || b == null) {
            if(a == null && b == null) {
                return true;
            } else {
                return false;
            }
        }

        if(a.size() != b.size()) {
            return false;
        }

        try {
            CommonTree match = null;
            ArrayList<CommonTree> checkThis = (ArrayList<CommonTree>) b.clone();
            for(CommonTree nA : a) {
                match = null;

                for(CommonTree nB : checkThis) {
                    if(nA == null || nB == null) {
                        if(nA == null && nB == null) {
                            match = nB;
                            break;
                        }
                        continue;
                    }

                    int typeA = nA.getType();
                    int typeB = nB.getType();

                    if(typeA != typeB) {
                        continue;
                    }

                    if(Spector.nexus.isIgnored(typeA) || Spector.nexus.hasType(typeA, TokenType.Identifier)) {
                        continue;
                    }

                    String textA = nA.getText();
                    String textB = nB.getText();

                    if(textA == null || textB == null) {
                        if(textA == null && textB == null) {
                            match = nB;
                            break;
                        } else {
                            continue;
                        }
                    }

                }

                if(match != null) {
                    checkThis.remove(match);
                }
            }

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    public static CommonTree getNodeByGroup(CommonTree node, TokenGroup group) {
        if(node == null || group == null) { return null; }

        try {
            int type = node.getType();

            if(Spector.nexus.inGroup(type, group)) {
                // If this node is a target, we found what we were looking for
                return node;
            } else if(Spector.nexus.inGroup(type, TokenGroup.Sequence)) {
                // Is this node the start of a sequence ? (in which case we don't care about the children)
                return null;
            }

            if(node.getChildCount() > 0) {
                for(CommonTree child : (List<CommonTree>) node.getChildren()) {
                    CommonTree childNode = getNodeByGroup(child, group);
                    if(childNode != null) {
                        // This child is in the group
                        return childNode;
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");

            if(node == null) {
                System.out.print("null");
            } else {
                System.out.print(node);
            }

            System.out.print(", " + group.toString() + ") @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return null;
    }

    public static HashMap<CommonTree, CommonTree> mapNodeToNodeByGroups(CommonTree node, TokenGroup parentGroup, TokenGroup childGroup) {
        HashMap<CommonTree, CommonTree> map = new HashMap<CommonTree, CommonTree>();
        if(node == null || parentGroup == null || childGroup == null) { return map; }

        try {
            int type = node.getType();
            String text = node.getText();

            // If its a target, the following nodes are under the group and must be added
            if(Spector.nexus.inGroup(type, parentGroup)) {
                CommonTree child = getNodeByGroup(node, childGroup);

                if(map.containsKey(node)) {
                    // This should not happen
                    System.out.println("Warning: I found a duplicate Node (" + node + ")");
                } else if(child == null) {
                    // The child may be null if there are no childGroup nodes under this one
                } else {
                    map.put(node, child);
                }
            }

            if(node.getChildCount() > 0) {
                for(CommonTree child : (List<CommonTree>) node.getChildren()) {
                    HashMap<CommonTree, CommonTree> childmap = mapNodeToNodeByGroups(child, parentGroup, childGroup);
                    if(childmap != null) {
                        for(CommonTree key : childmap.keySet()) {
                            if(map.containsKey(key)) {
                                // This should not happen
                                System.out.println("Warning: I found a duplicate Node (" + node + ")");
                            } else {
                                map.put(key, childmap.get(key));
                            }
                        }
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName() + "(");

            if(node == null) {
                System.out.print("null");
            } else {
                System.out.print(node);
            }

            System.out.print(", " + parentGroup.toString() + ", " + childGroup.toString() + ") @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return map;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Checks if a Suspect (b) is an exact copy of another Suspect (a)
     * @param a The first Suspect
     * @param b The second Suspect
     * @return  1.0 (100%) if it is an exact copy or 0.0 (0%) otherwise
     */
    public static double isExactCopy(Suspect a, Suspect b) {
        String[] name = {null, null};
        if(a != null) { name[0] = a.getName(); }
        if(b != null) { name[1] = b.getName(); }

        double measure = 0.0;
        int step = 10;

        try {
            ///////////////////////////////////////////////////////////////////////////////////////////////////
            // #10)  Match the number of nodes,

            if(matchNodes(a,b,true)) { step++; }

            if(debug) {
                System.out.println("1.a: Have the same number of nodes ? " + (step == 11));
                System.out.println("\tcountNodes(" + name[0] + "): " + countNodes(a, true));
                System.out.println("\tcountNodes(" + name[1] + "): " + countNodes(b, true));
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////
            // #11)  Match the number of nodes of the same type,

            if(step == 11) {
                if(matchNodesByType(a,b)) { step++; }

                if(debug) {
                    System.out.println("1.b: Have the same number of nodes by type ? " + (step == 12));
                    System.out.println("\tcountNodesByType(" + name[0] + "): " + countNodesByType(a));
                    System.out.println("\tcountNodesByType(" + name[1] + "): " + countNodesByType(b));
                }
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////
            // #12) Match the contents of every node. +100%

            if(step == 12) {
                if(matchNodesByExactContent(a,b)) { step++; }
                if(debug) {
                    System.out.println("1.c: Have the same node contents ? " + (step == 13));
                }

                if(step == 13) { measure = 1.0; }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        return measure;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double haveSimilarIdentifiers(Suspect a, Suspect b) {
        String[] name = {null, null};
        if(a != null) { name[0] = a.getName(); }
        if(b != null) { name[1] = b.getName(); }

        double[] weight = {0.18, 0.42, 0.38, 0.02};
        double[] measure = new double[4];
        int step = 20;

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #20)  Map identifiers to their occurrences,

        HashMap<String, ArrayList<CommonTree>> IOM_A, IOM_B;
        double A;

        try {
            IOM_A = filterByGroup(a.getTree(), TokenGroup.Identifier);
            IOM_B = filterByGroup(b.getTree(), TokenGroup.Identifier);

            A = IOM_A.size();
            if(A < IOM_B.size()) {
                A = IOM_B.size();
            }

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        result.addIdentifierMaps(IOM_A, IOM_B);

        if(debug) {
            System.out.println("2.a:");

            System.out.print("\tIOM_A = ");
            printStringToNodeMap(IOM_A);

            System.out.print("\tIOM_B = ");
            printStringToNodeMap(IOM_B);

            System.out.println("\tA = " + A);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #21)  Match the number of occurrences,

        HashMap<String, ArrayList<String>> candidates = new HashMap<String,  ArrayList<String>>();
        double B;

        try {
            for(String ida : IOM_A.keySet()) {
                ArrayList<CommonTree> ocsa = IOM_A.get(ida);
                ArrayList<CommonTree> ocsb = null;

                for(String idb : IOM_B.keySet()) {
                    ocsb = IOM_B.get(idb);

                    if(matchWithThreshold(ocsa.size(), ocsb.size())) {
                        if(candidates.containsKey(ida)) {
                            candidates.get(ida).add(idb);
                        } else {
                            ArrayList<String> list = new ArrayList<String>();
                            list.add(idb);
                            candidates.put(ida, list);
                        }
                    }
                }
            }

            B = candidates.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[0] = safeDiv(B, A) * weight[0];

        if(debug) {
            System.out.println("2.b: (B / A) * 0.18: " + df.format(measure[0]));
            System.out.print("\tCandidates = ");
            printStringToStringMap(candidates);
            System.out.println("\tB = " + B);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #22)  Match the types of occurrence nodes,

        HashMap<String, ArrayList<String>> suspects = new HashMap<String, ArrayList<String>>();
        double C;

        try {
            for(String ca : candidates.keySet()) {
                for(String cb : candidates.get(ca)) {
                    if(matchNodesByType(IOM_A.get(ca), IOM_B.get(cb))) {
                        ArrayList<String> list = null;

                        if(suspects.keySet().contains(ca)) {
                            list = suspects.get(ca);
                            list.add(cb);
                        } else {
                            list = new ArrayList<String>();
                            list.add(cb);
                        }

                        suspects.put(ca, list);
                    }
                }
            }

            C = suspects.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[1] = safeDiv(C, B) * weight[1];

        if(debug) {
            System.out.println("2.c: (C / B) * " + weight[1] + " = " + df.format(measure[1]));
            System.out.print("\tSuspects = ");
            printStringToStringMap(suspects);
            System.out.println("\tC = " + C);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #23)  Match neighbours.

        HashMap<String, ArrayList<String>> equivalent = new HashMap<String, ArrayList<String>>();
        double D;

        try {
            for(String sa : suspects.keySet()) {
                ArrayList<CommonTree> ocsA = IOM_A.get(sa);
                if(ocsA == null) { continue; }

                for(String sb : suspects.get(sa)) {
                    ArrayList<CommonTree> ocsB = IOM_B.get(sb);
                    if(ocsB == null) { continue; }

                    if(matchSimilarNodes(sa, ocsA, sb, ocsB, true)) {
                        ArrayList<String> list = null;

                        if(equivalent.containsKey(sa)) {
                            list = equivalent.get(sa);
                        } else {
                            list = new ArrayList<String>();
                        }

                        list.add(sb);
                        equivalent.put(sa, list);
                    }
                }
            }

            D = equivalent.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        // Divide the total number of identifiers of the IOM and the Candidates maps to calculate a percentage measure
        measure[2] = safeDiv(D, C) * weight[2];

        result.addIdentifierEquivalences(equivalent);

        if(debug) {
            System.out.println("2.d: (D / C) * " + weight[2] + " = " + df.format(measure[2]));
            System.out.print("\tEquivalent = ");
            printStringToStringMap(equivalent);
            System.out.println("\tD = " + D);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #24)  Match contents.

        HashMap<String, ArrayList<String>> copies = new HashMap<String, ArrayList<String>>();
        double E;

        try {
            for(String ea : equivalent.keySet()) {
                for(String eb : equivalent.get(ea)) {
                    if(ea.equals(eb)) {
                        ArrayList<String> list = null;
                        if(copies.containsKey(ea)) {
                            list = copies.get(ea);
                        } else {
                            list = new ArrayList<String>();
                        }
                        list.add(eb);
                        copies.put(ea, list);
                    }
                }
            }

            E = copies.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[3] = safeDiv(E, D) * weight[3];

        result.addIdentifierCopies(copies);

        if(debug) {
            System.out.println("2.e: (E / D) * " + weight[3] + " = " + df.format(measure[3]));
            System.out.print("\tCopies = ");
            printStringToStringMap(copies);
            System.out.println("\tE = " + E);
        }

        return sumMeasures(measure);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double haveSimilarExpressions(Suspect a, Suspect b) {
        String[] name = {null, null};
        if(a != null) { name[0] = a.getName(); }
        if(b != null) { name[1] = b.getName(); }

        double[] weight = {0.18, 0.42, 0.38, 0.02};
        double[] measure = new double[4];
        int step = 30;

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #30) Map expressions to their occurrences,

        HashMap<String, ArrayList<CommonTree>> EOM_A, EOM_B;
        double A;

        try {
            EOM_A = mapChildrenOfGroup(a.getTree(), TokenGroup.Expression);
            EOM_B = mapChildrenOfGroup(b.getTree(), TokenGroup.Expression);

            // Calculate the highest number of expressions between the EOMs (A)
            A = EOM_A.size();
            if(A < EOM_B.size()) {
                A = EOM_B.size();
            }

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        result.addExpressionMaps(EOM_A, EOM_B);

        if(debug) {
            System.out.println("3.a:");

            System.out.print("\tEOM_A = ");
            printStringToNodeMap(EOM_A);

            System.out.print("\tEOM_B = ");
            printStringToNodeMap(EOM_B);

            System.out.println("\tA = " + A);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #31) Match the number of occurrences,

        HashMap<String, ArrayList<String>> candidates = new HashMap<String,  ArrayList<String>>();
        double B;

        try {
            for(String ida : EOM_A.keySet()) {
                ArrayList<CommonTree> ocsa = EOM_A.get(ida);
                ArrayList<CommonTree> ocsb = null;

                for(String idb : EOM_B.keySet()) {
                    ocsb = EOM_B.get(idb);

                    // Match the number of occurrences,
                    if(matchWithThreshold(ocsa.size(), ocsb.size())) {
                        if(candidates.containsKey(ida)) {
                            candidates.get(ida).add(idb);
                        } else {
                            ArrayList<String> list = new ArrayList<String>();
                            list.add(idb);
                            candidates.put(ida, list);
                        }
                    }
                }
            }

            B = candidates.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[0] = safeDiv(B, A) * weight[0];

        if(debug) {
            System.out.println("3.b: (B / A) * " + weight[0] + " = " + df.format(measure[0]));
            System.out.print("\tCandidates = ");
            printStringToStringMap(candidates);
            System.out.println("\tB = " + B);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #32) Match the types of occurrence nodes,

        HashMap<String, ArrayList<String>> suspects = new HashMap<String, ArrayList<String>>();
        double C;

        try {
            for(String ca : candidates.keySet()) {
                for(String cb : candidates.get(ca)) {
                    if(matchNodesByType(EOM_A.get(ca), EOM_B.get(cb))) {
                        ArrayList<String> list = null;

                        if(suspects.keySet().contains(ca)) {
                            list = suspects.get(ca);
                            list.add(cb);
                        } else {
                            list = new ArrayList<String>();
                            list.add(cb);
                        }

                        suspects.put(ca, list);
                    }
                }
            }

            C = suspects.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[1] = safeDiv(C, B) * weight[1];

        if(debug) {
            System.out.println("3.c: (C / B) * " + weight[1] + " = " + df.format(measure[1]));
            System.out.print("\tSuspects: ");
            printStringToStringMap(suspects);
            System.out.println("\tC = " + C);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #33) Match neighbors.

        HashMap<String, ArrayList<String>> equivalent = new HashMap<String, ArrayList<String>>();
        double D;

        try {
            for(String sa : suspects.keySet()) {
                if(sa == null) { continue; }

                for(String sb : suspects.get(sa)) {
                    if(sb == null) { continue; }

                    if(checkNodesForEquivalence(sa, EOM_A.get(sa), sb, EOM_B.get(sb))) {
                        ArrayList<String> list = null;

                        if(equivalent.containsKey(sa)) {
                            list = equivalent.get(sa);
                        }

                        if(list == null) {
                            list = new ArrayList<String>();
                        }

                        list.add(sb);
                        equivalent.put(sa, list);
                    }
                }
            }

            D = equivalent.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[2] = safeDiv(D, C) * weight[2];

        result.addExpressionEquivalences(equivalent);

        if(debug) {
            System.out.println("3.d: (D / C) * " + weight[2] + " = " + df.format(measure[2]));
            System.out.print("\tEquivalent = ");
            printStringToStringMap(equivalent);
            System.out.println("\tD = " + D);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #34) Match contents.

        HashMap<String, ArrayList<String>> copies = new HashMap<String, ArrayList<String>>();
        double E;

        try {
            for(String sa : equivalent.keySet()) {
                ArrayList<String> temp = equivalent.get(sa);
                for(String sb : temp) {
                    if(checkNodesForSimilarity(sa, EOM_A.get(sa), sb, EOM_B.get(sb))) {
                        ArrayList<String> list;
                        if(copies.containsKey(sa)) {
                            list = copies.get(sa);
                        } else {
                            list = new ArrayList<String>();
                        }
                        list.add(sb);
                        copies.put(sa, list);
                    }
                }
            }

            E = copies.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[3] = safeDiv(E, D) * weight[3];

        result.addExpressionCopies(copies);

        if(debug) {
            System.out.println("3.e: (E / D) * " + weight[3] + " = " + df.format(measure[3]));
            System.out.print("\tCopies = ");
            printStringToStringMap(copies);
            System.out.println("\tE = " + E);
        }

        return sumMeasures(measure);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double haveSimilarConditionals(Suspect a, Suspect b) {
        String[] name = {null, null};
        if(a != null) { name[0] = a.getName(); }
        if(b != null) { name[1] = b.getName(); }

        double[] weight = {0.18, 0.42, 0.38, 0.02};
        double[] measure = new double[4];
        int step = 40;

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #40) Map control structures to their conditions (CCM_A, CCM_B)

        HashMap<CommonTree, CommonTree> CCM_A, CCM_B;
        double A;

        try {
            CCM_A = mapNodeToNodeByGroups(a.getTree(), TokenGroup.Conditional, TokenGroup.Condition);
            CCM_B = mapNodeToNodeByGroups(b.getTree(), TokenGroup.Conditional, TokenGroup.Condition);

            A = CCM_A.size();
            if(CCM_B.size() > A) {
                A = CCM_B.size();
            }

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        result.addConditionalMaps(CCM_A, CCM_B);

        if(debug) {
            System.out.println("4.a:");

            System.out.print("\tCCM_A: ");
            printNodeToExpressionMap(CCM_A);

            System.out.print("\tCCM_B: ");
            printNodeToExpressionMap(CCM_B);

            System.out.println("\tA: " + A);
        }

        if(A == 0.0) {
            return 0.0;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #41) Match the number of nodes in the conditions,

        HashMap<CommonTree, ArrayList<CommonTree>> candidates;
        double B;

        try {
            candidates = mapNodeToNodeByGroup(CCM_A, CCM_B, TokenGroup.Conditional);
            filterByConditionCount(candidates);

            B = candidates.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[0] = safeDiv(B, A) * weight[0];

        if(debug) {
            System.out.println("4.b: (B / A) * " + weight[0] + " = " + df.format(measure[0]));
            System.out.println("\tCandidates = " + candidates);
            System.out.println("\tB = " + B);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #42) Match the number of node types in the conditions,

        HashMap<CommonTree, ArrayList<CommonTree>> suspects;
        double C;

        try {
            suspects = (HashMap<CommonTree, ArrayList<CommonTree>>) candidates.clone();
            filterByConditionTypeCount(suspects);

            C = suspects.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[1] = safeDiv(C, B) * weight[1];

        if(debug) {
            System.out.println("4.c: (C / B) * " + weight[1] + " = " + df.format(measure[1]));
            System.out.println("\tSuspects = " + suspects);
            System.out.println("\tC = " + C);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #43) Match condition contents without identifiers,

        HashMap<CommonTree, ArrayList<CommonTree>> equivalent;
        double D;

        try {
            equivalent = (HashMap<CommonTree, ArrayList<CommonTree>>) suspects.clone();
            filterByConditionContents(equivalent);

            D = equivalent.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[2] = safeDiv(D, C) * weight[2];

        result.addConditionalEquivalences(equivalent);

        if(debug) {
            System.out.println("4.d: (D / C) * " + weight[2] + " = " + df.format(measure[2]));
            System.out.println("\tEquivalent = " + equivalent);
            System.out.println("\tD = " + D);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #44) Match contents.

        HashMap<CommonTree, ArrayList<CommonTree>> copies;
        double E;

        try {
            copies = (HashMap<CommonTree, ArrayList<CommonTree>>) equivalent.clone();
            filterByExactContents(copies);

            E = copies.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[3] = safeDiv(E, D) * weight[3];

        result.addConditionalCopies(copies);

        if(debug) {
            System.out.println("4.e: (E / D) * " + weight[3] + " = " + df.format(measure[3]));
            System.out.println("\tCopies = " + copies);
            System.out.println("\tE = " + E);
        }

        return sumMeasures(measure);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static double haveSuspiciousCalls(Suspect a, Suspect b) {
        String[] name = {null, null};
        if(a != null) { name[0] = a.getName(); }
        if(b != null) { name[1] = b.getName(); }

        double[] weight = {0.18, 0.42, 0.38, 0.02};
        double[] measure = new double[4];
        int step = 30;

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #50) Map block nodes to their names and (full) children (BNM_1, BNM_2, BCM_1, BCM_2),

        HashMap<CommonTree, String> BNM_A, BNM_B; // Block-Name Map
        HashMap<CommonTree, ArrayList<CommonTree>> BCM_A, BCM_B; // Block-Children Map
        double A;

        try {
            BNM_A = getBlockNodes(a);
            BNM_B = getBlockNodes(b);

            BCM_A = mapBlocksToChildren(BNM_A);
            BCM_B = mapBlocksToChildren(BNM_B);

            A = BNM_A.size();
            if(BNM_B.size() > A) {
                A = BNM_B.size();
            }

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        result.addBlockMaps(BCM_A, BCM_B);

        if(debug) {
            System.out.println("5.a:");

            System.out.print("\tBNM_A: ");
            printBlockMap(BNM_A);

            System.out.print("\tBNM_B: ");
            printBlockMap(BNM_B);

            System.out.print("\tBCM_A: ");
            printMap(BCM_A);

            System.out.print("\tBCM_B: ");
            printMap(BCM_B);

            System.out.println("\tA: " + A);
        }

        if(A == 0.0) {
            return 0.0;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #51) Match the number of child nodes in the blocks,

        // map the blocks that have a similar number of nodes

        HashMap<CommonTree, ArrayList<CommonTree>> candidates = new HashMap<CommonTree, ArrayList<CommonTree>>();
        double B;

        try {
            for(CommonTree bA : BCM_A.keySet()) {
                for(CommonTree bB : BCM_B.keySet()) {
                    if(bA == null || bB == null) {
                        if(bA != null || bB != null) {
                            continue;
                        }
                    } else {
                        int cA = BCM_A.get(bA).size();
                        int cB = BCM_B.get(bB).size();

                        if(!matchWithThreshold(cA, cB)) {
                            continue;
                        }
                    }

                    ArrayList<CommonTree> matches = null;
                    if(candidates.containsKey(bA)) {
                        matches = candidates.get(bA);
                    } else {
                        matches = new ArrayList<CommonTree>();
                    }
                    matches.add(bB);
                    candidates.put(bA, matches);
                }
            }

            B = candidates.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[0] = safeDiv(B, A) * weight[0];

        if(debug) {
            System.out.println("5.b: (B / A) * " + weight[0] + " = " + df.format(measure[0]));
            System.out.print("\tCandidates: ");
            printBlockMap(candidates, BNM_A, BNM_B);
            System.out.println("\tB = " + B);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #52) Match the number of the blocks child nodes by type,

        HashMap<CommonTree, ArrayList<CommonTree>> suspects = new HashMap<CommonTree, ArrayList<CommonTree>>();
        double C;

        try {
            for(CommonTree bA : candidates.keySet()) {
                if(bA == null) { continue; }

                for(CommonTree bB : candidates.get(bA)) {
                    if(bB == null) { continue; }

                    if(!matchNodesByType(bA, bB, false, true)) {
                        continue;
                    }

                    ArrayList<CommonTree> list = null;
                    if(suspects.containsKey(bA)) {
                        list = suspects.get(bA);
                    } else {
                        list = new ArrayList<CommonTree>();
                    }
                    list.add(bB);
                    suspects.put(bA, list);

                }
            }

            C = suspects.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[1] = safeDiv(C, B) * weight[1];

        if(debug) {
            System.out.println("5.c: (C / B) * " + weight[1] + " = " + df.format(measure[1]));
            System.out.print("\tSuspects: ");
            printBlockMap(suspects, BNM_A, BNM_B);
            System.out.println("\tC = " + C);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #53) Check if the contents are equivalent

        HashMap<CommonTree, ArrayList<CommonTree>> equivalent = new HashMap<CommonTree, ArrayList<CommonTree>>();
        double D;

        try {
            for(CommonTree bA : suspects.keySet()) {
                if(bA == null) { continue; }

                for(CommonTree bB : suspects.get(bA)) {
                    if(bB == null) { continue; }

                    if(!matchChildrenByContent(BCM_A.get(bA), BCM_B.get(bB))) {
                        continue;
                    }

                    ArrayList<CommonTree> list = null;
                    if(equivalent.containsKey(bA)) {
                        list = equivalent.get(bA);
                    } else {
                        list = new ArrayList<CommonTree>();
                    }
                    list.add(bB);
                    equivalent.put(bA, list);

                }
            }

            D = equivalent.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[2] = safeDiv(D, C) * weight[2];

        result.addBlockEquivalences(equivalent);

        if(debug) {
            System.out.println("5.d: (D / C) * " + weight[2] + " = " + df.format(measure[2]));
            System.out.print("\tEquivalent: ");
            printBlockMap(equivalent, BNM_A, BNM_B);
            System.out.println("\tD = " + D);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // #54) Check if the calls have the same name

        HashMap<CommonTree, ArrayList<CommonTree>> copies = new HashMap<CommonTree, ArrayList<CommonTree>>();
        double E;

        try {
            for(CommonTree eA : equivalent.keySet()) {
                for(CommonTree eB : equivalent.get(eA)) {
                    if(eA == null || eB == null) {
                        if(eA != null || eB != null) {
                            continue;
                        }
                    }

                    String tA = BNM_A.get(eA);
                    String tB = BNM_B.get(eB);

                    if(tA == null || tB == null) {
                        if(tA != null || tB != null) {
                            continue;
                        }
                    } else if(!tA.equals(tB)) {
                        continue;
                    }

                    ArrayList<CommonTree> list = null;

                    if(copies.containsKey(eA)) {
                        list = copies.get(eA);
                    } else {
                        list = new ArrayList<CommonTree>();
                    }
                    list.add(eB);
                    copies.put(eA, list);
                }
            }

            E = copies.size();

            step++;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[0];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.print("(" + name[0] + ", " + name[1]+ ") # " + step + " @ " + ste.getLineNumber() + ": " + x.getMessage());

            return 0.0;
        }

        measure[3] = safeDiv(E, D) * weight[3];

        result.addBlockCopies(copies);

        if(debug) {
            System.out.println("5.e: (E / D) * " + weight[3] + " = " + df.format(measure[3]));
            System.out.print("\tCopies: ");
            printBlockMap(copies, BNM_A, BNM_B);
            System.out.println("\tE = " + E);
        }

        return sumMeasures(measure);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Method that, given 2 suspects, calculates a measure
     * @param   a   The first Suspect
     * @param   b   The second Suspect
     * @return  The measure value
     */
    public static Comparison compareSuspects(Suspect a, Suspect b) {
        if(debug) { System.out.println("Inspector.compareSuspects("+a.getFile().getName()+", "+b.getFile().getName()+"):"); }

        //Comparison comparison = new Comparison();

        /*
        double[] measure = new double[5];
        measure[0] = 0.0; // 0: Exact Copy
        measure[1] = 0.0; // 1: Identifiers changed
        measure[2] = 0.0; // 2: Operands order switched
        measure[3] = 0.0; // 3: Variable types and control structures replaced
        measure[4] = 0.0; // 4: Group of calls turned into a function call or vice versa
        */

        ArrayList<Double> measure = new ArrayList<Double>();
        measure.ensureCapacity(5);
        double finalMeasure = 0.0;

        result = new Comparison(a, b);

///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 1)   Exact Copy

        if(debug) { System.out.println(); }
        measure.add(0, isExactCopy(a, b));
        if(debug) { System.out.println(); }

        measure.set(0, measure.get(0) * 100.0);
        result.setMeasure(Comparison.ResultType.ExactCopy, measure.get(0));

        if(debug) {
            if(measure.get(0) > 0.0) {
                System.out.println("1: Are exact Copies ? Yes (100%)");

                // Stop the main algorithm with an instant 100%
                finalMeasure = measure.get(0);

                System.out.println();
                System.out.println("Final Measure = "+Math.round(finalMeasure)+" %");

                return result;
            } else {
                if(debug) { System.out.println("1: Are exact Copies ? No (0%)"); }
            }
        } else {
            if(measure.get(0) > 0.0) {
                finalMeasure = measure.get(0) * 100.0;
                return result;
            }
        }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 2)   Identifiers changed

        if(debug) { System.out.println(); }
        measure.add(1, haveSimilarIdentifiers(a, b));
        if(debug) { System.out.println(); }

        measure.set(1, measure.get(1) * 100.0);
        result.setMeasure(Comparison.ResultType.SimilarIdentifiers, measure.get(1));

        if(debug) {
            System.out.print("2: Have Similar Identifiers ?");
            if(measure.get(1) > 0.0) { System.out.print(" Yes "); }
            else { System.out.print(" No "); }
            System.out.println("(" + df.format(measure.get(1)) + "%)");
        }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 3) Operands order switched

        if(debug) { System.out.println(); }
        measure.add(2, haveSimilarExpressions(a, b));
        if(debug) { System.out.println(); }

        measure.set(2, measure.get(2) * 100.0);
        result.setMeasure(Comparison.ResultType.SimilarExpressions, measure.get(2));

        if(debug) {
            System.out.print("3: Have Similar Expressions ?");
            if(measure.get(2) > 0.0) { System.out.print(" Yes "); }
            else { System.out.print(" No "); }
            System.out.println("(" + df.format(measure.get(2)) + "%)");
        }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 4) Variable types and control structures replaced

        if(debug) { System.out.println(); }
        measure.add(3, haveSimilarConditionals(a, b));
        if(debug) { System.out.println(); }

        measure.set(3, measure.get(3) * 100.0);
        result.setMeasure(Comparison.ResultType.SimilarConditionals, measure.get(3));

        if(debug) {
            System.out.print("4: Have Similar Conditionals ?");
            if(measure.get(3) > 0.0) { System.out.print(" Yes "); }
            else { System.out.print(" No "); }
            System.out.println("(" + df.format(measure.get(3)) + "%)");
        }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 5) Group of calls turned into a function call or vice versa

        if(debug) { System.out.println(); }
        measure.add(4, haveSuspiciousCalls(a, b));
        if(debug) { System.out.println(); }

        measure.set(4, measure.get(4) * 100.0);
        result.setMeasure(Comparison.ResultType.SuspiciousCalls, measure.get(4));

        if(debug) {
            System.out.print("5: Have Suspicious Blocks ?");
            if(measure.get(4) > 0.0) { System.out.print(" Yes "); }
            else { System.out.print(" No "); }
            System.out.println("(" + df.format(measure.get(4)) + "%)");
        }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Calculate the Final Measure

        if(debug) {
            System.out.println();
            System.out.print("Final Measure = ");
        }

        double total = sumMeasures(measure);
        if(debug) { System.out.println(" = " + df.format(result.getTotal()) +"%"); }

        //result.setMeasures(measure); Unecessary as it already has them
        return result;
    }
}
