package spector;

/*
Copyright (c) 2015, Vítor T. Martins
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import java.util.ArrayList;
import java.util.regex.*;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;

import lang.Suspect;         // Objects that containin all the information related to a file
import lang.java.JavaNexus;  // The connection between a Java ANTLR grammar and Spector

/**
 * Spector (Source Inspector)
 * @author  Vítor T. Martins
 */
public class Spector {
    private final static boolean debug = false;  // Toggle debugging information for this module
    private final static String me = "Spector"; // Name of the application
    private final static String version = "1.1.6"; // Version of the application, number of major changes, number of minor changes

    public static JavaNexus nexus = new JavaNexus(); // Nexus used to parse files

    /*
        Input and Output settings
        input:  File, Submission (single choice)
        output: Summary, Html, Ast (any combination)
    */
    public static enum InputOption {File, Submission};
    public static enum OutputOption {Summary, Details, HTML, AST, Timing};
    public static enum SortOption {Alphabetic, Ascending, Descending};

    public static InputOption input = InputOption.File;
    public static ArrayList<OutputOption> output = new ArrayList<OutputOption>();
    private static SortOption order = SortOption.Descending;

    private static long[] timing = new long[7];
    private static boolean recurse = false, bidirectional = false;
    private static File place = null; // Output directory

    private static ArrayList<String> ipath = null;  // Path to each input file/directory
    private static ArrayList<File> include = null;  // Source Code files/directories

    //private static ArrayList<String> epath = null;  // Path to each exclude file/directory
    //private static ArrayList<File> exclude = null;  // Base code files/directories

    // A group of regular expressions, used to identify arguments
    public static class regex {
        public static String
            version   = "^-{0,2}(v|version)$",
            help      = "^-{0,2}(h|help)$",
            recurse = "^-{0,2}(r|recurse)$",
            input     = "^-{0,2}(n|input)$",
            output    = "^-{0,2}(u|output)$",
            order    = "^-{0,2}(o|order)$",
            place     = "^-{0,2}(p|place)$",
            //language  = "^-{0,2}(l|language)$",
            threshold = "^-{0,2}(t|threshold)$",
            inspect   = "^-{0,2}(i|inspect)$",
            //exclude   = "^-{0,2}(x|exclude)$",
            bidirectional = "^-{0,2}(b|bidirectional)$",

            // input parameters
            file = "^f|files?$",
            submission = "^s|submissions?$",

            // sorting parameters
            alphabetic = "l|alphabetic",
            ascending = "s|ascending",
            descending = "d|descending";
    };

    /**
     * Show the application's version and license
     */
    public static void version() {
        System.out.println("Spector (Source Inspector) "+version);
        System.out.println("Copyright (c) 2015, Vítor T. Martins");
        System.out.println("All rights reserved.");
        System.out.println();
        System.out.println("Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:");
        System.out.println();
        System.out.println("1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.");
        System.out.println("2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.");
        System.out.println();
        System.out.println("THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.");
    }

    /**
     * Show usage information
     */
    public static void help() {
        help(null);
    }

    public static void help(String option) {
        if(option == null || option.isEmpty()) {
            System.out.println("Usage: spector [OPTION]... [FILE]...");
            System.out.println("Source code plagiarism detector for Academic Environments.");
            System.out.println();
            System.out.println("  -v, --version    Show details about this application.");
            System.out.println("  -h, --help       Show this information."); // or information about a specific option
            System.out.println("  -r, --recurse    Recurse through input folders.");
            System.out.println("  -n, --input      Set the input type to an InputType.");
            System.out.println("  -u, --output     Set the output to an OutputType.");
            System.out.println("  -o, --order      Order of the results in the console/html output.");
            System.out.println("  -p, --place      Set the output folder.");
            //System.out.println("  -l, --language   Saves the intermediate ASTs generated by ANTLR.");
            System.out.println("  -t, --threshold  Set the comparisons similarity threshold.");
            System.out.println("  -i, --inspect    Inspect pairs of files/directories or submissions.");
            //System.out.println("  -x, --exclude    Consider the files as base code.");
            System.out.println();
            System.out.println("InputType:");
            System.out.println("  f/file");
            System.out.println("  s/submission");
            System.out.println();
            System.out.println("OutputType: Any combination of ...");
            System.out.println("  s   (summary)");
            System.out.println("  d   (details)");
            System.out.println("  h   (html)");
            System.out.println("  a   (ast)");
            System.out.println("  i   (timing)");
            System.out.println();
            System.out.println("Order:");
            System.out.println("  l/alphabetic");
            System.out.println("  s/ascending");
            System.out.println("  d/descending");
        } else {
            System.out.println("Warning@help(" + option + "): This feature hasn't been implemented.");
        }
    }

    /**
     * Attempts to match a string to a regular expression
     * @param regex pattern
     * @param input string
     * @return result
     */
    private static boolean matchPattern(String pattern, String input) {
        boolean result = false;

        try {
            // Parse the pattern string and be sure to ignore its case
            Pattern pat = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher mat = pat.matcher(input);
            if(mat.matches()) {
                result = true;
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println("(" + pattern + ", " + input + ") @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }

        if(debug && result == true) { System.out.println("matchPattern("+pattern+","+input+"): "+result); }
        return result;
    }

    private static boolean sortList(ArrayList<File> list) {
        try {
            Comparator<File> sorter = new Comparator<File>() {
                @Override
                public int compare(File a, File b) {
                    return a.getPath().compareTo(b.getPath());
                }
            };

            Collections.sort(list, sorter);

            return true;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return false;
        }
    }

    private static String diff2string(long start, long stop) {
        String output = "";

        double diff = (double) stop - start; // difference in milliseconds

        double seconds = diff / 1000.0;

        double minutes = 0.0;
        if(seconds >= 60.0) {
            minutes = Math.floor(seconds / 60);
        }

        double hours = 0.0;
        if(minutes >= 60.0) {
            hours = Math.floor(minutes / 60.0);
        }

        minutes -= (int) hours * 60.0;
        seconds -= (int) (minutes * 60.0) + (hours * 3600.0);

        if(hours > 0.0) {output += String.format("%.0fh", hours);}
        if(minutes > 0.0) {output += String.format("%.0fm", minutes);}
        output += String.format("%.3fs", seconds);

        return output;
    }

    // Get the next element in an array when it is big enough
    private static String getNext(int index, String[] array) {
        // If the array has more arguments
        if((index + 1) < array.length) {
            // Return the next element
            return array[index + 1];
        } else {
            return "";
        }
    }

    public static void main(String[] args) {
        int aid = 0;  // Argument Identifier
        int skip = 0; // Number of arguments to skip

        for(aid = 0; aid < args.length; ++aid) {
            String arg = args[aid];

            if (matchPattern(regex.version, arg)) {
                version();
                System.exit(0);
            } else if (matchPattern(regex.help, arg)) {
                //String next = getNext(aid, args);
                //help(next);
                help();
                System.exit(0);
            } else if(matchPattern(regex.recurse, arg)) {
                recurse = true;
            } else if (matchPattern(regex.input, arg)) {
                try {
                    String next = getNext(aid, args);
                    skip = 1;

                    if(next.isEmpty()) {
                        System.out.println(me+" < "+arg+": No input mode specified");
                        System.exit(1);
                    }

                    if(matchPattern(regex.file, next)) {
                        input = InputOption.File;
                    } else if(matchPattern(regex.submission, next)) {
                        input = InputOption.Submission;
                    } else {
                        System.out.println(me+" < "+arg+": Invalid input mode specified");
                        System.exit(1);
                    }

                    if(debug) { System.out.println(me+" < "+arg+": Input mode was set to '"+input.toString()+"'"); }
                } catch(Exception x) {
                    StackTraceElement[] trace = x.getStackTrace();
                    StackTraceElement ste = trace[trace.length - 1];

                    System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
                    System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

                    System.exit(3);
                }
            } else if (matchPattern(regex.output, arg)) {
                try {
                    String next = getNext(aid, args);
                    skip = 1;

                    if(next.isEmpty()) {
                        System.out.println(me+" < "+arg+": No output mode specified");
                        System.exit(0);
                    }

                    int value = 0;

                    if(next.contains("s") || next.contains("S")) {
                        output.add(OutputOption.Summary);
                    }

                    if(next.contains("d") || next.contains("D")) {
                        output.add(OutputOption.Details);
                    }

                    if(next.contains("h") || next.contains("H")) {
                        output.add(OutputOption.HTML);
                    }

                    if(next.contains("a") || next.contains("A")) {
                        output.add(OutputOption.AST);
                    }

                    if(next.contains("i") || next.contains("I")) {
                        output.add(OutputOption.Timing);
                    }

                    if(output.isEmpty()) {
                        System.out.println(me+" < "+arg+": Invalid output mode specified");
                        System.exit(1);
                    }

                    if(debug) { System.out.println(me+" < "+arg+": Output mode was set to '"+output.toString()+"'"); }
                } catch(Exception x) {
                    StackTraceElement[] trace = x.getStackTrace();
                    StackTraceElement ste = trace[trace.length - 1];

                    System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
                    System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

                    System.exit(3);
                }
            } else if(matchPattern(regex.order, arg)) {
                String next = "";
                try {
                    next = getNext(aid,args);
                    skip = 1;

                    if(next.isEmpty()) {
                        System.out.println(me+" < "+arg+": Invalid order specified");
                        System.exit(1);
                    }

                    if(matchPattern(regex.alphabetic, next)) {
                        order = SortOption.Alphabetic;
                    } else if(matchPattern(regex.ascending, next)) {
                        order = SortOption.Ascending;
                    } else if(matchPattern(regex.descending, next)) {
                        order = SortOption.Descending;
                    } else {
                        System.out.println(me+" < "+arg+": Invalid order specified");
                        System.exit(1);
                    }

                    if(debug) { System.out.println(me+" < "+arg+": Order was set to '"+order.toString()+"'"); }
                } catch(Exception x) {
                    StackTraceElement[] trace = x.getStackTrace();
                    StackTraceElement ste = trace[trace.length - 1];

                    System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
                    System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

                    System.exit(3);
                }
            } else if (matchPattern(regex.place, arg)) {
                String next = "";
                try {
                    next = getNext(aid, args);
                    skip = 1;

                    if(next.isEmpty()) {
                        System.out.println(me+" < "+arg+": Invalid output path specified");
                        System.exit(1);
                    }

                    File out = new File(next);
                    if(out != null && out.isDirectory()) {
                        place = out;
                    }
                } catch(Exception x) {
                    StackTraceElement[] trace = x.getStackTrace();
                    StackTraceElement ste = trace[trace.length - 1];

                    System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
                    System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

                    System.exit(3);
                }

                if(debug) {
                    String name = "";
                    if(place != null)
                    { name = place.getName(); }

                    if(name.isEmpty()) {
                        System.out.println(me+" < "+arg+": Invalid directory ("+next+").");
                    } else {
                        System.out.println(me+" < "+arg+": Output directory was set to '"+name+"'.");
                    }
                }
/*
            } else if (matchPattern(regex.language, arg)) {
*/
            } else if (matchPattern(regex.threshold, arg)) {
                String next = "";
                double threshold = 0.0;

                try {
                    next = getNext(aid, args);
                    skip = 1;

                    if(next.isEmpty()) {
                        System.out.println(me+" < "+arg+": No threshold specified");
                        System.exit(1);
                    }

                    // Parse the next argument (must be a double)
                    threshold = Double.parseDouble(next);
                    Inspector.setThreshold(threshold);
                } catch(java.lang.NumberFormatException x) {
                    System.out.println(me+" < "+arg+": Expected a number but got '"+next+"'");
                } catch(Exception x) {
                    StackTraceElement[] trace = x.getStackTrace();
                    StackTraceElement ste = trace[trace.length - 1];

                    System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
                    System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

                    System.exit(3);
                }

                if(debug && threshold != 0.0) { System.out.println(me+" < "+arg+": The threshold was set to "+threshold); }
            } else if (matchPattern(regex.inspect, arg)) {
                try {
                    ipath = new ArrayList<String>();

                    String next = "";
                    boolean valid = true;

                    while(valid) {
                        next = getNext(aid+skip, args);
                        if(next.isEmpty()) {
                            valid = false;
                            continue;
                        }

                        if(FileHandler.isValid(next)) {
                            ipath.add(next);
                        } else {
                            valid = false;
                            continue;
                        }

                        // Skip once per valid file, remember the +1 per cicle
                        skip += 1;
                    }

                    if(debug) { System.out.println(me+" < "+arg+": Inspect "+ipath.size()+" files"); }
                } catch(Exception x) {
                    StackTraceElement[] trace = x.getStackTrace();
                    StackTraceElement ste = trace[trace.length - 1];

                    System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
                    System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

                    System.exit(3);
                }
/*
            } else if (matchPattern(regex.exclude, arg)) {
*/
            } else if(matchPattern(regex.bidirectional, arg)) {
                bidirectional = true;
            } else {
                System.out.println(me+" < "+arg+": Unknown argument, use 'help' to see a list of valid arguments.");
                System.exit(1);
            }

            aid += skip;
            skip = 0;
        }

        if(aid == 0 || ipath == null || ipath.isEmpty()) {
            System.out.println(me+": No arguments specified, use 'help' to see a list of valid arguments.");
            System.exit(1);
        }

        timing[0] = new Date().getTime();

        // Fill the list of files to include
        try {
            include = new ArrayList<File>();
            for(String path : ipath) {
                if(FileHandler.isValid(path)) {
                    if(input == InputOption.File) {
                        include.addAll(FileHandler.listFiles(path, recurse));
                    } else if(input == InputOption.Submission) {
                        include.addAll(FileHandler.listSubmissions(path, recurse));
                    }
                }
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            System.exit(3);
        }

        // Remove any element that is not a file or has an invalid extension
        {
            ArrayList<File> temp = (ArrayList<File>) include.clone();
            for(File f : temp) {
                if(!f.isFile() || !nexus.validExtension(f.getName())) {
                    include.remove(f);
                }
            }
        }

        // sort list alphabeticly
        sortList(include);

        if(output.contains(OutputOption.Timing)) {
            timing[1] = new Date().getTime();
            System.out.println("Listing the input files took " + diff2string(timing[0], timing[1]));
        }

        /*
        System.out.print("[");
        for(File i : include) {
            System.out.print(i.getPath() + ", ");
        }
        System.out.println("]");
        */

        // Build the Suspect database
        ArrayList<Suspect> candidates = new ArrayList<Suspect>();
        try {
            for(File f : include) {
                candidates.add(nexus.parse(f));
            }
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            System.exit(3);
        }

        if(debug) { System.out.println(); }

        if(output.contains(OutputOption.Timing)) {
            timing[2] = new Date().getTime();
            System.out.println("Parsing and transforming input into ASTs took " + diff2string(timing[1], timing[2]));
        }

        // Generate ASTs, when appropriate
        if(output.contains(OutputOption.AST)) {
            try {
                ArrayList<Suspect> versus = (ArrayList<Suspect>) candidates.clone();
                if(debug) {  System.out.println("Building ASTs for " + candidates.size() + " files"); }
                for(Suspect a : candidates) {
                    if(a == null) {
                        continue;
                    }
                    String path = a.getFile().getPath();
                    path = FileHandler.cutExtension(path)+".dot";
                    //if(debug) { System.out.println("\t" + path); }
                    String content = nexus.toDOT(a);
                    FileHandler.saveFile(path, content);
                    FileHandler.produceDOT(path);
                }
            } catch(Exception x) {
                StackTraceElement[] trace = x.getStackTrace();
                StackTraceElement ste = trace[trace.length - 1];

                System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
                System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
            }
        }

        if(output.contains(OutputOption.Timing)) {
            timing[3] = new Date().getTime();
            if(output.contains(OutputOption.AST)) {
                System.out.println("Building visual ASTs took " + diff2string(timing[2], timing[3]));
            }
        }

        if(debug) { System.out.println(); }

        // Presenter that will have all the comparison results and measures
        Presenter out = new Presenter();

        if(place != null) {
            out.setOutput(place);
        } else {
            out.setOutput("results");
        }

        // Enable the debug setting if requested
        if(output.contains(OutputOption.Details)) {
            Inspector.setDebug(true);
        }

        // Compare each pair of Suspects/Submissions
        if(input.equals(InputOption.File)) {
            try {
                ArrayList<Suspect> versus = (ArrayList<Suspect>) candidates.clone();

                for(Suspect a : candidates) {
                    if(a.getFile() == null || !a.getFile().exists()) {
                        continue;
                    }

                    if(!bidirectional) {
                        versus.remove(0); // remove this element (the first)
                    }

                    for(Suspect b : versus) {
                        if(b.getFile() == null || !b.getFile().exists()) {
                            continue;
                        }

                        // ignore the own file
                        if(a.equals(b)) {
                            continue;
                        }

                        Comparison c = Inspector.compareSuspects(a, b);
                        out.addComparison(c);
                    }
                }
            } catch(Exception x) {
                StackTraceElement[] trace = x.getStackTrace();
                StackTraceElement ste = trace[trace.length - 1];

                System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
                System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

                System.exit(3);
            }
        } else if(input.equals(InputOption.Submission)) {
            try {
                HashMap<String, ArrayList<Suspect>> groups = new HashMap<String, ArrayList<Suspect>>();

                for(Suspect s : candidates) {
                    ArrayList<Suspect> list = null;
                    String name = s.getFile().getName();
                    if(groups.containsKey(name)) {
                        list = groups.get(name);
                    } else {
                        list = new ArrayList<Suspect>();
                    }
                    list.add(s);
                    groups.put(name, list);
                }

                for(String name : groups.keySet()) {
                    if(name == null) { continue; }
                    ArrayList<Suspect> suspects = (ArrayList<Suspect>) groups.get(name).clone();
                    if(suspects == null || suspects.isEmpty()) { continue; }
                    ArrayList<Suspect> versus = (ArrayList<Suspect>) suspects.clone();

                    for(Suspect a : suspects) {
                        if(a == null || a.getFile() == null || !a.getFile().exists()) {
                            continue;
                        }

                        if(!bidirectional) {
                            versus.remove(0); // remove the first element
                        }

                        for(Suspect b : versus) {
                            if(b == null || b.getFile() == null || !b.getFile().exists()) {
                                continue;
                            }

                            // ignore the own file
                            if(a.equals(b)) {
                                continue;
                            }

                            Comparison c = Inspector.compareSuspects(a, b);
                            out.addComparison(c);
                        }
                    }
                }
            } catch(Exception x) {
                StackTraceElement[] trace = x.getStackTrace();
                StackTraceElement ste = trace[trace.length - 1];

                System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
                System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

                System.exit(3);
            }
        }

        if(order == SortOption.Ascending) {
            out.sort(1);
        } else if(order == SortOption.Descending) {
            out.sort(-1);
        }

        if(output.contains(OutputOption.Timing)) {
            timing[4] = new Date().getTime();
            if(output.contains(OutputOption.Details)) {
                System.out.println("Comparing suspects while printing details took " + diff2string(timing[3], timing[4]));
            } else {
                System.out.println("Comparing suspects took " + diff2string(timing[3], timing[4]));
            }
        }

        if((output.contains(OutputOption.Details) && output.contains(OutputOption.Summary)) || debug) { System.out.println(); }

        // Generate HTML files, when appropriate
        if(output.contains(OutputOption.HTML)) {
            boolean success = out.buildHTML();
            if(debug) {
                if(success) {
                    System.out.println("Build Presentation : Success");
                } else {
                    System.out.println("Build Presentation : Failure");
                }

                System.out.println();
            }
        }

        if(output.contains(OutputOption.Timing)) {
            timing[5] = new Date().getTime();
            if(output.contains(OutputOption.HTML)) {
                System.out.println("Producing HTML results took " + diff2string(timing[4], timing[5]));
            }
        }

        // Show results in the console, when appropriate
        if(output.isEmpty() || output.contains(OutputOption.Summary)) {
            if(debug) {
                System.out.println("Print Results:");
            }

            //out.printSummary();
            out.printFullSummary();
            //out.printMatches();
        }

        if(output.contains(OutputOption.Timing)) {
            timing[6] = new Date().getTime();
            if(output.contains(OutputOption.Timing)) {
                System.out.println("Printing the summary took " + diff2string(timing[5], timing[6]));
            }
        }

        if(output.contains(OutputOption.Timing)) {
            System.out.println("The entire operation took " + diff2string(timing[0], timing[6]));
        }
    }
}
