// $ANTLR 3.5.2 Java.g 2015-04-20 21:30:50
package lang.java;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class JavaParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ABSTRACT", "AMP", "AMPAMP", "AMPEQ", 
		"ANNOTATION", "ARGUMENTS", "ASSERT", "BANG", "BANGEQ", "BAR", "BARBAR", 
		"BAREQ", "BLOCK", "BOOLEAN", "BREAK", "BYTE", "CALL", "CARET", "CARETEQ", 
		"CASE", "CATCH", "CHAR", "CHARLITERAL", "CLASS", "COLON", "COMMA", "COMMENT", 
		"CONDITION", "CONDITIONAL", "CONST", "CONTINUE", "DECLARATION", "DEFAULT", 
		"DO", "DOT", "DOUBLE", "DOUBLELITERAL", "DoubleSuffix", "ELLIPSIS", "ELSE", 
		"ENUM", "EQ", "EQEQ", "EXPRESSION", "EXTENDS", "EscapeSequence", "Exponent", 
		"FALSE", "FIELD", "FINAL", "FINALLY", "FLOAT", "FLOATLITERAL", "FOR", 
		"FloatSuffix", "GOTO", "GT", "GT2EQ", "GT3EQ", "GTEQ", "HexDigit", "HexPrefix", 
		"IDENTIFIER", "IF", "IMPLEMENTS", "IMPORT", "INSTANCEOF", "INT", "INTERFACE", 
		"INTLITERAL", "IdentifierPart", "IdentifierStart", "IntegerNumber", "LBRACE", 
		"LBRACKET", "LINE_COMMENT", "LONG", "LONGLITERAL", "LPAREN", "LT", "LT2EQ", 
		"LTEQ", "LongSuffix", "METHOD", "MODIFIERS", "MONKEYS_AT", "NATIVE", "NEW", 
		"NULL", "NonIntegerNumber", "PACKAGE", "PARAMETER", "PARAMETERS", "PERCENT", 
		"PERCENTEQ", "PLUS", "PLUSEQ", "PLUSPLUS", "PRIVATE", "PROTECTED", "PUBLIC", 
		"QUES", "RBRACE", "RBRACKET", "RETURN", "RPAREN", "SEMI", "SEQUENCE", 
		"SHORT", "SLASH", "SLASHEQ", "STAR", "STAREQ", "STATEMENT", "STATIC", 
		"STRICTFP", "STRINGLITERAL", "SUB", "SUBEQ", "SUBSUB", "SUPER", "SWITCH", 
		"SYNCHRONIZED", "SurrogateIdentifer", "THEN", "THIS", "THROW", "THROWS", 
		"TILDE", "TRANSIENT", "TRUE", "TRY", "TYPEPARAMETERS", "VOID", "VOLATILE", 
		"WHILE", "WS"
	};
	public static final int EOF=-1;
	public static final int ABSTRACT=4;
	public static final int AMP=5;
	public static final int AMPAMP=6;
	public static final int AMPEQ=7;
	public static final int ANNOTATION=8;
	public static final int ARGUMENTS=9;
	public static final int ASSERT=10;
	public static final int BANG=11;
	public static final int BANGEQ=12;
	public static final int BAR=13;
	public static final int BARBAR=14;
	public static final int BAREQ=15;
	public static final int BLOCK=16;
	public static final int BOOLEAN=17;
	public static final int BREAK=18;
	public static final int BYTE=19;
	public static final int CALL=20;
	public static final int CARET=21;
	public static final int CARETEQ=22;
	public static final int CASE=23;
	public static final int CATCH=24;
	public static final int CHAR=25;
	public static final int CHARLITERAL=26;
	public static final int CLASS=27;
	public static final int COLON=28;
	public static final int COMMA=29;
	public static final int COMMENT=30;
	public static final int CONDITION=31;
	public static final int CONDITIONAL=32;
	public static final int CONST=33;
	public static final int CONTINUE=34;
	public static final int DECLARATION=35;
	public static final int DEFAULT=36;
	public static final int DO=37;
	public static final int DOT=38;
	public static final int DOUBLE=39;
	public static final int DOUBLELITERAL=40;
	public static final int DoubleSuffix=41;
	public static final int ELLIPSIS=42;
	public static final int ELSE=43;
	public static final int ENUM=44;
	public static final int EQ=45;
	public static final int EQEQ=46;
	public static final int EXPRESSION=47;
	public static final int EXTENDS=48;
	public static final int EscapeSequence=49;
	public static final int Exponent=50;
	public static final int FALSE=51;
	public static final int FIELD=52;
	public static final int FINAL=53;
	public static final int FINALLY=54;
	public static final int FLOAT=55;
	public static final int FLOATLITERAL=56;
	public static final int FOR=57;
	public static final int FloatSuffix=58;
	public static final int GOTO=59;
	public static final int GT=60;
	public static final int GT2EQ=61;
	public static final int GT3EQ=62;
	public static final int GTEQ=63;
	public static final int HexDigit=64;
	public static final int HexPrefix=65;
	public static final int IDENTIFIER=66;
	public static final int IF=67;
	public static final int IMPLEMENTS=68;
	public static final int IMPORT=69;
	public static final int INSTANCEOF=70;
	public static final int INT=71;
	public static final int INTERFACE=72;
	public static final int INTLITERAL=73;
	public static final int IdentifierPart=74;
	public static final int IdentifierStart=75;
	public static final int IntegerNumber=76;
	public static final int LBRACE=77;
	public static final int LBRACKET=78;
	public static final int LINE_COMMENT=79;
	public static final int LONG=80;
	public static final int LONGLITERAL=81;
	public static final int LPAREN=82;
	public static final int LT=83;
	public static final int LT2EQ=84;
	public static final int LTEQ=85;
	public static final int LongSuffix=86;
	public static final int METHOD=87;
	public static final int MODIFIERS=88;
	public static final int MONKEYS_AT=89;
	public static final int NATIVE=90;
	public static final int NEW=91;
	public static final int NULL=92;
	public static final int NonIntegerNumber=93;
	public static final int PACKAGE=94;
	public static final int PARAMETER=95;
	public static final int PARAMETERS=96;
	public static final int PERCENT=97;
	public static final int PERCENTEQ=98;
	public static final int PLUS=99;
	public static final int PLUSEQ=100;
	public static final int PLUSPLUS=101;
	public static final int PRIVATE=102;
	public static final int PROTECTED=103;
	public static final int PUBLIC=104;
	public static final int QUES=105;
	public static final int RBRACE=106;
	public static final int RBRACKET=107;
	public static final int RETURN=108;
	public static final int RPAREN=109;
	public static final int SEMI=110;
	public static final int SEQUENCE=111;
	public static final int SHORT=112;
	public static final int SLASH=113;
	public static final int SLASHEQ=114;
	public static final int STAR=115;
	public static final int STAREQ=116;
	public static final int STATEMENT=117;
	public static final int STATIC=118;
	public static final int STRICTFP=119;
	public static final int STRINGLITERAL=120;
	public static final int SUB=121;
	public static final int SUBEQ=122;
	public static final int SUBSUB=123;
	public static final int SUPER=124;
	public static final int SWITCH=125;
	public static final int SYNCHRONIZED=126;
	public static final int SurrogateIdentifer=127;
	public static final int THEN=128;
	public static final int THIS=129;
	public static final int THROW=130;
	public static final int THROWS=131;
	public static final int TILDE=132;
	public static final int TRANSIENT=133;
	public static final int TRUE=134;
	public static final int TRY=135;
	public static final int TYPEPARAMETERS=136;
	public static final int VOID=137;
	public static final int VOLATILE=138;
	public static final int WHILE=139;
	public static final int WS=140;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public JavaParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public JavaParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
		this.state.ruleMemo = new HashMap[381+1];


	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return JavaParser.tokenNames; }
	@Override public String getGrammarFileName() { return "Java.g"; }


	public static class compilationUnit_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "compilationUnit"
	// Java.g:89:1: compilationUnit : ( packageDeclaration )? ( importDeclaration )* ( typeDeclaration )* -> ( packageDeclaration )? ( importDeclaration )* ( typeDeclaration )* ;
	public final JavaParser.compilationUnit_return compilationUnit() throws RecognitionException {
		JavaParser.compilationUnit_return retval = new JavaParser.compilationUnit_return();
		retval.start = input.LT(1);
		int compilationUnit_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope packageDeclaration1 =null;
		ParserRuleReturnScope importDeclaration2 =null;
		ParserRuleReturnScope typeDeclaration3 =null;

		RewriteRuleSubtreeStream stream_packageDeclaration=new RewriteRuleSubtreeStream(adaptor,"rule packageDeclaration");
		RewriteRuleSubtreeStream stream_typeDeclaration=new RewriteRuleSubtreeStream(adaptor,"rule typeDeclaration");
		RewriteRuleSubtreeStream stream_importDeclaration=new RewriteRuleSubtreeStream(adaptor,"rule importDeclaration");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return retval; }

			// Java.g:90:2: ( ( packageDeclaration )? ( importDeclaration )* ( typeDeclaration )* -> ( packageDeclaration )? ( importDeclaration )* ( typeDeclaration )* )
			// Java.g:90:4: ( packageDeclaration )? ( importDeclaration )* ( typeDeclaration )*
			{
			// Java.g:90:4: ( packageDeclaration )?
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==MONKEYS_AT) ) {
				int LA1_1 = input.LA(2);
				if ( (synpred1_Java()) ) {
					alt1=1;
				}
			}
			else if ( (LA1_0==PACKAGE) ) {
				alt1=1;
			}
			switch (alt1) {
				case 1 :
					// Java.g:90:4: packageDeclaration
					{
					pushFollow(FOLLOW_packageDeclaration_in_compilationUnit196);
					packageDeclaration1=packageDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_packageDeclaration.add(packageDeclaration1.getTree());
					}
					break;

			}

			// Java.g:91:3: ( importDeclaration )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( (LA2_0==IMPORT) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// Java.g:91:3: importDeclaration
					{
					pushFollow(FOLLOW_importDeclaration_in_compilationUnit201);
					importDeclaration2=importDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_importDeclaration.add(importDeclaration2.getTree());
					}
					break;

				default :
					break loop2;
				}
			}

			// Java.g:92:3: ( typeDeclaration )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0==ABSTRACT||LA3_0==BOOLEAN||LA3_0==BYTE||LA3_0==CHAR||LA3_0==CLASS||LA3_0==DOUBLE||LA3_0==ENUM||LA3_0==FINAL||LA3_0==FLOAT||LA3_0==IDENTIFIER||(LA3_0 >= INT && LA3_0 <= INTERFACE)||LA3_0==LONG||LA3_0==LT||(LA3_0 >= MONKEYS_AT && LA3_0 <= NATIVE)||(LA3_0 >= PRIVATE && LA3_0 <= PUBLIC)||LA3_0==SEMI||LA3_0==SHORT||(LA3_0 >= STATIC && LA3_0 <= STRICTFP)||LA3_0==SYNCHRONIZED||LA3_0==TRANSIENT||(LA3_0 >= VOID && LA3_0 <= VOLATILE)) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// Java.g:92:3: typeDeclaration
					{
					pushFollow(FOLLOW_typeDeclaration_in_compilationUnit206);
					typeDeclaration3=typeDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_typeDeclaration.add(typeDeclaration3.getTree());
					}
					break;

				default :
					break loop3;
				}
			}

			// AST REWRITE
			// elements: packageDeclaration, typeDeclaration, importDeclaration
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 93:2: -> ( packageDeclaration )? ( importDeclaration )* ( typeDeclaration )*
			{
				// Java.g:93:5: ( packageDeclaration )?
				if ( stream_packageDeclaration.hasNext() ) {
					adaptor.addChild(root_0, stream_packageDeclaration.nextTree());
				}
				stream_packageDeclaration.reset();

				// Java.g:93:25: ( importDeclaration )*
				while ( stream_importDeclaration.hasNext() ) {
					adaptor.addChild(root_0, stream_importDeclaration.nextTree());
				}
				stream_importDeclaration.reset();

				// Java.g:93:44: ( typeDeclaration )*
				while ( stream_typeDeclaration.hasNext() ) {
					adaptor.addChild(root_0, stream_typeDeclaration.nextTree());
				}
				stream_typeDeclaration.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 1, compilationUnit_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "compilationUnit"


	public static class packageDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "packageDeclaration"
	// Java.g:97:1: packageDeclaration : ( annotations )? a= 'package' b= qualifiedName ';' -> ^( PACKAGE[$a] $b) ;
	public final JavaParser.packageDeclaration_return packageDeclaration() throws RecognitionException {
		JavaParser.packageDeclaration_return retval = new JavaParser.packageDeclaration_return();
		retval.start = input.LT(1);
		int packageDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token char_literal5=null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope annotations4 =null;

		CommonTree a_tree=null;
		CommonTree char_literal5_tree=null;
		RewriteRuleTokenStream stream_PACKAGE=new RewriteRuleTokenStream(adaptor,"token PACKAGE");
		RewriteRuleTokenStream stream_SEMI=new RewriteRuleTokenStream(adaptor,"token SEMI");
		RewriteRuleSubtreeStream stream_qualifiedName=new RewriteRuleSubtreeStream(adaptor,"rule qualifiedName");
		RewriteRuleSubtreeStream stream_annotations=new RewriteRuleSubtreeStream(adaptor,"rule annotations");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return retval; }

			// Java.g:98:2: ( ( annotations )? a= 'package' b= qualifiedName ';' -> ^( PACKAGE[$a] $b) )
			// Java.g:98:4: ( annotations )? a= 'package' b= qualifiedName ';'
			{
			// Java.g:98:4: ( annotations )?
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==MONKEYS_AT) ) {
				alt4=1;
			}
			switch (alt4) {
				case 1 :
					// Java.g:98:4: annotations
					{
					pushFollow(FOLLOW_annotations_in_packageDeclaration231);
					annotations4=annotations();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_annotations.add(annotations4.getTree());
					}
					break;

			}

			a=(Token)match(input,PACKAGE,FOLLOW_PACKAGE_in_packageDeclaration238); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_PACKAGE.add(a);

			pushFollow(FOLLOW_qualifiedName_in_packageDeclaration244);
			b=qualifiedName();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_qualifiedName.add(b.getTree());
			char_literal5=(Token)match(input,SEMI,FOLLOW_SEMI_in_packageDeclaration248); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_SEMI.add(char_literal5);

			// AST REWRITE
			// elements: b
			// token labels: 
			// rule labels: retval, b
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 102:2: -> ^( PACKAGE[$a] $b)
			{
				// Java.g:102:5: ^( PACKAGE[$a] $b)
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(PACKAGE, a), root_1);
				adaptor.addChild(root_1, stream_b.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 2, packageDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "packageDeclaration"


	public static class importDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "importDeclaration"
	// Java.g:106:1: importDeclaration : a= 'import' (b= 'static' )? c= qualifiedImportName ';' -> ^( IMPORT[$a] $c) ;
	public final JavaParser.importDeclaration_return importDeclaration() throws RecognitionException {
		JavaParser.importDeclaration_return retval = new JavaParser.importDeclaration_return();
		retval.start = input.LT(1);
		int importDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token b=null;
		Token char_literal6=null;
		ParserRuleReturnScope c =null;

		CommonTree a_tree=null;
		CommonTree b_tree=null;
		CommonTree char_literal6_tree=null;
		RewriteRuleTokenStream stream_IMPORT=new RewriteRuleTokenStream(adaptor,"token IMPORT");
		RewriteRuleTokenStream stream_SEMI=new RewriteRuleTokenStream(adaptor,"token SEMI");
		RewriteRuleTokenStream stream_STATIC=new RewriteRuleTokenStream(adaptor,"token STATIC");
		RewriteRuleSubtreeStream stream_qualifiedImportName=new RewriteRuleSubtreeStream(adaptor,"rule qualifiedImportName");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return retval; }

			// Java.g:107:2: (a= 'import' (b= 'static' )? c= qualifiedImportName ';' -> ^( IMPORT[$a] $c) )
			// Java.g:107:4: a= 'import' (b= 'static' )? c= qualifiedImportName ';'
			{
			a=(Token)match(input,IMPORT,FOLLOW_IMPORT_in_importDeclaration273); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IMPORT.add(a);

			// Java.g:108:4: (b= 'static' )?
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==STATIC) ) {
				alt5=1;
			}
			switch (alt5) {
				case 1 :
					// Java.g:108:4: b= 'static'
					{
					b=(Token)match(input,STATIC,FOLLOW_STATIC_in_importDeclaration279); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_STATIC.add(b);

					}
					break;

			}

			pushFollow(FOLLOW_qualifiedImportName_in_importDeclaration286);
			c=qualifiedImportName();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_qualifiedImportName.add(c.getTree());
			char_literal6=(Token)match(input,SEMI,FOLLOW_SEMI_in_importDeclaration290); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_SEMI.add(char_literal6);

			// AST REWRITE
			// elements: c
			// token labels: 
			// rule labels: retval, c
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"rule c",c!=null?c.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 111:3: -> ^( IMPORT[$a] $c)
			{
				// Java.g:111:6: ^( IMPORT[$a] $c)
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(IMPORT, a), root_1);
				adaptor.addChild(root_1, stream_c.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 3, importDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "importDeclaration"


	public static class typeDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "typeDeclaration"
	// Java.g:115:1: typeDeclaration : ( classOrInterfaceDeclaration | ';' !);
	public final JavaParser.typeDeclaration_return typeDeclaration() throws RecognitionException {
		JavaParser.typeDeclaration_return retval = new JavaParser.typeDeclaration_return();
		retval.start = input.LT(1);
		int typeDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal8=null;
		ParserRuleReturnScope classOrInterfaceDeclaration7 =null;

		CommonTree char_literal8_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return retval; }

			// Java.g:116:2: ( classOrInterfaceDeclaration | ';' !)
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0==ABSTRACT||LA6_0==BOOLEAN||LA6_0==BYTE||LA6_0==CHAR||LA6_0==CLASS||LA6_0==DOUBLE||LA6_0==ENUM||LA6_0==FINAL||LA6_0==FLOAT||LA6_0==IDENTIFIER||(LA6_0 >= INT && LA6_0 <= INTERFACE)||LA6_0==LONG||LA6_0==LT||(LA6_0 >= MONKEYS_AT && LA6_0 <= NATIVE)||(LA6_0 >= PRIVATE && LA6_0 <= PUBLIC)||LA6_0==SHORT||(LA6_0 >= STATIC && LA6_0 <= STRICTFP)||LA6_0==SYNCHRONIZED||LA6_0==TRANSIENT||(LA6_0 >= VOID && LA6_0 <= VOLATILE)) ) {
				alt6=1;
			}
			else if ( (LA6_0==SEMI) ) {
				alt6=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}

			switch (alt6) {
				case 1 :
					// Java.g:116:4: classOrInterfaceDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_classOrInterfaceDeclaration_in_typeDeclaration316);
					classOrInterfaceDeclaration7=classOrInterfaceDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classOrInterfaceDeclaration7.getTree());

					}
					break;
				case 2 :
					// Java.g:117:4: ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal8=(Token)match(input,SEMI,FOLLOW_SEMI_in_typeDeclaration321); if (state.failed) return retval;
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 4, typeDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "typeDeclaration"


	public static class classOrInterfaceDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "classOrInterfaceDeclaration"
	// Java.g:120:1: classOrInterfaceDeclaration : ( classDeclaration | interfaceDeclaration );
	public final JavaParser.classOrInterfaceDeclaration_return classOrInterfaceDeclaration() throws RecognitionException {
		JavaParser.classOrInterfaceDeclaration_return retval = new JavaParser.classOrInterfaceDeclaration_return();
		retval.start = input.LT(1);
		int classOrInterfaceDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope classDeclaration9 =null;
		ParserRuleReturnScope interfaceDeclaration10 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return retval; }

			// Java.g:121:2: ( classDeclaration | interfaceDeclaration )
			int alt7=2;
			switch ( input.LA(1) ) {
			case MONKEYS_AT:
				{
				int LA7_1 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case PUBLIC:
				{
				int LA7_2 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case PROTECTED:
				{
				int LA7_3 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case PRIVATE:
				{
				int LA7_4 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case STATIC:
				{
				int LA7_5 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case ABSTRACT:
				{
				int LA7_6 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case FINAL:
				{
				int LA7_7 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case NATIVE:
				{
				int LA7_8 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case SYNCHRONIZED:
				{
				int LA7_9 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case TRANSIENT:
				{
				int LA7_10 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case VOLATILE:
				{
				int LA7_11 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case STRICTFP:
				{
				int LA7_12 = input.LA(2);
				if ( (synpred7_Java()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case CLASS:
			case ENUM:
				{
				alt7=1;
				}
				break;
			case INTERFACE:
				{
				alt7=2;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}
			switch (alt7) {
				case 1 :
					// Java.g:121:4: classDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_classDeclaration_in_classOrInterfaceDeclaration333);
					classDeclaration9=classDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classDeclaration9.getTree());

					}
					break;
				case 2 :
					// Java.g:122:4: interfaceDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_interfaceDeclaration_in_classOrInterfaceDeclaration338);
					interfaceDeclaration10=interfaceDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, interfaceDeclaration10.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 5, classOrInterfaceDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "classOrInterfaceDeclaration"


	public static class modifiers_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "modifiers"
	// Java.g:126:1: modifiers : ( annotation |mod+= 'public' |mod+= 'protected' |mod+= 'private' |mod+= 'static' |mod+= 'abstract' |mod+= 'final' |mod+= 'native' |mod+= 'synchronized' |mod+= 'transient' |mod+= 'volatile' |mod+= 'strictfp' )* -> ( ^( MODIFIERS ( $mod)* ) )? ;
	public final JavaParser.modifiers_return modifiers() throws RecognitionException {
		JavaParser.modifiers_return retval = new JavaParser.modifiers_return();
		retval.start = input.LT(1);
		int modifiers_StartIndex = input.index();

		CommonTree root_0 = null;

		Token mod=null;
		List<Object> list_mod=null;
		ParserRuleReturnScope annotation11 =null;

		CommonTree mod_tree=null;
		RewriteRuleTokenStream stream_NATIVE=new RewriteRuleTokenStream(adaptor,"token NATIVE");
		RewriteRuleTokenStream stream_PROTECTED=new RewriteRuleTokenStream(adaptor,"token PROTECTED");
		RewriteRuleTokenStream stream_FINAL=new RewriteRuleTokenStream(adaptor,"token FINAL");
		RewriteRuleTokenStream stream_SYNCHRONIZED=new RewriteRuleTokenStream(adaptor,"token SYNCHRONIZED");
		RewriteRuleTokenStream stream_ABSTRACT=new RewriteRuleTokenStream(adaptor,"token ABSTRACT");
		RewriteRuleTokenStream stream_VOLATILE=new RewriteRuleTokenStream(adaptor,"token VOLATILE");
		RewriteRuleTokenStream stream_PUBLIC=new RewriteRuleTokenStream(adaptor,"token PUBLIC");
		RewriteRuleTokenStream stream_PRIVATE=new RewriteRuleTokenStream(adaptor,"token PRIVATE");
		RewriteRuleTokenStream stream_STATIC=new RewriteRuleTokenStream(adaptor,"token STATIC");
		RewriteRuleTokenStream stream_TRANSIENT=new RewriteRuleTokenStream(adaptor,"token TRANSIENT");
		RewriteRuleTokenStream stream_STRICTFP=new RewriteRuleTokenStream(adaptor,"token STRICTFP");
		RewriteRuleSubtreeStream stream_annotation=new RewriteRuleSubtreeStream(adaptor,"rule annotation");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return retval; }

			// Java.g:127:2: ( ( annotation |mod+= 'public' |mod+= 'protected' |mod+= 'private' |mod+= 'static' |mod+= 'abstract' |mod+= 'final' |mod+= 'native' |mod+= 'synchronized' |mod+= 'transient' |mod+= 'volatile' |mod+= 'strictfp' )* -> ( ^( MODIFIERS ( $mod)* ) )? )
			// Java.g:127:3: ( annotation |mod+= 'public' |mod+= 'protected' |mod+= 'private' |mod+= 'static' |mod+= 'abstract' |mod+= 'final' |mod+= 'native' |mod+= 'synchronized' |mod+= 'transient' |mod+= 'volatile' |mod+= 'strictfp' )*
			{
			// Java.g:127:3: ( annotation |mod+= 'public' |mod+= 'protected' |mod+= 'private' |mod+= 'static' |mod+= 'abstract' |mod+= 'final' |mod+= 'native' |mod+= 'synchronized' |mod+= 'transient' |mod+= 'volatile' |mod+= 'strictfp' )*
			loop8:
			while (true) {
				int alt8=13;
				switch ( input.LA(1) ) {
				case MONKEYS_AT:
					{
					int LA8_2 = input.LA(2);
					if ( (LA8_2==IDENTIFIER) ) {
						alt8=1;
					}

					}
					break;
				case PUBLIC:
					{
					alt8=2;
					}
					break;
				case PROTECTED:
					{
					alt8=3;
					}
					break;
				case PRIVATE:
					{
					alt8=4;
					}
					break;
				case STATIC:
					{
					alt8=5;
					}
					break;
				case ABSTRACT:
					{
					alt8=6;
					}
					break;
				case FINAL:
					{
					alt8=7;
					}
					break;
				case NATIVE:
					{
					alt8=8;
					}
					break;
				case SYNCHRONIZED:
					{
					alt8=9;
					}
					break;
				case TRANSIENT:
					{
					alt8=10;
					}
					break;
				case VOLATILE:
					{
					alt8=11;
					}
					break;
				case STRICTFP:
					{
					alt8=12;
					}
					break;
				}
				switch (alt8) {
				case 1 :
					// Java.g:128:3: annotation
					{
					pushFollow(FOLLOW_annotation_in_modifiers353);
					annotation11=annotation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_annotation.add(annotation11.getTree());
					}
					break;
				case 2 :
					// Java.g:129:4: mod+= 'public'
					{
					mod=(Token)match(input,PUBLIC,FOLLOW_PUBLIC_in_modifiers360); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PUBLIC.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;
				case 3 :
					// Java.g:130:4: mod+= 'protected'
					{
					mod=(Token)match(input,PROTECTED,FOLLOW_PROTECTED_in_modifiers367); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PROTECTED.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;
				case 4 :
					// Java.g:131:4: mod+= 'private'
					{
					mod=(Token)match(input,PRIVATE,FOLLOW_PRIVATE_in_modifiers374); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PRIVATE.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;
				case 5 :
					// Java.g:132:4: mod+= 'static'
					{
					mod=(Token)match(input,STATIC,FOLLOW_STATIC_in_modifiers381); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_STATIC.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;
				case 6 :
					// Java.g:133:4: mod+= 'abstract'
					{
					mod=(Token)match(input,ABSTRACT,FOLLOW_ABSTRACT_in_modifiers388); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ABSTRACT.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;
				case 7 :
					// Java.g:134:4: mod+= 'final'
					{
					mod=(Token)match(input,FINAL,FOLLOW_FINAL_in_modifiers395); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_FINAL.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;
				case 8 :
					// Java.g:135:4: mod+= 'native'
					{
					mod=(Token)match(input,NATIVE,FOLLOW_NATIVE_in_modifiers402); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_NATIVE.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;
				case 9 :
					// Java.g:136:4: mod+= 'synchronized'
					{
					mod=(Token)match(input,SYNCHRONIZED,FOLLOW_SYNCHRONIZED_in_modifiers409); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SYNCHRONIZED.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;
				case 10 :
					// Java.g:137:4: mod+= 'transient'
					{
					mod=(Token)match(input,TRANSIENT,FOLLOW_TRANSIENT_in_modifiers416); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_TRANSIENT.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;
				case 11 :
					// Java.g:138:4: mod+= 'volatile'
					{
					mod=(Token)match(input,VOLATILE,FOLLOW_VOLATILE_in_modifiers423); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_VOLATILE.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;
				case 12 :
					// Java.g:139:4: mod+= 'strictfp'
					{
					mod=(Token)match(input,STRICTFP,FOLLOW_STRICTFP_in_modifiers430); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_STRICTFP.add(mod);

					if (list_mod==null) list_mod=new ArrayList<Object>();
					list_mod.add(mod);
					}
					break;

				default :
					break loop8;
				}
			}

			// AST REWRITE
			// elements: mod
			// token labels: 
			// rule labels: retval
			// token list labels: mod
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_mod=new RewriteRuleTokenStream(adaptor,"token mod", list_mod);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 141:2: -> ( ^( MODIFIERS ( $mod)* ) )?
			{
				// Java.g:141:5: ( ^( MODIFIERS ( $mod)* ) )?
				if ( stream_mod.hasNext() ) {
					// Java.g:141:5: ^( MODIFIERS ( $mod)* )
					{
					CommonTree root_1 = (CommonTree)adaptor.nil();
					root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(MODIFIERS, "MODIFIERS"), root_1);
					// Java.g:141:18: ( $mod)*
					while ( stream_mod.hasNext() ) {
						adaptor.addChild(root_1, stream_mod.nextNode());
					}
					stream_mod.reset();

					adaptor.addChild(root_0, root_1);
					}

				}
				stream_mod.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 6, modifiers_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "modifiers"


	public static class variableModifiers_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "variableModifiers"
	// Java.g:145:1: variableModifiers : ( 'final' | annotation )* ;
	public final JavaParser.variableModifiers_return variableModifiers() throws RecognitionException {
		JavaParser.variableModifiers_return retval = new JavaParser.variableModifiers_return();
		retval.start = input.LT(1);
		int variableModifiers_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal12=null;
		ParserRuleReturnScope annotation13 =null;

		CommonTree string_literal12_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return retval; }

			// Java.g:146:2: ( ( 'final' | annotation )* )
			// Java.g:146:4: ( 'final' | annotation )*
			{
			root_0 = (CommonTree)adaptor.nil();


			// Java.g:146:4: ( 'final' | annotation )*
			loop9:
			while (true) {
				int alt9=3;
				int LA9_0 = input.LA(1);
				if ( (LA9_0==FINAL) ) {
					alt9=1;
				}
				else if ( (LA9_0==MONKEYS_AT) ) {
					alt9=2;
				}

				switch (alt9) {
				case 1 :
					// Java.g:146:5: 'final'
					{
					string_literal12=(Token)match(input,FINAL,FOLLOW_FINAL_in_variableModifiers459); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal12_tree = (CommonTree)adaptor.create(string_literal12);
					adaptor.addChild(root_0, string_literal12_tree);
					}

					}
					break;
				case 2 :
					// Java.g:146:13: annotation
					{
					pushFollow(FOLLOW_annotation_in_variableModifiers461);
					annotation13=annotation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, annotation13.getTree());

					}
					break;

				default :
					break loop9;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 7, variableModifiers_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "variableModifiers"


	public static class classDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "classDeclaration"
	// Java.g:150:1: classDeclaration : ( normalClassDeclaration | enumDeclaration );
	public final JavaParser.classDeclaration_return classDeclaration() throws RecognitionException {
		JavaParser.classDeclaration_return retval = new JavaParser.classDeclaration_return();
		retval.start = input.LT(1);
		int classDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope normalClassDeclaration14 =null;
		ParserRuleReturnScope enumDeclaration15 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return retval; }

			// Java.g:151:2: ( normalClassDeclaration | enumDeclaration )
			int alt10=2;
			switch ( input.LA(1) ) {
			case MONKEYS_AT:
				{
				int LA10_1 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case PUBLIC:
				{
				int LA10_2 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case PROTECTED:
				{
				int LA10_3 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case PRIVATE:
				{
				int LA10_4 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case STATIC:
				{
				int LA10_5 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case ABSTRACT:
				{
				int LA10_6 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case FINAL:
				{
				int LA10_7 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case NATIVE:
				{
				int LA10_8 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case SYNCHRONIZED:
				{
				int LA10_9 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case TRANSIENT:
				{
				int LA10_10 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case VOLATILE:
				{
				int LA10_11 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case STRICTFP:
				{
				int LA10_12 = input.LA(2);
				if ( (synpred22_Java()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case CLASS:
				{
				alt10=1;
				}
				break;
			case ENUM:
				{
				alt10=2;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				throw nvae;
			}
			switch (alt10) {
				case 1 :
					// Java.g:151:4: normalClassDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_normalClassDeclaration_in_classDeclaration475);
					normalClassDeclaration14=normalClassDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, normalClassDeclaration14.getTree());

					}
					break;
				case 2 :
					// Java.g:152:4: enumDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_enumDeclaration_in_classDeclaration480);
					enumDeclaration15=enumDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, enumDeclaration15.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 8, classDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "classDeclaration"


	public static class normalClassDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "normalClassDeclaration"
	// Java.g:156:1: normalClassDeclaration : a= modifiers b= 'class' c= IDENTIFIER (d= typeParameters )? (e= 'extends' f= type )? (g= 'implements' h= typeList )? i= classBody -> ^( CLASS[$c] ( $d)? ( ^( $e $f) )? ( ^( $g $h) )? $i) ;
	public final JavaParser.normalClassDeclaration_return normalClassDeclaration() throws RecognitionException {
		JavaParser.normalClassDeclaration_return retval = new JavaParser.normalClassDeclaration_return();
		retval.start = input.LT(1);
		int normalClassDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token b=null;
		Token c=null;
		Token e=null;
		Token g=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope d =null;
		ParserRuleReturnScope f =null;
		ParserRuleReturnScope h =null;
		ParserRuleReturnScope i =null;

		CommonTree b_tree=null;
		CommonTree c_tree=null;
		CommonTree e_tree=null;
		CommonTree g_tree=null;
		RewriteRuleTokenStream stream_CLASS=new RewriteRuleTokenStream(adaptor,"token CLASS");
		RewriteRuleTokenStream stream_IMPLEMENTS=new RewriteRuleTokenStream(adaptor,"token IMPLEMENTS");
		RewriteRuleTokenStream stream_EXTENDS=new RewriteRuleTokenStream(adaptor,"token EXTENDS");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_typeParameters=new RewriteRuleSubtreeStream(adaptor,"rule typeParameters");
		RewriteRuleSubtreeStream stream_classBody=new RewriteRuleSubtreeStream(adaptor,"rule classBody");
		RewriteRuleSubtreeStream stream_modifiers=new RewriteRuleSubtreeStream(adaptor,"rule modifiers");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");
		RewriteRuleSubtreeStream stream_typeList=new RewriteRuleSubtreeStream(adaptor,"rule typeList");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return retval; }

			// Java.g:157:2: (a= modifiers b= 'class' c= IDENTIFIER (d= typeParameters )? (e= 'extends' f= type )? (g= 'implements' h= typeList )? i= classBody -> ^( CLASS[$c] ( $d)? ( ^( $e $f) )? ( ^( $g $h) )? $i) )
			// Java.g:157:4: a= modifiers b= 'class' c= IDENTIFIER (d= typeParameters )? (e= 'extends' f= type )? (g= 'implements' h= typeList )? i= classBody
			{
			pushFollow(FOLLOW_modifiers_in_normalClassDeclaration494);
			a=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_modifiers.add(a.getTree());
			b=(Token)match(input,CLASS,FOLLOW_CLASS_in_normalClassDeclaration500); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_CLASS.add(b);

			c=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_normalClassDeclaration506); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(c);

			// Java.g:160:4: (d= typeParameters )?
			int alt11=2;
			int LA11_0 = input.LA(1);
			if ( (LA11_0==LT) ) {
				alt11=1;
			}
			switch (alt11) {
				case 1 :
					// Java.g:160:4: d= typeParameters
					{
					pushFollow(FOLLOW_typeParameters_in_normalClassDeclaration512);
					d=typeParameters();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_typeParameters.add(d.getTree());
					}
					break;

			}

			// Java.g:161:3: (e= 'extends' f= type )?
			int alt12=2;
			int LA12_0 = input.LA(1);
			if ( (LA12_0==EXTENDS) ) {
				alt12=1;
			}
			switch (alt12) {
				case 1 :
					// Java.g:161:4: e= 'extends' f= type
					{
					e=(Token)match(input,EXTENDS,FOLLOW_EXTENDS_in_normalClassDeclaration520); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EXTENDS.add(e);

					pushFollow(FOLLOW_type_in_normalClassDeclaration524);
					f=type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_type.add(f.getTree());
					}
					break;

			}

			// Java.g:162:3: (g= 'implements' h= typeList )?
			int alt13=2;
			int LA13_0 = input.LA(1);
			if ( (LA13_0==IMPLEMENTS) ) {
				alt13=1;
			}
			switch (alt13) {
				case 1 :
					// Java.g:162:4: g= 'implements' h= typeList
					{
					g=(Token)match(input,IMPLEMENTS,FOLLOW_IMPLEMENTS_in_normalClassDeclaration533); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IMPLEMENTS.add(g);

					pushFollow(FOLLOW_typeList_in_normalClassDeclaration537);
					h=typeList();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_typeList.add(h.getTree());
					}
					break;

			}

			pushFollow(FOLLOW_classBody_in_normalClassDeclaration545);
			i=classBody();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_classBody.add(i.getTree());
			// AST REWRITE
			// elements: e, h, i, f, g, d
			// token labels: g, e
			// rule labels: f, retval, d, h, i
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_g=new RewriteRuleTokenStream(adaptor,"token g",g);
			RewriteRuleTokenStream stream_e=new RewriteRuleTokenStream(adaptor,"token e",e);
			RewriteRuleSubtreeStream stream_f=new RewriteRuleSubtreeStream(adaptor,"rule f",f!=null?f.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_d=new RewriteRuleSubtreeStream(adaptor,"rule d",d!=null?d.getTree():null);
			RewriteRuleSubtreeStream stream_h=new RewriteRuleSubtreeStream(adaptor,"rule h",h!=null?h.getTree():null);
			RewriteRuleSubtreeStream stream_i=new RewriteRuleSubtreeStream(adaptor,"rule i",i!=null?i.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 164:2: -> ^( CLASS[$c] ( $d)? ( ^( $e $f) )? ( ^( $g $h) )? $i)
			{
				// Java.g:164:5: ^( CLASS[$c] ( $d)? ( ^( $e $f) )? ( ^( $g $h) )? $i)
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CLASS, c), root_1);
				// Java.g:164:26: ( $d)?
				if ( stream_d.hasNext() ) {
					adaptor.addChild(root_1, stream_d.nextTree());
				}
				stream_d.reset();

				// Java.g:164:29: ( ^( $e $f) )?
				if ( stream_e.hasNext()||stream_f.hasNext() ) {
					// Java.g:164:29: ^( $e $f)
					{
					CommonTree root_2 = (CommonTree)adaptor.nil();
					root_2 = (CommonTree)adaptor.becomeRoot(stream_e.nextNode(), root_2);
					adaptor.addChild(root_2, stream_f.nextTree());
					adaptor.addChild(root_1, root_2);
					}

				}
				stream_e.reset();
				stream_f.reset();

				// Java.g:164:39: ( ^( $g $h) )?
				if ( stream_h.hasNext()||stream_g.hasNext() ) {
					// Java.g:164:39: ^( $g $h)
					{
					CommonTree root_2 = (CommonTree)adaptor.nil();
					root_2 = (CommonTree)adaptor.becomeRoot(stream_g.nextNode(), root_2);
					adaptor.addChild(root_2, stream_h.nextTree());
					adaptor.addChild(root_1, root_2);
					}

				}
				stream_h.reset();
				stream_g.reset();

				adaptor.addChild(root_1, stream_i.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 9, normalClassDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "normalClassDeclaration"


	public static class typeParameters_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "typeParameters"
	// Java.g:168:1: typeParameters : '<' typeParameter ( ',' typeParameter )* '>' -> ( ^( PARAMETERS ( typeParameter )+ ) )? ;
	public final JavaParser.typeParameters_return typeParameters() throws RecognitionException {
		JavaParser.typeParameters_return retval = new JavaParser.typeParameters_return();
		retval.start = input.LT(1);
		int typeParameters_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal16=null;
		Token char_literal18=null;
		Token char_literal20=null;
		ParserRuleReturnScope typeParameter17 =null;
		ParserRuleReturnScope typeParameter19 =null;

		CommonTree char_literal16_tree=null;
		CommonTree char_literal18_tree=null;
		CommonTree char_literal20_tree=null;
		RewriteRuleTokenStream stream_GT=new RewriteRuleTokenStream(adaptor,"token GT");
		RewriteRuleTokenStream stream_LT=new RewriteRuleTokenStream(adaptor,"token LT");
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_typeParameter=new RewriteRuleSubtreeStream(adaptor,"rule typeParameter");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return retval; }

			// Java.g:169:2: ( '<' typeParameter ( ',' typeParameter )* '>' -> ( ^( PARAMETERS ( typeParameter )+ ) )? )
			// Java.g:169:4: '<' typeParameter ( ',' typeParameter )* '>'
			{
			char_literal16=(Token)match(input,LT,FOLLOW_LT_in_typeParameters592); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LT.add(char_literal16);

			pushFollow(FOLLOW_typeParameter_in_typeParameters596);
			typeParameter17=typeParameter();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_typeParameter.add(typeParameter17.getTree());
			// Java.g:171:3: ( ',' typeParameter )*
			loop14:
			while (true) {
				int alt14=2;
				int LA14_0 = input.LA(1);
				if ( (LA14_0==COMMA) ) {
					alt14=1;
				}

				switch (alt14) {
				case 1 :
					// Java.g:171:4: ',' typeParameter
					{
					char_literal18=(Token)match(input,COMMA,FOLLOW_COMMA_in_typeParameters601); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(char_literal18);

					pushFollow(FOLLOW_typeParameter_in_typeParameters603);
					typeParameter19=typeParameter();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_typeParameter.add(typeParameter19.getTree());
					}
					break;

				default :
					break loop14;
				}
			}

			char_literal20=(Token)match(input,GT,FOLLOW_GT_in_typeParameters609); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_GT.add(char_literal20);

			// AST REWRITE
			// elements: typeParameter
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 173:2: -> ( ^( PARAMETERS ( typeParameter )+ ) )?
			{
				// Java.g:173:5: ( ^( PARAMETERS ( typeParameter )+ ) )?
				if ( stream_typeParameter.hasNext() ) {
					// Java.g:173:5: ^( PARAMETERS ( typeParameter )+ )
					{
					CommonTree root_1 = (CommonTree)adaptor.nil();
					root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(PARAMETERS, "PARAMETERS"), root_1);
					if ( !(stream_typeParameter.hasNext()) ) {
						throw new RewriteEarlyExitException();
					}
					while ( stream_typeParameter.hasNext() ) {
						adaptor.addChild(root_1, stream_typeParameter.nextTree());
					}
					stream_typeParameter.reset();

					adaptor.addChild(root_0, root_1);
					}

				}
				stream_typeParameter.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 10, typeParameters_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "typeParameters"


	public static class typeParameter_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "typeParameter"
	// Java.g:177:1: typeParameter : a= IDENTIFIER (b= 'extends' c= typeBound )? -> ^( PARAMETER $a ( ^( $b $c) )? ) ;
	public final JavaParser.typeParameter_return typeParameter() throws RecognitionException {
		JavaParser.typeParameter_return retval = new JavaParser.typeParameter_return();
		retval.start = input.LT(1);
		int typeParameter_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token b=null;
		ParserRuleReturnScope c =null;

		CommonTree a_tree=null;
		CommonTree b_tree=null;
		RewriteRuleTokenStream stream_EXTENDS=new RewriteRuleTokenStream(adaptor,"token EXTENDS");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_typeBound=new RewriteRuleSubtreeStream(adaptor,"rule typeBound");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return retval; }

			// Java.g:178:2: (a= IDENTIFIER (b= 'extends' c= typeBound )? -> ^( PARAMETER $a ( ^( $b $c) )? ) )
			// Java.g:178:4: a= IDENTIFIER (b= 'extends' c= typeBound )?
			{
			a=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_typeParameter634); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(a);

			// Java.g:179:3: (b= 'extends' c= typeBound )?
			int alt15=2;
			int LA15_0 = input.LA(1);
			if ( (LA15_0==EXTENDS) ) {
				alt15=1;
			}
			switch (alt15) {
				case 1 :
					// Java.g:179:4: b= 'extends' c= typeBound
					{
					b=(Token)match(input,EXTENDS,FOLLOW_EXTENDS_in_typeParameter641); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EXTENDS.add(b);

					pushFollow(FOLLOW_typeBound_in_typeParameter645);
					c=typeBound();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_typeBound.add(c.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: c, b, a
			// token labels: b, a
			// rule labels: retval, c
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_b=new RewriteRuleTokenStream(adaptor,"token b",b);
			RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"rule c",c!=null?c.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 180:2: -> ^( PARAMETER $a ( ^( $b $c) )? )
			{
				// Java.g:180:5: ^( PARAMETER $a ( ^( $b $c) )? )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(PARAMETER, "PARAMETER"), root_1);
				adaptor.addChild(root_1, stream_a.nextNode());
				// Java.g:180:20: ( ^( $b $c) )?
				if ( stream_c.hasNext()||stream_b.hasNext() ) {
					// Java.g:180:20: ^( $b $c)
					{
					CommonTree root_2 = (CommonTree)adaptor.nil();
					root_2 = (CommonTree)adaptor.becomeRoot(stream_b.nextNode(), root_2);
					adaptor.addChild(root_2, stream_c.nextTree());
					adaptor.addChild(root_1, root_2);
					}

				}
				stream_c.reset();
				stream_b.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 11, typeParameter_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "typeParameter"


	public static class typeBound_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "typeBound"
	// Java.g:184:1: typeBound : type ( '&' type )* -> ( type )+ ;
	public final JavaParser.typeBound_return typeBound() throws RecognitionException {
		JavaParser.typeBound_return retval = new JavaParser.typeBound_return();
		retval.start = input.LT(1);
		int typeBound_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal22=null;
		ParserRuleReturnScope type21 =null;
		ParserRuleReturnScope type23 =null;

		CommonTree char_literal22_tree=null;
		RewriteRuleTokenStream stream_AMP=new RewriteRuleTokenStream(adaptor,"token AMP");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return retval; }

			// Java.g:185:2: ( type ( '&' type )* -> ( type )+ )
			// Java.g:185:4: type ( '&' type )*
			{
			pushFollow(FOLLOW_type_in_typeBound678);
			type21=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_type.add(type21.getTree());
			// Java.g:185:9: ( '&' type )*
			loop16:
			while (true) {
				int alt16=2;
				int LA16_0 = input.LA(1);
				if ( (LA16_0==AMP) ) {
					alt16=1;
				}

				switch (alt16) {
				case 1 :
					// Java.g:185:10: '&' type
					{
					char_literal22=(Token)match(input,AMP,FOLLOW_AMP_in_typeBound681); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_AMP.add(char_literal22);

					pushFollow(FOLLOW_type_in_typeBound683);
					type23=type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_type.add(type23.getTree());
					}
					break;

				default :
					break loop16;
				}
			}

			// AST REWRITE
			// elements: type
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 186:2: -> ( type )+
			{
				if ( !(stream_type.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_type.hasNext() ) {
					adaptor.addChild(root_0, stream_type.nextTree());
				}
				stream_type.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 12, typeBound_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "typeBound"


	public static class enumDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "enumDeclaration"
	// Java.g:190:1: enumDeclaration : a= modifiers b= 'enum' c= IDENTIFIER (d= 'implements' e= typeList )? f= enumBody -> ^( $b $c $f) ;
	public final JavaParser.enumDeclaration_return enumDeclaration() throws RecognitionException {
		JavaParser.enumDeclaration_return retval = new JavaParser.enumDeclaration_return();
		retval.start = input.LT(1);
		int enumDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token b=null;
		Token c=null;
		Token d=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope e =null;
		ParserRuleReturnScope f =null;

		CommonTree b_tree=null;
		CommonTree c_tree=null;
		CommonTree d_tree=null;
		RewriteRuleTokenStream stream_ENUM=new RewriteRuleTokenStream(adaptor,"token ENUM");
		RewriteRuleTokenStream stream_IMPLEMENTS=new RewriteRuleTokenStream(adaptor,"token IMPLEMENTS");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_enumBody=new RewriteRuleSubtreeStream(adaptor,"rule enumBody");
		RewriteRuleSubtreeStream stream_modifiers=new RewriteRuleSubtreeStream(adaptor,"rule modifiers");
		RewriteRuleSubtreeStream stream_typeList=new RewriteRuleSubtreeStream(adaptor,"rule typeList");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return retval; }

			// Java.g:191:2: (a= modifiers b= 'enum' c= IDENTIFIER (d= 'implements' e= typeList )? f= enumBody -> ^( $b $c $f) )
			// Java.g:191:4: a= modifiers b= 'enum' c= IDENTIFIER (d= 'implements' e= typeList )? f= enumBody
			{
			pushFollow(FOLLOW_modifiers_in_enumDeclaration705);
			a=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_modifiers.add(a.getTree());
			b=(Token)match(input,ENUM,FOLLOW_ENUM_in_enumDeclaration711); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_ENUM.add(b);

			c=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_enumDeclaration717); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(c);

			// Java.g:194:3: (d= 'implements' e= typeList )?
			int alt17=2;
			int LA17_0 = input.LA(1);
			if ( (LA17_0==IMPLEMENTS) ) {
				alt17=1;
			}
			switch (alt17) {
				case 1 :
					// Java.g:194:4: d= 'implements' e= typeList
					{
					d=(Token)match(input,IMPLEMENTS,FOLLOW_IMPLEMENTS_in_enumDeclaration724); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IMPLEMENTS.add(d);

					pushFollow(FOLLOW_typeList_in_enumDeclaration728);
					e=typeList();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_typeList.add(e.getTree());
					}
					break;

			}

			pushFollow(FOLLOW_enumBody_in_enumDeclaration736);
			f=enumBody();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_enumBody.add(f.getTree());
			// AST REWRITE
			// elements: b, f, c
			// token labels: b, c
			// rule labels: f, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_b=new RewriteRuleTokenStream(adaptor,"token b",b);
			RewriteRuleTokenStream stream_c=new RewriteRuleTokenStream(adaptor,"token c",c);
			RewriteRuleSubtreeStream stream_f=new RewriteRuleSubtreeStream(adaptor,"rule f",f!=null?f.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 196:2: -> ^( $b $c $f)
			{
				// Java.g:196:5: ^( $b $c $f)
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot(stream_b.nextNode(), root_1);
				adaptor.addChild(root_1, stream_c.nextNode());
				adaptor.addChild(root_1, stream_f.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 13, enumDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "enumDeclaration"


	public static class enumBody_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "enumBody"
	// Java.g:200:1: enumBody : '{' ( enumConstants )? ( ',' )? ( enumBodyDeclarations )? '}' ;
	public final JavaParser.enumBody_return enumBody() throws RecognitionException {
		JavaParser.enumBody_return retval = new JavaParser.enumBody_return();
		retval.start = input.LT(1);
		int enumBody_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal24=null;
		Token char_literal26=null;
		Token char_literal28=null;
		ParserRuleReturnScope enumConstants25 =null;
		ParserRuleReturnScope enumBodyDeclarations27 =null;

		CommonTree char_literal24_tree=null;
		CommonTree char_literal26_tree=null;
		CommonTree char_literal28_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return retval; }

			// Java.g:201:2: ( '{' ( enumConstants )? ( ',' )? ( enumBodyDeclarations )? '}' )
			// Java.g:201:4: '{' ( enumConstants )? ( ',' )? ( enumBodyDeclarations )? '}'
			{
			root_0 = (CommonTree)adaptor.nil();


			char_literal24=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_enumBody764); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal24_tree = (CommonTree)adaptor.create(char_literal24);
			adaptor.addChild(root_0, char_literal24_tree);
			}

			// Java.g:202:3: ( enumConstants )?
			int alt18=2;
			int LA18_0 = input.LA(1);
			if ( (LA18_0==IDENTIFIER||LA18_0==MONKEYS_AT) ) {
				alt18=1;
			}
			switch (alt18) {
				case 1 :
					// Java.g:202:4: enumConstants
					{
					pushFollow(FOLLOW_enumConstants_in_enumBody769);
					enumConstants25=enumConstants();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, enumConstants25.getTree());

					}
					break;

			}

			// Java.g:203:3: ( ',' )?
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==COMMA) ) {
				alt19=1;
			}
			switch (alt19) {
				case 1 :
					// Java.g:203:3: ','
					{
					char_literal26=(Token)match(input,COMMA,FOLLOW_COMMA_in_enumBody775); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal26_tree = (CommonTree)adaptor.create(char_literal26);
					adaptor.addChild(root_0, char_literal26_tree);
					}

					}
					break;

			}

			// Java.g:204:3: ( enumBodyDeclarations )?
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0==SEMI) ) {
				alt20=1;
			}
			switch (alt20) {
				case 1 :
					// Java.g:204:4: enumBodyDeclarations
					{
					pushFollow(FOLLOW_enumBodyDeclarations_in_enumBody781);
					enumBodyDeclarations27=enumBodyDeclarations();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, enumBodyDeclarations27.getTree());

					}
					break;

			}

			char_literal28=(Token)match(input,RBRACE,FOLLOW_RBRACE_in_enumBody787); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal28_tree = (CommonTree)adaptor.create(char_literal28);
			adaptor.addChild(root_0, char_literal28_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 14, enumBody_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "enumBody"


	public static class enumConstants_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "enumConstants"
	// Java.g:209:1: enumConstants : enumConstant ( ',' enumConstant )* -> ( enumConstant )+ ;
	public final JavaParser.enumConstants_return enumConstants() throws RecognitionException {
		JavaParser.enumConstants_return retval = new JavaParser.enumConstants_return();
		retval.start = input.LT(1);
		int enumConstants_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal30=null;
		ParserRuleReturnScope enumConstant29 =null;
		ParserRuleReturnScope enumConstant31 =null;

		CommonTree char_literal30_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_enumConstant=new RewriteRuleSubtreeStream(adaptor,"rule enumConstant");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return retval; }

			// Java.g:210:2: ( enumConstant ( ',' enumConstant )* -> ( enumConstant )+ )
			// Java.g:210:4: enumConstant ( ',' enumConstant )*
			{
			pushFollow(FOLLOW_enumConstant_in_enumConstants799);
			enumConstant29=enumConstant();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_enumConstant.add(enumConstant29.getTree());
			// Java.g:210:17: ( ',' enumConstant )*
			loop21:
			while (true) {
				int alt21=2;
				int LA21_0 = input.LA(1);
				if ( (LA21_0==COMMA) ) {
					int LA21_1 = input.LA(2);
					if ( (LA21_1==IDENTIFIER||LA21_1==MONKEYS_AT) ) {
						alt21=1;
					}

				}

				switch (alt21) {
				case 1 :
					// Java.g:210:18: ',' enumConstant
					{
					char_literal30=(Token)match(input,COMMA,FOLLOW_COMMA_in_enumConstants802); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(char_literal30);

					pushFollow(FOLLOW_enumConstant_in_enumConstants804);
					enumConstant31=enumConstant();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_enumConstant.add(enumConstant31.getTree());
					}
					break;

				default :
					break loop21;
				}
			}

			// AST REWRITE
			// elements: enumConstant
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 211:2: -> ( enumConstant )+
			{
				if ( !(stream_enumConstant.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_enumConstant.hasNext() ) {
					adaptor.addChild(root_0, stream_enumConstant.nextTree());
				}
				stream_enumConstant.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 15, enumConstants_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "enumConstants"


	public static class enumConstant_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "enumConstant"
	// Java.g:218:1: enumConstant : ( annotations )? IDENTIFIER ( arguments )? ( classBody )? ;
	public final JavaParser.enumConstant_return enumConstant() throws RecognitionException {
		JavaParser.enumConstant_return retval = new JavaParser.enumConstant_return();
		retval.start = input.LT(1);
		int enumConstant_StartIndex = input.index();

		CommonTree root_0 = null;

		Token IDENTIFIER33=null;
		ParserRuleReturnScope annotations32 =null;
		ParserRuleReturnScope arguments34 =null;
		ParserRuleReturnScope classBody35 =null;

		CommonTree IDENTIFIER33_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return retval; }

			// Java.g:219:2: ( ( annotations )? IDENTIFIER ( arguments )? ( classBody )? )
			// Java.g:219:4: ( annotations )? IDENTIFIER ( arguments )? ( classBody )?
			{
			root_0 = (CommonTree)adaptor.nil();


			// Java.g:219:4: ( annotations )?
			int alt22=2;
			int LA22_0 = input.LA(1);
			if ( (LA22_0==MONKEYS_AT) ) {
				alt22=1;
			}
			switch (alt22) {
				case 1 :
					// Java.g:219:4: annotations
					{
					pushFollow(FOLLOW_annotations_in_enumConstant825);
					annotations32=annotations();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, annotations32.getTree());

					}
					break;

			}

			IDENTIFIER33=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_enumConstant830); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER33_tree = (CommonTree)adaptor.create(IDENTIFIER33);
			adaptor.addChild(root_0, IDENTIFIER33_tree);
			}

			// Java.g:221:3: ( arguments )?
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==LPAREN) ) {
				alt23=1;
			}
			switch (alt23) {
				case 1 :
					// Java.g:221:3: arguments
					{
					pushFollow(FOLLOW_arguments_in_enumConstant834);
					arguments34=arguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arguments34.getTree());

					}
					break;

			}

			// Java.g:222:3: ( classBody )?
			int alt24=2;
			int LA24_0 = input.LA(1);
			if ( (LA24_0==LBRACE) ) {
				alt24=1;
			}
			switch (alt24) {
				case 1 :
					// Java.g:222:3: classBody
					{
					pushFollow(FOLLOW_classBody_in_enumConstant839);
					classBody35=classBody();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classBody35.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 16, enumConstant_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "enumConstant"


	public static class enumBodyDeclarations_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "enumBodyDeclarations"
	// Java.g:228:1: enumBodyDeclarations : ';' ! ( classBodyDeclaration )* ;
	public final JavaParser.enumBodyDeclarations_return enumBodyDeclarations() throws RecognitionException {
		JavaParser.enumBodyDeclarations_return retval = new JavaParser.enumBodyDeclarations_return();
		retval.start = input.LT(1);
		int enumBodyDeclarations_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal36=null;
		ParserRuleReturnScope classBodyDeclaration37 =null;

		CommonTree char_literal36_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return retval; }

			// Java.g:229:2: ( ';' ! ( classBodyDeclaration )* )
			// Java.g:229:4: ';' ! ( classBodyDeclaration )*
			{
			root_0 = (CommonTree)adaptor.nil();


			char_literal36=(Token)match(input,SEMI,FOLLOW_SEMI_in_enumBodyDeclarations856); if (state.failed) return retval;
			// Java.g:230:3: ( classBodyDeclaration )*
			loop25:
			while (true) {
				int alt25=2;
				int LA25_0 = input.LA(1);
				if ( (LA25_0==ABSTRACT||LA25_0==BOOLEAN||LA25_0==BYTE||LA25_0==CHAR||LA25_0==CLASS||LA25_0==DOUBLE||LA25_0==ENUM||LA25_0==FINAL||LA25_0==FLOAT||LA25_0==IDENTIFIER||(LA25_0 >= INT && LA25_0 <= INTERFACE)||LA25_0==LBRACE||LA25_0==LONG||LA25_0==LT||(LA25_0 >= MONKEYS_AT && LA25_0 <= NATIVE)||(LA25_0 >= PRIVATE && LA25_0 <= PUBLIC)||LA25_0==SEMI||LA25_0==SHORT||(LA25_0 >= STATIC && LA25_0 <= STRICTFP)||LA25_0==SYNCHRONIZED||LA25_0==TRANSIENT||(LA25_0 >= VOID && LA25_0 <= VOLATILE)) ) {
					alt25=1;
				}

				switch (alt25) {
				case 1 :
					// Java.g:230:3: classBodyDeclaration
					{
					pushFollow(FOLLOW_classBodyDeclaration_in_enumBodyDeclarations861);
					classBodyDeclaration37=classBodyDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classBodyDeclaration37.getTree());

					}
					break;

				default :
					break loop25;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 17, enumBodyDeclarations_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "enumBodyDeclarations"


	public static class interfaceDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "interfaceDeclaration"
	// Java.g:234:1: interfaceDeclaration : ( normalInterfaceDeclaration | annotationTypeDeclaration );
	public final JavaParser.interfaceDeclaration_return interfaceDeclaration() throws RecognitionException {
		JavaParser.interfaceDeclaration_return retval = new JavaParser.interfaceDeclaration_return();
		retval.start = input.LT(1);
		int interfaceDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope normalInterfaceDeclaration38 =null;
		ParserRuleReturnScope annotationTypeDeclaration39 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return retval; }

			// Java.g:235:2: ( normalInterfaceDeclaration | annotationTypeDeclaration )
			int alt26=2;
			switch ( input.LA(1) ) {
			case MONKEYS_AT:
				{
				int LA26_1 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case PUBLIC:
				{
				int LA26_2 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case PROTECTED:
				{
				int LA26_3 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case PRIVATE:
				{
				int LA26_4 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case STATIC:
				{
				int LA26_5 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case ABSTRACT:
				{
				int LA26_6 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case FINAL:
				{
				int LA26_7 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case NATIVE:
				{
				int LA26_8 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case SYNCHRONIZED:
				{
				int LA26_9 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case TRANSIENT:
				{
				int LA26_10 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case VOLATILE:
				{
				int LA26_11 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case STRICTFP:
				{
				int LA26_12 = input.LA(2);
				if ( (synpred38_Java()) ) {
					alt26=1;
				}
				else if ( (true) ) {
					alt26=2;
				}

				}
				break;
			case INTERFACE:
				{
				alt26=1;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}
			switch (alt26) {
				case 1 :
					// Java.g:235:4: normalInterfaceDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_normalInterfaceDeclaration_in_interfaceDeclaration874);
					normalInterfaceDeclaration38=normalInterfaceDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, normalInterfaceDeclaration38.getTree());

					}
					break;
				case 2 :
					// Java.g:236:4: annotationTypeDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_annotationTypeDeclaration_in_interfaceDeclaration879);
					annotationTypeDeclaration39=annotationTypeDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, annotationTypeDeclaration39.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 18, interfaceDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "interfaceDeclaration"


	public static class normalInterfaceDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "normalInterfaceDeclaration"
	// Java.g:240:1: normalInterfaceDeclaration : a= modifiers b= 'interface' c= IDENTIFIER (d= typeParameters )? (e= 'extends' f= typeList )? g= interfaceBody -> ^( INTERFACE[$b] $c ( ^( $e $f) )? $g) ;
	public final JavaParser.normalInterfaceDeclaration_return normalInterfaceDeclaration() throws RecognitionException {
		JavaParser.normalInterfaceDeclaration_return retval = new JavaParser.normalInterfaceDeclaration_return();
		retval.start = input.LT(1);
		int normalInterfaceDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token b=null;
		Token c=null;
		Token e=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope d =null;
		ParserRuleReturnScope f =null;
		ParserRuleReturnScope g =null;

		CommonTree b_tree=null;
		CommonTree c_tree=null;
		CommonTree e_tree=null;
		RewriteRuleTokenStream stream_INTERFACE=new RewriteRuleTokenStream(adaptor,"token INTERFACE");
		RewriteRuleTokenStream stream_EXTENDS=new RewriteRuleTokenStream(adaptor,"token EXTENDS");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_typeParameters=new RewriteRuleSubtreeStream(adaptor,"rule typeParameters");
		RewriteRuleSubtreeStream stream_interfaceBody=new RewriteRuleSubtreeStream(adaptor,"rule interfaceBody");
		RewriteRuleSubtreeStream stream_modifiers=new RewriteRuleSubtreeStream(adaptor,"rule modifiers");
		RewriteRuleSubtreeStream stream_typeList=new RewriteRuleSubtreeStream(adaptor,"rule typeList");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return retval; }

			// Java.g:241:2: (a= modifiers b= 'interface' c= IDENTIFIER (d= typeParameters )? (e= 'extends' f= typeList )? g= interfaceBody -> ^( INTERFACE[$b] $c ( ^( $e $f) )? $g) )
			// Java.g:241:4: a= modifiers b= 'interface' c= IDENTIFIER (d= typeParameters )? (e= 'extends' f= typeList )? g= interfaceBody
			{
			pushFollow(FOLLOW_modifiers_in_normalInterfaceDeclaration893);
			a=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_modifiers.add(a.getTree());
			b=(Token)match(input,INTERFACE,FOLLOW_INTERFACE_in_normalInterfaceDeclaration899); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_INTERFACE.add(b);

			c=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_normalInterfaceDeclaration905); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(c);

			// Java.g:244:3: (d= typeParameters )?
			int alt27=2;
			int LA27_0 = input.LA(1);
			if ( (LA27_0==LT) ) {
				alt27=1;
			}
			switch (alt27) {
				case 1 :
					// Java.g:244:4: d= typeParameters
					{
					pushFollow(FOLLOW_typeParameters_in_normalInterfaceDeclaration912);
					d=typeParameters();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_typeParameters.add(d.getTree());
					}
					break;

			}

			// Java.g:245:3: (e= 'extends' f= typeList )?
			int alt28=2;
			int LA28_0 = input.LA(1);
			if ( (LA28_0==EXTENDS) ) {
				alt28=1;
			}
			switch (alt28) {
				case 1 :
					// Java.g:245:4: e= 'extends' f= typeList
					{
					e=(Token)match(input,EXTENDS,FOLLOW_EXTENDS_in_normalInterfaceDeclaration921); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EXTENDS.add(e);

					pushFollow(FOLLOW_typeList_in_normalInterfaceDeclaration925);
					f=typeList();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_typeList.add(f.getTree());
					}
					break;

			}

			pushFollow(FOLLOW_interfaceBody_in_normalInterfaceDeclaration933);
			g=interfaceBody();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_interfaceBody.add(g.getTree());
			// AST REWRITE
			// elements: g, e, f, c
			// token labels: e, c
			// rule labels: f, g, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_e=new RewriteRuleTokenStream(adaptor,"token e",e);
			RewriteRuleTokenStream stream_c=new RewriteRuleTokenStream(adaptor,"token c",c);
			RewriteRuleSubtreeStream stream_f=new RewriteRuleSubtreeStream(adaptor,"rule f",f!=null?f.getTree():null);
			RewriteRuleSubtreeStream stream_g=new RewriteRuleSubtreeStream(adaptor,"rule g",g!=null?g.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 247:2: -> ^( INTERFACE[$b] $c ( ^( $e $f) )? $g)
			{
				// Java.g:247:5: ^( INTERFACE[$b] $c ( ^( $e $f) )? $g)
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(INTERFACE, b), root_1);
				adaptor.addChild(root_1, stream_c.nextNode());
				// Java.g:247:36: ( ^( $e $f) )?
				if ( stream_e.hasNext()||stream_f.hasNext() ) {
					// Java.g:247:36: ^( $e $f)
					{
					CommonTree root_2 = (CommonTree)adaptor.nil();
					root_2 = (CommonTree)adaptor.becomeRoot(stream_e.nextNode(), root_2);
					adaptor.addChild(root_2, stream_f.nextTree());
					adaptor.addChild(root_1, root_2);
					}

				}
				stream_e.reset();
				stream_f.reset();

				adaptor.addChild(root_1, stream_g.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 19, normalInterfaceDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "normalInterfaceDeclaration"


	public static class typeList_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "typeList"
	// Java.g:251:1: typeList : type ( ',' type )* -> ( type )+ ;
	public final JavaParser.typeList_return typeList() throws RecognitionException {
		JavaParser.typeList_return retval = new JavaParser.typeList_return();
		retval.start = input.LT(1);
		int typeList_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal41=null;
		ParserRuleReturnScope type40 =null;
		ParserRuleReturnScope type42 =null;

		CommonTree char_literal41_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return retval; }

			// Java.g:252:2: ( type ( ',' type )* -> ( type )+ )
			// Java.g:252:4: type ( ',' type )*
			{
			pushFollow(FOLLOW_type_in_typeList970);
			type40=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_type.add(type40.getTree());
			// Java.g:252:9: ( ',' type )*
			loop29:
			while (true) {
				int alt29=2;
				int LA29_0 = input.LA(1);
				if ( (LA29_0==COMMA) ) {
					alt29=1;
				}

				switch (alt29) {
				case 1 :
					// Java.g:252:10: ',' type
					{
					char_literal41=(Token)match(input,COMMA,FOLLOW_COMMA_in_typeList973); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(char_literal41);

					pushFollow(FOLLOW_type_in_typeList975);
					type42=type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_type.add(type42.getTree());
					}
					break;

				default :
					break loop29;
				}
			}

			// AST REWRITE
			// elements: type
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 253:2: -> ( type )+
			{
				if ( !(stream_type.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_type.hasNext() ) {
					adaptor.addChild(root_0, stream_type.nextTree());
				}
				stream_type.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 20, typeList_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "typeList"


	public static class classBody_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "classBody"
	// Java.g:257:1: classBody : a= '{' (b+= classBodyDeclaration )* '}' -> ^( BLOCK[$a,\"BLOCK\"] ( $b)* ) ;
	public final JavaParser.classBody_return classBody() throws RecognitionException {
		JavaParser.classBody_return retval = new JavaParser.classBody_return();
		retval.start = input.LT(1);
		int classBody_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token char_literal43=null;
		List<Object> list_b=null;
		RuleReturnScope b = null;
		CommonTree a_tree=null;
		CommonTree char_literal43_tree=null;
		RewriteRuleTokenStream stream_RBRACE=new RewriteRuleTokenStream(adaptor,"token RBRACE");
		RewriteRuleTokenStream stream_LBRACE=new RewriteRuleTokenStream(adaptor,"token LBRACE");
		RewriteRuleSubtreeStream stream_classBodyDeclaration=new RewriteRuleSubtreeStream(adaptor,"rule classBodyDeclaration");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return retval; }

			// Java.g:258:2: (a= '{' (b+= classBodyDeclaration )* '}' -> ^( BLOCK[$a,\"BLOCK\"] ( $b)* ) )
			// Java.g:258:4: a= '{' (b+= classBodyDeclaration )* '}'
			{
			a=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_classBody997); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LBRACE.add(a);

			// Java.g:259:4: (b+= classBodyDeclaration )*
			loop30:
			while (true) {
				int alt30=2;
				int LA30_0 = input.LA(1);
				if ( (LA30_0==ABSTRACT||LA30_0==BOOLEAN||LA30_0==BYTE||LA30_0==CHAR||LA30_0==CLASS||LA30_0==DOUBLE||LA30_0==ENUM||LA30_0==FINAL||LA30_0==FLOAT||LA30_0==IDENTIFIER||(LA30_0 >= INT && LA30_0 <= INTERFACE)||LA30_0==LBRACE||LA30_0==LONG||LA30_0==LT||(LA30_0 >= MONKEYS_AT && LA30_0 <= NATIVE)||(LA30_0 >= PRIVATE && LA30_0 <= PUBLIC)||LA30_0==SEMI||LA30_0==SHORT||(LA30_0 >= STATIC && LA30_0 <= STRICTFP)||LA30_0==SYNCHRONIZED||LA30_0==TRANSIENT||(LA30_0 >= VOID && LA30_0 <= VOLATILE)) ) {
					alt30=1;
				}

				switch (alt30) {
				case 1 :
					// Java.g:259:4: b+= classBodyDeclaration
					{
					pushFollow(FOLLOW_classBodyDeclaration_in_classBody1003);
					b=classBodyDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_classBodyDeclaration.add(b.getTree());
					if (list_b==null) list_b=new ArrayList<Object>();
					list_b.add(b.getTree());
					}
					break;

				default :
					break loop30;
				}
			}

			char_literal43=(Token)match(input,RBRACE,FOLLOW_RBRACE_in_classBody1008); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RBRACE.add(char_literal43);

			// AST REWRITE
			// elements: b
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: b
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"token b",list_b);
			root_0 = (CommonTree)adaptor.nil();
			// 261:2: -> ^( BLOCK[$a,\"BLOCK\"] ( $b)* )
			{
				// Java.g:261:5: ^( BLOCK[$a,\"BLOCK\"] ( $b)* )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BLOCK, a, "BLOCK"), root_1);
				// Java.g:261:26: ( $b)*
				while ( stream_b.hasNext() ) {
					adaptor.addChild(root_1, stream_b.nextTree());
				}
				stream_b.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 21, classBody_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "classBody"


	public static class interfaceBody_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "interfaceBody"
	// Java.g:265:1: interfaceBody : '{' ( interfaceBodyDeclaration )* '}' -> ( interfaceBodyDeclaration )* ;
	public final JavaParser.interfaceBody_return interfaceBody() throws RecognitionException {
		JavaParser.interfaceBody_return retval = new JavaParser.interfaceBody_return();
		retval.start = input.LT(1);
		int interfaceBody_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal44=null;
		Token char_literal46=null;
		ParserRuleReturnScope interfaceBodyDeclaration45 =null;

		CommonTree char_literal44_tree=null;
		CommonTree char_literal46_tree=null;
		RewriteRuleTokenStream stream_RBRACE=new RewriteRuleTokenStream(adaptor,"token RBRACE");
		RewriteRuleTokenStream stream_LBRACE=new RewriteRuleTokenStream(adaptor,"token LBRACE");
		RewriteRuleSubtreeStream stream_interfaceBodyDeclaration=new RewriteRuleSubtreeStream(adaptor,"rule interfaceBodyDeclaration");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return retval; }

			// Java.g:266:2: ( '{' ( interfaceBodyDeclaration )* '}' -> ( interfaceBodyDeclaration )* )
			// Java.g:266:4: '{' ( interfaceBodyDeclaration )* '}'
			{
			char_literal44=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_interfaceBody1032); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LBRACE.add(char_literal44);

			// Java.g:267:3: ( interfaceBodyDeclaration )*
			loop31:
			while (true) {
				int alt31=2;
				int LA31_0 = input.LA(1);
				if ( (LA31_0==ABSTRACT||LA31_0==BOOLEAN||LA31_0==BYTE||LA31_0==CHAR||LA31_0==CLASS||LA31_0==DOUBLE||LA31_0==ENUM||LA31_0==FINAL||LA31_0==FLOAT||LA31_0==IDENTIFIER||(LA31_0 >= INT && LA31_0 <= INTERFACE)||LA31_0==LONG||LA31_0==LT||(LA31_0 >= MONKEYS_AT && LA31_0 <= NATIVE)||(LA31_0 >= PRIVATE && LA31_0 <= PUBLIC)||LA31_0==SEMI||LA31_0==SHORT||(LA31_0 >= STATIC && LA31_0 <= STRICTFP)||LA31_0==SYNCHRONIZED||LA31_0==TRANSIENT||(LA31_0 >= VOID && LA31_0 <= VOLATILE)) ) {
					alt31=1;
				}

				switch (alt31) {
				case 1 :
					// Java.g:267:4: interfaceBodyDeclaration
					{
					pushFollow(FOLLOW_interfaceBodyDeclaration_in_interfaceBody1037);
					interfaceBodyDeclaration45=interfaceBodyDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_interfaceBodyDeclaration.add(interfaceBodyDeclaration45.getTree());
					}
					break;

				default :
					break loop31;
				}
			}

			char_literal46=(Token)match(input,RBRACE,FOLLOW_RBRACE_in_interfaceBody1043); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RBRACE.add(char_literal46);

			// AST REWRITE
			// elements: interfaceBodyDeclaration
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 269:2: -> ( interfaceBodyDeclaration )*
			{
				// Java.g:269:5: ( interfaceBodyDeclaration )*
				while ( stream_interfaceBodyDeclaration.hasNext() ) {
					adaptor.addChild(root_0, stream_interfaceBodyDeclaration.nextTree());
				}
				stream_interfaceBodyDeclaration.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 22, interfaceBody_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "interfaceBody"


	public static class classBodyDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "classBodyDeclaration"
	// Java.g:273:1: classBodyDeclaration : ( ';' ->| ( 'static' )? block -> block | memberDecl );
	public final JavaParser.classBodyDeclaration_return classBodyDeclaration() throws RecognitionException {
		JavaParser.classBodyDeclaration_return retval = new JavaParser.classBodyDeclaration_return();
		retval.start = input.LT(1);
		int classBodyDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal47=null;
		Token string_literal48=null;
		ParserRuleReturnScope block49 =null;
		ParserRuleReturnScope memberDecl50 =null;

		CommonTree char_literal47_tree=null;
		CommonTree string_literal48_tree=null;
		RewriteRuleTokenStream stream_STATIC=new RewriteRuleTokenStream(adaptor,"token STATIC");
		RewriteRuleTokenStream stream_SEMI=new RewriteRuleTokenStream(adaptor,"token SEMI");
		RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 23) ) { return retval; }

			// Java.g:274:2: ( ';' ->| ( 'static' )? block -> block | memberDecl )
			int alt33=3;
			switch ( input.LA(1) ) {
			case SEMI:
				{
				alt33=1;
				}
				break;
			case STATIC:
				{
				int LA33_2 = input.LA(2);
				if ( (LA33_2==LBRACE) ) {
					alt33=2;
				}
				else if ( (LA33_2==ABSTRACT||LA33_2==BOOLEAN||LA33_2==BYTE||LA33_2==CHAR||LA33_2==CLASS||LA33_2==DOUBLE||LA33_2==ENUM||LA33_2==FINAL||LA33_2==FLOAT||LA33_2==IDENTIFIER||(LA33_2 >= INT && LA33_2 <= INTERFACE)||LA33_2==LONG||LA33_2==LT||(LA33_2 >= MONKEYS_AT && LA33_2 <= NATIVE)||(LA33_2 >= PRIVATE && LA33_2 <= PUBLIC)||LA33_2==SHORT||(LA33_2 >= STATIC && LA33_2 <= STRICTFP)||LA33_2==SYNCHRONIZED||LA33_2==TRANSIENT||(LA33_2 >= VOID && LA33_2 <= VOLATILE)) ) {
					alt33=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 33, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case LBRACE:
				{
				alt33=2;
				}
				break;
			case ABSTRACT:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case CLASS:
			case DOUBLE:
			case ENUM:
			case FINAL:
			case FLOAT:
			case IDENTIFIER:
			case INT:
			case INTERFACE:
			case LONG:
			case LT:
			case MONKEYS_AT:
			case NATIVE:
			case PRIVATE:
			case PROTECTED:
			case PUBLIC:
			case SHORT:
			case STRICTFP:
			case SYNCHRONIZED:
			case TRANSIENT:
			case VOID:
			case VOLATILE:
				{
				alt33=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 33, 0, input);
				throw nvae;
			}
			switch (alt33) {
				case 1 :
					// Java.g:274:4: ';'
					{
					char_literal47=(Token)match(input,SEMI,FOLLOW_SEMI_in_classBodyDeclaration1061); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SEMI.add(char_literal47);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 275:2: ->
					{
						root_0 = null;
					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:276:4: ( 'static' )? block
					{
					// Java.g:276:4: ( 'static' )?
					int alt32=2;
					int LA32_0 = input.LA(1);
					if ( (LA32_0==STATIC) ) {
						alt32=1;
					}
					switch (alt32) {
						case 1 :
							// Java.g:276:5: 'static'
							{
							string_literal48=(Token)match(input,STATIC,FOLLOW_STATIC_in_classBodyDeclaration1070); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_STATIC.add(string_literal48);

							}
							break;

					}

					pushFollow(FOLLOW_block_in_classBodyDeclaration1076);
					block49=block();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_block.add(block49.getTree());
					// AST REWRITE
					// elements: block
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 278:2: -> block
					{
						adaptor.addChild(root_0, stream_block.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// Java.g:279:4: memberDecl
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_memberDecl_in_classBodyDeclaration1086);
					memberDecl50=memberDecl();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, memberDecl50.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 23, classBodyDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "classBodyDeclaration"


	public static class memberDecl_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "memberDecl"
	// Java.g:283:1: memberDecl : ( fieldDeclaration | methodDeclaration | classDeclaration | interfaceDeclaration );
	public final JavaParser.memberDecl_return memberDecl() throws RecognitionException {
		JavaParser.memberDecl_return retval = new JavaParser.memberDecl_return();
		retval.start = input.LT(1);
		int memberDecl_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope fieldDeclaration51 =null;
		ParserRuleReturnScope methodDeclaration52 =null;
		ParserRuleReturnScope classDeclaration53 =null;
		ParserRuleReturnScope interfaceDeclaration54 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 24) ) { return retval; }

			// Java.g:284:2: ( fieldDeclaration | methodDeclaration | classDeclaration | interfaceDeclaration )
			int alt34=4;
			switch ( input.LA(1) ) {
			case MONKEYS_AT:
				{
				int LA34_1 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case PUBLIC:
				{
				int LA34_2 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case PROTECTED:
				{
				int LA34_3 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case PRIVATE:
				{
				int LA34_4 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case STATIC:
				{
				int LA34_5 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case ABSTRACT:
				{
				int LA34_6 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case FINAL:
				{
				int LA34_7 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case NATIVE:
				{
				int LA34_8 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case SYNCHRONIZED:
				{
				int LA34_9 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case TRANSIENT:
				{
				int LA34_10 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case VOLATILE:
				{
				int LA34_11 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case STRICTFP:
				{
				int LA34_12 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}
				else if ( (synpred49_Java()) ) {
					alt34=3;
				}
				else if ( (true) ) {
					alt34=4;
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA34_13 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 34, 13, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				{
				int LA34_14 = input.LA(2);
				if ( (synpred47_Java()) ) {
					alt34=1;
				}
				else if ( (synpred48_Java()) ) {
					alt34=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 34, 14, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case LT:
			case VOID:
				{
				alt34=2;
				}
				break;
			case CLASS:
			case ENUM:
				{
				alt34=3;
				}
				break;
			case INTERFACE:
				{
				alt34=4;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 34, 0, input);
				throw nvae;
			}
			switch (alt34) {
				case 1 :
					// Java.g:284:4: fieldDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_fieldDeclaration_in_memberDecl1098);
					fieldDeclaration51=fieldDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, fieldDeclaration51.getTree());

					}
					break;
				case 2 :
					// Java.g:285:4: methodDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_methodDeclaration_in_memberDecl1103);
					methodDeclaration52=methodDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, methodDeclaration52.getTree());

					}
					break;
				case 3 :
					// Java.g:286:4: classDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_classDeclaration_in_memberDecl1108);
					classDeclaration53=classDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classDeclaration53.getTree());

					}
					break;
				case 4 :
					// Java.g:287:4: interfaceDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_interfaceDeclaration_in_memberDecl1113);
					interfaceDeclaration54=interfaceDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, interfaceDeclaration54.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 24, memberDecl_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "memberDecl"


	public static class methodDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "methodDeclaration"
	// Java.g:291:1: methodDeclaration : (a= modifiers (b= typeParameters )? c= IDENTIFIER d= formalParameters ( 'throws' qualifiedNameList )? e= '{' (f= explicitConstructorInvocation )? (g+= blockStatement )* '}' -> ^( METHOD[$c] ( $d)? ^( BLOCK[$e,\"BLOCK\"] ( $f)? ( $g)* ) ) |a= modifiers (b= typeParameters )? (h= type | 'void' ) c= IDENTIFIER d= formalParameters ( '[' ']' )* ( 'throws' qualifiedNameList )? (i= block | ';' ) -> ^( METHOD[$c] ( $d)? ( $h)? ( $i)? ) );
	public final JavaParser.methodDeclaration_return methodDeclaration() throws RecognitionException {
		JavaParser.methodDeclaration_return retval = new JavaParser.methodDeclaration_return();
		retval.start = input.LT(1);
		int methodDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token c=null;
		Token e=null;
		Token string_literal55=null;
		Token char_literal57=null;
		Token string_literal58=null;
		Token char_literal59=null;
		Token char_literal60=null;
		Token string_literal61=null;
		Token char_literal63=null;
		List<Object> list_g=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope d =null;
		ParserRuleReturnScope f =null;
		ParserRuleReturnScope h =null;
		ParserRuleReturnScope i =null;
		ParserRuleReturnScope qualifiedNameList56 =null;
		ParserRuleReturnScope qualifiedNameList62 =null;
		RuleReturnScope g = null;
		CommonTree c_tree=null;
		CommonTree e_tree=null;
		CommonTree string_literal55_tree=null;
		CommonTree char_literal57_tree=null;
		CommonTree string_literal58_tree=null;
		CommonTree char_literal59_tree=null;
		CommonTree char_literal60_tree=null;
		CommonTree string_literal61_tree=null;
		CommonTree char_literal63_tree=null;
		RewriteRuleTokenStream stream_LBRACKET=new RewriteRuleTokenStream(adaptor,"token LBRACKET");
		RewriteRuleTokenStream stream_THROWS=new RewriteRuleTokenStream(adaptor,"token THROWS");
		RewriteRuleTokenStream stream_VOID=new RewriteRuleTokenStream(adaptor,"token VOID");
		RewriteRuleTokenStream stream_RBRACE=new RewriteRuleTokenStream(adaptor,"token RBRACE");
		RewriteRuleTokenStream stream_RBRACKET=new RewriteRuleTokenStream(adaptor,"token RBRACKET");
		RewriteRuleTokenStream stream_SEMI=new RewriteRuleTokenStream(adaptor,"token SEMI");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_LBRACE=new RewriteRuleTokenStream(adaptor,"token LBRACE");
		RewriteRuleSubtreeStream stream_explicitConstructorInvocation=new RewriteRuleSubtreeStream(adaptor,"rule explicitConstructorInvocation");
		RewriteRuleSubtreeStream stream_typeParameters=new RewriteRuleSubtreeStream(adaptor,"rule typeParameters");
		RewriteRuleSubtreeStream stream_formalParameters=new RewriteRuleSubtreeStream(adaptor,"rule formalParameters");
		RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
		RewriteRuleSubtreeStream stream_blockStatement=new RewriteRuleSubtreeStream(adaptor,"rule blockStatement");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");
		RewriteRuleSubtreeStream stream_modifiers=new RewriteRuleSubtreeStream(adaptor,"rule modifiers");
		RewriteRuleSubtreeStream stream_qualifiedNameList=new RewriteRuleSubtreeStream(adaptor,"rule qualifiedNameList");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 25) ) { return retval; }

			// Java.g:292:2: (a= modifiers (b= typeParameters )? c= IDENTIFIER d= formalParameters ( 'throws' qualifiedNameList )? e= '{' (f= explicitConstructorInvocation )? (g+= blockStatement )* '}' -> ^( METHOD[$c] ( $d)? ^( BLOCK[$e,\"BLOCK\"] ( $f)? ( $g)* ) ) |a= modifiers (b= typeParameters )? (h= type | 'void' ) c= IDENTIFIER d= formalParameters ( '[' ']' )* ( 'throws' qualifiedNameList )? (i= block | ';' ) -> ^( METHOD[$c] ( $d)? ( $h)? ( $i)? ) )
			int alt44=2;
			switch ( input.LA(1) ) {
			case MONKEYS_AT:
				{
				int LA44_1 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case PUBLIC:
				{
				int LA44_2 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case PROTECTED:
				{
				int LA44_3 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case PRIVATE:
				{
				int LA44_4 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case STATIC:
				{
				int LA44_5 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case ABSTRACT:
				{
				int LA44_6 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case FINAL:
				{
				int LA44_7 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case NATIVE:
				{
				int LA44_8 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case SYNCHRONIZED:
				{
				int LA44_9 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case TRANSIENT:
				{
				int LA44_10 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case VOLATILE:
				{
				int LA44_11 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case STRICTFP:
				{
				int LA44_12 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case LT:
				{
				int LA44_13 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA44_14 = input.LA(2);
				if ( (synpred54_Java()) ) {
					alt44=1;
				}
				else if ( (true) ) {
					alt44=2;
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
			case VOID:
				{
				alt44=2;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 44, 0, input);
				throw nvae;
			}
			switch (alt44) {
				case 1 :
					// Java.g:293:3: a= modifiers (b= typeParameters )? c= IDENTIFIER d= formalParameters ( 'throws' qualifiedNameList )? e= '{' (f= explicitConstructorInvocation )? (g+= blockStatement )* '}'
					{
					pushFollow(FOLLOW_modifiers_in_methodDeclaration1131);
					a=modifiers();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_modifiers.add(a.getTree());
					// Java.g:294:4: (b= typeParameters )?
					int alt35=2;
					int LA35_0 = input.LA(1);
					if ( (LA35_0==LT) ) {
						alt35=1;
					}
					switch (alt35) {
						case 1 :
							// Java.g:294:4: b= typeParameters
							{
							pushFollow(FOLLOW_typeParameters_in_methodDeclaration1137);
							b=typeParameters();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_typeParameters.add(b.getTree());
							}
							break;

					}

					c=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_methodDeclaration1144); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(c);

					pushFollow(FOLLOW_formalParameters_in_methodDeclaration1150);
					d=formalParameters();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_formalParameters.add(d.getTree());
					// Java.g:297:3: ( 'throws' qualifiedNameList )?
					int alt36=2;
					int LA36_0 = input.LA(1);
					if ( (LA36_0==THROWS) ) {
						alt36=1;
					}
					switch (alt36) {
						case 1 :
							// Java.g:297:4: 'throws' qualifiedNameList
							{
							string_literal55=(Token)match(input,THROWS,FOLLOW_THROWS_in_methodDeclaration1155); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_THROWS.add(string_literal55);

							pushFollow(FOLLOW_qualifiedNameList_in_methodDeclaration1157);
							qualifiedNameList56=qualifiedNameList();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_qualifiedNameList.add(qualifiedNameList56.getTree());
							}
							break;

					}

					e=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_methodDeclaration1165); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LBRACE.add(e);

					// Java.g:299:4: (f= explicitConstructorInvocation )?
					int alt37=2;
					switch ( input.LA(1) ) {
						case LT:
							{
							alt37=1;
							}
							break;
						case THIS:
							{
							int LA37_2 = input.LA(2);
							if ( (synpred52_Java()) ) {
								alt37=1;
							}
							}
							break;
						case LPAREN:
							{
							int LA37_3 = input.LA(2);
							if ( (synpred52_Java()) ) {
								alt37=1;
							}
							}
							break;
						case SUPER:
							{
							int LA37_4 = input.LA(2);
							if ( (synpred52_Java()) ) {
								alt37=1;
							}
							}
							break;
						case IDENTIFIER:
							{
							int LA37_5 = input.LA(2);
							if ( (synpred52_Java()) ) {
								alt37=1;
							}
							}
							break;
						case CHARLITERAL:
						case DOUBLELITERAL:
						case FALSE:
						case FLOATLITERAL:
						case INTLITERAL:
						case LONGLITERAL:
						case NULL:
						case STRINGLITERAL:
						case TRUE:
							{
							int LA37_6 = input.LA(2);
							if ( (synpred52_Java()) ) {
								alt37=1;
							}
							}
							break;
						case NEW:
							{
							int LA37_7 = input.LA(2);
							if ( (synpred52_Java()) ) {
								alt37=1;
							}
							}
							break;
						case BOOLEAN:
						case BYTE:
						case CHAR:
						case DOUBLE:
						case FLOAT:
						case INT:
						case LONG:
						case SHORT:
							{
							int LA37_8 = input.LA(2);
							if ( (synpred52_Java()) ) {
								alt37=1;
							}
							}
							break;
						case VOID:
							{
							int LA37_9 = input.LA(2);
							if ( (synpred52_Java()) ) {
								alt37=1;
							}
							}
							break;
					}
					switch (alt37) {
						case 1 :
							// Java.g:299:4: f= explicitConstructorInvocation
							{
							pushFollow(FOLLOW_explicitConstructorInvocation_in_methodDeclaration1171);
							f=explicitConstructorInvocation();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_explicitConstructorInvocation.add(f.getTree());
							}
							break;

					}

					// Java.g:300:4: (g+= blockStatement )*
					loop38:
					while (true) {
						int alt38=2;
						int LA38_0 = input.LA(1);
						if ( (LA38_0==ABSTRACT||(LA38_0 >= ASSERT && LA38_0 <= BANG)||(LA38_0 >= BOOLEAN && LA38_0 <= BYTE)||(LA38_0 >= CHAR && LA38_0 <= CLASS)||LA38_0==CONTINUE||LA38_0==DO||(LA38_0 >= DOUBLE && LA38_0 <= DOUBLELITERAL)||LA38_0==ENUM||LA38_0==FALSE||LA38_0==FINAL||(LA38_0 >= FLOAT && LA38_0 <= FOR)||(LA38_0 >= IDENTIFIER && LA38_0 <= IF)||(LA38_0 >= INT && LA38_0 <= INTLITERAL)||LA38_0==LBRACE||(LA38_0 >= LONG && LA38_0 <= LT)||(LA38_0 >= MONKEYS_AT && LA38_0 <= NULL)||LA38_0==PLUS||(LA38_0 >= PLUSPLUS && LA38_0 <= PUBLIC)||LA38_0==RETURN||LA38_0==SEMI||LA38_0==SHORT||(LA38_0 >= STATIC && LA38_0 <= SUB)||(LA38_0 >= SUBSUB && LA38_0 <= SYNCHRONIZED)||(LA38_0 >= THIS && LA38_0 <= THROW)||(LA38_0 >= TILDE && LA38_0 <= TRY)||(LA38_0 >= VOID && LA38_0 <= WHILE)) ) {
							alt38=1;
						}

						switch (alt38) {
						case 1 :
							// Java.g:300:4: g+= blockStatement
							{
							pushFollow(FOLLOW_blockStatement_in_methodDeclaration1178);
							g=blockStatement();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_blockStatement.add(g.getTree());
							if (list_g==null) list_g=new ArrayList<Object>();
							list_g.add(g.getTree());
							}
							break;

						default :
							break loop38;
						}
					}

					char_literal57=(Token)match(input,RBRACE,FOLLOW_RBRACE_in_methodDeclaration1183); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RBRACE.add(char_literal57);

					// AST REWRITE
					// elements: g, f, d
					// token labels: 
					// rule labels: f, retval, d
					// token list labels: 
					// rule list labels: g
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_f=new RewriteRuleSubtreeStream(adaptor,"rule f",f!=null?f.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_d=new RewriteRuleSubtreeStream(adaptor,"rule d",d!=null?d.getTree():null);
					RewriteRuleSubtreeStream stream_g=new RewriteRuleSubtreeStream(adaptor,"token g",list_g);
					root_0 = (CommonTree)adaptor.nil();
					// 302:3: -> ^( METHOD[$c] ( $d)? ^( BLOCK[$e,\"BLOCK\"] ( $f)? ( $g)* ) )
					{
						// Java.g:302:6: ^( METHOD[$c] ( $d)? ^( BLOCK[$e,\"BLOCK\"] ( $f)? ( $g)* ) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(METHOD, c), root_1);
						// Java.g:302:20: ( $d)?
						if ( stream_d.hasNext() ) {
							adaptor.addChild(root_1, stream_d.nextTree());
						}
						stream_d.reset();

						// Java.g:302:23: ^( BLOCK[$e,\"BLOCK\"] ( $f)? ( $g)* )
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BLOCK, e, "BLOCK"), root_2);
						// Java.g:302:44: ( $f)?
						if ( stream_f.hasNext() ) {
							adaptor.addChild(root_2, stream_f.nextTree());
						}
						stream_f.reset();

						// Java.g:302:48: ( $g)*
						while ( stream_g.hasNext() ) {
							adaptor.addChild(root_2, stream_g.nextTree());
						}
						stream_g.reset();

						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:304:4: a= modifiers (b= typeParameters )? (h= type | 'void' ) c= IDENTIFIER d= formalParameters ( '[' ']' )* ( 'throws' qualifiedNameList )? (i= block | ';' )
					{
					pushFollow(FOLLOW_modifiers_in_methodDeclaration1219);
					a=modifiers();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_modifiers.add(a.getTree());
					// Java.g:305:4: (b= typeParameters )?
					int alt39=2;
					int LA39_0 = input.LA(1);
					if ( (LA39_0==LT) ) {
						alt39=1;
					}
					switch (alt39) {
						case 1 :
							// Java.g:305:4: b= typeParameters
							{
							pushFollow(FOLLOW_typeParameters_in_methodDeclaration1225);
							b=typeParameters();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_typeParameters.add(b.getTree());
							}
							break;

					}

					// Java.g:306:3: (h= type | 'void' )
					int alt40=2;
					int LA40_0 = input.LA(1);
					if ( (LA40_0==BOOLEAN||LA40_0==BYTE||LA40_0==CHAR||LA40_0==DOUBLE||LA40_0==FLOAT||LA40_0==IDENTIFIER||LA40_0==INT||LA40_0==LONG||LA40_0==SHORT) ) {
						alt40=1;
					}
					else if ( (LA40_0==VOID) ) {
						alt40=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 40, 0, input);
						throw nvae;
					}

					switch (alt40) {
						case 1 :
							// Java.g:306:4: h= type
							{
							pushFollow(FOLLOW_type_in_methodDeclaration1233);
							h=type();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_type.add(h.getTree());
							}
							break;
						case 2 :
							// Java.g:306:13: 'void'
							{
							string_literal58=(Token)match(input,VOID,FOLLOW_VOID_in_methodDeclaration1237); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_VOID.add(string_literal58);

							}
							break;

					}

					c=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_methodDeclaration1244); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(c);

					pushFollow(FOLLOW_formalParameters_in_methodDeclaration1250);
					d=formalParameters();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_formalParameters.add(d.getTree());
					// Java.g:309:3: ( '[' ']' )*
					loop41:
					while (true) {
						int alt41=2;
						int LA41_0 = input.LA(1);
						if ( (LA41_0==LBRACKET) ) {
							alt41=1;
						}

						switch (alt41) {
						case 1 :
							// Java.g:309:4: '[' ']'
							{
							char_literal59=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_methodDeclaration1255); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_LBRACKET.add(char_literal59);

							char_literal60=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_methodDeclaration1257); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_RBRACKET.add(char_literal60);

							}
							break;

						default :
							break loop41;
						}
					}

					// Java.g:310:3: ( 'throws' qualifiedNameList )?
					int alt42=2;
					int LA42_0 = input.LA(1);
					if ( (LA42_0==THROWS) ) {
						alt42=1;
					}
					switch (alt42) {
						case 1 :
							// Java.g:310:4: 'throws' qualifiedNameList
							{
							string_literal61=(Token)match(input,THROWS,FOLLOW_THROWS_in_methodDeclaration1264); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_THROWS.add(string_literal61);

							pushFollow(FOLLOW_qualifiedNameList_in_methodDeclaration1266);
							qualifiedNameList62=qualifiedNameList();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_qualifiedNameList.add(qualifiedNameList62.getTree());
							}
							break;

					}

					// Java.g:311:3: (i= block | ';' )
					int alt43=2;
					int LA43_0 = input.LA(1);
					if ( (LA43_0==LBRACE) ) {
						alt43=1;
					}
					else if ( (LA43_0==SEMI) ) {
						alt43=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 43, 0, input);
						throw nvae;
					}

					switch (alt43) {
						case 1 :
							// Java.g:311:4: i= block
							{
							pushFollow(FOLLOW_block_in_methodDeclaration1275);
							i=block();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_block.add(i.getTree());
							}
							break;
						case 2 :
							// Java.g:311:14: ';'
							{
							char_literal63=(Token)match(input,SEMI,FOLLOW_SEMI_in_methodDeclaration1279); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_SEMI.add(char_literal63);

							}
							break;

					}

					// AST REWRITE
					// elements: i, h, d
					// token labels: 
					// rule labels: retval, d, h, i
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_d=new RewriteRuleSubtreeStream(adaptor,"rule d",d!=null?d.getTree():null);
					RewriteRuleSubtreeStream stream_h=new RewriteRuleSubtreeStream(adaptor,"rule h",h!=null?h.getTree():null);
					RewriteRuleSubtreeStream stream_i=new RewriteRuleSubtreeStream(adaptor,"rule i",i!=null?i.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 312:3: -> ^( METHOD[$c] ( $d)? ( $h)? ( $i)? )
					{
						// Java.g:312:6: ^( METHOD[$c] ( $d)? ( $h)? ( $i)? )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(METHOD, c), root_1);
						// Java.g:312:20: ( $d)?
						if ( stream_d.hasNext() ) {
							adaptor.addChild(root_1, stream_d.nextTree());
						}
						stream_d.reset();

						// Java.g:312:24: ( $h)?
						if ( stream_h.hasNext() ) {
							adaptor.addChild(root_1, stream_h.nextTree());
						}
						stream_h.reset();

						// Java.g:312:28: ( $i)?
						if ( stream_i.hasNext() ) {
							adaptor.addChild(root_1, stream_i.nextTree());
						}
						stream_i.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 25, methodDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "methodDeclaration"


	public static class fieldDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "fieldDeclaration"
	// Java.g:317:1: fieldDeclaration : a= modifiers b= type c+= variableDeclarator ( ',' c+= variableDeclarator )* ';' -> ^( DECLARATION[$b.text] ( $c)+ ) ;
	public final JavaParser.fieldDeclaration_return fieldDeclaration() throws RecognitionException {
		JavaParser.fieldDeclaration_return retval = new JavaParser.fieldDeclaration_return();
		retval.start = input.LT(1);
		int fieldDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal64=null;
		Token char_literal65=null;
		List<Object> list_c=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		RuleReturnScope c = null;
		CommonTree char_literal64_tree=null;
		CommonTree char_literal65_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_SEMI=new RewriteRuleTokenStream(adaptor,"token SEMI");
		RewriteRuleSubtreeStream stream_variableDeclarator=new RewriteRuleSubtreeStream(adaptor,"rule variableDeclarator");
		RewriteRuleSubtreeStream stream_modifiers=new RewriteRuleSubtreeStream(adaptor,"rule modifiers");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 26) ) { return retval; }

			// Java.g:318:2: (a= modifiers b= type c+= variableDeclarator ( ',' c+= variableDeclarator )* ';' -> ^( DECLARATION[$b.text] ( $c)+ ) )
			// Java.g:318:4: a= modifiers b= type c+= variableDeclarator ( ',' c+= variableDeclarator )* ';'
			{
			pushFollow(FOLLOW_modifiers_in_fieldDeclaration1318);
			a=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_modifiers.add(a.getTree());
			pushFollow(FOLLOW_type_in_fieldDeclaration1324);
			b=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_type.add(b.getTree());
			pushFollow(FOLLOW_variableDeclarator_in_fieldDeclaration1330);
			c=variableDeclarator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_variableDeclarator.add(c.getTree());
			if (list_c==null) list_c=new ArrayList<Object>();
			list_c.add(c.getTree());
			// Java.g:321:3: ( ',' c+= variableDeclarator )*
			loop45:
			while (true) {
				int alt45=2;
				int LA45_0 = input.LA(1);
				if ( (LA45_0==COMMA) ) {
					alt45=1;
				}

				switch (alt45) {
				case 1 :
					// Java.g:321:4: ',' c+= variableDeclarator
					{
					char_literal64=(Token)match(input,COMMA,FOLLOW_COMMA_in_fieldDeclaration1335); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(char_literal64);

					pushFollow(FOLLOW_variableDeclarator_in_fieldDeclaration1339);
					c=variableDeclarator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_variableDeclarator.add(c.getTree());
					if (list_c==null) list_c=new ArrayList<Object>();
					list_c.add(c.getTree());
					}
					break;

				default :
					break loop45;
				}
			}

			char_literal65=(Token)match(input,SEMI,FOLLOW_SEMI_in_fieldDeclaration1345); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_SEMI.add(char_literal65);

			// AST REWRITE
			// elements: c
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: c
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"token c",list_c);
			root_0 = (CommonTree)adaptor.nil();
			// 323:2: -> ^( DECLARATION[$b.text] ( $c)+ )
			{
				// Java.g:323:5: ^( DECLARATION[$b.text] ( $c)+ )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(DECLARATION, (b!=null?input.toString(b.start,b.stop):null)), root_1);
				if ( !(stream_c.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_c.hasNext() ) {
					adaptor.addChild(root_1, stream_c.nextTree());
				}
				stream_c.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 26, fieldDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "fieldDeclaration"


	public static class variableDeclarator_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "variableDeclarator"
	// Java.g:327:1: variableDeclarator : a= IDENTIFIER ( '[' ']' )* (b= '=' c= variableInitializer )? -> {$b!=null}? ^( EXPRESSION ^( $b $a $c) ) -> ^( EXPRESSION $a) ;
	public final JavaParser.variableDeclarator_return variableDeclarator() throws RecognitionException {
		JavaParser.variableDeclarator_return retval = new JavaParser.variableDeclarator_return();
		retval.start = input.LT(1);
		int variableDeclarator_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token b=null;
		Token char_literal66=null;
		Token char_literal67=null;
		ParserRuleReturnScope c =null;

		CommonTree a_tree=null;
		CommonTree b_tree=null;
		CommonTree char_literal66_tree=null;
		CommonTree char_literal67_tree=null;
		RewriteRuleTokenStream stream_LBRACKET=new RewriteRuleTokenStream(adaptor,"token LBRACKET");
		RewriteRuleTokenStream stream_RBRACKET=new RewriteRuleTokenStream(adaptor,"token RBRACKET");
		RewriteRuleTokenStream stream_EQ=new RewriteRuleTokenStream(adaptor,"token EQ");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_variableInitializer=new RewriteRuleSubtreeStream(adaptor,"rule variableInitializer");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 27) ) { return retval; }

			// Java.g:328:2: (a= IDENTIFIER ( '[' ']' )* (b= '=' c= variableInitializer )? -> {$b!=null}? ^( EXPRESSION ^( $b $a $c) ) -> ^( EXPRESSION $a) )
			// Java.g:328:4: a= IDENTIFIER ( '[' ']' )* (b= '=' c= variableInitializer )?
			{
			a=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_variableDeclarator1372); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(a);

			// Java.g:329:3: ( '[' ']' )*
			loop46:
			while (true) {
				int alt46=2;
				int LA46_0 = input.LA(1);
				if ( (LA46_0==LBRACKET) ) {
					alt46=1;
				}

				switch (alt46) {
				case 1 :
					// Java.g:329:4: '[' ']'
					{
					char_literal66=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_variableDeclarator1377); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LBRACKET.add(char_literal66);

					char_literal67=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_variableDeclarator1379); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RBRACKET.add(char_literal67);

					}
					break;

				default :
					break loop46;
				}
			}

			// Java.g:330:3: (b= '=' c= variableInitializer )?
			int alt47=2;
			int LA47_0 = input.LA(1);
			if ( (LA47_0==EQ) ) {
				alt47=1;
			}
			switch (alt47) {
				case 1 :
					// Java.g:330:4: b= '=' c= variableInitializer
					{
					b=(Token)match(input,EQ,FOLLOW_EQ_in_variableDeclarator1388); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EQ.add(b);

					pushFollow(FOLLOW_variableInitializer_in_variableDeclarator1392);
					c=variableInitializer();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_variableInitializer.add(c.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: a, a, c, b
			// token labels: b, a
			// rule labels: retval, c
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_b=new RewriteRuleTokenStream(adaptor,"token b",b);
			RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"rule c",c!=null?c.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 331:3: -> {$b!=null}? ^( EXPRESSION ^( $b $a $c) )
			if (b!=null) {
				// Java.g:331:18: ^( EXPRESSION ^( $b $a $c) )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(EXPRESSION, "EXPRESSION"), root_1);
				// Java.g:331:31: ^( $b $a $c)
				{
				CommonTree root_2 = (CommonTree)adaptor.nil();
				root_2 = (CommonTree)adaptor.becomeRoot(stream_b.nextNode(), root_2);
				adaptor.addChild(root_2, stream_a.nextNode());
				adaptor.addChild(root_2, stream_c.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}

			else // 332:3: -> ^( EXPRESSION $a)
			{
				// Java.g:332:6: ^( EXPRESSION $a)
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(EXPRESSION, "EXPRESSION"), root_1);
				adaptor.addChild(root_1, stream_a.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 27, variableDeclarator_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "variableDeclarator"


	public static class interfaceBodyDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "interfaceBodyDeclaration"
	// Java.g:338:1: interfaceBodyDeclaration : ( interfaceFieldDeclaration | interfaceMethodDeclaration | interfaceDeclaration | classDeclaration | ';' !);
	public final JavaParser.interfaceBodyDeclaration_return interfaceBodyDeclaration() throws RecognitionException {
		JavaParser.interfaceBodyDeclaration_return retval = new JavaParser.interfaceBodyDeclaration_return();
		retval.start = input.LT(1);
		int interfaceBodyDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal72=null;
		ParserRuleReturnScope interfaceFieldDeclaration68 =null;
		ParserRuleReturnScope interfaceMethodDeclaration69 =null;
		ParserRuleReturnScope interfaceDeclaration70 =null;
		ParserRuleReturnScope classDeclaration71 =null;

		CommonTree char_literal72_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 28) ) { return retval; }

			// Java.g:339:2: ( interfaceFieldDeclaration | interfaceMethodDeclaration | interfaceDeclaration | classDeclaration | ';' !)
			int alt48=5;
			switch ( input.LA(1) ) {
			case MONKEYS_AT:
				{
				int LA48_1 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case PUBLIC:
				{
				int LA48_2 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case PROTECTED:
				{
				int LA48_3 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case PRIVATE:
				{
				int LA48_4 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case STATIC:
				{
				int LA48_5 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 5, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case ABSTRACT:
				{
				int LA48_6 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 6, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case FINAL:
				{
				int LA48_7 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 7, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case NATIVE:
				{
				int LA48_8 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 8, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case SYNCHRONIZED:
				{
				int LA48_9 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 9, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case TRANSIENT:
				{
				int LA48_10 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 10, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case VOLATILE:
				{
				int LA48_11 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 11, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case STRICTFP:
				{
				int LA48_12 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}
				else if ( (synpred65_Java()) ) {
					alt48=3;
				}
				else if ( (synpred66_Java()) ) {
					alt48=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 12, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA48_13 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 13, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				{
				int LA48_14 = input.LA(2);
				if ( (synpred63_Java()) ) {
					alt48=1;
				}
				else if ( (synpred64_Java()) ) {
					alt48=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 48, 14, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case LT:
			case VOID:
				{
				alt48=2;
				}
				break;
			case INTERFACE:
				{
				alt48=3;
				}
				break;
			case CLASS:
			case ENUM:
				{
				alt48=4;
				}
				break;
			case SEMI:
				{
				alt48=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 48, 0, input);
				throw nvae;
			}
			switch (alt48) {
				case 1 :
					// Java.g:339:4: interfaceFieldDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_interfaceFieldDeclaration_in_interfaceBodyDeclaration1439);
					interfaceFieldDeclaration68=interfaceFieldDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, interfaceFieldDeclaration68.getTree());

					}
					break;
				case 2 :
					// Java.g:340:4: interfaceMethodDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_interfaceMethodDeclaration_in_interfaceBodyDeclaration1444);
					interfaceMethodDeclaration69=interfaceMethodDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, interfaceMethodDeclaration69.getTree());

					}
					break;
				case 3 :
					// Java.g:341:4: interfaceDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_interfaceDeclaration_in_interfaceBodyDeclaration1449);
					interfaceDeclaration70=interfaceDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, interfaceDeclaration70.getTree());

					}
					break;
				case 4 :
					// Java.g:342:4: classDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_classDeclaration_in_interfaceBodyDeclaration1454);
					classDeclaration71=classDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classDeclaration71.getTree());

					}
					break;
				case 5 :
					// Java.g:343:4: ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal72=(Token)match(input,SEMI,FOLLOW_SEMI_in_interfaceBodyDeclaration1459); if (state.failed) return retval;
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 28, interfaceBodyDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "interfaceBodyDeclaration"


	public static class interfaceMethodDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "interfaceMethodDeclaration"
	// Java.g:346:1: interfaceMethodDeclaration : a= modifiers (b= typeParameters )? c= (c1= type |c2= 'void' ) d= IDENTIFIER e= formalParameters ( '[' ']' )* ( 'throws' f= qualifiedNameList )? ';' -> {$c2==null}? ^( INTERFACE[$c1.text] $d $e ( $f)? ) -> ^( INTERFACE[$c2] $d $e ( $f)? ) ;
	public final JavaParser.interfaceMethodDeclaration_return interfaceMethodDeclaration() throws RecognitionException {
		JavaParser.interfaceMethodDeclaration_return retval = new JavaParser.interfaceMethodDeclaration_return();
		retval.start = input.LT(1);
		int interfaceMethodDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token c2=null;
		Token c=null;
		Token d=null;
		Token char_literal73=null;
		Token char_literal74=null;
		Token string_literal75=null;
		Token char_literal76=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope c1 =null;
		ParserRuleReturnScope e =null;
		ParserRuleReturnScope f =null;

		CommonTree c2_tree=null;
		CommonTree c_tree=null;
		CommonTree d_tree=null;
		CommonTree char_literal73_tree=null;
		CommonTree char_literal74_tree=null;
		CommonTree string_literal75_tree=null;
		CommonTree char_literal76_tree=null;
		RewriteRuleTokenStream stream_LBRACKET=new RewriteRuleTokenStream(adaptor,"token LBRACKET");
		RewriteRuleTokenStream stream_THROWS=new RewriteRuleTokenStream(adaptor,"token THROWS");
		RewriteRuleTokenStream stream_VOID=new RewriteRuleTokenStream(adaptor,"token VOID");
		RewriteRuleTokenStream stream_RBRACKET=new RewriteRuleTokenStream(adaptor,"token RBRACKET");
		RewriteRuleTokenStream stream_SEMI=new RewriteRuleTokenStream(adaptor,"token SEMI");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_typeParameters=new RewriteRuleSubtreeStream(adaptor,"rule typeParameters");
		RewriteRuleSubtreeStream stream_formalParameters=new RewriteRuleSubtreeStream(adaptor,"rule formalParameters");
		RewriteRuleSubtreeStream stream_modifiers=new RewriteRuleSubtreeStream(adaptor,"rule modifiers");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");
		RewriteRuleSubtreeStream stream_qualifiedNameList=new RewriteRuleSubtreeStream(adaptor,"rule qualifiedNameList");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 29) ) { return retval; }

			// Java.g:347:2: (a= modifiers (b= typeParameters )? c= (c1= type |c2= 'void' ) d= IDENTIFIER e= formalParameters ( '[' ']' )* ( 'throws' f= qualifiedNameList )? ';' -> {$c2==null}? ^( INTERFACE[$c1.text] $d $e ( $f)? ) -> ^( INTERFACE[$c2] $d $e ( $f)? ) )
			// Java.g:347:4: a= modifiers (b= typeParameters )? c= (c1= type |c2= 'void' ) d= IDENTIFIER e= formalParameters ( '[' ']' )* ( 'throws' f= qualifiedNameList )? ';'
			{
			pushFollow(FOLLOW_modifiers_in_interfaceMethodDeclaration1473);
			a=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_modifiers.add(a.getTree());
			// Java.g:348:4: (b= typeParameters )?
			int alt49=2;
			int LA49_0 = input.LA(1);
			if ( (LA49_0==LT) ) {
				alt49=1;
			}
			switch (alt49) {
				case 1 :
					// Java.g:348:4: b= typeParameters
					{
					pushFollow(FOLLOW_typeParameters_in_interfaceMethodDeclaration1479);
					b=typeParameters();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_typeParameters.add(b.getTree());
					}
					break;

			}

			// Java.g:349:5: (c1= type |c2= 'void' )
			int alt50=2;
			int LA50_0 = input.LA(1);
			if ( (LA50_0==BOOLEAN||LA50_0==BYTE||LA50_0==CHAR||LA50_0==DOUBLE||LA50_0==FLOAT||LA50_0==IDENTIFIER||LA50_0==INT||LA50_0==LONG||LA50_0==SHORT) ) {
				alt50=1;
			}
			else if ( (LA50_0==VOID) ) {
				alt50=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 50, 0, input);
				throw nvae;
			}

			switch (alt50) {
				case 1 :
					// Java.g:349:6: c1= type
					{
					pushFollow(FOLLOW_type_in_interfaceMethodDeclaration1489);
					c1=type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_type.add(c1.getTree());
					}
					break;
				case 2 :
					// Java.g:349:16: c2= 'void'
					{
					c2=(Token)match(input,VOID,FOLLOW_VOID_in_interfaceMethodDeclaration1495); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_VOID.add(c2);

					}
					break;

			}

			d=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_interfaceMethodDeclaration1502); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(d);

			pushFollow(FOLLOW_formalParameters_in_interfaceMethodDeclaration1508);
			e=formalParameters();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_formalParameters.add(e.getTree());
			// Java.g:352:3: ( '[' ']' )*
			loop51:
			while (true) {
				int alt51=2;
				int LA51_0 = input.LA(1);
				if ( (LA51_0==LBRACKET) ) {
					alt51=1;
				}

				switch (alt51) {
				case 1 :
					// Java.g:352:4: '[' ']'
					{
					char_literal73=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_interfaceMethodDeclaration1513); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LBRACKET.add(char_literal73);

					char_literal74=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_interfaceMethodDeclaration1515); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RBRACKET.add(char_literal74);

					}
					break;

				default :
					break loop51;
				}
			}

			// Java.g:353:3: ( 'throws' f= qualifiedNameList )?
			int alt52=2;
			int LA52_0 = input.LA(1);
			if ( (LA52_0==THROWS) ) {
				alt52=1;
			}
			switch (alt52) {
				case 1 :
					// Java.g:353:4: 'throws' f= qualifiedNameList
					{
					string_literal75=(Token)match(input,THROWS,FOLLOW_THROWS_in_interfaceMethodDeclaration1522); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_THROWS.add(string_literal75);

					pushFollow(FOLLOW_qualifiedNameList_in_interfaceMethodDeclaration1526);
					f=qualifiedNameList();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_qualifiedNameList.add(f.getTree());
					}
					break;

			}

			char_literal76=(Token)match(input,SEMI,FOLLOW_SEMI_in_interfaceMethodDeclaration1532); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_SEMI.add(char_literal76);

			// AST REWRITE
			// elements: d, f, d, f, e, e
			// token labels: d
			// rule labels: f, retval, e
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_d=new RewriteRuleTokenStream(adaptor,"token d",d);
			RewriteRuleSubtreeStream stream_f=new RewriteRuleSubtreeStream(adaptor,"rule f",f!=null?f.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_e=new RewriteRuleSubtreeStream(adaptor,"rule e",e!=null?e.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 355:2: -> {$c2==null}? ^( INTERFACE[$c1.text] $d $e ( $f)? )
			if (c2==null) {
				// Java.g:355:18: ^( INTERFACE[$c1.text] $d $e ( $f)? )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(INTERFACE, (c1!=null?input.toString(c1.start,c1.stop):null)), root_1);
				adaptor.addChild(root_1, stream_d.nextNode());
				adaptor.addChild(root_1, stream_e.nextTree());
				// Java.g:355:59: ( $f)?
				if ( stream_f.hasNext() ) {
					adaptor.addChild(root_1, stream_f.nextTree());
				}
				stream_f.reset();

				adaptor.addChild(root_0, root_1);
				}

			}

			else // 356:2: -> ^( INTERFACE[$c2] $d $e ( $f)? )
			{
				// Java.g:356:5: ^( INTERFACE[$c2] $d $e ( $f)? )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(INTERFACE, c2), root_1);
				adaptor.addChild(root_1, stream_d.nextNode());
				adaptor.addChild(root_1, stream_e.nextTree());
				// Java.g:356:41: ( $f)?
				if ( stream_f.hasNext() ) {
					adaptor.addChild(root_1, stream_f.nextTree());
				}
				stream_f.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 29, interfaceMethodDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "interfaceMethodDeclaration"


	public static class interfaceFieldDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "interfaceFieldDeclaration"
	// Java.g:364:1: interfaceFieldDeclaration : a= modifiers b= type c+= variableDeclarator ( ',' c+= variableDeclarator )* ';' -> ^( DECLARATION[$b.text] ( $c)+ ) ;
	public final JavaParser.interfaceFieldDeclaration_return interfaceFieldDeclaration() throws RecognitionException {
		JavaParser.interfaceFieldDeclaration_return retval = new JavaParser.interfaceFieldDeclaration_return();
		retval.start = input.LT(1);
		int interfaceFieldDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal77=null;
		Token char_literal78=null;
		List<Object> list_c=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		RuleReturnScope c = null;
		CommonTree char_literal77_tree=null;
		CommonTree char_literal78_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleTokenStream stream_SEMI=new RewriteRuleTokenStream(adaptor,"token SEMI");
		RewriteRuleSubtreeStream stream_variableDeclarator=new RewriteRuleSubtreeStream(adaptor,"rule variableDeclarator");
		RewriteRuleSubtreeStream stream_modifiers=new RewriteRuleSubtreeStream(adaptor,"rule modifiers");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 30) ) { return retval; }

			// Java.g:365:2: (a= modifiers b= type c+= variableDeclarator ( ',' c+= variableDeclarator )* ';' -> ^( DECLARATION[$b.text] ( $c)+ ) )
			// Java.g:365:4: a= modifiers b= type c+= variableDeclarator ( ',' c+= variableDeclarator )* ';'
			{
			pushFollow(FOLLOW_modifiers_in_interfaceFieldDeclaration1589);
			a=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_modifiers.add(a.getTree());
			pushFollow(FOLLOW_type_in_interfaceFieldDeclaration1595);
			b=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_type.add(b.getTree());
			pushFollow(FOLLOW_variableDeclarator_in_interfaceFieldDeclaration1601);
			c=variableDeclarator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_variableDeclarator.add(c.getTree());
			if (list_c==null) list_c=new ArrayList<Object>();
			list_c.add(c.getTree());
			// Java.g:368:3: ( ',' c+= variableDeclarator )*
			loop53:
			while (true) {
				int alt53=2;
				int LA53_0 = input.LA(1);
				if ( (LA53_0==COMMA) ) {
					alt53=1;
				}

				switch (alt53) {
				case 1 :
					// Java.g:368:4: ',' c+= variableDeclarator
					{
					char_literal77=(Token)match(input,COMMA,FOLLOW_COMMA_in_interfaceFieldDeclaration1606); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(char_literal77);

					pushFollow(FOLLOW_variableDeclarator_in_interfaceFieldDeclaration1610);
					c=variableDeclarator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_variableDeclarator.add(c.getTree());
					if (list_c==null) list_c=new ArrayList<Object>();
					list_c.add(c.getTree());
					}
					break;

				default :
					break loop53;
				}
			}

			char_literal78=(Token)match(input,SEMI,FOLLOW_SEMI_in_interfaceFieldDeclaration1616); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_SEMI.add(char_literal78);

			// AST REWRITE
			// elements: c
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: c
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"token c",list_c);
			root_0 = (CommonTree)adaptor.nil();
			// 370:3: -> ^( DECLARATION[$b.text] ( $c)+ )
			{
				// Java.g:370:6: ^( DECLARATION[$b.text] ( $c)+ )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(DECLARATION, (b!=null?input.toString(b.start,b.stop):null)), root_1);
				if ( !(stream_c.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_c.hasNext() ) {
					adaptor.addChild(root_1, stream_c.nextTree());
				}
				stream_c.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 30, interfaceFieldDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "interfaceFieldDeclaration"


	public static class type_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "type"
	// Java.g:374:1: type : ( classOrInterfaceType ( '[' ']' )* | primitiveType ( '[' ']' )* );
	public final JavaParser.type_return type() throws RecognitionException {
		JavaParser.type_return retval = new JavaParser.type_return();
		retval.start = input.LT(1);
		int type_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal80=null;
		Token char_literal81=null;
		Token char_literal83=null;
		Token char_literal84=null;
		ParserRuleReturnScope classOrInterfaceType79 =null;
		ParserRuleReturnScope primitiveType82 =null;

		CommonTree char_literal80_tree=null;
		CommonTree char_literal81_tree=null;
		CommonTree char_literal83_tree=null;
		CommonTree char_literal84_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 31) ) { return retval; }

			// Java.g:375:2: ( classOrInterfaceType ( '[' ']' )* | primitiveType ( '[' ']' )* )
			int alt56=2;
			int LA56_0 = input.LA(1);
			if ( (LA56_0==IDENTIFIER) ) {
				alt56=1;
			}
			else if ( (LA56_0==BOOLEAN||LA56_0==BYTE||LA56_0==CHAR||LA56_0==DOUBLE||LA56_0==FLOAT||LA56_0==INT||LA56_0==LONG||LA56_0==SHORT) ) {
				alt56=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 56, 0, input);
				throw nvae;
			}

			switch (alt56) {
				case 1 :
					// Java.g:375:4: classOrInterfaceType ( '[' ']' )*
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_classOrInterfaceType_in_type1643);
					classOrInterfaceType79=classOrInterfaceType();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classOrInterfaceType79.getTree());

					// Java.g:375:25: ( '[' ']' )*
					loop54:
					while (true) {
						int alt54=2;
						int LA54_0 = input.LA(1);
						if ( (LA54_0==LBRACKET) ) {
							alt54=1;
						}

						switch (alt54) {
						case 1 :
							// Java.g:375:26: '[' ']'
							{
							char_literal80=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_type1646); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal80_tree = (CommonTree)adaptor.create(char_literal80);
							adaptor.addChild(root_0, char_literal80_tree);
							}

							char_literal81=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_type1648); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal81_tree = (CommonTree)adaptor.create(char_literal81);
							adaptor.addChild(root_0, char_literal81_tree);
							}

							}
							break;

						default :
							break loop54;
						}
					}

					}
					break;
				case 2 :
					// Java.g:376:4: primitiveType ( '[' ']' )*
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_primitiveType_in_type1655);
					primitiveType82=primitiveType();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, primitiveType82.getTree());

					// Java.g:376:18: ( '[' ']' )*
					loop55:
					while (true) {
						int alt55=2;
						int LA55_0 = input.LA(1);
						if ( (LA55_0==LBRACKET) ) {
							alt55=1;
						}

						switch (alt55) {
						case 1 :
							// Java.g:376:19: '[' ']'
							{
							char_literal83=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_type1658); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal83_tree = (CommonTree)adaptor.create(char_literal83);
							adaptor.addChild(root_0, char_literal83_tree);
							}

							char_literal84=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_type1660); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal84_tree = (CommonTree)adaptor.create(char_literal84);
							adaptor.addChild(root_0, char_literal84_tree);
							}

							}
							break;

						default :
							break loop55;
						}
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 31, type_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "type"


	public static class classOrInterfaceType_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "classOrInterfaceType"
	// Java.g:380:1: classOrInterfaceType : IDENTIFIER ( typeArguments )? ( '.' IDENTIFIER ( typeArguments )? )* ;
	public final JavaParser.classOrInterfaceType_return classOrInterfaceType() throws RecognitionException {
		JavaParser.classOrInterfaceType_return retval = new JavaParser.classOrInterfaceType_return();
		retval.start = input.LT(1);
		int classOrInterfaceType_StartIndex = input.index();

		CommonTree root_0 = null;

		Token IDENTIFIER85=null;
		Token char_literal87=null;
		Token IDENTIFIER88=null;
		ParserRuleReturnScope typeArguments86 =null;
		ParserRuleReturnScope typeArguments89 =null;

		CommonTree IDENTIFIER85_tree=null;
		CommonTree char_literal87_tree=null;
		CommonTree IDENTIFIER88_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 32) ) { return retval; }

			// Java.g:381:2: ( IDENTIFIER ( typeArguments )? ( '.' IDENTIFIER ( typeArguments )? )* )
			// Java.g:381:4: IDENTIFIER ( typeArguments )? ( '.' IDENTIFIER ( typeArguments )? )*
			{
			root_0 = (CommonTree)adaptor.nil();


			IDENTIFIER85=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_classOrInterfaceType1674); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER85_tree = (CommonTree)adaptor.create(IDENTIFIER85);
			adaptor.addChild(root_0, IDENTIFIER85_tree);
			}

			// Java.g:382:3: ( typeArguments )?
			int alt57=2;
			int LA57_0 = input.LA(1);
			if ( (LA57_0==LT) ) {
				int LA57_1 = input.LA(2);
				if ( (LA57_1==BOOLEAN||LA57_1==BYTE||LA57_1==CHAR||LA57_1==DOUBLE||LA57_1==FLOAT||LA57_1==IDENTIFIER||LA57_1==INT||LA57_1==LONG||LA57_1==QUES||LA57_1==SHORT) ) {
					alt57=1;
				}
			}
			switch (alt57) {
				case 1 :
					// Java.g:382:3: typeArguments
					{
					pushFollow(FOLLOW_typeArguments_in_classOrInterfaceType1678);
					typeArguments86=typeArguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, typeArguments86.getTree());

					}
					break;

			}

			// Java.g:383:3: ( '.' IDENTIFIER ( typeArguments )? )*
			loop59:
			while (true) {
				int alt59=2;
				int LA59_0 = input.LA(1);
				if ( (LA59_0==DOT) ) {
					alt59=1;
				}

				switch (alt59) {
				case 1 :
					// Java.g:383:4: '.' IDENTIFIER ( typeArguments )?
					{
					char_literal87=(Token)match(input,DOT,FOLLOW_DOT_in_classOrInterfaceType1684); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal87_tree = (CommonTree)adaptor.create(char_literal87);
					adaptor.addChild(root_0, char_literal87_tree);
					}

					IDENTIFIER88=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_classOrInterfaceType1686); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER88_tree = (CommonTree)adaptor.create(IDENTIFIER88);
					adaptor.addChild(root_0, IDENTIFIER88_tree);
					}

					// Java.g:384:4: ( typeArguments )?
					int alt58=2;
					int LA58_0 = input.LA(1);
					if ( (LA58_0==LT) ) {
						int LA58_1 = input.LA(2);
						if ( (LA58_1==BOOLEAN||LA58_1==BYTE||LA58_1==CHAR||LA58_1==DOUBLE||LA58_1==FLOAT||LA58_1==IDENTIFIER||LA58_1==INT||LA58_1==LONG||LA58_1==QUES||LA58_1==SHORT) ) {
							alt58=1;
						}
					}
					switch (alt58) {
						case 1 :
							// Java.g:384:4: typeArguments
							{
							pushFollow(FOLLOW_typeArguments_in_classOrInterfaceType1691);
							typeArguments89=typeArguments();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, typeArguments89.getTree());

							}
							break;

					}

					}
					break;

				default :
					break loop59;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 32, classOrInterfaceType_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "classOrInterfaceType"


	public static class primitiveType_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "primitiveType"
	// Java.g:389:1: primitiveType : ( 'boolean' | 'char' | 'byte' | 'short' | 'int' | 'long' | 'float' | 'double' );
	public final JavaParser.primitiveType_return primitiveType() throws RecognitionException {
		JavaParser.primitiveType_return retval = new JavaParser.primitiveType_return();
		retval.start = input.LT(1);
		int primitiveType_StartIndex = input.index();

		CommonTree root_0 = null;

		Token set90=null;

		CommonTree set90_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 33) ) { return retval; }

			// Java.g:390:2: ( 'boolean' | 'char' | 'byte' | 'short' | 'int' | 'long' | 'float' | 'double' )
			// Java.g:
			{
			root_0 = (CommonTree)adaptor.nil();


			set90=input.LT(1);
			if ( input.LA(1)==BOOLEAN||input.LA(1)==BYTE||input.LA(1)==CHAR||input.LA(1)==DOUBLE||input.LA(1)==FLOAT||input.LA(1)==INT||input.LA(1)==LONG||input.LA(1)==SHORT ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (CommonTree)adaptor.create(set90));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 33, primitiveType_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "primitiveType"


	public static class typeArguments_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "typeArguments"
	// Java.g:401:1: typeArguments : '<' typeArgument ( ',' typeArgument )* '>' -> ( typeArgument )+ ;
	public final JavaParser.typeArguments_return typeArguments() throws RecognitionException {
		JavaParser.typeArguments_return retval = new JavaParser.typeArguments_return();
		retval.start = input.LT(1);
		int typeArguments_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal91=null;
		Token char_literal93=null;
		Token char_literal95=null;
		ParserRuleReturnScope typeArgument92 =null;
		ParserRuleReturnScope typeArgument94 =null;

		CommonTree char_literal91_tree=null;
		CommonTree char_literal93_tree=null;
		CommonTree char_literal95_tree=null;
		RewriteRuleTokenStream stream_GT=new RewriteRuleTokenStream(adaptor,"token GT");
		RewriteRuleTokenStream stream_LT=new RewriteRuleTokenStream(adaptor,"token LT");
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_typeArgument=new RewriteRuleSubtreeStream(adaptor,"rule typeArgument");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 34) ) { return retval; }

			// Java.g:402:2: ( '<' typeArgument ( ',' typeArgument )* '>' -> ( typeArgument )+ )
			// Java.g:402:4: '<' typeArgument ( ',' typeArgument )* '>'
			{
			char_literal91=(Token)match(input,LT,FOLLOW_LT_in_typeArguments1756); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LT.add(char_literal91);

			pushFollow(FOLLOW_typeArgument_in_typeArguments1758);
			typeArgument92=typeArgument();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_typeArgument.add(typeArgument92.getTree());
			// Java.g:403:3: ( ',' typeArgument )*
			loop60:
			while (true) {
				int alt60=2;
				int LA60_0 = input.LA(1);
				if ( (LA60_0==COMMA) ) {
					alt60=1;
				}

				switch (alt60) {
				case 1 :
					// Java.g:403:4: ',' typeArgument
					{
					char_literal93=(Token)match(input,COMMA,FOLLOW_COMMA_in_typeArguments1763); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(char_literal93);

					pushFollow(FOLLOW_typeArgument_in_typeArguments1765);
					typeArgument94=typeArgument();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_typeArgument.add(typeArgument94.getTree());
					}
					break;

				default :
					break loop60;
				}
			}

			char_literal95=(Token)match(input,GT,FOLLOW_GT_in_typeArguments1771); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_GT.add(char_literal95);

			// AST REWRITE
			// elements: typeArgument
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 405:2: -> ( typeArgument )+
			{
				if ( !(stream_typeArgument.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_typeArgument.hasNext() ) {
					adaptor.addChild(root_0, stream_typeArgument.nextTree());
				}
				stream_typeArgument.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 34, typeArguments_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "typeArguments"


	public static class typeArgument_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "typeArgument"
	// Java.g:409:1: typeArgument : ( type | '?' ( ( 'extends' | 'super' ) type )? );
	public final JavaParser.typeArgument_return typeArgument() throws RecognitionException {
		JavaParser.typeArgument_return retval = new JavaParser.typeArgument_return();
		retval.start = input.LT(1);
		int typeArgument_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal97=null;
		Token set98=null;
		ParserRuleReturnScope type96 =null;
		ParserRuleReturnScope type99 =null;

		CommonTree char_literal97_tree=null;
		CommonTree set98_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 35) ) { return retval; }

			// Java.g:410:2: ( type | '?' ( ( 'extends' | 'super' ) type )? )
			int alt62=2;
			int LA62_0 = input.LA(1);
			if ( (LA62_0==BOOLEAN||LA62_0==BYTE||LA62_0==CHAR||LA62_0==DOUBLE||LA62_0==FLOAT||LA62_0==IDENTIFIER||LA62_0==INT||LA62_0==LONG||LA62_0==SHORT) ) {
				alt62=1;
			}
			else if ( (LA62_0==QUES) ) {
				alt62=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 62, 0, input);
				throw nvae;
			}

			switch (alt62) {
				case 1 :
					// Java.g:410:4: type
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_type_in_typeArgument1789);
					type96=type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, type96.getTree());

					}
					break;
				case 2 :
					// Java.g:411:4: '?' ( ( 'extends' | 'super' ) type )?
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal97=(Token)match(input,QUES,FOLLOW_QUES_in_typeArgument1794); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal97_tree = (CommonTree)adaptor.create(char_literal97);
					adaptor.addChild(root_0, char_literal97_tree);
					}

					// Java.g:412:3: ( ( 'extends' | 'super' ) type )?
					int alt61=2;
					int LA61_0 = input.LA(1);
					if ( (LA61_0==EXTENDS||LA61_0==SUPER) ) {
						alt61=1;
					}
					switch (alt61) {
						case 1 :
							// Java.g:413:4: ( 'extends' | 'super' ) type
							{
							set98=input.LT(1);
							if ( input.LA(1)==EXTENDS||input.LA(1)==SUPER ) {
								input.consume();
								if ( state.backtracking==0 ) adaptor.addChild(root_0, (CommonTree)adaptor.create(set98));
								state.errorRecovery=false;
								state.failed=false;
							}
							else {
								if (state.backtracking>0) {state.failed=true; return retval;}
								MismatchedSetException mse = new MismatchedSetException(null,input);
								throw mse;
							}
							pushFollow(FOLLOW_type_in_typeArgument1814);
							type99=type();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, type99.getTree());

							}
							break;

					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 35, typeArgument_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "typeArgument"


	public static class qualifiedNameList_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "qualifiedNameList"
	// Java.g:419:1: qualifiedNameList : qualifiedName ( ',' qualifiedName )* -> ( qualifiedName )+ ;
	public final JavaParser.qualifiedNameList_return qualifiedNameList() throws RecognitionException {
		JavaParser.qualifiedNameList_return retval = new JavaParser.qualifiedNameList_return();
		retval.start = input.LT(1);
		int qualifiedNameList_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal101=null;
		ParserRuleReturnScope qualifiedName100 =null;
		ParserRuleReturnScope qualifiedName102 =null;

		CommonTree char_literal101_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_qualifiedName=new RewriteRuleSubtreeStream(adaptor,"rule qualifiedName");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 36) ) { return retval; }

			// Java.g:420:2: ( qualifiedName ( ',' qualifiedName )* -> ( qualifiedName )+ )
			// Java.g:420:4: qualifiedName ( ',' qualifiedName )*
			{
			pushFollow(FOLLOW_qualifiedName_in_qualifiedNameList1831);
			qualifiedName100=qualifiedName();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_qualifiedName.add(qualifiedName100.getTree());
			// Java.g:421:3: ( ',' qualifiedName )*
			loop63:
			while (true) {
				int alt63=2;
				int LA63_0 = input.LA(1);
				if ( (LA63_0==COMMA) ) {
					alt63=1;
				}

				switch (alt63) {
				case 1 :
					// Java.g:421:4: ',' qualifiedName
					{
					char_literal101=(Token)match(input,COMMA,FOLLOW_COMMA_in_qualifiedNameList1836); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(char_literal101);

					pushFollow(FOLLOW_qualifiedName_in_qualifiedNameList1838);
					qualifiedName102=qualifiedName();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_qualifiedName.add(qualifiedName102.getTree());
					}
					break;

				default :
					break loop63;
				}
			}

			// AST REWRITE
			// elements: qualifiedName
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 422:2: -> ( qualifiedName )+
			{
				if ( !(stream_qualifiedName.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_qualifiedName.hasNext() ) {
					adaptor.addChild(root_0, stream_qualifiedName.nextTree());
				}
				stream_qualifiedName.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 36, qualifiedNameList_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "qualifiedNameList"


	public static class formalParameters_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "formalParameters"
	// Java.g:426:1: formalParameters : '(' ( formalParameterDecls )? ')' -> ( formalParameterDecls )? ;
	public final JavaParser.formalParameters_return formalParameters() throws RecognitionException {
		JavaParser.formalParameters_return retval = new JavaParser.formalParameters_return();
		retval.start = input.LT(1);
		int formalParameters_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal103=null;
		Token char_literal105=null;
		ParserRuleReturnScope formalParameterDecls104 =null;

		CommonTree char_literal103_tree=null;
		CommonTree char_literal105_tree=null;
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleSubtreeStream stream_formalParameterDecls=new RewriteRuleSubtreeStream(adaptor,"rule formalParameterDecls");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 37) ) { return retval; }

			// Java.g:427:2: ( '(' ( formalParameterDecls )? ')' -> ( formalParameterDecls )? )
			// Java.g:427:4: '(' ( formalParameterDecls )? ')'
			{
			char_literal103=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_formalParameters1858); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LPAREN.add(char_literal103);

			// Java.g:428:3: ( formalParameterDecls )?
			int alt64=2;
			int LA64_0 = input.LA(1);
			if ( (LA64_0==BOOLEAN||LA64_0==BYTE||LA64_0==CHAR||LA64_0==DOUBLE||LA64_0==FINAL||LA64_0==FLOAT||LA64_0==IDENTIFIER||LA64_0==INT||LA64_0==LONG||LA64_0==MONKEYS_AT||LA64_0==SHORT) ) {
				alt64=1;
			}
			switch (alt64) {
				case 1 :
					// Java.g:428:3: formalParameterDecls
					{
					pushFollow(FOLLOW_formalParameterDecls_in_formalParameters1862);
					formalParameterDecls104=formalParameterDecls();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_formalParameterDecls.add(formalParameterDecls104.getTree());
					}
					break;

			}

			char_literal105=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_formalParameters1867); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RPAREN.add(char_literal105);

			// AST REWRITE
			// elements: formalParameterDecls
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 430:3: -> ( formalParameterDecls )?
			{
				// Java.g:430:6: ( formalParameterDecls )?
				if ( stream_formalParameterDecls.hasNext() ) {
					adaptor.addChild(root_0, stream_formalParameterDecls.nextTree());
				}
				stream_formalParameterDecls.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 37, formalParameters_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "formalParameters"


	public static class formalParameterDecls_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "formalParameterDecls"
	// Java.g:434:1: formalParameterDecls : (b= ellipsisParameterDecl -> ^( PARAMETERS $b) |a+= normalParameterDecl ( ',' a+= normalParameterDecl )* -> ^( PARAMETERS ( $a)+ ) | (a+= normalParameterDecl ',' )+ b= ellipsisParameterDecl -> ^( PARAMETERS ( $a)+ $b) );
	public final JavaParser.formalParameterDecls_return formalParameterDecls() throws RecognitionException {
		JavaParser.formalParameterDecls_return retval = new JavaParser.formalParameterDecls_return();
		retval.start = input.LT(1);
		int formalParameterDecls_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal106=null;
		Token char_literal107=null;
		List<Object> list_a=null;
		ParserRuleReturnScope b =null;
		RuleReturnScope a = null;
		CommonTree char_literal106_tree=null;
		CommonTree char_literal107_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_ellipsisParameterDecl=new RewriteRuleSubtreeStream(adaptor,"rule ellipsisParameterDecl");
		RewriteRuleSubtreeStream stream_normalParameterDecl=new RewriteRuleSubtreeStream(adaptor,"rule normalParameterDecl");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 38) ) { return retval; }

			// Java.g:435:2: (b= ellipsisParameterDecl -> ^( PARAMETERS $b) |a+= normalParameterDecl ( ',' a+= normalParameterDecl )* -> ^( PARAMETERS ( $a)+ ) | (a+= normalParameterDecl ',' )+ b= ellipsisParameterDecl -> ^( PARAMETERS ( $a)+ $b) )
			int alt67=3;
			switch ( input.LA(1) ) {
			case FINAL:
				{
				int LA67_1 = input.LA(2);
				if ( (synpred91_Java()) ) {
					alt67=1;
				}
				else if ( (synpred93_Java()) ) {
					alt67=2;
				}
				else if ( (true) ) {
					alt67=3;
				}

				}
				break;
			case MONKEYS_AT:
				{
				int LA67_2 = input.LA(2);
				if ( (synpred91_Java()) ) {
					alt67=1;
				}
				else if ( (synpred93_Java()) ) {
					alt67=2;
				}
				else if ( (true) ) {
					alt67=3;
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA67_3 = input.LA(2);
				if ( (synpred91_Java()) ) {
					alt67=1;
				}
				else if ( (synpred93_Java()) ) {
					alt67=2;
				}
				else if ( (true) ) {
					alt67=3;
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				{
				int LA67_4 = input.LA(2);
				if ( (synpred91_Java()) ) {
					alt67=1;
				}
				else if ( (synpred93_Java()) ) {
					alt67=2;
				}
				else if ( (true) ) {
					alt67=3;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 67, 0, input);
				throw nvae;
			}
			switch (alt67) {
				case 1 :
					// Java.g:435:4: b= ellipsisParameterDecl
					{
					pushFollow(FOLLOW_ellipsisParameterDecl_in_formalParameterDecls1888);
					b=ellipsisParameterDecl();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_ellipsisParameterDecl.add(b.getTree());
					// AST REWRITE
					// elements: b
					// token labels: 
					// rule labels: retval, b
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 436:3: -> ^( PARAMETERS $b)
					{
						// Java.g:436:6: ^( PARAMETERS $b)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(PARAMETERS, "PARAMETERS"), root_1);
						adaptor.addChild(root_1, stream_b.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:437:4: a+= normalParameterDecl ( ',' a+= normalParameterDecl )*
					{
					pushFollow(FOLLOW_normalParameterDecl_in_formalParameterDecls1906);
					a=normalParameterDecl();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_normalParameterDecl.add(a.getTree());
					if (list_a==null) list_a=new ArrayList<Object>();
					list_a.add(a.getTree());
					// Java.g:438:3: ( ',' a+= normalParameterDecl )*
					loop65:
					while (true) {
						int alt65=2;
						int LA65_0 = input.LA(1);
						if ( (LA65_0==COMMA) ) {
							alt65=1;
						}

						switch (alt65) {
						case 1 :
							// Java.g:438:4: ',' a+= normalParameterDecl
							{
							char_literal106=(Token)match(input,COMMA,FOLLOW_COMMA_in_formalParameterDecls1911); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_COMMA.add(char_literal106);

							pushFollow(FOLLOW_normalParameterDecl_in_formalParameterDecls1915);
							a=normalParameterDecl();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_normalParameterDecl.add(a.getTree());
							if (list_a==null) list_a=new ArrayList<Object>();
							list_a.add(a.getTree());
							}
							break;

						default :
							break loop65;
						}
					}

					// AST REWRITE
					// elements: a
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: a
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"token a",list_a);
					root_0 = (CommonTree)adaptor.nil();
					// 439:3: -> ^( PARAMETERS ( $a)+ )
					{
						// Java.g:439:6: ^( PARAMETERS ( $a)+ )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(PARAMETERS, "PARAMETERS"), root_1);
						if ( !(stream_a.hasNext()) ) {
							throw new RewriteEarlyExitException();
						}
						while ( stream_a.hasNext() ) {
							adaptor.addChild(root_1, stream_a.nextTree());
						}
						stream_a.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// Java.g:440:4: (a+= normalParameterDecl ',' )+ b= ellipsisParameterDecl
					{
					// Java.g:440:4: (a+= normalParameterDecl ',' )+
					int cnt66=0;
					loop66:
					while (true) {
						int alt66=2;
						switch ( input.LA(1) ) {
						case FINAL:
							{
							int LA66_1 = input.LA(2);
							if ( (synpred94_Java()) ) {
								alt66=1;
							}

							}
							break;
						case MONKEYS_AT:
							{
							int LA66_2 = input.LA(2);
							if ( (synpred94_Java()) ) {
								alt66=1;
							}

							}
							break;
						case IDENTIFIER:
							{
							int LA66_3 = input.LA(2);
							if ( (synpred94_Java()) ) {
								alt66=1;
							}

							}
							break;
						case BOOLEAN:
						case BYTE:
						case CHAR:
						case DOUBLE:
						case FLOAT:
						case INT:
						case LONG:
						case SHORT:
							{
							int LA66_4 = input.LA(2);
							if ( (synpred94_Java()) ) {
								alt66=1;
							}

							}
							break;
						}
						switch (alt66) {
						case 1 :
							// Java.g:440:5: a+= normalParameterDecl ','
							{
							pushFollow(FOLLOW_normalParameterDecl_in_formalParameterDecls1937);
							a=normalParameterDecl();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_normalParameterDecl.add(a.getTree());
							if (list_a==null) list_a=new ArrayList<Object>();
							list_a.add(a.getTree());
							char_literal107=(Token)match(input,COMMA,FOLLOW_COMMA_in_formalParameterDecls1941); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_COMMA.add(char_literal107);

							}
							break;

						default :
							if ( cnt66 >= 1 ) break loop66;
							if (state.backtracking>0) {state.failed=true; return retval;}
							EarlyExitException eee = new EarlyExitException(66, input);
							throw eee;
						}
						cnt66++;
					}

					pushFollow(FOLLOW_ellipsisParameterDecl_in_formalParameterDecls1949);
					b=ellipsisParameterDecl();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_ellipsisParameterDecl.add(b.getTree());
					// AST REWRITE
					// elements: b, a
					// token labels: 
					// rule labels: retval, b
					// token list labels: 
					// rule list labels: a
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);
					RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"token a",list_a);
					root_0 = (CommonTree)adaptor.nil();
					// 443:3: -> ^( PARAMETERS ( $a)+ $b)
					{
						// Java.g:443:6: ^( PARAMETERS ( $a)+ $b)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(PARAMETERS, "PARAMETERS"), root_1);
						if ( !(stream_a.hasNext()) ) {
							throw new RewriteEarlyExitException();
						}
						while ( stream_a.hasNext() ) {
							adaptor.addChild(root_1, stream_a.nextTree());
						}
						stream_a.reset();

						adaptor.addChild(root_1, stream_b.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 38, formalParameterDecls_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "formalParameterDecls"


	public static class normalParameterDecl_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "normalParameterDecl"
	// Java.g:447:1: normalParameterDecl : variableModifiers a= type b= IDENTIFIER ( '[' ']' )* -> ^( $a $b) ;
	public final JavaParser.normalParameterDecl_return normalParameterDecl() throws RecognitionException {
		JavaParser.normalParameterDecl_return retval = new JavaParser.normalParameterDecl_return();
		retval.start = input.LT(1);
		int normalParameterDecl_StartIndex = input.index();

		CommonTree root_0 = null;

		Token b=null;
		Token char_literal109=null;
		Token char_literal110=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope variableModifiers108 =null;

		CommonTree b_tree=null;
		CommonTree char_literal109_tree=null;
		CommonTree char_literal110_tree=null;
		RewriteRuleTokenStream stream_LBRACKET=new RewriteRuleTokenStream(adaptor,"token LBRACKET");
		RewriteRuleTokenStream stream_RBRACKET=new RewriteRuleTokenStream(adaptor,"token RBRACKET");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_variableModifiers=new RewriteRuleSubtreeStream(adaptor,"rule variableModifiers");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 39) ) { return retval; }

			// Java.g:448:2: ( variableModifiers a= type b= IDENTIFIER ( '[' ']' )* -> ^( $a $b) )
			// Java.g:448:4: variableModifiers a= type b= IDENTIFIER ( '[' ']' )*
			{
			pushFollow(FOLLOW_variableModifiers_in_normalParameterDecl1976);
			variableModifiers108=variableModifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_variableModifiers.add(variableModifiers108.getTree());
			pushFollow(FOLLOW_type_in_normalParameterDecl1982);
			a=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_type.add(a.getTree());
			b=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_normalParameterDecl1988); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(b);

			// Java.g:451:3: ( '[' ']' )*
			loop68:
			while (true) {
				int alt68=2;
				int LA68_0 = input.LA(1);
				if ( (LA68_0==LBRACKET) ) {
					alt68=1;
				}

				switch (alt68) {
				case 1 :
					// Java.g:451:4: '[' ']'
					{
					char_literal109=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_normalParameterDecl1993); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LBRACKET.add(char_literal109);

					char_literal110=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_normalParameterDecl1995); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RBRACKET.add(char_literal110);

					}
					break;

				default :
					break loop68;
				}
			}

			// AST REWRITE
			// elements: a, b
			// token labels: b
			// rule labels: retval, a
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_b=new RewriteRuleTokenStream(adaptor,"token b",b);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 452:3: -> ^( $a $b)
			{
				// Java.g:452:6: ^( $a $b)
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot(stream_a.nextNode(), root_1);
				adaptor.addChild(root_1, stream_b.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 39, normalParameterDecl_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "normalParameterDecl"


	public static class ellipsisParameterDecl_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "ellipsisParameterDecl"
	// Java.g:456:1: ellipsisParameterDecl : variableModifiers a= type '...' b= IDENTIFIER -> ^( $a '...' $b) ;
	public final JavaParser.ellipsisParameterDecl_return ellipsisParameterDecl() throws RecognitionException {
		JavaParser.ellipsisParameterDecl_return retval = new JavaParser.ellipsisParameterDecl_return();
		retval.start = input.LT(1);
		int ellipsisParameterDecl_StartIndex = input.index();

		CommonTree root_0 = null;

		Token b=null;
		Token string_literal112=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope variableModifiers111 =null;

		CommonTree b_tree=null;
		CommonTree string_literal112_tree=null;
		RewriteRuleTokenStream stream_ELLIPSIS=new RewriteRuleTokenStream(adaptor,"token ELLIPSIS");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_variableModifiers=new RewriteRuleSubtreeStream(adaptor,"rule variableModifiers");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 40) ) { return retval; }

			// Java.g:457:2: ( variableModifiers a= type '...' b= IDENTIFIER -> ^( $a '...' $b) )
			// Java.g:457:4: variableModifiers a= type '...' b= IDENTIFIER
			{
			pushFollow(FOLLOW_variableModifiers_in_ellipsisParameterDecl2021);
			variableModifiers111=variableModifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_variableModifiers.add(variableModifiers111.getTree());
			pushFollow(FOLLOW_type_in_ellipsisParameterDecl2027);
			a=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_type.add(a.getTree());
			string_literal112=(Token)match(input,ELLIPSIS,FOLLOW_ELLIPSIS_in_ellipsisParameterDecl2031); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_ELLIPSIS.add(string_literal112);

			b=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_ellipsisParameterDecl2037); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(b);

			// AST REWRITE
			// elements: ELLIPSIS, b, a
			// token labels: b
			// rule labels: retval, a
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleTokenStream stream_b=new RewriteRuleTokenStream(adaptor,"token b",b);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 461:3: -> ^( $a '...' $b)
			{
				// Java.g:461:6: ^( $a '...' $b)
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot(stream_a.nextNode(), root_1);
				adaptor.addChild(root_1, stream_ELLIPSIS.nextNode());
				adaptor.addChild(root_1, stream_b.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 40, ellipsisParameterDecl_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "ellipsisParameterDecl"


	public static class explicitConstructorInvocation_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "explicitConstructorInvocation"
	// Java.g:465:1: explicitConstructorInvocation : ( ( nonWildcardTypeArguments )? ( 'this' | 'super' ) arguments ';' !| primary '.' ( nonWildcardTypeArguments )? 'super' arguments ';' !);
	public final JavaParser.explicitConstructorInvocation_return explicitConstructorInvocation() throws RecognitionException {
		JavaParser.explicitConstructorInvocation_return retval = new JavaParser.explicitConstructorInvocation_return();
		retval.start = input.LT(1);
		int explicitConstructorInvocation_StartIndex = input.index();

		CommonTree root_0 = null;

		Token set114=null;
		Token char_literal116=null;
		Token char_literal118=null;
		Token string_literal120=null;
		Token char_literal122=null;
		ParserRuleReturnScope nonWildcardTypeArguments113 =null;
		ParserRuleReturnScope arguments115 =null;
		ParserRuleReturnScope primary117 =null;
		ParserRuleReturnScope nonWildcardTypeArguments119 =null;
		ParserRuleReturnScope arguments121 =null;

		CommonTree set114_tree=null;
		CommonTree char_literal116_tree=null;
		CommonTree char_literal118_tree=null;
		CommonTree string_literal120_tree=null;
		CommonTree char_literal122_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 41) ) { return retval; }

			// Java.g:466:2: ( ( nonWildcardTypeArguments )? ( 'this' | 'super' ) arguments ';' !| primary '.' ( nonWildcardTypeArguments )? 'super' arguments ';' !)
			int alt71=2;
			switch ( input.LA(1) ) {
			case LT:
				{
				alt71=1;
				}
				break;
			case THIS:
				{
				int LA71_2 = input.LA(2);
				if ( (synpred98_Java()) ) {
					alt71=1;
				}
				else if ( (true) ) {
					alt71=2;
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case CHARLITERAL:
			case DOUBLE:
			case DOUBLELITERAL:
			case FALSE:
			case FLOAT:
			case FLOATLITERAL:
			case IDENTIFIER:
			case INT:
			case INTLITERAL:
			case LONG:
			case LONGLITERAL:
			case LPAREN:
			case NEW:
			case NULL:
			case SHORT:
			case STRINGLITERAL:
			case TRUE:
			case VOID:
				{
				alt71=2;
				}
				break;
			case SUPER:
				{
				int LA71_4 = input.LA(2);
				if ( (synpred98_Java()) ) {
					alt71=1;
				}
				else if ( (true) ) {
					alt71=2;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 71, 0, input);
				throw nvae;
			}
			switch (alt71) {
				case 1 :
					// Java.g:466:4: ( nonWildcardTypeArguments )? ( 'this' | 'super' ) arguments ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					// Java.g:466:4: ( nonWildcardTypeArguments )?
					int alt69=2;
					int LA69_0 = input.LA(1);
					if ( (LA69_0==LT) ) {
						alt69=1;
					}
					switch (alt69) {
						case 1 :
							// Java.g:466:5: nonWildcardTypeArguments
							{
							pushFollow(FOLLOW_nonWildcardTypeArguments_in_explicitConstructorInvocation2064);
							nonWildcardTypeArguments113=nonWildcardTypeArguments();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, nonWildcardTypeArguments113.getTree());

							}
							break;

					}

					set114=input.LT(1);
					if ( input.LA(1)==SUPER||input.LA(1)==THIS ) {
						input.consume();
						if ( state.backtracking==0 ) adaptor.addChild(root_0, (CommonTree)adaptor.create(set114));
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					pushFollow(FOLLOW_arguments_in_explicitConstructorInvocation2088);
					arguments115=arguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arguments115.getTree());

					char_literal116=(Token)match(input,SEMI,FOLLOW_SEMI_in_explicitConstructorInvocation2090); if (state.failed) return retval;
					}
					break;
				case 2 :
					// Java.g:470:4: primary '.' ( nonWildcardTypeArguments )? 'super' arguments ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_primary_in_explicitConstructorInvocation2096);
					primary117=primary();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, primary117.getTree());

					char_literal118=(Token)match(input,DOT,FOLLOW_DOT_in_explicitConstructorInvocation2100); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal118_tree = (CommonTree)adaptor.create(char_literal118);
					adaptor.addChild(root_0, char_literal118_tree);
					}

					// Java.g:472:3: ( nonWildcardTypeArguments )?
					int alt70=2;
					int LA70_0 = input.LA(1);
					if ( (LA70_0==LT) ) {
						alt70=1;
					}
					switch (alt70) {
						case 1 :
							// Java.g:472:4: nonWildcardTypeArguments
							{
							pushFollow(FOLLOW_nonWildcardTypeArguments_in_explicitConstructorInvocation2105);
							nonWildcardTypeArguments119=nonWildcardTypeArguments();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, nonWildcardTypeArguments119.getTree());

							}
							break;

					}

					string_literal120=(Token)match(input,SUPER,FOLLOW_SUPER_in_explicitConstructorInvocation2111); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal120_tree = (CommonTree)adaptor.create(string_literal120);
					adaptor.addChild(root_0, string_literal120_tree);
					}

					pushFollow(FOLLOW_arguments_in_explicitConstructorInvocation2115);
					arguments121=arguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arguments121.getTree());

					char_literal122=(Token)match(input,SEMI,FOLLOW_SEMI_in_explicitConstructorInvocation2117); if (state.failed) return retval;
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 41, explicitConstructorInvocation_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "explicitConstructorInvocation"


	public static class qualifiedName_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "qualifiedName"
	// Java.g:478:1: qualifiedName : (a= IDENTIFIER b= '.' c= qualifiedName -> ^( SEQUENCE[$b] $a $c) |a= IDENTIFIER -> $a);
	public final JavaParser.qualifiedName_return qualifiedName() throws RecognitionException {
		JavaParser.qualifiedName_return retval = new JavaParser.qualifiedName_return();
		retval.start = input.LT(1);
		int qualifiedName_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token b=null;
		ParserRuleReturnScope c =null;

		CommonTree a_tree=null;
		CommonTree b_tree=null;
		RewriteRuleTokenStream stream_DOT=new RewriteRuleTokenStream(adaptor,"token DOT");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_qualifiedName=new RewriteRuleSubtreeStream(adaptor,"rule qualifiedName");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 42) ) { return retval; }

			// Java.g:479:2: (a= IDENTIFIER b= '.' c= qualifiedName -> ^( SEQUENCE[$b] $a $c) |a= IDENTIFIER -> $a)
			int alt72=2;
			int LA72_0 = input.LA(1);
			if ( (LA72_0==IDENTIFIER) ) {
				int LA72_1 = input.LA(2);
				if ( (LA72_1==DOT) ) {
					alt72=1;
				}
				else if ( (LA72_1==EOF||LA72_1==ABSTRACT||LA72_1==BOOLEAN||LA72_1==BYTE||LA72_1==CHAR||LA72_1==CLASS||LA72_1==COMMA||LA72_1==DOUBLE||LA72_1==ENUM||LA72_1==FINAL||LA72_1==FLOAT||LA72_1==IDENTIFIER||(LA72_1 >= INT && LA72_1 <= INTERFACE)||LA72_1==LBRACE||LA72_1==LONG||(LA72_1 >= LPAREN && LA72_1 <= LT)||(LA72_1 >= MONKEYS_AT && LA72_1 <= NATIVE)||LA72_1==PACKAGE||(LA72_1 >= PRIVATE && LA72_1 <= PUBLIC)||LA72_1==RBRACE||(LA72_1 >= RPAREN && LA72_1 <= SEMI)||LA72_1==SHORT||(LA72_1 >= STATIC && LA72_1 <= STRICTFP)||LA72_1==SYNCHRONIZED||LA72_1==TRANSIENT||(LA72_1 >= VOID && LA72_1 <= VOLATILE)) ) {
					alt72=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 72, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 72, 0, input);
				throw nvae;
			}

			switch (alt72) {
				case 1 :
					// Java.g:479:4: a= IDENTIFIER b= '.' c= qualifiedName
					{
					a=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_qualifiedName2132); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(a);

					b=(Token)match(input,DOT,FOLLOW_DOT_in_qualifiedName2136); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOT.add(b);

					pushFollow(FOLLOW_qualifiedName_in_qualifiedName2140);
					c=qualifiedName();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_qualifiedName.add(c.getTree());
					// AST REWRITE
					// elements: c, a
					// token labels: a
					// rule labels: retval, c
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"rule c",c!=null?c.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 480:2: -> ^( SEQUENCE[$b] $a $c)
					{
						// Java.g:480:5: ^( SEQUENCE[$b] $a $c)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(SEQUENCE, b), root_1);
						adaptor.addChild(root_1, stream_a.nextNode());
						adaptor.addChild(root_1, stream_c.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:481:4: a= IDENTIFIER
					{
					a=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_qualifiedName2161); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(a);

					// AST REWRITE
					// elements: a
					// token labels: a
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 482:2: -> $a
					{
						adaptor.addChild(root_0, stream_a.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 42, qualifiedName_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "qualifiedName"


	public static class qualifiedImportName_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "qualifiedImportName"
	// Java.g:486:1: qualifiedImportName : (a= IDENTIFIER b= '.' c= qualifiedImportName -> ^( SEQUENCE[$b] $a $c) |a= IDENTIFIER -> $a| '*' );
	public final JavaParser.qualifiedImportName_return qualifiedImportName() throws RecognitionException {
		JavaParser.qualifiedImportName_return retval = new JavaParser.qualifiedImportName_return();
		retval.start = input.LT(1);
		int qualifiedImportName_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token b=null;
		Token char_literal123=null;
		ParserRuleReturnScope c =null;

		CommonTree a_tree=null;
		CommonTree b_tree=null;
		CommonTree char_literal123_tree=null;
		RewriteRuleTokenStream stream_DOT=new RewriteRuleTokenStream(adaptor,"token DOT");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_qualifiedImportName=new RewriteRuleSubtreeStream(adaptor,"rule qualifiedImportName");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 43) ) { return retval; }

			// Java.g:487:2: (a= IDENTIFIER b= '.' c= qualifiedImportName -> ^( SEQUENCE[$b] $a $c) |a= IDENTIFIER -> $a| '*' )
			int alt73=3;
			int LA73_0 = input.LA(1);
			if ( (LA73_0==IDENTIFIER) ) {
				int LA73_1 = input.LA(2);
				if ( (LA73_1==DOT) ) {
					alt73=1;
				}
				else if ( (LA73_1==EOF||LA73_1==SEMI) ) {
					alt73=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 73, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA73_0==STAR) ) {
				alt73=3;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 73, 0, input);
				throw nvae;
			}

			switch (alt73) {
				case 1 :
					// Java.g:487:4: a= IDENTIFIER b= '.' c= qualifiedImportName
					{
					a=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_qualifiedImportName2181); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(a);

					b=(Token)match(input,DOT,FOLLOW_DOT_in_qualifiedImportName2185); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOT.add(b);

					pushFollow(FOLLOW_qualifiedImportName_in_qualifiedImportName2189);
					c=qualifiedImportName();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_qualifiedImportName.add(c.getTree());
					// AST REWRITE
					// elements: c, a
					// token labels: a
					// rule labels: retval, c
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"rule c",c!=null?c.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 488:2: -> ^( SEQUENCE[$b] $a $c)
					{
						// Java.g:488:5: ^( SEQUENCE[$b] $a $c)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(SEQUENCE, b), root_1);
						adaptor.addChild(root_1, stream_a.nextNode());
						adaptor.addChild(root_1, stream_c.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:489:4: a= IDENTIFIER
					{
					a=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_qualifiedImportName2210); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(a);

					// AST REWRITE
					// elements: a
					// token labels: a
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 490:2: -> $a
					{
						adaptor.addChild(root_0, stream_a.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// Java.g:491:4: '*'
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal123=(Token)match(input,STAR,FOLLOW_STAR_in_qualifiedImportName2221); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal123_tree = (CommonTree)adaptor.create(char_literal123);
					adaptor.addChild(root_0, char_literal123_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 43, qualifiedImportName_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "qualifiedImportName"


	public static class annotations_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "annotations"
	// Java.g:495:1: annotations : ( annotation )+ -> ^( ANNOTATION ( annotation )+ ) ;
	public final JavaParser.annotations_return annotations() throws RecognitionException {
		JavaParser.annotations_return retval = new JavaParser.annotations_return();
		retval.start = input.LT(1);
		int annotations_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope annotation124 =null;

		RewriteRuleSubtreeStream stream_annotation=new RewriteRuleSubtreeStream(adaptor,"rule annotation");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 44) ) { return retval; }

			// Java.g:496:2: ( ( annotation )+ -> ^( ANNOTATION ( annotation )+ ) )
			// Java.g:496:4: ( annotation )+
			{
			// Java.g:496:4: ( annotation )+
			int cnt74=0;
			loop74:
			while (true) {
				int alt74=2;
				int LA74_0 = input.LA(1);
				if ( (LA74_0==MONKEYS_AT) ) {
					alt74=1;
				}

				switch (alt74) {
				case 1 :
					// Java.g:496:5: annotation
					{
					pushFollow(FOLLOW_annotation_in_annotations2234);
					annotation124=annotation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_annotation.add(annotation124.getTree());
					}
					break;

				default :
					if ( cnt74 >= 1 ) break loop74;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(74, input);
					throw eee;
				}
				cnt74++;
			}

			// AST REWRITE
			// elements: annotation
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 497:2: -> ^( ANNOTATION ( annotation )+ )
			{
				// Java.g:497:5: ^( ANNOTATION ( annotation )+ )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ANNOTATION, "ANNOTATION"), root_1);
				if ( !(stream_annotation.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_annotation.hasNext() ) {
					adaptor.addChild(root_1, stream_annotation.nextTree());
				}
				stream_annotation.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 44, annotations_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "annotations"


	public static class annotation_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "annotation"
	// Java.g:505:1: annotation : '@' qualifiedName ( '(' ( elementValuePairs | elementValue )? ')' )? -> ^( ANNOTATION qualifiedName ) ;
	public final JavaParser.annotation_return annotation() throws RecognitionException {
		JavaParser.annotation_return retval = new JavaParser.annotation_return();
		retval.start = input.LT(1);
		int annotation_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal125=null;
		Token char_literal127=null;
		Token char_literal130=null;
		ParserRuleReturnScope qualifiedName126 =null;
		ParserRuleReturnScope elementValuePairs128 =null;
		ParserRuleReturnScope elementValue129 =null;

		CommonTree char_literal125_tree=null;
		CommonTree char_literal127_tree=null;
		CommonTree char_literal130_tree=null;
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_MONKEYS_AT=new RewriteRuleTokenStream(adaptor,"token MONKEYS_AT");
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleSubtreeStream stream_elementValue=new RewriteRuleSubtreeStream(adaptor,"rule elementValue");
		RewriteRuleSubtreeStream stream_qualifiedName=new RewriteRuleSubtreeStream(adaptor,"rule qualifiedName");
		RewriteRuleSubtreeStream stream_elementValuePairs=new RewriteRuleSubtreeStream(adaptor,"rule elementValuePairs");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 45) ) { return retval; }

			// Java.g:506:2: ( '@' qualifiedName ( '(' ( elementValuePairs | elementValue )? ')' )? -> ^( ANNOTATION qualifiedName ) )
			// Java.g:506:4: '@' qualifiedName ( '(' ( elementValuePairs | elementValue )? ')' )?
			{
			char_literal125=(Token)match(input,MONKEYS_AT,FOLLOW_MONKEYS_AT_in_annotation2260); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_MONKEYS_AT.add(char_literal125);

			pushFollow(FOLLOW_qualifiedName_in_annotation2262);
			qualifiedName126=qualifiedName();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_qualifiedName.add(qualifiedName126.getTree());
			// Java.g:507:3: ( '(' ( elementValuePairs | elementValue )? ')' )?
			int alt76=2;
			int LA76_0 = input.LA(1);
			if ( (LA76_0==LPAREN) ) {
				alt76=1;
			}
			switch (alt76) {
				case 1 :
					// Java.g:507:4: '(' ( elementValuePairs | elementValue )? ')'
					{
					char_literal127=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_annotation2267); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(char_literal127);

					// Java.g:508:4: ( elementValuePairs | elementValue )?
					int alt75=3;
					int LA75_0 = input.LA(1);
					if ( (LA75_0==IDENTIFIER) ) {
						int LA75_1 = input.LA(2);
						if ( (LA75_1==EQ) ) {
							alt75=1;
						}
						else if ( ((LA75_1 >= AMP && LA75_1 <= AMPAMP)||(LA75_1 >= BANGEQ && LA75_1 <= BARBAR)||LA75_1==CARET||LA75_1==DOT||LA75_1==EQEQ||LA75_1==GT||LA75_1==INSTANCEOF||LA75_1==LBRACKET||(LA75_1 >= LPAREN && LA75_1 <= LT)||LA75_1==PERCENT||LA75_1==PLUS||LA75_1==PLUSPLUS||LA75_1==QUES||LA75_1==RPAREN||LA75_1==SLASH||LA75_1==STAR||LA75_1==SUB||LA75_1==SUBSUB) ) {
							alt75=2;
						}
					}
					else if ( (LA75_0==BANG||LA75_0==BOOLEAN||LA75_0==BYTE||(LA75_0 >= CHAR && LA75_0 <= CHARLITERAL)||(LA75_0 >= DOUBLE && LA75_0 <= DOUBLELITERAL)||LA75_0==FALSE||(LA75_0 >= FLOAT && LA75_0 <= FLOATLITERAL)||LA75_0==INT||LA75_0==INTLITERAL||LA75_0==LBRACE||(LA75_0 >= LONG && LA75_0 <= LPAREN)||LA75_0==MONKEYS_AT||(LA75_0 >= NEW && LA75_0 <= NULL)||LA75_0==PLUS||LA75_0==PLUSPLUS||LA75_0==SHORT||(LA75_0 >= STRINGLITERAL && LA75_0 <= SUB)||(LA75_0 >= SUBSUB && LA75_0 <= SUPER)||LA75_0==THIS||LA75_0==TILDE||LA75_0==TRUE||LA75_0==VOID) ) {
						alt75=2;
					}
					switch (alt75) {
						case 1 :
							// Java.g:508:6: elementValuePairs
							{
							pushFollow(FOLLOW_elementValuePairs_in_annotation2274);
							elementValuePairs128=elementValuePairs();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_elementValuePairs.add(elementValuePairs128.getTree());
							}
							break;
						case 2 :
							// Java.g:509:6: elementValue
							{
							pushFollow(FOLLOW_elementValue_in_annotation2281);
							elementValue129=elementValue();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_elementValue.add(elementValue129.getTree());
							}
							break;

					}

					char_literal130=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_annotation2288); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(char_literal130);

					}
					break;

			}

			// AST REWRITE
			// elements: qualifiedName
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 511:2: -> ^( ANNOTATION qualifiedName )
			{
				// Java.g:511:5: ^( ANNOTATION qualifiedName )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ANNOTATION, "ANNOTATION"), root_1);
				adaptor.addChild(root_1, stream_qualifiedName.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 45, annotation_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "annotation"


	public static class elementValuePairs_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "elementValuePairs"
	// Java.g:515:1: elementValuePairs : elementValuePair ( ',' elementValuePair )* -> ( elementValuePair )+ ;
	public final JavaParser.elementValuePairs_return elementValuePairs() throws RecognitionException {
		JavaParser.elementValuePairs_return retval = new JavaParser.elementValuePairs_return();
		retval.start = input.LT(1);
		int elementValuePairs_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal132=null;
		ParserRuleReturnScope elementValuePair131 =null;
		ParserRuleReturnScope elementValuePair133 =null;

		CommonTree char_literal132_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_elementValuePair=new RewriteRuleSubtreeStream(adaptor,"rule elementValuePair");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 46) ) { return retval; }

			// Java.g:516:2: ( elementValuePair ( ',' elementValuePair )* -> ( elementValuePair )+ )
			// Java.g:516:4: elementValuePair ( ',' elementValuePair )*
			{
			pushFollow(FOLLOW_elementValuePair_in_elementValuePairs2311);
			elementValuePair131=elementValuePair();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_elementValuePair.add(elementValuePair131.getTree());
			// Java.g:517:3: ( ',' elementValuePair )*
			loop77:
			while (true) {
				int alt77=2;
				int LA77_0 = input.LA(1);
				if ( (LA77_0==COMMA) ) {
					alt77=1;
				}

				switch (alt77) {
				case 1 :
					// Java.g:517:4: ',' elementValuePair
					{
					char_literal132=(Token)match(input,COMMA,FOLLOW_COMMA_in_elementValuePairs2316); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(char_literal132);

					pushFollow(FOLLOW_elementValuePair_in_elementValuePairs2318);
					elementValuePair133=elementValuePair();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_elementValuePair.add(elementValuePair133.getTree());
					}
					break;

				default :
					break loop77;
				}
			}

			// AST REWRITE
			// elements: elementValuePair
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 518:2: -> ( elementValuePair )+
			{
				if ( !(stream_elementValuePair.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_elementValuePair.hasNext() ) {
					adaptor.addChild(root_0, stream_elementValuePair.nextTree());
				}
				stream_elementValuePair.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 46, elementValuePairs_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "elementValuePairs"


	public static class elementValuePair_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "elementValuePair"
	// Java.g:522:1: elementValuePair : IDENTIFIER '=' ^ elementValue ;
	public final JavaParser.elementValuePair_return elementValuePair() throws RecognitionException {
		JavaParser.elementValuePair_return retval = new JavaParser.elementValuePair_return();
		retval.start = input.LT(1);
		int elementValuePair_StartIndex = input.index();

		CommonTree root_0 = null;

		Token IDENTIFIER134=null;
		Token char_literal135=null;
		ParserRuleReturnScope elementValue136 =null;

		CommonTree IDENTIFIER134_tree=null;
		CommonTree char_literal135_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 47) ) { return retval; }

			// Java.g:523:2: ( IDENTIFIER '=' ^ elementValue )
			// Java.g:523:4: IDENTIFIER '=' ^ elementValue
			{
			root_0 = (CommonTree)adaptor.nil();


			IDENTIFIER134=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_elementValuePair2338); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER134_tree = (CommonTree)adaptor.create(IDENTIFIER134);
			adaptor.addChild(root_0, IDENTIFIER134_tree);
			}

			char_literal135=(Token)match(input,EQ,FOLLOW_EQ_in_elementValuePair2340); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal135_tree = (CommonTree)adaptor.create(char_literal135);
			root_0 = (CommonTree)adaptor.becomeRoot(char_literal135_tree, root_0);
			}

			pushFollow(FOLLOW_elementValue_in_elementValuePair2343);
			elementValue136=elementValue();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, elementValue136.getTree());

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 47, elementValuePair_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "elementValuePair"


	public static class elementValue_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "elementValue"
	// Java.g:527:1: elementValue : ( conditionalExpression | annotation | elementValueArrayInitializer );
	public final JavaParser.elementValue_return elementValue() throws RecognitionException {
		JavaParser.elementValue_return retval = new JavaParser.elementValue_return();
		retval.start = input.LT(1);
		int elementValue_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope conditionalExpression137 =null;
		ParserRuleReturnScope annotation138 =null;
		ParserRuleReturnScope elementValueArrayInitializer139 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 48) ) { return retval; }

			// Java.g:528:2: ( conditionalExpression | annotation | elementValueArrayInitializer )
			int alt78=3;
			switch ( input.LA(1) ) {
			case BANG:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case CHARLITERAL:
			case DOUBLE:
			case DOUBLELITERAL:
			case FALSE:
			case FLOAT:
			case FLOATLITERAL:
			case IDENTIFIER:
			case INT:
			case INTLITERAL:
			case LONG:
			case LONGLITERAL:
			case LPAREN:
			case NEW:
			case NULL:
			case PLUS:
			case PLUSPLUS:
			case SHORT:
			case STRINGLITERAL:
			case SUB:
			case SUBSUB:
			case SUPER:
			case THIS:
			case TILDE:
			case TRUE:
			case VOID:
				{
				alt78=1;
				}
				break;
			case MONKEYS_AT:
				{
				alt78=2;
				}
				break;
			case LBRACE:
				{
				alt78=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 78, 0, input);
				throw nvae;
			}
			switch (alt78) {
				case 1 :
					// Java.g:528:4: conditionalExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_conditionalExpression_in_elementValue2355);
					conditionalExpression137=conditionalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, conditionalExpression137.getTree());

					}
					break;
				case 2 :
					// Java.g:529:4: annotation
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_annotation_in_elementValue2360);
					annotation138=annotation();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, annotation138.getTree());

					}
					break;
				case 3 :
					// Java.g:530:4: elementValueArrayInitializer
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_elementValueArrayInitializer_in_elementValue2365);
					elementValueArrayInitializer139=elementValueArrayInitializer();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, elementValueArrayInitializer139.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 48, elementValue_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "elementValue"


	public static class elementValueArrayInitializer_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "elementValueArrayInitializer"
	// Java.g:534:1: elementValueArrayInitializer : '{' ( elementValue ( ',' elementValue )* )? ( ',' )? '}' ;
	public final JavaParser.elementValueArrayInitializer_return elementValueArrayInitializer() throws RecognitionException {
		JavaParser.elementValueArrayInitializer_return retval = new JavaParser.elementValueArrayInitializer_return();
		retval.start = input.LT(1);
		int elementValueArrayInitializer_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal140=null;
		Token char_literal142=null;
		Token char_literal144=null;
		Token char_literal145=null;
		ParserRuleReturnScope elementValue141 =null;
		ParserRuleReturnScope elementValue143 =null;

		CommonTree char_literal140_tree=null;
		CommonTree char_literal142_tree=null;
		CommonTree char_literal144_tree=null;
		CommonTree char_literal145_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 49) ) { return retval; }

			// Java.g:535:2: ( '{' ( elementValue ( ',' elementValue )* )? ( ',' )? '}' )
			// Java.g:535:4: '{' ( elementValue ( ',' elementValue )* )? ( ',' )? '}'
			{
			root_0 = (CommonTree)adaptor.nil();


			char_literal140=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_elementValueArrayInitializer2377); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal140_tree = (CommonTree)adaptor.create(char_literal140);
			adaptor.addChild(root_0, char_literal140_tree);
			}

			// Java.g:536:3: ( elementValue ( ',' elementValue )* )?
			int alt80=2;
			int LA80_0 = input.LA(1);
			if ( (LA80_0==BANG||LA80_0==BOOLEAN||LA80_0==BYTE||(LA80_0 >= CHAR && LA80_0 <= CHARLITERAL)||(LA80_0 >= DOUBLE && LA80_0 <= DOUBLELITERAL)||LA80_0==FALSE||(LA80_0 >= FLOAT && LA80_0 <= FLOATLITERAL)||LA80_0==IDENTIFIER||LA80_0==INT||LA80_0==INTLITERAL||LA80_0==LBRACE||(LA80_0 >= LONG && LA80_0 <= LPAREN)||LA80_0==MONKEYS_AT||(LA80_0 >= NEW && LA80_0 <= NULL)||LA80_0==PLUS||LA80_0==PLUSPLUS||LA80_0==SHORT||(LA80_0 >= STRINGLITERAL && LA80_0 <= SUB)||(LA80_0 >= SUBSUB && LA80_0 <= SUPER)||LA80_0==THIS||LA80_0==TILDE||LA80_0==TRUE||LA80_0==VOID) ) {
				alt80=1;
			}
			switch (alt80) {
				case 1 :
					// Java.g:536:4: elementValue ( ',' elementValue )*
					{
					pushFollow(FOLLOW_elementValue_in_elementValueArrayInitializer2382);
					elementValue141=elementValue();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, elementValue141.getTree());

					// Java.g:537:4: ( ',' elementValue )*
					loop79:
					while (true) {
						int alt79=2;
						int LA79_0 = input.LA(1);
						if ( (LA79_0==COMMA) ) {
							int LA79_1 = input.LA(2);
							if ( (LA79_1==BANG||LA79_1==BOOLEAN||LA79_1==BYTE||(LA79_1 >= CHAR && LA79_1 <= CHARLITERAL)||(LA79_1 >= DOUBLE && LA79_1 <= DOUBLELITERAL)||LA79_1==FALSE||(LA79_1 >= FLOAT && LA79_1 <= FLOATLITERAL)||LA79_1==IDENTIFIER||LA79_1==INT||LA79_1==INTLITERAL||LA79_1==LBRACE||(LA79_1 >= LONG && LA79_1 <= LPAREN)||LA79_1==MONKEYS_AT||(LA79_1 >= NEW && LA79_1 <= NULL)||LA79_1==PLUS||LA79_1==PLUSPLUS||LA79_1==SHORT||(LA79_1 >= STRINGLITERAL && LA79_1 <= SUB)||(LA79_1 >= SUBSUB && LA79_1 <= SUPER)||LA79_1==THIS||LA79_1==TILDE||LA79_1==TRUE||LA79_1==VOID) ) {
								alt79=1;
							}

						}

						switch (alt79) {
						case 1 :
							// Java.g:537:5: ',' elementValue
							{
							char_literal142=(Token)match(input,COMMA,FOLLOW_COMMA_in_elementValueArrayInitializer2388); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal142_tree = (CommonTree)adaptor.create(char_literal142);
							adaptor.addChild(root_0, char_literal142_tree);
							}

							pushFollow(FOLLOW_elementValue_in_elementValueArrayInitializer2390);
							elementValue143=elementValue();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, elementValue143.getTree());

							}
							break;

						default :
							break loop79;
						}
					}

					}
					break;

			}

			// Java.g:538:6: ( ',' )?
			int alt81=2;
			int LA81_0 = input.LA(1);
			if ( (LA81_0==COMMA) ) {
				alt81=1;
			}
			switch (alt81) {
				case 1 :
					// Java.g:538:7: ','
					{
					char_literal144=(Token)match(input,COMMA,FOLLOW_COMMA_in_elementValueArrayInitializer2400); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal144_tree = (CommonTree)adaptor.create(char_literal144);
					adaptor.addChild(root_0, char_literal144_tree);
					}

					}
					break;

			}

			char_literal145=(Token)match(input,RBRACE,FOLLOW_RBRACE_in_elementValueArrayInitializer2404); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal145_tree = (CommonTree)adaptor.create(char_literal145);
			adaptor.addChild(root_0, char_literal145_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 49, elementValueArrayInitializer_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "elementValueArrayInitializer"


	public static class annotationTypeDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "annotationTypeDeclaration"
	// Java.g:545:1: annotationTypeDeclaration : modifiers ! '@' 'interface' IDENTIFIER annotationTypeBody ;
	public final JavaParser.annotationTypeDeclaration_return annotationTypeDeclaration() throws RecognitionException {
		JavaParser.annotationTypeDeclaration_return retval = new JavaParser.annotationTypeDeclaration_return();
		retval.start = input.LT(1);
		int annotationTypeDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal147=null;
		Token string_literal148=null;
		Token IDENTIFIER149=null;
		ParserRuleReturnScope modifiers146 =null;
		ParserRuleReturnScope annotationTypeBody150 =null;

		CommonTree char_literal147_tree=null;
		CommonTree string_literal148_tree=null;
		CommonTree IDENTIFIER149_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 50) ) { return retval; }

			// Java.g:546:2: ( modifiers ! '@' 'interface' IDENTIFIER annotationTypeBody )
			// Java.g:546:4: modifiers ! '@' 'interface' IDENTIFIER annotationTypeBody
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_modifiers_in_annotationTypeDeclaration2418);
			modifiers146=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			char_literal147=(Token)match(input,MONKEYS_AT,FOLLOW_MONKEYS_AT_in_annotationTypeDeclaration2421); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal147_tree = (CommonTree)adaptor.create(char_literal147);
			adaptor.addChild(root_0, char_literal147_tree);
			}

			string_literal148=(Token)match(input,INTERFACE,FOLLOW_INTERFACE_in_annotationTypeDeclaration2425); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal148_tree = (CommonTree)adaptor.create(string_literal148);
			adaptor.addChild(root_0, string_literal148_tree);
			}

			IDENTIFIER149=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_annotationTypeDeclaration2429); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER149_tree = (CommonTree)adaptor.create(IDENTIFIER149);
			adaptor.addChild(root_0, IDENTIFIER149_tree);
			}

			pushFollow(FOLLOW_annotationTypeBody_in_annotationTypeDeclaration2433);
			annotationTypeBody150=annotationTypeBody();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, annotationTypeBody150.getTree());

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 50, annotationTypeDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "annotationTypeDeclaration"


	public static class annotationTypeBody_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "annotationTypeBody"
	// Java.g:553:1: annotationTypeBody : a= '{' (b+= annotationTypeElementDeclaration )* '}' -> ^( BLOCK[$a,\"BLOCK\"] ( $b)* ) ;
	public final JavaParser.annotationTypeBody_return annotationTypeBody() throws RecognitionException {
		JavaParser.annotationTypeBody_return retval = new JavaParser.annotationTypeBody_return();
		retval.start = input.LT(1);
		int annotationTypeBody_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token char_literal151=null;
		List<Object> list_b=null;
		RuleReturnScope b = null;
		CommonTree a_tree=null;
		CommonTree char_literal151_tree=null;
		RewriteRuleTokenStream stream_RBRACE=new RewriteRuleTokenStream(adaptor,"token RBRACE");
		RewriteRuleTokenStream stream_LBRACE=new RewriteRuleTokenStream(adaptor,"token LBRACE");
		RewriteRuleSubtreeStream stream_annotationTypeElementDeclaration=new RewriteRuleSubtreeStream(adaptor,"rule annotationTypeElementDeclaration");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 51) ) { return retval; }

			// Java.g:554:2: (a= '{' (b+= annotationTypeElementDeclaration )* '}' -> ^( BLOCK[$a,\"BLOCK\"] ( $b)* ) )
			// Java.g:554:4: a= '{' (b+= annotationTypeElementDeclaration )* '}'
			{
			a=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_annotationTypeBody2447); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LBRACE.add(a);

			// Java.g:555:4: (b+= annotationTypeElementDeclaration )*
			loop82:
			while (true) {
				int alt82=2;
				int LA82_0 = input.LA(1);
				if ( (LA82_0==ABSTRACT||LA82_0==BOOLEAN||LA82_0==BYTE||LA82_0==CHAR||LA82_0==CLASS||LA82_0==DOUBLE||LA82_0==ENUM||LA82_0==FINAL||LA82_0==FLOAT||LA82_0==IDENTIFIER||(LA82_0 >= INT && LA82_0 <= INTERFACE)||LA82_0==LONG||LA82_0==LT||(LA82_0 >= MONKEYS_AT && LA82_0 <= NATIVE)||(LA82_0 >= PRIVATE && LA82_0 <= PUBLIC)||LA82_0==SEMI||LA82_0==SHORT||(LA82_0 >= STATIC && LA82_0 <= STRICTFP)||LA82_0==SYNCHRONIZED||LA82_0==TRANSIENT||(LA82_0 >= VOID && LA82_0 <= VOLATILE)) ) {
					alt82=1;
				}

				switch (alt82) {
				case 1 :
					// Java.g:555:4: b+= annotationTypeElementDeclaration
					{
					pushFollow(FOLLOW_annotationTypeElementDeclaration_in_annotationTypeBody2453);
					b=annotationTypeElementDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_annotationTypeElementDeclaration.add(b.getTree());
					if (list_b==null) list_b=new ArrayList<Object>();
					list_b.add(b.getTree());
					}
					break;

				default :
					break loop82;
				}
			}

			char_literal151=(Token)match(input,RBRACE,FOLLOW_RBRACE_in_annotationTypeBody2458); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RBRACE.add(char_literal151);

			// AST REWRITE
			// elements: b
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: b
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"token b",list_b);
			root_0 = (CommonTree)adaptor.nil();
			// 557:2: -> ^( BLOCK[$a,\"BLOCK\"] ( $b)* )
			{
				// Java.g:557:5: ^( BLOCK[$a,\"BLOCK\"] ( $b)* )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BLOCK, a, "BLOCK"), root_1);
				// Java.g:557:26: ( $b)*
				while ( stream_b.hasNext() ) {
					adaptor.addChild(root_1, stream_b.nextTree());
				}
				stream_b.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 51, annotationTypeBody_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "annotationTypeBody"


	public static class annotationTypeElementDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "annotationTypeElementDeclaration"
	// Java.g:564:1: annotationTypeElementDeclaration : ( annotationMethodDeclaration | interfaceFieldDeclaration | normalClassDeclaration | normalInterfaceDeclaration | enumDeclaration | annotationTypeDeclaration | ';' !);
	public final JavaParser.annotationTypeElementDeclaration_return annotationTypeElementDeclaration() throws RecognitionException {
		JavaParser.annotationTypeElementDeclaration_return retval = new JavaParser.annotationTypeElementDeclaration_return();
		retval.start = input.LT(1);
		int annotationTypeElementDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal158=null;
		ParserRuleReturnScope annotationMethodDeclaration152 =null;
		ParserRuleReturnScope interfaceFieldDeclaration153 =null;
		ParserRuleReturnScope normalClassDeclaration154 =null;
		ParserRuleReturnScope normalInterfaceDeclaration155 =null;
		ParserRuleReturnScope enumDeclaration156 =null;
		ParserRuleReturnScope annotationTypeDeclaration157 =null;

		CommonTree char_literal158_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 52) ) { return retval; }

			// Java.g:565:2: ( annotationMethodDeclaration | interfaceFieldDeclaration | normalClassDeclaration | normalInterfaceDeclaration | enumDeclaration | annotationTypeDeclaration | ';' !)
			int alt83=7;
			switch ( input.LA(1) ) {
			case MONKEYS_AT:
				{
				int LA83_1 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case PUBLIC:
				{
				int LA83_2 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case PROTECTED:
				{
				int LA83_3 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case PRIVATE:
				{
				int LA83_4 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case STATIC:
				{
				int LA83_5 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 5, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case ABSTRACT:
				{
				int LA83_6 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 6, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case FINAL:
				{
				int LA83_7 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 7, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case NATIVE:
				{
				int LA83_8 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 8, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case SYNCHRONIZED:
				{
				int LA83_9 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 9, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case TRANSIENT:
				{
				int LA83_10 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 10, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case VOLATILE:
				{
				int LA83_11 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 11, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case STRICTFP:
				{
				int LA83_12 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}
				else if ( (synpred116_Java()) ) {
					alt83=3;
				}
				else if ( (synpred117_Java()) ) {
					alt83=4;
				}
				else if ( (synpred118_Java()) ) {
					alt83=5;
				}
				else if ( (synpred119_Java()) ) {
					alt83=6;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 12, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA83_13 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 13, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				{
				int LA83_14 = input.LA(2);
				if ( (synpred114_Java()) ) {
					alt83=1;
				}
				else if ( (synpred115_Java()) ) {
					alt83=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 83, 14, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case CLASS:
				{
				alt83=3;
				}
				break;
			case INTERFACE:
				{
				alt83=4;
				}
				break;
			case ENUM:
				{
				alt83=5;
				}
				break;
			case SEMI:
				{
				alt83=7;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 83, 0, input);
				throw nvae;
			}
			switch (alt83) {
				case 1 :
					// Java.g:565:4: annotationMethodDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_annotationMethodDeclaration_in_annotationTypeElementDeclaration2484);
					annotationMethodDeclaration152=annotationMethodDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, annotationMethodDeclaration152.getTree());

					}
					break;
				case 2 :
					// Java.g:566:4: interfaceFieldDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_interfaceFieldDeclaration_in_annotationTypeElementDeclaration2489);
					interfaceFieldDeclaration153=interfaceFieldDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, interfaceFieldDeclaration153.getTree());

					}
					break;
				case 3 :
					// Java.g:567:4: normalClassDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_normalClassDeclaration_in_annotationTypeElementDeclaration2494);
					normalClassDeclaration154=normalClassDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, normalClassDeclaration154.getTree());

					}
					break;
				case 4 :
					// Java.g:568:4: normalInterfaceDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_normalInterfaceDeclaration_in_annotationTypeElementDeclaration2499);
					normalInterfaceDeclaration155=normalInterfaceDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, normalInterfaceDeclaration155.getTree());

					}
					break;
				case 5 :
					// Java.g:569:4: enumDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_enumDeclaration_in_annotationTypeElementDeclaration2504);
					enumDeclaration156=enumDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, enumDeclaration156.getTree());

					}
					break;
				case 6 :
					// Java.g:570:4: annotationTypeDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_annotationTypeDeclaration_in_annotationTypeElementDeclaration2509);
					annotationTypeDeclaration157=annotationTypeDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, annotationTypeDeclaration157.getTree());

					}
					break;
				case 7 :
					// Java.g:571:4: ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal158=(Token)match(input,SEMI,FOLLOW_SEMI_in_annotationTypeElementDeclaration2514); if (state.failed) return retval;
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 52, annotationTypeElementDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "annotationTypeElementDeclaration"


	public static class annotationMethodDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "annotationMethodDeclaration"
	// Java.g:575:1: annotationMethodDeclaration : modifiers type IDENTIFIER '(' ')' ( 'default' elementValue )? ';' -> ^( ANNOTATION IDENTIFIER ( elementValue )? ) ;
	public final JavaParser.annotationMethodDeclaration_return annotationMethodDeclaration() throws RecognitionException {
		JavaParser.annotationMethodDeclaration_return retval = new JavaParser.annotationMethodDeclaration_return();
		retval.start = input.LT(1);
		int annotationMethodDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token IDENTIFIER161=null;
		Token char_literal162=null;
		Token char_literal163=null;
		Token string_literal164=null;
		Token char_literal166=null;
		ParserRuleReturnScope modifiers159 =null;
		ParserRuleReturnScope type160 =null;
		ParserRuleReturnScope elementValue165 =null;

		CommonTree IDENTIFIER161_tree=null;
		CommonTree char_literal162_tree=null;
		CommonTree char_literal163_tree=null;
		CommonTree string_literal164_tree=null;
		CommonTree char_literal166_tree=null;
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_SEMI=new RewriteRuleTokenStream(adaptor,"token SEMI");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleTokenStream stream_DEFAULT=new RewriteRuleTokenStream(adaptor,"token DEFAULT");
		RewriteRuleSubtreeStream stream_elementValue=new RewriteRuleSubtreeStream(adaptor,"rule elementValue");
		RewriteRuleSubtreeStream stream_modifiers=new RewriteRuleSubtreeStream(adaptor,"rule modifiers");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 53) ) { return retval; }

			// Java.g:576:2: ( modifiers type IDENTIFIER '(' ')' ( 'default' elementValue )? ';' -> ^( ANNOTATION IDENTIFIER ( elementValue )? ) )
			// Java.g:576:4: modifiers type IDENTIFIER '(' ')' ( 'default' elementValue )? ';'
			{
			pushFollow(FOLLOW_modifiers_in_annotationMethodDeclaration2527);
			modifiers159=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_modifiers.add(modifiers159.getTree());
			pushFollow(FOLLOW_type_in_annotationMethodDeclaration2531);
			type160=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_type.add(type160.getTree());
			IDENTIFIER161=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_annotationMethodDeclaration2535); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER161);

			char_literal162=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_annotationMethodDeclaration2537); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LPAREN.add(char_literal162);

			char_literal163=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_annotationMethodDeclaration2539); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RPAREN.add(char_literal163);

			// Java.g:579:3: ( 'default' elementValue )?
			int alt84=2;
			int LA84_0 = input.LA(1);
			if ( (LA84_0==DEFAULT) ) {
				alt84=1;
			}
			switch (alt84) {
				case 1 :
					// Java.g:580:4: 'default' elementValue
					{
					string_literal164=(Token)match(input,DEFAULT,FOLLOW_DEFAULT_in_annotationMethodDeclaration2548); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DEFAULT.add(string_literal164);

					pushFollow(FOLLOW_elementValue_in_annotationMethodDeclaration2553);
					elementValue165=elementValue();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_elementValue.add(elementValue165.getTree());
					}
					break;

			}

			char_literal166=(Token)match(input,SEMI,FOLLOW_SEMI_in_annotationMethodDeclaration2562); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_SEMI.add(char_literal166);

			// AST REWRITE
			// elements: elementValue, IDENTIFIER
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 584:2: -> ^( ANNOTATION IDENTIFIER ( elementValue )? )
			{
				// Java.g:584:5: ^( ANNOTATION IDENTIFIER ( elementValue )? )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ANNOTATION, "ANNOTATION"), root_1);
				adaptor.addChild(root_1, stream_IDENTIFIER.nextNode());
				// Java.g:584:29: ( elementValue )?
				if ( stream_elementValue.hasNext() ) {
					adaptor.addChild(root_1, stream_elementValue.nextTree());
				}
				stream_elementValue.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 53, annotationMethodDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "annotationMethodDeclaration"


	public static class block_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "block"
	// Java.g:588:1: block : a= '{' (b+= blockStatement )* '}' -> ^( BLOCK[$a,\"BLOCK\"] ( $b)* ) ;
	public final JavaParser.block_return block() throws RecognitionException {
		JavaParser.block_return retval = new JavaParser.block_return();
		retval.start = input.LT(1);
		int block_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token char_literal167=null;
		List<Object> list_b=null;
		RuleReturnScope b = null;
		CommonTree a_tree=null;
		CommonTree char_literal167_tree=null;
		RewriteRuleTokenStream stream_RBRACE=new RewriteRuleTokenStream(adaptor,"token RBRACE");
		RewriteRuleTokenStream stream_LBRACE=new RewriteRuleTokenStream(adaptor,"token LBRACE");
		RewriteRuleSubtreeStream stream_blockStatement=new RewriteRuleSubtreeStream(adaptor,"rule blockStatement");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 54) ) { return retval; }

			// Java.g:589:2: (a= '{' (b+= blockStatement )* '}' -> ^( BLOCK[$a,\"BLOCK\"] ( $b)* ) )
			// Java.g:589:4: a= '{' (b+= blockStatement )* '}'
			{
			a=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_block2588); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LBRACE.add(a);

			// Java.g:590:4: (b+= blockStatement )*
			loop85:
			while (true) {
				int alt85=2;
				int LA85_0 = input.LA(1);
				if ( (LA85_0==ABSTRACT||(LA85_0 >= ASSERT && LA85_0 <= BANG)||(LA85_0 >= BOOLEAN && LA85_0 <= BYTE)||(LA85_0 >= CHAR && LA85_0 <= CLASS)||LA85_0==CONTINUE||LA85_0==DO||(LA85_0 >= DOUBLE && LA85_0 <= DOUBLELITERAL)||LA85_0==ENUM||LA85_0==FALSE||LA85_0==FINAL||(LA85_0 >= FLOAT && LA85_0 <= FOR)||(LA85_0 >= IDENTIFIER && LA85_0 <= IF)||(LA85_0 >= INT && LA85_0 <= INTLITERAL)||LA85_0==LBRACE||(LA85_0 >= LONG && LA85_0 <= LT)||(LA85_0 >= MONKEYS_AT && LA85_0 <= NULL)||LA85_0==PLUS||(LA85_0 >= PLUSPLUS && LA85_0 <= PUBLIC)||LA85_0==RETURN||LA85_0==SEMI||LA85_0==SHORT||(LA85_0 >= STATIC && LA85_0 <= SUB)||(LA85_0 >= SUBSUB && LA85_0 <= SYNCHRONIZED)||(LA85_0 >= THIS && LA85_0 <= THROW)||(LA85_0 >= TILDE && LA85_0 <= TRY)||(LA85_0 >= VOID && LA85_0 <= WHILE)) ) {
					alt85=1;
				}

				switch (alt85) {
				case 1 :
					// Java.g:590:4: b+= blockStatement
					{
					pushFollow(FOLLOW_blockStatement_in_block2594);
					b=blockStatement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_blockStatement.add(b.getTree());
					if (list_b==null) list_b=new ArrayList<Object>();
					list_b.add(b.getTree());
					}
					break;

				default :
					break loop85;
				}
			}

			char_literal167=(Token)match(input,RBRACE,FOLLOW_RBRACE_in_block2599); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RBRACE.add(char_literal167);

			// AST REWRITE
			// elements: b
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: b
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"token b",list_b);
			root_0 = (CommonTree)adaptor.nil();
			// 592:2: -> ^( BLOCK[$a,\"BLOCK\"] ( $b)* )
			{
				// Java.g:592:5: ^( BLOCK[$a,\"BLOCK\"] ( $b)* )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BLOCK, a, "BLOCK"), root_1);
				// Java.g:592:26: ( $b)*
				while ( stream_b.hasNext() ) {
					adaptor.addChild(root_1, stream_b.nextTree());
				}
				stream_b.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 54, block_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "block"


	public static class blockStatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "blockStatement"
	// Java.g:620:1: blockStatement : ( localVariableDeclarationStatement | classOrInterfaceDeclaration | statement );
	public final JavaParser.blockStatement_return blockStatement() throws RecognitionException {
		JavaParser.blockStatement_return retval = new JavaParser.blockStatement_return();
		retval.start = input.LT(1);
		int blockStatement_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope localVariableDeclarationStatement168 =null;
		ParserRuleReturnScope classOrInterfaceDeclaration169 =null;
		ParserRuleReturnScope statement170 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 55) ) { return retval; }

			// Java.g:621:2: ( localVariableDeclarationStatement | classOrInterfaceDeclaration | statement )
			int alt86=3;
			switch ( input.LA(1) ) {
			case FINAL:
				{
				int LA86_1 = input.LA(2);
				if ( (synpred122_Java()) ) {
					alt86=1;
				}
				else if ( (synpred123_Java()) ) {
					alt86=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 86, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case MONKEYS_AT:
				{
				int LA86_2 = input.LA(2);
				if ( (synpred122_Java()) ) {
					alt86=1;
				}
				else if ( (synpred123_Java()) ) {
					alt86=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 86, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA86_3 = input.LA(2);
				if ( (synpred122_Java()) ) {
					alt86=1;
				}
				else if ( (true) ) {
					alt86=3;
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				{
				int LA86_4 = input.LA(2);
				if ( (synpred122_Java()) ) {
					alt86=1;
				}
				else if ( (true) ) {
					alt86=3;
				}

				}
				break;
			case ABSTRACT:
			case CLASS:
			case ENUM:
			case INTERFACE:
			case NATIVE:
			case PRIVATE:
			case PROTECTED:
			case PUBLIC:
			case STATIC:
			case STRICTFP:
			case TRANSIENT:
			case VOLATILE:
				{
				alt86=2;
				}
				break;
			case SYNCHRONIZED:
				{
				int LA86_11 = input.LA(2);
				if ( (synpred123_Java()) ) {
					alt86=2;
				}
				else if ( (true) ) {
					alt86=3;
				}

				}
				break;
			case ASSERT:
			case BANG:
			case BREAK:
			case CHARLITERAL:
			case CONTINUE:
			case DO:
			case DOUBLELITERAL:
			case FALSE:
			case FLOATLITERAL:
			case FOR:
			case IF:
			case INTLITERAL:
			case LBRACE:
			case LONGLITERAL:
			case LPAREN:
			case NEW:
			case NULL:
			case PLUS:
			case PLUSPLUS:
			case RETURN:
			case SEMI:
			case STRINGLITERAL:
			case SUB:
			case SUBSUB:
			case SUPER:
			case SWITCH:
			case THIS:
			case THROW:
			case TILDE:
			case TRUE:
			case TRY:
			case VOID:
			case WHILE:
				{
				alt86=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 86, 0, input);
				throw nvae;
			}
			switch (alt86) {
				case 1 :
					// Java.g:621:4: localVariableDeclarationStatement
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_localVariableDeclarationStatement_in_blockStatement2625);
					localVariableDeclarationStatement168=localVariableDeclarationStatement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, localVariableDeclarationStatement168.getTree());

					}
					break;
				case 2 :
					// Java.g:622:4: classOrInterfaceDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_classOrInterfaceDeclaration_in_blockStatement2630);
					classOrInterfaceDeclaration169=classOrInterfaceDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classOrInterfaceDeclaration169.getTree());

					}
					break;
				case 3 :
					// Java.g:623:4: statement
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_statement_in_blockStatement2635);
					statement170=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, statement170.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 55, blockStatement_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "blockStatement"


	public static class localVariableDeclarationStatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "localVariableDeclarationStatement"
	// Java.g:627:1: localVariableDeclarationStatement : localVariableDeclaration ';' !;
	public final JavaParser.localVariableDeclarationStatement_return localVariableDeclarationStatement() throws RecognitionException {
		JavaParser.localVariableDeclarationStatement_return retval = new JavaParser.localVariableDeclarationStatement_return();
		retval.start = input.LT(1);
		int localVariableDeclarationStatement_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal172=null;
		ParserRuleReturnScope localVariableDeclaration171 =null;

		CommonTree char_literal172_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 56) ) { return retval; }

			// Java.g:628:2: ( localVariableDeclaration ';' !)
			// Java.g:628:4: localVariableDeclaration ';' !
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_localVariableDeclaration_in_localVariableDeclarationStatement2647);
			localVariableDeclaration171=localVariableDeclaration();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, localVariableDeclaration171.getTree());

			char_literal172=(Token)match(input,SEMI,FOLLOW_SEMI_in_localVariableDeclarationStatement2649); if (state.failed) return retval;
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 56, localVariableDeclarationStatement_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "localVariableDeclarationStatement"


	public static class localVariableDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "localVariableDeclaration"
	// Java.g:632:1: localVariableDeclaration : a= variableModifiers b= type c+= variableDeclarator ( ',' c+= variableDeclarator )* -> ^( DECLARATION[$b.text] ( $c)+ ) ;
	public final JavaParser.localVariableDeclaration_return localVariableDeclaration() throws RecognitionException {
		JavaParser.localVariableDeclaration_return retval = new JavaParser.localVariableDeclaration_return();
		retval.start = input.LT(1);
		int localVariableDeclaration_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal173=null;
		List<Object> list_c=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		RuleReturnScope c = null;
		CommonTree char_literal173_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_variableDeclarator=new RewriteRuleSubtreeStream(adaptor,"rule variableDeclarator");
		RewriteRuleSubtreeStream stream_variableModifiers=new RewriteRuleSubtreeStream(adaptor,"rule variableModifiers");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 57) ) { return retval; }

			// Java.g:633:2: (a= variableModifiers b= type c+= variableDeclarator ( ',' c+= variableDeclarator )* -> ^( DECLARATION[$b.text] ( $c)+ ) )
			// Java.g:633:4: a= variableModifiers b= type c+= variableDeclarator ( ',' c+= variableDeclarator )*
			{
			pushFollow(FOLLOW_variableModifiers_in_localVariableDeclaration2664);
			a=variableModifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_variableModifiers.add(a.getTree());
			pushFollow(FOLLOW_type_in_localVariableDeclaration2670);
			b=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_type.add(b.getTree());
			pushFollow(FOLLOW_variableDeclarator_in_localVariableDeclaration2676);
			c=variableDeclarator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_variableDeclarator.add(c.getTree());
			if (list_c==null) list_c=new ArrayList<Object>();
			list_c.add(c.getTree());
			// Java.g:636:3: ( ',' c+= variableDeclarator )*
			loop87:
			while (true) {
				int alt87=2;
				int LA87_0 = input.LA(1);
				if ( (LA87_0==COMMA) ) {
					alt87=1;
				}

				switch (alt87) {
				case 1 :
					// Java.g:636:4: ',' c+= variableDeclarator
					{
					char_literal173=(Token)match(input,COMMA,FOLLOW_COMMA_in_localVariableDeclaration2681); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(char_literal173);

					pushFollow(FOLLOW_variableDeclarator_in_localVariableDeclaration2685);
					c=variableDeclarator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_variableDeclarator.add(c.getTree());
					if (list_c==null) list_c=new ArrayList<Object>();
					list_c.add(c.getTree());
					}
					break;

				default :
					break loop87;
				}
			}

			// AST REWRITE
			// elements: c
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: c
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
			RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"token c",list_c);
			root_0 = (CommonTree)adaptor.nil();
			// 637:2: -> ^( DECLARATION[$b.text] ( $c)+ )
			{
				// Java.g:637:5: ^( DECLARATION[$b.text] ( $c)+ )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(DECLARATION, (b!=null?input.toString(b.start,b.stop):null)), root_1);
				if ( !(stream_c.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_c.hasNext() ) {
					adaptor.addChild(root_1, stream_c.nextTree());
				}
				stream_c.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 57, localVariableDeclaration_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "localVariableDeclaration"


	public static class statement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "statement"
	// Java.g:640:1: statement : ( block |a= 'assert' b= expression ( ':' c= expression )? ';' -> ^( STATEMENT[$a] $b ( $c)? ) |d= 'if' e= parExpression f= statement g= ( 'else' h= statement )? -> {$g!=null}? ^( CONDITIONAL[$d] ^( CONDITION $e) ^( THEN $f) ^( ELSE $h) ) -> ^( CONDITIONAL[$d] ^( CONDITION $e) ^( THEN $f) ) | forstatement |i= 'while' j= parExpression k= statement -> ^( CONDITIONAL[$i] ^( CONDITION $j) ^( THEN $k) ) |l= 'do' m= statement 'while' n= parExpression ';' -> ^( CONDITIONAL[$l] ^( CONDITION $n) ^( THEN $m) ) | trystatement |o= 'switch' p= parExpression '{' q= switchBlockStatementGroups '}' -> ^( SWITCH[$o] ^( CONDITION $p) ^( BLOCK ( $q)* ) ) | 'synchronized' parExpression block |u= 'return' (v= expression )? ';' -> ^( STATEMENT[$u] ( $v)? ) | 'throw' expression ';' !|w= 'break' (x= IDENTIFIER )? ';' -> ^( BREAK[$w] ( $x)? ) |y= 'continue' (z= IDENTIFIER )? ';' -> ^( CONTINUE[$y] ( $z)? ) | expression ';' !| IDENTIFIER ':' ^ statement | ';' !);
	public final JavaParser.statement_return statement() throws RecognitionException {
		JavaParser.statement_return retval = new JavaParser.statement_return();
		retval.start = input.LT(1);
		int statement_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token d=null;
		Token g=null;
		Token i=null;
		Token l=null;
		Token o=null;
		Token u=null;
		Token w=null;
		Token x=null;
		Token y=null;
		Token z=null;
		Token char_literal175=null;
		Token char_literal176=null;
		Token string_literal178=null;
		Token char_literal179=null;
		Token char_literal181=null;
		Token char_literal182=null;
		Token string_literal183=null;
		Token char_literal186=null;
		Token string_literal187=null;
		Token char_literal189=null;
		Token char_literal190=null;
		Token char_literal191=null;
		Token char_literal193=null;
		Token IDENTIFIER194=null;
		Token char_literal195=null;
		Token char_literal197=null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope c =null;
		ParserRuleReturnScope e =null;
		ParserRuleReturnScope f =null;
		ParserRuleReturnScope h =null;
		ParserRuleReturnScope j =null;
		ParserRuleReturnScope k =null;
		ParserRuleReturnScope m =null;
		ParserRuleReturnScope n =null;
		ParserRuleReturnScope p =null;
		ParserRuleReturnScope q =null;
		ParserRuleReturnScope v =null;
		ParserRuleReturnScope block174 =null;
		ParserRuleReturnScope forstatement177 =null;
		ParserRuleReturnScope trystatement180 =null;
		ParserRuleReturnScope parExpression184 =null;
		ParserRuleReturnScope block185 =null;
		ParserRuleReturnScope expression188 =null;
		ParserRuleReturnScope expression192 =null;
		ParserRuleReturnScope statement196 =null;

		CommonTree a_tree=null;
		CommonTree d_tree=null;
		CommonTree g_tree=null;
		CommonTree i_tree=null;
		CommonTree l_tree=null;
		CommonTree o_tree=null;
		CommonTree u_tree=null;
		CommonTree w_tree=null;
		CommonTree x_tree=null;
		CommonTree y_tree=null;
		CommonTree z_tree=null;
		CommonTree char_literal175_tree=null;
		CommonTree char_literal176_tree=null;
		CommonTree string_literal178_tree=null;
		CommonTree char_literal179_tree=null;
		CommonTree char_literal181_tree=null;
		CommonTree char_literal182_tree=null;
		CommonTree string_literal183_tree=null;
		CommonTree char_literal186_tree=null;
		CommonTree string_literal187_tree=null;
		CommonTree char_literal189_tree=null;
		CommonTree char_literal190_tree=null;
		CommonTree char_literal191_tree=null;
		CommonTree char_literal193_tree=null;
		CommonTree IDENTIFIER194_tree=null;
		CommonTree char_literal195_tree=null;
		CommonTree char_literal197_tree=null;
		RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
		RewriteRuleTokenStream stream_RBRACE=new RewriteRuleTokenStream(adaptor,"token RBRACE");
		RewriteRuleTokenStream stream_WHILE=new RewriteRuleTokenStream(adaptor,"token WHILE");
		RewriteRuleTokenStream stream_CONTINUE=new RewriteRuleTokenStream(adaptor,"token CONTINUE");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_SWITCH=new RewriteRuleTokenStream(adaptor,"token SWITCH");
		RewriteRuleTokenStream stream_ELSE=new RewriteRuleTokenStream(adaptor,"token ELSE");
		RewriteRuleTokenStream stream_LBRACE=new RewriteRuleTokenStream(adaptor,"token LBRACE");
		RewriteRuleTokenStream stream_RETURN=new RewriteRuleTokenStream(adaptor,"token RETURN");
		RewriteRuleTokenStream stream_DO=new RewriteRuleTokenStream(adaptor,"token DO");
		RewriteRuleTokenStream stream_SEMI=new RewriteRuleTokenStream(adaptor,"token SEMI");
		RewriteRuleTokenStream stream_ASSERT=new RewriteRuleTokenStream(adaptor,"token ASSERT");
		RewriteRuleTokenStream stream_BREAK=new RewriteRuleTokenStream(adaptor,"token BREAK");
		RewriteRuleTokenStream stream_IF=new RewriteRuleTokenStream(adaptor,"token IF");
		RewriteRuleSubtreeStream stream_statement=new RewriteRuleSubtreeStream(adaptor,"rule statement");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
		RewriteRuleSubtreeStream stream_parExpression=new RewriteRuleSubtreeStream(adaptor,"rule parExpression");
		RewriteRuleSubtreeStream stream_switchBlockStatementGroups=new RewriteRuleSubtreeStream(adaptor,"rule switchBlockStatementGroups");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 58) ) { return retval; }

			// Java.g:641:2: ( block |a= 'assert' b= expression ( ':' c= expression )? ';' -> ^( STATEMENT[$a] $b ( $c)? ) |d= 'if' e= parExpression f= statement g= ( 'else' h= statement )? -> {$g!=null}? ^( CONDITIONAL[$d] ^( CONDITION $e) ^( THEN $f) ^( ELSE $h) ) -> ^( CONDITIONAL[$d] ^( CONDITION $e) ^( THEN $f) ) | forstatement |i= 'while' j= parExpression k= statement -> ^( CONDITIONAL[$i] ^( CONDITION $j) ^( THEN $k) ) |l= 'do' m= statement 'while' n= parExpression ';' -> ^( CONDITIONAL[$l] ^( CONDITION $n) ^( THEN $m) ) | trystatement |o= 'switch' p= parExpression '{' q= switchBlockStatementGroups '}' -> ^( SWITCH[$o] ^( CONDITION $p) ^( BLOCK ( $q)* ) ) | 'synchronized' parExpression block |u= 'return' (v= expression )? ';' -> ^( STATEMENT[$u] ( $v)? ) | 'throw' expression ';' !|w= 'break' (x= IDENTIFIER )? ';' -> ^( BREAK[$w] ( $x)? ) |y= 'continue' (z= IDENTIFIER )? ';' -> ^( CONTINUE[$y] ( $z)? ) | expression ';' !| IDENTIFIER ':' ^ statement | ';' !)
			int alt93=16;
			switch ( input.LA(1) ) {
			case LBRACE:
				{
				alt93=1;
				}
				break;
			case ASSERT:
				{
				alt93=2;
				}
				break;
			case IF:
				{
				alt93=3;
				}
				break;
			case FOR:
				{
				alt93=4;
				}
				break;
			case WHILE:
				{
				alt93=5;
				}
				break;
			case DO:
				{
				alt93=6;
				}
				break;
			case TRY:
				{
				alt93=7;
				}
				break;
			case SWITCH:
				{
				alt93=8;
				}
				break;
			case SYNCHRONIZED:
				{
				alt93=9;
				}
				break;
			case RETURN:
				{
				alt93=10;
				}
				break;
			case THROW:
				{
				alt93=11;
				}
				break;
			case BREAK:
				{
				alt93=12;
				}
				break;
			case CONTINUE:
				{
				alt93=13;
				}
				break;
			case BANG:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case CHARLITERAL:
			case DOUBLE:
			case DOUBLELITERAL:
			case FALSE:
			case FLOAT:
			case FLOATLITERAL:
			case INT:
			case INTLITERAL:
			case LONG:
			case LONGLITERAL:
			case LPAREN:
			case NEW:
			case NULL:
			case PLUS:
			case PLUSPLUS:
			case SHORT:
			case STRINGLITERAL:
			case SUB:
			case SUBSUB:
			case SUPER:
			case THIS:
			case TILDE:
			case TRUE:
			case VOID:
				{
				alt93=14;
				}
				break;
			case IDENTIFIER:
				{
				int LA93_15 = input.LA(2);
				if ( (LA93_15==COLON) ) {
					alt93=15;
				}
				else if ( ((LA93_15 >= AMP && LA93_15 <= AMPEQ)||(LA93_15 >= BANGEQ && LA93_15 <= BAREQ)||(LA93_15 >= CARET && LA93_15 <= CARETEQ)||LA93_15==DOT||(LA93_15 >= EQ && LA93_15 <= EQEQ)||LA93_15==GT||LA93_15==INSTANCEOF||LA93_15==LBRACKET||(LA93_15 >= LPAREN && LA93_15 <= LT)||(LA93_15 >= PERCENT && LA93_15 <= PLUSPLUS)||LA93_15==QUES||LA93_15==SEMI||(LA93_15 >= SLASH && LA93_15 <= STAREQ)||(LA93_15 >= SUB && LA93_15 <= SUBSUB)) ) {
					alt93=14;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 93, 15, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case SEMI:
				{
				alt93=16;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 93, 0, input);
				throw nvae;
			}
			switch (alt93) {
				case 1 :
					// Java.g:641:4: block
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_block_in_statement2712);
					block174=block();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, block174.getTree());

					}
					break;
				case 2 :
					// Java.g:642:4: a= 'assert' b= expression ( ':' c= expression )? ';'
					{
					a=(Token)match(input,ASSERT,FOLLOW_ASSERT_in_statement2719); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_ASSERT.add(a);

					pushFollow(FOLLOW_expression_in_statement2723);
					b=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expression.add(b.getTree());
					// Java.g:642:28: ( ':' c= expression )?
					int alt88=2;
					int LA88_0 = input.LA(1);
					if ( (LA88_0==COLON) ) {
						alt88=1;
					}
					switch (alt88) {
						case 1 :
							// Java.g:642:29: ':' c= expression
							{
							char_literal175=(Token)match(input,COLON,FOLLOW_COLON_in_statement2726); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_COLON.add(char_literal175);

							pushFollow(FOLLOW_expression_in_statement2730);
							c=expression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_expression.add(c.getTree());
							}
							break;

					}

					char_literal176=(Token)match(input,SEMI,FOLLOW_SEMI_in_statement2734); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SEMI.add(char_literal176);

					// AST REWRITE
					// elements: b, c
					// token labels: 
					// rule labels: retval, b, c
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);
					RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"rule c",c!=null?c.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 643:2: -> ^( STATEMENT[$a] $b ( $c)? )
					{
						// Java.g:643:5: ^( STATEMENT[$a] $b ( $c)? )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(STATEMENT, a), root_1);
						adaptor.addChild(root_1, stream_b.nextTree());
						// Java.g:643:25: ( $c)?
						if ( stream_c.hasNext() ) {
							adaptor.addChild(root_1, stream_c.nextTree());
						}
						stream_c.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// Java.g:644:4: d= 'if' e= parExpression f= statement g= ( 'else' h= statement )?
					{
					d=(Token)match(input,IF,FOLLOW_IF_in_statement2756); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IF.add(d);

					pushFollow(FOLLOW_parExpression_in_statement2760);
					e=parExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_parExpression.add(e.getTree());
					pushFollow(FOLLOW_statement_in_statement2764);
					f=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_statement.add(f.getTree());
					// Java.g:644:41: ( 'else' h= statement )?
					int alt89=2;
					int LA89_0 = input.LA(1);
					if ( (LA89_0==ELSE) ) {
						int LA89_1 = input.LA(2);
						if ( (synpred128_Java()) ) {
							alt89=1;
						}
					}
					switch (alt89) {
						case 1 :
							// Java.g:644:42: 'else' h= statement
							{
							g=(Token)match(input,ELSE,FOLLOW_ELSE_in_statement2769); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_ELSE.add(g);

							pushFollow(FOLLOW_statement_in_statement2773);
							h=statement();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_statement.add(h.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: e, f, e, f, h
					// token labels: 
					// rule labels: f, retval, e, h
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_f=new RewriteRuleSubtreeStream(adaptor,"rule f",f!=null?f.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_e=new RewriteRuleSubtreeStream(adaptor,"rule e",e!=null?e.getTree():null);
					RewriteRuleSubtreeStream stream_h=new RewriteRuleSubtreeStream(adaptor,"rule h",h!=null?h.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 645:2: -> {$g!=null}? ^( CONDITIONAL[$d] ^( CONDITION $e) ^( THEN $f) ^( ELSE $h) )
					if (g!=null) {
						// Java.g:645:17: ^( CONDITIONAL[$d] ^( CONDITION $e) ^( THEN $f) ^( ELSE $h) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITIONAL, d), root_1);
						// Java.g:645:35: ^( CONDITION $e)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITION, "CONDITION"), root_2);
						adaptor.addChild(root_2, stream_e.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// Java.g:645:51: ^( THEN $f)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(THEN, "THEN"), root_2);
						adaptor.addChild(root_2, stream_f.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// Java.g:645:62: ^( ELSE $h)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ELSE, "ELSE"), root_2);
						adaptor.addChild(root_2, stream_h.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}

					else // 646:2: -> ^( CONDITIONAL[$d] ^( CONDITION $e) ^( THEN $f) )
					{
						// Java.g:646:5: ^( CONDITIONAL[$d] ^( CONDITION $e) ^( THEN $f) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITIONAL, d), root_1);
						// Java.g:646:23: ^( CONDITION $e)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITION, "CONDITION"), root_2);
						adaptor.addChild(root_2, stream_e.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// Java.g:646:39: ^( THEN $f)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(THEN, "THEN"), root_2);
						adaptor.addChild(root_2, stream_f.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 4 :
					// Java.g:647:4: forstatement
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_forstatement_in_statement2833);
					forstatement177=forstatement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, forstatement177.getTree());

					}
					break;
				case 5 :
					// Java.g:648:4: i= 'while' j= parExpression k= statement
					{
					i=(Token)match(input,WHILE,FOLLOW_WHILE_in_statement2840); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_WHILE.add(i);

					pushFollow(FOLLOW_parExpression_in_statement2844);
					j=parExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_parExpression.add(j.getTree());
					pushFollow(FOLLOW_statement_in_statement2848);
					k=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_statement.add(k.getTree());
					// AST REWRITE
					// elements: j, k
					// token labels: 
					// rule labels: retval, j, k
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_j=new RewriteRuleSubtreeStream(adaptor,"rule j",j!=null?j.getTree():null);
					RewriteRuleSubtreeStream stream_k=new RewriteRuleSubtreeStream(adaptor,"rule k",k!=null?k.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 649:2: -> ^( CONDITIONAL[$i] ^( CONDITION $j) ^( THEN $k) )
					{
						// Java.g:649:5: ^( CONDITIONAL[$i] ^( CONDITION $j) ^( THEN $k) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITIONAL, i), root_1);
						// Java.g:649:23: ^( CONDITION $j)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITION, "CONDITION"), root_2);
						adaptor.addChild(root_2, stream_j.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// Java.g:649:39: ^( THEN $k)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(THEN, "THEN"), root_2);
						adaptor.addChild(root_2, stream_k.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 6 :
					// Java.g:650:4: l= 'do' m= statement 'while' n= parExpression ';'
					{
					l=(Token)match(input,DO,FOLLOW_DO_in_statement2877); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DO.add(l);

					pushFollow(FOLLOW_statement_in_statement2881);
					m=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_statement.add(m.getTree());
					string_literal178=(Token)match(input,WHILE,FOLLOW_WHILE_in_statement2883); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_WHILE.add(string_literal178);

					pushFollow(FOLLOW_parExpression_in_statement2887);
					n=parExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_parExpression.add(n.getTree());
					char_literal179=(Token)match(input,SEMI,FOLLOW_SEMI_in_statement2889); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SEMI.add(char_literal179);

					// AST REWRITE
					// elements: n, m
					// token labels: 
					// rule labels: retval, n, m
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_n=new RewriteRuleSubtreeStream(adaptor,"rule n",n!=null?n.getTree():null);
					RewriteRuleSubtreeStream stream_m=new RewriteRuleSubtreeStream(adaptor,"rule m",m!=null?m.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 651:2: -> ^( CONDITIONAL[$l] ^( CONDITION $n) ^( THEN $m) )
					{
						// Java.g:651:5: ^( CONDITIONAL[$l] ^( CONDITION $n) ^( THEN $m) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITIONAL, l), root_1);
						// Java.g:651:23: ^( CONDITION $n)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITION, "CONDITION"), root_2);
						adaptor.addChild(root_2, stream_n.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// Java.g:651:39: ^( THEN $m)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(THEN, "THEN"), root_2);
						adaptor.addChild(root_2, stream_m.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 7 :
					// Java.g:652:4: trystatement
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_trystatement_in_statement2916);
					trystatement180=trystatement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, trystatement180.getTree());

					}
					break;
				case 8 :
					// Java.g:653:4: o= 'switch' p= parExpression '{' q= switchBlockStatementGroups '}'
					{
					o=(Token)match(input,SWITCH,FOLLOW_SWITCH_in_statement2923); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SWITCH.add(o);

					pushFollow(FOLLOW_parExpression_in_statement2927);
					p=parExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_parExpression.add(p.getTree());
					char_literal181=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_statement2929); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LBRACE.add(char_literal181);

					pushFollow(FOLLOW_switchBlockStatementGroups_in_statement2933);
					q=switchBlockStatementGroups();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_switchBlockStatementGroups.add(q.getTree());
					char_literal182=(Token)match(input,RBRACE,FOLLOW_RBRACE_in_statement2935); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RBRACE.add(char_literal182);

					// AST REWRITE
					// elements: q, p
					// token labels: 
					// rule labels: retval, q, p
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_q=new RewriteRuleSubtreeStream(adaptor,"rule q",q!=null?q.getTree():null);
					RewriteRuleSubtreeStream stream_p=new RewriteRuleSubtreeStream(adaptor,"rule p",p!=null?p.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 654:2: -> ^( SWITCH[$o] ^( CONDITION $p) ^( BLOCK ( $q)* ) )
					{
						// Java.g:654:5: ^( SWITCH[$o] ^( CONDITION $p) ^( BLOCK ( $q)* ) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(SWITCH, o), root_1);
						// Java.g:654:18: ^( CONDITION $p)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITION, "CONDITION"), root_2);
						adaptor.addChild(root_2, stream_p.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// Java.g:654:34: ^( BLOCK ( $q)* )
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BLOCK, "BLOCK"), root_2);
						// Java.g:654:43: ( $q)*
						while ( stream_q.hasNext() ) {
							adaptor.addChild(root_2, stream_q.nextTree());
						}
						stream_q.reset();

						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 9 :
					// Java.g:655:4: 'synchronized' parExpression block
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal183=(Token)match(input,SYNCHRONIZED,FOLLOW_SYNCHRONIZED_in_statement2963); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal183_tree = (CommonTree)adaptor.create(string_literal183);
					adaptor.addChild(root_0, string_literal183_tree);
					}

					pushFollow(FOLLOW_parExpression_in_statement2965);
					parExpression184=parExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, parExpression184.getTree());

					pushFollow(FOLLOW_block_in_statement2967);
					block185=block();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, block185.getTree());

					}
					break;
				case 10 :
					// Java.g:656:4: u= 'return' (v= expression )? ';'
					{
					u=(Token)match(input,RETURN,FOLLOW_RETURN_in_statement2974); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RETURN.add(u);

					// Java.g:656:15: (v= expression )?
					int alt90=2;
					int LA90_0 = input.LA(1);
					if ( (LA90_0==BANG||LA90_0==BOOLEAN||LA90_0==BYTE||(LA90_0 >= CHAR && LA90_0 <= CHARLITERAL)||(LA90_0 >= DOUBLE && LA90_0 <= DOUBLELITERAL)||LA90_0==FALSE||(LA90_0 >= FLOAT && LA90_0 <= FLOATLITERAL)||LA90_0==IDENTIFIER||LA90_0==INT||LA90_0==INTLITERAL||(LA90_0 >= LONG && LA90_0 <= LPAREN)||(LA90_0 >= NEW && LA90_0 <= NULL)||LA90_0==PLUS||LA90_0==PLUSPLUS||LA90_0==SHORT||(LA90_0 >= STRINGLITERAL && LA90_0 <= SUB)||(LA90_0 >= SUBSUB && LA90_0 <= SUPER)||LA90_0==THIS||LA90_0==TILDE||LA90_0==TRUE||LA90_0==VOID) ) {
						alt90=1;
					}
					switch (alt90) {
						case 1 :
							// Java.g:656:16: v= expression
							{
							pushFollow(FOLLOW_expression_in_statement2979);
							v=expression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_expression.add(v.getTree());
							}
							break;

					}

					char_literal186=(Token)match(input,SEMI,FOLLOW_SEMI_in_statement2983); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SEMI.add(char_literal186);

					// AST REWRITE
					// elements: v
					// token labels: 
					// rule labels: v, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_v=new RewriteRuleSubtreeStream(adaptor,"rule v",v!=null?v.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 657:2: -> ^( STATEMENT[$u] ( $v)? )
					{
						// Java.g:657:5: ^( STATEMENT[$u] ( $v)? )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(STATEMENT, u), root_1);
						// Java.g:657:22: ( $v)?
						if ( stream_v.hasNext() ) {
							adaptor.addChild(root_1, stream_v.nextTree());
						}
						stream_v.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 11 :
					// Java.g:658:4: 'throw' expression ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal187=(Token)match(input,THROW,FOLLOW_THROW_in_statement3000); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal187_tree = (CommonTree)adaptor.create(string_literal187);
					adaptor.addChild(root_0, string_literal187_tree);
					}

					pushFollow(FOLLOW_expression_in_statement3002);
					expression188=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression188.getTree());

					char_literal189=(Token)match(input,SEMI,FOLLOW_SEMI_in_statement3004); if (state.failed) return retval;
					}
					break;
				case 12 :
					// Java.g:659:4: w= 'break' (x= IDENTIFIER )? ';'
					{
					w=(Token)match(input,BREAK,FOLLOW_BREAK_in_statement3012); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BREAK.add(w);

					// Java.g:659:15: (x= IDENTIFIER )?
					int alt91=2;
					int LA91_0 = input.LA(1);
					if ( (LA91_0==IDENTIFIER) ) {
						alt91=1;
					}
					switch (alt91) {
						case 1 :
							// Java.g:659:15: x= IDENTIFIER
							{
							x=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_statement3016); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_IDENTIFIER.add(x);

							}
							break;

					}

					char_literal190=(Token)match(input,SEMI,FOLLOW_SEMI_in_statement3019); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SEMI.add(char_literal190);

					// AST REWRITE
					// elements: x
					// token labels: x
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_x=new RewriteRuleTokenStream(adaptor,"token x",x);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 660:2: -> ^( BREAK[$w] ( $x)? )
					{
						// Java.g:660:5: ^( BREAK[$w] ( $x)? )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BREAK, w), root_1);
						// Java.g:660:18: ( $x)?
						if ( stream_x.hasNext() ) {
							adaptor.addChild(root_1, stream_x.nextNode());
						}
						stream_x.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 13 :
					// Java.g:661:4: y= 'continue' (z= IDENTIFIER )? ';'
					{
					y=(Token)match(input,CONTINUE,FOLLOW_CONTINUE_in_statement3038); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_CONTINUE.add(y);

					// Java.g:661:18: (z= IDENTIFIER )?
					int alt92=2;
					int LA92_0 = input.LA(1);
					if ( (LA92_0==IDENTIFIER) ) {
						alt92=1;
					}
					switch (alt92) {
						case 1 :
							// Java.g:661:18: z= IDENTIFIER
							{
							z=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_statement3042); if (state.failed) return retval; 
							if ( state.backtracking==0 ) stream_IDENTIFIER.add(z);

							}
							break;

					}

					char_literal191=(Token)match(input,SEMI,FOLLOW_SEMI_in_statement3045); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SEMI.add(char_literal191);

					// AST REWRITE
					// elements: z
					// token labels: z
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_z=new RewriteRuleTokenStream(adaptor,"token z",z);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 662:2: -> ^( CONTINUE[$y] ( $z)? )
					{
						// Java.g:662:5: ^( CONTINUE[$y] ( $z)? )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONTINUE, y), root_1);
						// Java.g:662:21: ( $z)?
						if ( stream_z.hasNext() ) {
							adaptor.addChild(root_1, stream_z.nextNode());
						}
						stream_z.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 14 :
					// Java.g:663:4: expression ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_expression_in_statement3062);
					expression192=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression192.getTree());

					char_literal193=(Token)match(input,SEMI,FOLLOW_SEMI_in_statement3065); if (state.failed) return retval;
					}
					break;
				case 15 :
					// Java.g:664:4: IDENTIFIER ':' ^ statement
					{
					root_0 = (CommonTree)adaptor.nil();


					IDENTIFIER194=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_statement3071); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER194_tree = (CommonTree)adaptor.create(IDENTIFIER194);
					adaptor.addChild(root_0, IDENTIFIER194_tree);
					}

					char_literal195=(Token)match(input,COLON,FOLLOW_COLON_in_statement3073); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal195_tree = (CommonTree)adaptor.create(char_literal195);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal195_tree, root_0);
					}

					pushFollow(FOLLOW_statement_in_statement3076);
					statement196=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, statement196.getTree());

					}
					break;
				case 16 :
					// Java.g:665:4: ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal197=(Token)match(input,SEMI,FOLLOW_SEMI_in_statement3081); if (state.failed) return retval;
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 58, statement_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "statement"


	public static class switchBlockStatementGroups_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "switchBlockStatementGroups"
	// Java.g:669:1: switchBlockStatementGroups : ( switchBlockStatementGroup )* ;
	public final JavaParser.switchBlockStatementGroups_return switchBlockStatementGroups() throws RecognitionException {
		JavaParser.switchBlockStatementGroups_return retval = new JavaParser.switchBlockStatementGroups_return();
		retval.start = input.LT(1);
		int switchBlockStatementGroups_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope switchBlockStatementGroup198 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 59) ) { return retval; }

			// Java.g:670:2: ( ( switchBlockStatementGroup )* )
			// Java.g:670:4: ( switchBlockStatementGroup )*
			{
			root_0 = (CommonTree)adaptor.nil();


			// Java.g:670:4: ( switchBlockStatementGroup )*
			loop94:
			while (true) {
				int alt94=2;
				int LA94_0 = input.LA(1);
				if ( (LA94_0==CASE||LA94_0==DEFAULT) ) {
					alt94=1;
				}

				switch (alt94) {
				case 1 :
					// Java.g:670:5: switchBlockStatementGroup
					{
					pushFollow(FOLLOW_switchBlockStatementGroup_in_switchBlockStatementGroups3095);
					switchBlockStatementGroup198=switchBlockStatementGroup();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, switchBlockStatementGroup198.getTree());

					}
					break;

				default :
					break loop94;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 59, switchBlockStatementGroups_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "switchBlockStatementGroups"


	public static class switchBlockStatementGroup_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "switchBlockStatementGroup"
	// Java.g:674:1: switchBlockStatementGroup : (a= 'case' b= expression ':' (c= blockStatement )* -> ^( CASE[$a] ^( CONDITION $b) ^( THEN ( blockStatement )* ) ) |a= 'default' ':' (c= blockStatement )* -> ^( CASE[$a] ^( THEN ( blockStatement )* ) ) );
	public final JavaParser.switchBlockStatementGroup_return switchBlockStatementGroup() throws RecognitionException {
		JavaParser.switchBlockStatementGroup_return retval = new JavaParser.switchBlockStatementGroup_return();
		retval.start = input.LT(1);
		int switchBlockStatementGroup_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token char_literal199=null;
		Token char_literal200=null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope c =null;

		CommonTree a_tree=null;
		CommonTree char_literal199_tree=null;
		CommonTree char_literal200_tree=null;
		RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
		RewriteRuleTokenStream stream_DEFAULT=new RewriteRuleTokenStream(adaptor,"token DEFAULT");
		RewriteRuleTokenStream stream_CASE=new RewriteRuleTokenStream(adaptor,"token CASE");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
		RewriteRuleSubtreeStream stream_blockStatement=new RewriteRuleSubtreeStream(adaptor,"rule blockStatement");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 60) ) { return retval; }

			// Java.g:675:2: (a= 'case' b= expression ':' (c= blockStatement )* -> ^( CASE[$a] ^( CONDITION $b) ^( THEN ( blockStatement )* ) ) |a= 'default' ':' (c= blockStatement )* -> ^( CASE[$a] ^( THEN ( blockStatement )* ) ) )
			int alt97=2;
			int LA97_0 = input.LA(1);
			if ( (LA97_0==CASE) ) {
				alt97=1;
			}
			else if ( (LA97_0==DEFAULT) ) {
				alt97=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 97, 0, input);
				throw nvae;
			}

			switch (alt97) {
				case 1 :
					// Java.g:675:4: a= 'case' b= expression ':' (c= blockStatement )*
					{
					a=(Token)match(input,CASE,FOLLOW_CASE_in_switchBlockStatementGroup3111); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_CASE.add(a);

					pushFollow(FOLLOW_expression_in_switchBlockStatementGroup3115);
					b=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expression.add(b.getTree());
					char_literal199=(Token)match(input,COLON,FOLLOW_COLON_in_switchBlockStatementGroup3117); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COLON.add(char_literal199);

					// Java.g:675:31: (c= blockStatement )*
					loop95:
					while (true) {
						int alt95=2;
						int LA95_0 = input.LA(1);
						if ( (LA95_0==ABSTRACT||(LA95_0 >= ASSERT && LA95_0 <= BANG)||(LA95_0 >= BOOLEAN && LA95_0 <= BYTE)||(LA95_0 >= CHAR && LA95_0 <= CLASS)||LA95_0==CONTINUE||LA95_0==DO||(LA95_0 >= DOUBLE && LA95_0 <= DOUBLELITERAL)||LA95_0==ENUM||LA95_0==FALSE||LA95_0==FINAL||(LA95_0 >= FLOAT && LA95_0 <= FOR)||(LA95_0 >= IDENTIFIER && LA95_0 <= IF)||(LA95_0 >= INT && LA95_0 <= INTLITERAL)||LA95_0==LBRACE||(LA95_0 >= LONG && LA95_0 <= LT)||(LA95_0 >= MONKEYS_AT && LA95_0 <= NULL)||LA95_0==PLUS||(LA95_0 >= PLUSPLUS && LA95_0 <= PUBLIC)||LA95_0==RETURN||LA95_0==SEMI||LA95_0==SHORT||(LA95_0 >= STATIC && LA95_0 <= SUB)||(LA95_0 >= SUBSUB && LA95_0 <= SYNCHRONIZED)||(LA95_0 >= THIS && LA95_0 <= THROW)||(LA95_0 >= TILDE && LA95_0 <= TRY)||(LA95_0 >= VOID && LA95_0 <= WHILE)) ) {
							alt95=1;
						}

						switch (alt95) {
						case 1 :
							// Java.g:675:31: c= blockStatement
							{
							pushFollow(FOLLOW_blockStatement_in_switchBlockStatementGroup3121);
							c=blockStatement();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_blockStatement.add(c.getTree());
							}
							break;

						default :
							break loop95;
						}
					}

					// AST REWRITE
					// elements: blockStatement, b
					// token labels: 
					// rule labels: retval, b
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 676:2: -> ^( CASE[$a] ^( CONDITION $b) ^( THEN ( blockStatement )* ) )
					{
						// Java.g:676:5: ^( CASE[$a] ^( CONDITION $b) ^( THEN ( blockStatement )* ) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CASE, a), root_1);
						// Java.g:676:16: ^( CONDITION $b)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITION, "CONDITION"), root_2);
						adaptor.addChild(root_2, stream_b.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// Java.g:676:32: ^( THEN ( blockStatement )* )
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(THEN, "THEN"), root_2);
						// Java.g:676:39: ( blockStatement )*
						while ( stream_blockStatement.hasNext() ) {
							adaptor.addChild(root_2, stream_blockStatement.nextTree());
						}
						stream_blockStatement.reset();

						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:677:4: a= 'default' ':' (c= blockStatement )*
					{
					a=(Token)match(input,DEFAULT,FOLLOW_DEFAULT_in_switchBlockStatementGroup3151); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DEFAULT.add(a);

					char_literal200=(Token)match(input,COLON,FOLLOW_COLON_in_switchBlockStatementGroup3153); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COLON.add(char_literal200);

					// Java.g:677:21: (c= blockStatement )*
					loop96:
					while (true) {
						int alt96=2;
						int LA96_0 = input.LA(1);
						if ( (LA96_0==ABSTRACT||(LA96_0 >= ASSERT && LA96_0 <= BANG)||(LA96_0 >= BOOLEAN && LA96_0 <= BYTE)||(LA96_0 >= CHAR && LA96_0 <= CLASS)||LA96_0==CONTINUE||LA96_0==DO||(LA96_0 >= DOUBLE && LA96_0 <= DOUBLELITERAL)||LA96_0==ENUM||LA96_0==FALSE||LA96_0==FINAL||(LA96_0 >= FLOAT && LA96_0 <= FOR)||(LA96_0 >= IDENTIFIER && LA96_0 <= IF)||(LA96_0 >= INT && LA96_0 <= INTLITERAL)||LA96_0==LBRACE||(LA96_0 >= LONG && LA96_0 <= LT)||(LA96_0 >= MONKEYS_AT && LA96_0 <= NULL)||LA96_0==PLUS||(LA96_0 >= PLUSPLUS && LA96_0 <= PUBLIC)||LA96_0==RETURN||LA96_0==SEMI||LA96_0==SHORT||(LA96_0 >= STATIC && LA96_0 <= SUB)||(LA96_0 >= SUBSUB && LA96_0 <= SYNCHRONIZED)||(LA96_0 >= THIS && LA96_0 <= THROW)||(LA96_0 >= TILDE && LA96_0 <= TRY)||(LA96_0 >= VOID && LA96_0 <= WHILE)) ) {
							alt96=1;
						}

						switch (alt96) {
						case 1 :
							// Java.g:677:21: c= blockStatement
							{
							pushFollow(FOLLOW_blockStatement_in_switchBlockStatementGroup3157);
							c=blockStatement();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_blockStatement.add(c.getTree());
							}
							break;

						default :
							break loop96;
						}
					}

					// AST REWRITE
					// elements: blockStatement
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 678:2: -> ^( CASE[$a] ^( THEN ( blockStatement )* ) )
					{
						// Java.g:678:5: ^( CASE[$a] ^( THEN ( blockStatement )* ) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CASE, a), root_1);
						// Java.g:678:16: ^( THEN ( blockStatement )* )
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(THEN, "THEN"), root_2);
						// Java.g:678:23: ( blockStatement )*
						while ( stream_blockStatement.hasNext() ) {
							adaptor.addChild(root_2, stream_blockStatement.nextTree());
						}
						stream_blockStatement.reset();

						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 60, switchBlockStatementGroup_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "switchBlockStatementGroup"


	public static class trystatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "trystatement"
	// Java.g:682:1: trystatement : 'try' block ( catches 'finally' block | catches | 'finally' block ) ;
	public final JavaParser.trystatement_return trystatement() throws RecognitionException {
		JavaParser.trystatement_return retval = new JavaParser.trystatement_return();
		retval.start = input.LT(1);
		int trystatement_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal201=null;
		Token string_literal204=null;
		Token string_literal207=null;
		ParserRuleReturnScope block202 =null;
		ParserRuleReturnScope catches203 =null;
		ParserRuleReturnScope block205 =null;
		ParserRuleReturnScope catches206 =null;
		ParserRuleReturnScope block208 =null;

		CommonTree string_literal201_tree=null;
		CommonTree string_literal204_tree=null;
		CommonTree string_literal207_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 61) ) { return retval; }

			// Java.g:683:2: ( 'try' block ( catches 'finally' block | catches | 'finally' block ) )
			// Java.g:683:4: 'try' block ( catches 'finally' block | catches | 'finally' block )
			{
			root_0 = (CommonTree)adaptor.nil();


			string_literal201=(Token)match(input,TRY,FOLLOW_TRY_in_trystatement3185); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal201_tree = (CommonTree)adaptor.create(string_literal201);
			adaptor.addChild(root_0, string_literal201_tree);
			}

			pushFollow(FOLLOW_block_in_trystatement3187);
			block202=block();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, block202.getTree());

			// Java.g:684:3: ( catches 'finally' block | catches | 'finally' block )
			int alt98=3;
			int LA98_0 = input.LA(1);
			if ( (LA98_0==CATCH) ) {
				int LA98_1 = input.LA(2);
				if ( (synpred149_Java()) ) {
					alt98=1;
				}
				else if ( (synpred150_Java()) ) {
					alt98=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 98, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA98_0==FINALLY) ) {
				alt98=3;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 98, 0, input);
				throw nvae;
			}

			switch (alt98) {
				case 1 :
					// Java.g:684:5: catches 'finally' block
					{
					pushFollow(FOLLOW_catches_in_trystatement3193);
					catches203=catches();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, catches203.getTree());

					string_literal204=(Token)match(input,FINALLY,FOLLOW_FINALLY_in_trystatement3195); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal204_tree = (CommonTree)adaptor.create(string_literal204);
					adaptor.addChild(root_0, string_literal204_tree);
					}

					pushFollow(FOLLOW_block_in_trystatement3197);
					block205=block();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, block205.getTree());

					}
					break;
				case 2 :
					// Java.g:685:5: catches
					{
					pushFollow(FOLLOW_catches_in_trystatement3203);
					catches206=catches();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, catches206.getTree());

					}
					break;
				case 3 :
					// Java.g:686:5: 'finally' block
					{
					string_literal207=(Token)match(input,FINALLY,FOLLOW_FINALLY_in_trystatement3209); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal207_tree = (CommonTree)adaptor.create(string_literal207);
					adaptor.addChild(root_0, string_literal207_tree);
					}

					pushFollow(FOLLOW_block_in_trystatement3211);
					block208=block();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, block208.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 61, trystatement_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "trystatement"


	public static class catches_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "catches"
	// Java.g:691:1: catches : catchClause ( catchClause )* ;
	public final JavaParser.catches_return catches() throws RecognitionException {
		JavaParser.catches_return retval = new JavaParser.catches_return();
		retval.start = input.LT(1);
		int catches_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope catchClause209 =null;
		ParserRuleReturnScope catchClause210 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 62) ) { return retval; }

			// Java.g:692:2: ( catchClause ( catchClause )* )
			// Java.g:692:4: catchClause ( catchClause )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_catchClause_in_catches3227);
			catchClause209=catchClause();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, catchClause209.getTree());

			// Java.g:693:3: ( catchClause )*
			loop99:
			while (true) {
				int alt99=2;
				int LA99_0 = input.LA(1);
				if ( (LA99_0==CATCH) ) {
					alt99=1;
				}

				switch (alt99) {
				case 1 :
					// Java.g:693:4: catchClause
					{
					pushFollow(FOLLOW_catchClause_in_catches3232);
					catchClause210=catchClause();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, catchClause210.getTree());

					}
					break;

				default :
					break loop99;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 62, catches_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "catches"


	public static class catchClause_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "catchClause"
	// Java.g:697:1: catchClause : 'catch' '(' formalParameter ')' block ;
	public final JavaParser.catchClause_return catchClause() throws RecognitionException {
		JavaParser.catchClause_return retval = new JavaParser.catchClause_return();
		retval.start = input.LT(1);
		int catchClause_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal211=null;
		Token char_literal212=null;
		Token char_literal214=null;
		ParserRuleReturnScope formalParameter213 =null;
		ParserRuleReturnScope block215 =null;

		CommonTree string_literal211_tree=null;
		CommonTree char_literal212_tree=null;
		CommonTree char_literal214_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 63) ) { return retval; }

			// Java.g:698:2: ( 'catch' '(' formalParameter ')' block )
			// Java.g:698:4: 'catch' '(' formalParameter ')' block
			{
			root_0 = (CommonTree)adaptor.nil();


			string_literal211=(Token)match(input,CATCH,FOLLOW_CATCH_in_catchClause3246); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal211_tree = (CommonTree)adaptor.create(string_literal211);
			adaptor.addChild(root_0, string_literal211_tree);
			}

			char_literal212=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_catchClause3250); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal212_tree = (CommonTree)adaptor.create(char_literal212);
			adaptor.addChild(root_0, char_literal212_tree);
			}

			pushFollow(FOLLOW_formalParameter_in_catchClause3252);
			formalParameter213=formalParameter();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, formalParameter213.getTree());

			char_literal214=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_catchClause3254); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal214_tree = (CommonTree)adaptor.create(char_literal214);
			adaptor.addChild(root_0, char_literal214_tree);
			}

			pushFollow(FOLLOW_block_in_catchClause3258);
			block215=block();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, block215.getTree());

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 63, catchClause_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "catchClause"


	public static class formalParameter_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "formalParameter"
	// Java.g:704:1: formalParameter : variableModifiers type IDENTIFIER ( '[' ']' )* ;
	public final JavaParser.formalParameter_return formalParameter() throws RecognitionException {
		JavaParser.formalParameter_return retval = new JavaParser.formalParameter_return();
		retval.start = input.LT(1);
		int formalParameter_StartIndex = input.index();

		CommonTree root_0 = null;

		Token IDENTIFIER218=null;
		Token char_literal219=null;
		Token char_literal220=null;
		ParserRuleReturnScope variableModifiers216 =null;
		ParserRuleReturnScope type217 =null;

		CommonTree IDENTIFIER218_tree=null;
		CommonTree char_literal219_tree=null;
		CommonTree char_literal220_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 64) ) { return retval; }

			// Java.g:705:2: ( variableModifiers type IDENTIFIER ( '[' ']' )* )
			// Java.g:705:4: variableModifiers type IDENTIFIER ( '[' ']' )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_variableModifiers_in_formalParameter3270);
			variableModifiers216=variableModifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, variableModifiers216.getTree());

			pushFollow(FOLLOW_type_in_formalParameter3274);
			type217=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, type217.getTree());

			IDENTIFIER218=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_formalParameter3278); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER218_tree = (CommonTree)adaptor.create(IDENTIFIER218);
			adaptor.addChild(root_0, IDENTIFIER218_tree);
			}

			// Java.g:708:3: ( '[' ']' )*
			loop100:
			while (true) {
				int alt100=2;
				int LA100_0 = input.LA(1);
				if ( (LA100_0==LBRACKET) ) {
					alt100=1;
				}

				switch (alt100) {
				case 1 :
					// Java.g:708:4: '[' ']'
					{
					char_literal219=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_formalParameter3283); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal219_tree = (CommonTree)adaptor.create(char_literal219);
					adaptor.addChild(root_0, char_literal219_tree);
					}

					char_literal220=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_formalParameter3285); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal220_tree = (CommonTree)adaptor.create(char_literal220);
					adaptor.addChild(root_0, char_literal220_tree);
					}

					}
					break;

				default :
					break loop100;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 64, formalParameter_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "formalParameter"


	public static class forstatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "forstatement"
	// Java.g:712:1: forstatement : (a= 'for' '(' variableModifiers type IDENTIFIER ':' expression ')' statement -> ^( CONDITIONAL[$a] ) |a= 'for' '(' ( forInit )? ';' ( expression )? ';' ( expressionList )? ')' statement -> ^( CONDITIONAL[$a] ) );
	public final JavaParser.forstatement_return forstatement() throws RecognitionException {
		JavaParser.forstatement_return retval = new JavaParser.forstatement_return();
		retval.start = input.LT(1);
		int forstatement_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token char_literal221=null;
		Token IDENTIFIER224=null;
		Token char_literal225=null;
		Token char_literal227=null;
		Token char_literal229=null;
		Token char_literal231=null;
		Token char_literal233=null;
		Token char_literal235=null;
		ParserRuleReturnScope variableModifiers222 =null;
		ParserRuleReturnScope type223 =null;
		ParserRuleReturnScope expression226 =null;
		ParserRuleReturnScope statement228 =null;
		ParserRuleReturnScope forInit230 =null;
		ParserRuleReturnScope expression232 =null;
		ParserRuleReturnScope expressionList234 =null;
		ParserRuleReturnScope statement236 =null;

		CommonTree a_tree=null;
		CommonTree char_literal221_tree=null;
		CommonTree IDENTIFIER224_tree=null;
		CommonTree char_literal225_tree=null;
		CommonTree char_literal227_tree=null;
		CommonTree char_literal229_tree=null;
		CommonTree char_literal231_tree=null;
		CommonTree char_literal233_tree=null;
		CommonTree char_literal235_tree=null;
		RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_FOR=new RewriteRuleTokenStream(adaptor,"token FOR");
		RewriteRuleTokenStream stream_SEMI=new RewriteRuleTokenStream(adaptor,"token SEMI");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
		RewriteRuleSubtreeStream stream_statement=new RewriteRuleSubtreeStream(adaptor,"rule statement");
		RewriteRuleSubtreeStream stream_expressionList=new RewriteRuleSubtreeStream(adaptor,"rule expressionList");
		RewriteRuleSubtreeStream stream_variableModifiers=new RewriteRuleSubtreeStream(adaptor,"rule variableModifiers");
		RewriteRuleSubtreeStream stream_forInit=new RewriteRuleSubtreeStream(adaptor,"rule forInit");
		RewriteRuleSubtreeStream stream_type=new RewriteRuleSubtreeStream(adaptor,"rule type");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 65) ) { return retval; }

			// Java.g:713:2: (a= 'for' '(' variableModifiers type IDENTIFIER ':' expression ')' statement -> ^( CONDITIONAL[$a] ) |a= 'for' '(' ( forInit )? ';' ( expression )? ';' ( expressionList )? ')' statement -> ^( CONDITIONAL[$a] ) )
			int alt104=2;
			int LA104_0 = input.LA(1);
			if ( (LA104_0==FOR) ) {
				int LA104_1 = input.LA(2);
				if ( (synpred153_Java()) ) {
					alt104=1;
				}
				else if ( (true) ) {
					alt104=2;
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 104, 0, input);
				throw nvae;
			}

			switch (alt104) {
				case 1 :
					// Java.g:713:4: a= 'for' '(' variableModifiers type IDENTIFIER ':' expression ')' statement
					{
					a=(Token)match(input,FOR,FOLLOW_FOR_in_forstatement3301); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_FOR.add(a);

					char_literal221=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_forstatement3303); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(char_literal221);

					pushFollow(FOLLOW_variableModifiers_in_forstatement3309);
					variableModifiers222=variableModifiers();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_variableModifiers.add(variableModifiers222.getTree());
					pushFollow(FOLLOW_type_in_forstatement3314);
					type223=type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_type.add(type223.getTree());
					IDENTIFIER224=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_forstatement3319); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(IDENTIFIER224);

					char_literal225=(Token)match(input,COLON,FOLLOW_COLON_in_forstatement3324); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COLON.add(char_literal225);

					pushFollow(FOLLOW_expression_in_forstatement3329);
					expression226=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expression.add(expression226.getTree());
					char_literal227=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_forstatement3333); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(char_literal227);

					pushFollow(FOLLOW_statement_in_forstatement3337);
					statement228=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_statement.add(statement228.getTree());
					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 721:2: -> ^( CONDITIONAL[$a] )
					{
						// Java.g:721:5: ^( CONDITIONAL[$a] )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITIONAL, a), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:722:4: a= 'for' '(' ( forInit )? ';' ( expression )? ';' ( expressionList )? ')' statement
					{
					a=(Token)match(input,FOR,FOLLOW_FOR_in_forstatement3353); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_FOR.add(a);

					char_literal229=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_forstatement3355); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(char_literal229);

					// Java.g:723:4: ( forInit )?
					int alt101=2;
					int LA101_0 = input.LA(1);
					if ( (LA101_0==BANG||LA101_0==BOOLEAN||LA101_0==BYTE||(LA101_0 >= CHAR && LA101_0 <= CHARLITERAL)||(LA101_0 >= DOUBLE && LA101_0 <= DOUBLELITERAL)||LA101_0==FALSE||LA101_0==FINAL||(LA101_0 >= FLOAT && LA101_0 <= FLOATLITERAL)||LA101_0==IDENTIFIER||LA101_0==INT||LA101_0==INTLITERAL||(LA101_0 >= LONG && LA101_0 <= LPAREN)||LA101_0==MONKEYS_AT||(LA101_0 >= NEW && LA101_0 <= NULL)||LA101_0==PLUS||LA101_0==PLUSPLUS||LA101_0==SHORT||(LA101_0 >= STRINGLITERAL && LA101_0 <= SUB)||(LA101_0 >= SUBSUB && LA101_0 <= SUPER)||LA101_0==THIS||LA101_0==TILDE||LA101_0==TRUE||LA101_0==VOID) ) {
						alt101=1;
					}
					switch (alt101) {
						case 1 :
							// Java.g:723:4: forInit
							{
							pushFollow(FOLLOW_forInit_in_forstatement3360);
							forInit230=forInit();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_forInit.add(forInit230.getTree());
							}
							break;

					}

					char_literal231=(Token)match(input,SEMI,FOLLOW_SEMI_in_forstatement3366); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SEMI.add(char_literal231);

					// Java.g:725:4: ( expression )?
					int alt102=2;
					int LA102_0 = input.LA(1);
					if ( (LA102_0==BANG||LA102_0==BOOLEAN||LA102_0==BYTE||(LA102_0 >= CHAR && LA102_0 <= CHARLITERAL)||(LA102_0 >= DOUBLE && LA102_0 <= DOUBLELITERAL)||LA102_0==FALSE||(LA102_0 >= FLOAT && LA102_0 <= FLOATLITERAL)||LA102_0==IDENTIFIER||LA102_0==INT||LA102_0==INTLITERAL||(LA102_0 >= LONG && LA102_0 <= LPAREN)||(LA102_0 >= NEW && LA102_0 <= NULL)||LA102_0==PLUS||LA102_0==PLUSPLUS||LA102_0==SHORT||(LA102_0 >= STRINGLITERAL && LA102_0 <= SUB)||(LA102_0 >= SUBSUB && LA102_0 <= SUPER)||LA102_0==THIS||LA102_0==TILDE||LA102_0==TRUE||LA102_0==VOID) ) {
						alt102=1;
					}
					switch (alt102) {
						case 1 :
							// Java.g:725:4: expression
							{
							pushFollow(FOLLOW_expression_in_forstatement3371);
							expression232=expression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_expression.add(expression232.getTree());
							}
							break;

					}

					char_literal233=(Token)match(input,SEMI,FOLLOW_SEMI_in_forstatement3377); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SEMI.add(char_literal233);

					// Java.g:727:4: ( expressionList )?
					int alt103=2;
					int LA103_0 = input.LA(1);
					if ( (LA103_0==BANG||LA103_0==BOOLEAN||LA103_0==BYTE||(LA103_0 >= CHAR && LA103_0 <= CHARLITERAL)||(LA103_0 >= DOUBLE && LA103_0 <= DOUBLELITERAL)||LA103_0==FALSE||(LA103_0 >= FLOAT && LA103_0 <= FLOATLITERAL)||LA103_0==IDENTIFIER||LA103_0==INT||LA103_0==INTLITERAL||(LA103_0 >= LONG && LA103_0 <= LPAREN)||(LA103_0 >= NEW && LA103_0 <= NULL)||LA103_0==PLUS||LA103_0==PLUSPLUS||LA103_0==SHORT||(LA103_0 >= STRINGLITERAL && LA103_0 <= SUB)||(LA103_0 >= SUBSUB && LA103_0 <= SUPER)||LA103_0==THIS||LA103_0==TILDE||LA103_0==TRUE||LA103_0==VOID) ) {
						alt103=1;
					}
					switch (alt103) {
						case 1 :
							// Java.g:727:4: expressionList
							{
							pushFollow(FOLLOW_expressionList_in_forstatement3382);
							expressionList234=expressionList();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_expressionList.add(expressionList234.getTree());
							}
							break;

					}

					char_literal235=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_forstatement3387); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(char_literal235);

					pushFollow(FOLLOW_statement_in_forstatement3391);
					statement236=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_statement.add(statement236.getTree());
					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 730:2: -> ^( CONDITIONAL[$a] )
					{
						// Java.g:730:5: ^( CONDITIONAL[$a] )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CONDITIONAL, a), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 65, forstatement_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "forstatement"


	public static class forInit_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "forInit"
	// Java.g:734:1: forInit : ( localVariableDeclaration | expressionList );
	public final JavaParser.forInit_return forInit() throws RecognitionException {
		JavaParser.forInit_return retval = new JavaParser.forInit_return();
		retval.start = input.LT(1);
		int forInit_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope localVariableDeclaration237 =null;
		ParserRuleReturnScope expressionList238 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 66) ) { return retval; }

			// Java.g:735:2: ( localVariableDeclaration | expressionList )
			int alt105=2;
			switch ( input.LA(1) ) {
			case FINAL:
			case MONKEYS_AT:
				{
				alt105=1;
				}
				break;
			case IDENTIFIER:
				{
				int LA105_3 = input.LA(2);
				if ( (synpred157_Java()) ) {
					alt105=1;
				}
				else if ( (true) ) {
					alt105=2;
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				{
				int LA105_4 = input.LA(2);
				if ( (synpred157_Java()) ) {
					alt105=1;
				}
				else if ( (true) ) {
					alt105=2;
				}

				}
				break;
			case BANG:
			case CHARLITERAL:
			case DOUBLELITERAL:
			case FALSE:
			case FLOATLITERAL:
			case INTLITERAL:
			case LONGLITERAL:
			case LPAREN:
			case NEW:
			case NULL:
			case PLUS:
			case PLUSPLUS:
			case STRINGLITERAL:
			case SUB:
			case SUBSUB:
			case SUPER:
			case THIS:
			case TILDE:
			case TRUE:
			case VOID:
				{
				alt105=2;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 105, 0, input);
				throw nvae;
			}
			switch (alt105) {
				case 1 :
					// Java.g:735:4: localVariableDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_localVariableDeclaration_in_forInit3412);
					localVariableDeclaration237=localVariableDeclaration();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, localVariableDeclaration237.getTree());

					}
					break;
				case 2 :
					// Java.g:736:4: expressionList
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_expressionList_in_forInit3417);
					expressionList238=expressionList();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expressionList238.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 66, forInit_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "forInit"


	public static class parExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "parExpression"
	// Java.g:740:1: parExpression : '(' expression ')' -> expression ;
	public final JavaParser.parExpression_return parExpression() throws RecognitionException {
		JavaParser.parExpression_return retval = new JavaParser.parExpression_return();
		retval.start = input.LT(1);
		int parExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal239=null;
		Token char_literal241=null;
		ParserRuleReturnScope expression240 =null;

		CommonTree char_literal239_tree=null;
		CommonTree char_literal241_tree=null;
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 67) ) { return retval; }

			// Java.g:741:2: ( '(' expression ')' -> expression )
			// Java.g:741:4: '(' expression ')'
			{
			char_literal239=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_parExpression3429); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_LPAREN.add(char_literal239);

			pushFollow(FOLLOW_expression_in_parExpression3431);
			expression240=expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_expression.add(expression240.getTree());
			char_literal241=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_parExpression3433); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_RPAREN.add(char_literal241);

			// AST REWRITE
			// elements: expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 742:2: -> expression
			{
				adaptor.addChild(root_0, stream_expression.nextTree());
			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 67, parExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "parExpression"


	public static class expressionList_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expressionList"
	// Java.g:746:1: expressionList : expression ( ',' expression )* -> ( expression )+ ;
	public final JavaParser.expressionList_return expressionList() throws RecognitionException {
		JavaParser.expressionList_return retval = new JavaParser.expressionList_return();
		retval.start = input.LT(1);
		int expressionList_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal243=null;
		ParserRuleReturnScope expression242 =null;
		ParserRuleReturnScope expression244 =null;

		CommonTree char_literal243_tree=null;
		RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 68) ) { return retval; }

			// Java.g:747:2: ( expression ( ',' expression )* -> ( expression )+ )
			// Java.g:747:4: expression ( ',' expression )*
			{
			pushFollow(FOLLOW_expression_in_expressionList3450);
			expression242=expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) stream_expression.add(expression242.getTree());
			// Java.g:748:3: ( ',' expression )*
			loop106:
			while (true) {
				int alt106=2;
				int LA106_0 = input.LA(1);
				if ( (LA106_0==COMMA) ) {
					alt106=1;
				}

				switch (alt106) {
				case 1 :
					// Java.g:748:4: ',' expression
					{
					char_literal243=(Token)match(input,COMMA,FOLLOW_COMMA_in_expressionList3455); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_COMMA.add(char_literal243);

					pushFollow(FOLLOW_expression_in_expressionList3457);
					expression244=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expression.add(expression244.getTree());
					}
					break;

				default :
					break loop106;
				}
			}

			// AST REWRITE
			// elements: expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 749:2: -> ( expression )+
			{
				if ( !(stream_expression.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_expression.hasNext() ) {
					adaptor.addChild(root_0, stream_expression.nextTree());
				}
				stream_expression.reset();

			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 68, expressionList_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "expressionList"


	public static class expression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expression"
	// Java.g:753:1: expression : (a= conditionalExpression b= assignmentOperator c= subExpression -> ^( EXPRESSION ^( $b $a $c) ) |a= conditionalExpression -> ^( EXPRESSION $a) );
	public final JavaParser.expression_return expression() throws RecognitionException {
		JavaParser.expression_return retval = new JavaParser.expression_return();
		retval.start = input.LT(1);
		int expression_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope c =null;

		RewriteRuleSubtreeStream stream_assignmentOperator=new RewriteRuleSubtreeStream(adaptor,"rule assignmentOperator");
		RewriteRuleSubtreeStream stream_subExpression=new RewriteRuleSubtreeStream(adaptor,"rule subExpression");
		RewriteRuleSubtreeStream stream_conditionalExpression=new RewriteRuleSubtreeStream(adaptor,"rule conditionalExpression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 69) ) { return retval; }

			// Java.g:763:2: (a= conditionalExpression b= assignmentOperator c= subExpression -> ^( EXPRESSION ^( $b $a $c) ) |a= conditionalExpression -> ^( EXPRESSION $a) )
			int alt107=2;
			switch ( input.LA(1) ) {
			case PLUS:
				{
				int LA107_1 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case SUB:
				{
				int LA107_2 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case PLUSPLUS:
				{
				int LA107_3 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case SUBSUB:
				{
				int LA107_4 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case TILDE:
				{
				int LA107_5 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case BANG:
				{
				int LA107_6 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case LPAREN:
				{
				int LA107_7 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case THIS:
				{
				int LA107_8 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA107_9 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case SUPER:
				{
				int LA107_10 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case CHARLITERAL:
			case DOUBLELITERAL:
			case FALSE:
			case FLOATLITERAL:
			case INTLITERAL:
			case LONGLITERAL:
			case NULL:
			case STRINGLITERAL:
			case TRUE:
				{
				int LA107_11 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case NEW:
				{
				int LA107_12 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				{
				int LA107_13 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			case VOID:
				{
				int LA107_14 = input.LA(2);
				if ( (synpred159_Java()) ) {
					alt107=1;
				}
				else if ( (true) ) {
					alt107=2;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 107, 0, input);
				throw nvae;
			}
			switch (alt107) {
				case 1 :
					// Java.g:763:4: a= conditionalExpression b= assignmentOperator c= subExpression
					{
					pushFollow(FOLLOW_conditionalExpression_in_expression3481);
					a=conditionalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_conditionalExpression.add(a.getTree());
					pushFollow(FOLLOW_assignmentOperator_in_expression3485);
					b=assignmentOperator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_assignmentOperator.add(b.getTree());
					pushFollow(FOLLOW_subExpression_in_expression3489);
					c=subExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_subExpression.add(c.getTree());
					// AST REWRITE
					// elements: b, c, a
					// token labels: 
					// rule labels: retval, b, c, a
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);
					RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"rule c",c!=null?c.getTree():null);
					RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 764:2: -> ^( EXPRESSION ^( $b $a $c) )
					{
						// Java.g:764:5: ^( EXPRESSION ^( $b $a $c) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(EXPRESSION, "EXPRESSION"), root_1);
						// Java.g:764:18: ^( $b $a $c)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot(stream_b.nextNode(), root_2);
						adaptor.addChild(root_2, stream_a.nextTree());
						adaptor.addChild(root_2, stream_c.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:765:4: a= conditionalExpression
					{
					pushFollow(FOLLOW_conditionalExpression_in_expression3514);
					a=conditionalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_conditionalExpression.add(a.getTree());
					// AST REWRITE
					// elements: a
					// token labels: 
					// rule labels: retval, a
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 766:2: -> ^( EXPRESSION $a)
					{
						// Java.g:766:5: ^( EXPRESSION $a)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(EXPRESSION, "EXPRESSION"), root_1);
						adaptor.addChild(root_1, stream_a.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 69, expression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "expression"


	public static class subExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "subExpression"
	// Java.g:769:1: subExpression : (a= conditionalExpression b= assignmentOperator c= subExpression -> ^( $b $a $c) |a= conditionalExpression -> $a);
	public final JavaParser.subExpression_return subExpression() throws RecognitionException {
		JavaParser.subExpression_return retval = new JavaParser.subExpression_return();
		retval.start = input.LT(1);
		int subExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope c =null;

		RewriteRuleSubtreeStream stream_assignmentOperator=new RewriteRuleSubtreeStream(adaptor,"rule assignmentOperator");
		RewriteRuleSubtreeStream stream_subExpression=new RewriteRuleSubtreeStream(adaptor,"rule subExpression");
		RewriteRuleSubtreeStream stream_conditionalExpression=new RewriteRuleSubtreeStream(adaptor,"rule conditionalExpression");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 70) ) { return retval; }

			// Java.g:770:2: (a= conditionalExpression b= assignmentOperator c= subExpression -> ^( $b $a $c) |a= conditionalExpression -> $a)
			int alt108=2;
			switch ( input.LA(1) ) {
			case PLUS:
				{
				int LA108_1 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case SUB:
				{
				int LA108_2 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case PLUSPLUS:
				{
				int LA108_3 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case SUBSUB:
				{
				int LA108_4 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case TILDE:
				{
				int LA108_5 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case BANG:
				{
				int LA108_6 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case LPAREN:
				{
				int LA108_7 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case THIS:
				{
				int LA108_8 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA108_9 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case SUPER:
				{
				int LA108_10 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case CHARLITERAL:
			case DOUBLELITERAL:
			case FALSE:
			case FLOATLITERAL:
			case INTLITERAL:
			case LONGLITERAL:
			case NULL:
			case STRINGLITERAL:
			case TRUE:
				{
				int LA108_11 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case NEW:
				{
				int LA108_12 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				{
				int LA108_13 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			case VOID:
				{
				int LA108_14 = input.LA(2);
				if ( (synpred160_Java()) ) {
					alt108=1;
				}
				else if ( (true) ) {
					alt108=2;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 108, 0, input);
				throw nvae;
			}
			switch (alt108) {
				case 1 :
					// Java.g:770:4: a= conditionalExpression b= assignmentOperator c= subExpression
					{
					pushFollow(FOLLOW_conditionalExpression_in_subExpression3537);
					a=conditionalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_conditionalExpression.add(a.getTree());
					pushFollow(FOLLOW_assignmentOperator_in_subExpression3541);
					b=assignmentOperator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_assignmentOperator.add(b.getTree());
					pushFollow(FOLLOW_subExpression_in_subExpression3545);
					c=subExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_subExpression.add(c.getTree());
					// AST REWRITE
					// elements: a, c, b
					// token labels: 
					// rule labels: retval, b, c, a
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);
					RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"rule c",c!=null?c.getTree():null);
					RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 771:2: -> ^( $b $a $c)
					{
						// Java.g:771:5: ^( $b $a $c)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot(stream_b.nextNode(), root_1);
						adaptor.addChild(root_1, stream_a.nextTree());
						adaptor.addChild(root_1, stream_c.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:772:4: a= conditionalExpression
					{
					pushFollow(FOLLOW_conditionalExpression_in_subExpression3566);
					a=conditionalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_conditionalExpression.add(a.getTree());
					// AST REWRITE
					// elements: a
					// token labels: 
					// rule labels: retval, a
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 773:2: -> $a
					{
						adaptor.addChild(root_0, stream_a.nextTree());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 70, subExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "subExpression"


	public static class assignmentOperator_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "assignmentOperator"
	// Java.g:777:1: assignmentOperator : ( '=' -> '=' | '+=' -> '+=' | '-=' -> '-=' | '*=' -> '*=' | '/=' -> '/=' | '&=' -> '&=' | '|=' -> '|=' | '^=' -> '^=' | '%=' -> '%=' | '<' '<' '=' -> LT2EQ | '>' '>' '>' '=' -> GT3EQ | '>' '>' '=' -> GT2EQ );
	public final JavaParser.assignmentOperator_return assignmentOperator() throws RecognitionException {
		JavaParser.assignmentOperator_return retval = new JavaParser.assignmentOperator_return();
		retval.start = input.LT(1);
		int assignmentOperator_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal245=null;
		Token string_literal246=null;
		Token string_literal247=null;
		Token string_literal248=null;
		Token string_literal249=null;
		Token string_literal250=null;
		Token string_literal251=null;
		Token string_literal252=null;
		Token string_literal253=null;
		Token char_literal254=null;
		Token char_literal255=null;
		Token char_literal256=null;
		Token char_literal257=null;
		Token char_literal258=null;
		Token char_literal259=null;
		Token char_literal260=null;
		Token char_literal261=null;
		Token char_literal262=null;
		Token char_literal263=null;

		CommonTree char_literal245_tree=null;
		CommonTree string_literal246_tree=null;
		CommonTree string_literal247_tree=null;
		CommonTree string_literal248_tree=null;
		CommonTree string_literal249_tree=null;
		CommonTree string_literal250_tree=null;
		CommonTree string_literal251_tree=null;
		CommonTree string_literal252_tree=null;
		CommonTree string_literal253_tree=null;
		CommonTree char_literal254_tree=null;
		CommonTree char_literal255_tree=null;
		CommonTree char_literal256_tree=null;
		CommonTree char_literal257_tree=null;
		CommonTree char_literal258_tree=null;
		CommonTree char_literal259_tree=null;
		CommonTree char_literal260_tree=null;
		CommonTree char_literal261_tree=null;
		CommonTree char_literal262_tree=null;
		CommonTree char_literal263_tree=null;
		RewriteRuleTokenStream stream_SUBEQ=new RewriteRuleTokenStream(adaptor,"token SUBEQ");
		RewriteRuleTokenStream stream_GT=new RewriteRuleTokenStream(adaptor,"token GT");
		RewriteRuleTokenStream stream_CARETEQ=new RewriteRuleTokenStream(adaptor,"token CARETEQ");
		RewriteRuleTokenStream stream_LT=new RewriteRuleTokenStream(adaptor,"token LT");
		RewriteRuleTokenStream stream_SLASHEQ=new RewriteRuleTokenStream(adaptor,"token SLASHEQ");
		RewriteRuleTokenStream stream_PERCENTEQ=new RewriteRuleTokenStream(adaptor,"token PERCENTEQ");
		RewriteRuleTokenStream stream_BAREQ=new RewriteRuleTokenStream(adaptor,"token BAREQ");
		RewriteRuleTokenStream stream_EQ=new RewriteRuleTokenStream(adaptor,"token EQ");
		RewriteRuleTokenStream stream_AMPEQ=new RewriteRuleTokenStream(adaptor,"token AMPEQ");
		RewriteRuleTokenStream stream_STAREQ=new RewriteRuleTokenStream(adaptor,"token STAREQ");
		RewriteRuleTokenStream stream_PLUSEQ=new RewriteRuleTokenStream(adaptor,"token PLUSEQ");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 71) ) { return retval; }

			// Java.g:778:2: ( '=' -> '=' | '+=' -> '+=' | '-=' -> '-=' | '*=' -> '*=' | '/=' -> '/=' | '&=' -> '&=' | '|=' -> '|=' | '^=' -> '^=' | '%=' -> '%=' | '<' '<' '=' -> LT2EQ | '>' '>' '>' '=' -> GT3EQ | '>' '>' '=' -> GT2EQ )
			int alt109=12;
			switch ( input.LA(1) ) {
			case EQ:
				{
				alt109=1;
				}
				break;
			case PLUSEQ:
				{
				alt109=2;
				}
				break;
			case SUBEQ:
				{
				alt109=3;
				}
				break;
			case STAREQ:
				{
				alt109=4;
				}
				break;
			case SLASHEQ:
				{
				alt109=5;
				}
				break;
			case AMPEQ:
				{
				alt109=6;
				}
				break;
			case BAREQ:
				{
				alt109=7;
				}
				break;
			case CARETEQ:
				{
				alt109=8;
				}
				break;
			case PERCENTEQ:
				{
				alt109=9;
				}
				break;
			case LT:
				{
				alt109=10;
				}
				break;
			case GT:
				{
				int LA109_11 = input.LA(2);
				if ( (LA109_11==GT) ) {
					int LA109_12 = input.LA(3);
					if ( (LA109_12==GT) ) {
						alt109=11;
					}
					else if ( (LA109_12==EQ) ) {
						alt109=12;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 109, 12, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 109, 11, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 109, 0, input);
				throw nvae;
			}
			switch (alt109) {
				case 1 :
					// Java.g:778:4: '='
					{
					char_literal245=(Token)match(input,EQ,FOLLOW_EQ_in_assignmentOperator3584); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EQ.add(char_literal245);

					// AST REWRITE
					// elements: EQ
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 778:8: -> '='
					{
						adaptor.addChild(root_0, stream_EQ.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:779:4: '+='
					{
					string_literal246=(Token)match(input,PLUSEQ,FOLLOW_PLUSEQ_in_assignmentOperator3593); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PLUSEQ.add(string_literal246);

					// AST REWRITE
					// elements: PLUSEQ
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 779:9: -> '+='
					{
						adaptor.addChild(root_0, stream_PLUSEQ.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// Java.g:780:4: '-='
					{
					string_literal247=(Token)match(input,SUBEQ,FOLLOW_SUBEQ_in_assignmentOperator3602); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SUBEQ.add(string_literal247);

					// AST REWRITE
					// elements: SUBEQ
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 780:9: -> '-='
					{
						adaptor.addChild(root_0, stream_SUBEQ.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 4 :
					// Java.g:781:4: '*='
					{
					string_literal248=(Token)match(input,STAREQ,FOLLOW_STAREQ_in_assignmentOperator3611); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_STAREQ.add(string_literal248);

					// AST REWRITE
					// elements: STAREQ
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 781:9: -> '*='
					{
						adaptor.addChild(root_0, stream_STAREQ.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 5 :
					// Java.g:782:4: '/='
					{
					string_literal249=(Token)match(input,SLASHEQ,FOLLOW_SLASHEQ_in_assignmentOperator3620); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_SLASHEQ.add(string_literal249);

					// AST REWRITE
					// elements: SLASHEQ
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 782:9: -> '/='
					{
						adaptor.addChild(root_0, stream_SLASHEQ.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 6 :
					// Java.g:783:4: '&='
					{
					string_literal250=(Token)match(input,AMPEQ,FOLLOW_AMPEQ_in_assignmentOperator3629); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_AMPEQ.add(string_literal250);

					// AST REWRITE
					// elements: AMPEQ
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 783:9: -> '&='
					{
						adaptor.addChild(root_0, stream_AMPEQ.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 7 :
					// Java.g:784:4: '|='
					{
					string_literal251=(Token)match(input,BAREQ,FOLLOW_BAREQ_in_assignmentOperator3638); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_BAREQ.add(string_literal251);

					// AST REWRITE
					// elements: BAREQ
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 784:9: -> '|='
					{
						adaptor.addChild(root_0, stream_BAREQ.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 8 :
					// Java.g:785:4: '^='
					{
					string_literal252=(Token)match(input,CARETEQ,FOLLOW_CARETEQ_in_assignmentOperator3647); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_CARETEQ.add(string_literal252);

					// AST REWRITE
					// elements: CARETEQ
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 785:9: -> '^='
					{
						adaptor.addChild(root_0, stream_CARETEQ.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 9 :
					// Java.g:786:4: '%='
					{
					string_literal253=(Token)match(input,PERCENTEQ,FOLLOW_PERCENTEQ_in_assignmentOperator3656); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_PERCENTEQ.add(string_literal253);

					// AST REWRITE
					// elements: PERCENTEQ
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 786:9: -> '%='
					{
						adaptor.addChild(root_0, stream_PERCENTEQ.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;
				case 10 :
					// Java.g:787:4: '<' '<' '='
					{
					char_literal254=(Token)match(input,LT,FOLLOW_LT_in_assignmentOperator3665); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LT.add(char_literal254);

					char_literal255=(Token)match(input,LT,FOLLOW_LT_in_assignmentOperator3667); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LT.add(char_literal255);

					char_literal256=(Token)match(input,EQ,FOLLOW_EQ_in_assignmentOperator3669); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EQ.add(char_literal256);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 787:16: -> LT2EQ
					{
						adaptor.addChild(root_0, (CommonTree)adaptor.create(LT2EQ, "LT2EQ"));
					}


					retval.tree = root_0;
					}

					}
					break;
				case 11 :
					// Java.g:788:4: '>' '>' '>' '='
					{
					char_literal257=(Token)match(input,GT,FOLLOW_GT_in_assignmentOperator3678); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_GT.add(char_literal257);

					char_literal258=(Token)match(input,GT,FOLLOW_GT_in_assignmentOperator3680); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_GT.add(char_literal258);

					char_literal259=(Token)match(input,GT,FOLLOW_GT_in_assignmentOperator3682); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_GT.add(char_literal259);

					char_literal260=(Token)match(input,EQ,FOLLOW_EQ_in_assignmentOperator3684); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EQ.add(char_literal260);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 788:20: -> GT3EQ
					{
						adaptor.addChild(root_0, (CommonTree)adaptor.create(GT3EQ, "GT3EQ"));
					}


					retval.tree = root_0;
					}

					}
					break;
				case 12 :
					// Java.g:789:4: '>' '>' '='
					{
					char_literal261=(Token)match(input,GT,FOLLOW_GT_in_assignmentOperator3693); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_GT.add(char_literal261);

					char_literal262=(Token)match(input,GT,FOLLOW_GT_in_assignmentOperator3695); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_GT.add(char_literal262);

					char_literal263=(Token)match(input,EQ,FOLLOW_EQ_in_assignmentOperator3697); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EQ.add(char_literal263);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 789:16: -> GT2EQ
					{
						adaptor.addChild(root_0, (CommonTree)adaptor.create(GT2EQ, "GT2EQ"));
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 71, assignmentOperator_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "assignmentOperator"


	public static class conditionalExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "conditionalExpression"
	// Java.g:793:1: conditionalExpression : conditionalOrExpression ( '?' expression ':' conditionalExpression )? ;
	public final JavaParser.conditionalExpression_return conditionalExpression() throws RecognitionException {
		JavaParser.conditionalExpression_return retval = new JavaParser.conditionalExpression_return();
		retval.start = input.LT(1);
		int conditionalExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal265=null;
		Token char_literal267=null;
		ParserRuleReturnScope conditionalOrExpression264 =null;
		ParserRuleReturnScope expression266 =null;
		ParserRuleReturnScope conditionalExpression268 =null;

		CommonTree char_literal265_tree=null;
		CommonTree char_literal267_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 72) ) { return retval; }

			// Java.g:794:2: ( conditionalOrExpression ( '?' expression ':' conditionalExpression )? )
			// Java.g:794:4: conditionalOrExpression ( '?' expression ':' conditionalExpression )?
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_conditionalOrExpression_in_conditionalExpression3713);
			conditionalOrExpression264=conditionalOrExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, conditionalOrExpression264.getTree());

			// Java.g:795:3: ( '?' expression ':' conditionalExpression )?
			int alt110=2;
			int LA110_0 = input.LA(1);
			if ( (LA110_0==QUES) ) {
				alt110=1;
			}
			switch (alt110) {
				case 1 :
					// Java.g:795:4: '?' expression ':' conditionalExpression
					{
					char_literal265=(Token)match(input,QUES,FOLLOW_QUES_in_conditionalExpression3718); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal265_tree = (CommonTree)adaptor.create(char_literal265);
					adaptor.addChild(root_0, char_literal265_tree);
					}

					pushFollow(FOLLOW_expression_in_conditionalExpression3722);
					expression266=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression266.getTree());

					char_literal267=(Token)match(input,COLON,FOLLOW_COLON_in_conditionalExpression3726); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal267_tree = (CommonTree)adaptor.create(char_literal267);
					adaptor.addChild(root_0, char_literal267_tree);
					}

					pushFollow(FOLLOW_conditionalExpression_in_conditionalExpression3730);
					conditionalExpression268=conditionalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, conditionalExpression268.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 72, conditionalExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "conditionalExpression"


	public static class conditionalOrExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "conditionalOrExpression"
	// Java.g:803:1: conditionalOrExpression : conditionalAndExpression ( '||' ^ conditionalAndExpression )* ;
	public final JavaParser.conditionalOrExpression_return conditionalOrExpression() throws RecognitionException {
		JavaParser.conditionalOrExpression_return retval = new JavaParser.conditionalOrExpression_return();
		retval.start = input.LT(1);
		int conditionalOrExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal270=null;
		ParserRuleReturnScope conditionalAndExpression269 =null;
		ParserRuleReturnScope conditionalAndExpression271 =null;

		CommonTree string_literal270_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 73) ) { return retval; }

			// Java.g:804:2: ( conditionalAndExpression ( '||' ^ conditionalAndExpression )* )
			// Java.g:804:4: conditionalAndExpression ( '||' ^ conditionalAndExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_conditionalAndExpression_in_conditionalOrExpression3747);
			conditionalAndExpression269=conditionalAndExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, conditionalAndExpression269.getTree());

			// Java.g:805:3: ( '||' ^ conditionalAndExpression )*
			loop111:
			while (true) {
				int alt111=2;
				int LA111_0 = input.LA(1);
				if ( (LA111_0==BARBAR) ) {
					alt111=1;
				}

				switch (alt111) {
				case 1 :
					// Java.g:805:4: '||' ^ conditionalAndExpression
					{
					string_literal270=(Token)match(input,BARBAR,FOLLOW_BARBAR_in_conditionalOrExpression3752); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal270_tree = (CommonTree)adaptor.create(string_literal270);
					root_0 = (CommonTree)adaptor.becomeRoot(string_literal270_tree, root_0);
					}

					pushFollow(FOLLOW_conditionalAndExpression_in_conditionalOrExpression3755);
					conditionalAndExpression271=conditionalAndExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, conditionalAndExpression271.getTree());

					}
					break;

				default :
					break loop111;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 73, conditionalOrExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "conditionalOrExpression"


	public static class conditionalAndExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "conditionalAndExpression"
	// Java.g:809:1: conditionalAndExpression : inclusiveOrExpression ( '&&' ^ inclusiveOrExpression )* ;
	public final JavaParser.conditionalAndExpression_return conditionalAndExpression() throws RecognitionException {
		JavaParser.conditionalAndExpression_return retval = new JavaParser.conditionalAndExpression_return();
		retval.start = input.LT(1);
		int conditionalAndExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal273=null;
		ParserRuleReturnScope inclusiveOrExpression272 =null;
		ParserRuleReturnScope inclusiveOrExpression274 =null;

		CommonTree string_literal273_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 74) ) { return retval; }

			// Java.g:810:2: ( inclusiveOrExpression ( '&&' ^ inclusiveOrExpression )* )
			// Java.g:810:4: inclusiveOrExpression ( '&&' ^ inclusiveOrExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_inclusiveOrExpression_in_conditionalAndExpression3769);
			inclusiveOrExpression272=inclusiveOrExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, inclusiveOrExpression272.getTree());

			// Java.g:811:3: ( '&&' ^ inclusiveOrExpression )*
			loop112:
			while (true) {
				int alt112=2;
				int LA112_0 = input.LA(1);
				if ( (LA112_0==AMPAMP) ) {
					alt112=1;
				}

				switch (alt112) {
				case 1 :
					// Java.g:811:4: '&&' ^ inclusiveOrExpression
					{
					string_literal273=(Token)match(input,AMPAMP,FOLLOW_AMPAMP_in_conditionalAndExpression3774); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal273_tree = (CommonTree)adaptor.create(string_literal273);
					root_0 = (CommonTree)adaptor.becomeRoot(string_literal273_tree, root_0);
					}

					pushFollow(FOLLOW_inclusiveOrExpression_in_conditionalAndExpression3777);
					inclusiveOrExpression274=inclusiveOrExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, inclusiveOrExpression274.getTree());

					}
					break;

				default :
					break loop112;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 74, conditionalAndExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "conditionalAndExpression"


	public static class inclusiveOrExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "inclusiveOrExpression"
	// Java.g:815:1: inclusiveOrExpression : exclusiveOrExpression ( '|' ^ exclusiveOrExpression )* ;
	public final JavaParser.inclusiveOrExpression_return inclusiveOrExpression() throws RecognitionException {
		JavaParser.inclusiveOrExpression_return retval = new JavaParser.inclusiveOrExpression_return();
		retval.start = input.LT(1);
		int inclusiveOrExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal276=null;
		ParserRuleReturnScope exclusiveOrExpression275 =null;
		ParserRuleReturnScope exclusiveOrExpression277 =null;

		CommonTree char_literal276_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 75) ) { return retval; }

			// Java.g:816:2: ( exclusiveOrExpression ( '|' ^ exclusiveOrExpression )* )
			// Java.g:816:4: exclusiveOrExpression ( '|' ^ exclusiveOrExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression3791);
			exclusiveOrExpression275=exclusiveOrExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, exclusiveOrExpression275.getTree());

			// Java.g:817:3: ( '|' ^ exclusiveOrExpression )*
			loop113:
			while (true) {
				int alt113=2;
				int LA113_0 = input.LA(1);
				if ( (LA113_0==BAR) ) {
					alt113=1;
				}

				switch (alt113) {
				case 1 :
					// Java.g:817:4: '|' ^ exclusiveOrExpression
					{
					char_literal276=(Token)match(input,BAR,FOLLOW_BAR_in_inclusiveOrExpression3796); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal276_tree = (CommonTree)adaptor.create(char_literal276);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal276_tree, root_0);
					}

					pushFollow(FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression3799);
					exclusiveOrExpression277=exclusiveOrExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, exclusiveOrExpression277.getTree());

					}
					break;

				default :
					break loop113;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 75, inclusiveOrExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "inclusiveOrExpression"


	public static class exclusiveOrExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "exclusiveOrExpression"
	// Java.g:821:1: exclusiveOrExpression : andExpression ( '^' ^ andExpression )* ;
	public final JavaParser.exclusiveOrExpression_return exclusiveOrExpression() throws RecognitionException {
		JavaParser.exclusiveOrExpression_return retval = new JavaParser.exclusiveOrExpression_return();
		retval.start = input.LT(1);
		int exclusiveOrExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal279=null;
		ParserRuleReturnScope andExpression278 =null;
		ParserRuleReturnScope andExpression280 =null;

		CommonTree char_literal279_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 76) ) { return retval; }

			// Java.g:822:2: ( andExpression ( '^' ^ andExpression )* )
			// Java.g:822:4: andExpression ( '^' ^ andExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_andExpression_in_exclusiveOrExpression3813);
			andExpression278=andExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, andExpression278.getTree());

			// Java.g:823:3: ( '^' ^ andExpression )*
			loop114:
			while (true) {
				int alt114=2;
				int LA114_0 = input.LA(1);
				if ( (LA114_0==CARET) ) {
					alt114=1;
				}

				switch (alt114) {
				case 1 :
					// Java.g:823:4: '^' ^ andExpression
					{
					char_literal279=(Token)match(input,CARET,FOLLOW_CARET_in_exclusiveOrExpression3818); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal279_tree = (CommonTree)adaptor.create(char_literal279);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal279_tree, root_0);
					}

					pushFollow(FOLLOW_andExpression_in_exclusiveOrExpression3821);
					andExpression280=andExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, andExpression280.getTree());

					}
					break;

				default :
					break loop114;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 76, exclusiveOrExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "exclusiveOrExpression"


	public static class andExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "andExpression"
	// Java.g:827:1: andExpression : equalityExpression ( '&' ^ equalityExpression )* ;
	public final JavaParser.andExpression_return andExpression() throws RecognitionException {
		JavaParser.andExpression_return retval = new JavaParser.andExpression_return();
		retval.start = input.LT(1);
		int andExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal282=null;
		ParserRuleReturnScope equalityExpression281 =null;
		ParserRuleReturnScope equalityExpression283 =null;

		CommonTree char_literal282_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 77) ) { return retval; }

			// Java.g:828:2: ( equalityExpression ( '&' ^ equalityExpression )* )
			// Java.g:828:4: equalityExpression ( '&' ^ equalityExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_equalityExpression_in_andExpression3835);
			equalityExpression281=equalityExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, equalityExpression281.getTree());

			// Java.g:829:3: ( '&' ^ equalityExpression )*
			loop115:
			while (true) {
				int alt115=2;
				int LA115_0 = input.LA(1);
				if ( (LA115_0==AMP) ) {
					alt115=1;
				}

				switch (alt115) {
				case 1 :
					// Java.g:829:4: '&' ^ equalityExpression
					{
					char_literal282=(Token)match(input,AMP,FOLLOW_AMP_in_andExpression3840); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal282_tree = (CommonTree)adaptor.create(char_literal282);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal282_tree, root_0);
					}

					pushFollow(FOLLOW_equalityExpression_in_andExpression3843);
					equalityExpression283=equalityExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, equalityExpression283.getTree());

					}
					break;

				default :
					break loop115;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 77, andExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "andExpression"


	public static class equalityExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "equalityExpression"
	// Java.g:833:1: equalityExpression : instanceOfExpression ( ( '==' | '!=' ) ^ instanceOfExpression )* ;
	public final JavaParser.equalityExpression_return equalityExpression() throws RecognitionException {
		JavaParser.equalityExpression_return retval = new JavaParser.equalityExpression_return();
		retval.start = input.LT(1);
		int equalityExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token set285=null;
		ParserRuleReturnScope instanceOfExpression284 =null;
		ParserRuleReturnScope instanceOfExpression286 =null;

		CommonTree set285_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 78) ) { return retval; }

			// Java.g:834:2: ( instanceOfExpression ( ( '==' | '!=' ) ^ instanceOfExpression )* )
			// Java.g:834:4: instanceOfExpression ( ( '==' | '!=' ) ^ instanceOfExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_instanceOfExpression_in_equalityExpression3857);
			instanceOfExpression284=instanceOfExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, instanceOfExpression284.getTree());

			// Java.g:835:3: ( ( '==' | '!=' ) ^ instanceOfExpression )*
			loop116:
			while (true) {
				int alt116=2;
				int LA116_0 = input.LA(1);
				if ( (LA116_0==BANGEQ||LA116_0==EQEQ) ) {
					alt116=1;
				}

				switch (alt116) {
				case 1 :
					// Java.g:836:4: ( '==' | '!=' ) ^ instanceOfExpression
					{
					set285=input.LT(1);
					set285=input.LT(1);
					if ( input.LA(1)==BANGEQ||input.LA(1)==EQEQ ) {
						input.consume();
						if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(set285), root_0);
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					pushFollow(FOLLOW_instanceOfExpression_in_equalityExpression3878);
					instanceOfExpression286=instanceOfExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, instanceOfExpression286.getTree());

					}
					break;

				default :
					break loop116;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 78, equalityExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "equalityExpression"


	public static class instanceOfExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "instanceOfExpression"
	// Java.g:842:1: instanceOfExpression : relationalExpression ( 'instanceof' ^ type )? ;
	public final JavaParser.instanceOfExpression_return instanceOfExpression() throws RecognitionException {
		JavaParser.instanceOfExpression_return retval = new JavaParser.instanceOfExpression_return();
		retval.start = input.LT(1);
		int instanceOfExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal288=null;
		ParserRuleReturnScope relationalExpression287 =null;
		ParserRuleReturnScope type289 =null;

		CommonTree string_literal288_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 79) ) { return retval; }

			// Java.g:843:2: ( relationalExpression ( 'instanceof' ^ type )? )
			// Java.g:843:4: relationalExpression ( 'instanceof' ^ type )?
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_relationalExpression_in_instanceOfExpression3895);
			relationalExpression287=relationalExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, relationalExpression287.getTree());

			// Java.g:844:3: ( 'instanceof' ^ type )?
			int alt117=2;
			int LA117_0 = input.LA(1);
			if ( (LA117_0==INSTANCEOF) ) {
				alt117=1;
			}
			switch (alt117) {
				case 1 :
					// Java.g:844:4: 'instanceof' ^ type
					{
					string_literal288=(Token)match(input,INSTANCEOF,FOLLOW_INSTANCEOF_in_instanceOfExpression3900); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal288_tree = (CommonTree)adaptor.create(string_literal288);
					root_0 = (CommonTree)adaptor.becomeRoot(string_literal288_tree, root_0);
					}

					pushFollow(FOLLOW_type_in_instanceOfExpression3903);
					type289=type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, type289.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 79, instanceOfExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "instanceOfExpression"


	public static class relationalExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "relationalExpression"
	// Java.g:848:1: relationalExpression : shiftExpression ( relationalOp ^ shiftExpression )* ;
	public final JavaParser.relationalExpression_return relationalExpression() throws RecognitionException {
		JavaParser.relationalExpression_return retval = new JavaParser.relationalExpression_return();
		retval.start = input.LT(1);
		int relationalExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope shiftExpression290 =null;
		ParserRuleReturnScope relationalOp291 =null;
		ParserRuleReturnScope shiftExpression292 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 80) ) { return retval; }

			// Java.g:849:2: ( shiftExpression ( relationalOp ^ shiftExpression )* )
			// Java.g:849:4: shiftExpression ( relationalOp ^ shiftExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_shiftExpression_in_relationalExpression3917);
			shiftExpression290=shiftExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, shiftExpression290.getTree());

			// Java.g:850:3: ( relationalOp ^ shiftExpression )*
			loop118:
			while (true) {
				int alt118=2;
				int LA118_0 = input.LA(1);
				if ( (LA118_0==LT) ) {
					int LA118_2 = input.LA(2);
					if ( (LA118_2==BANG||LA118_2==BOOLEAN||LA118_2==BYTE||(LA118_2 >= CHAR && LA118_2 <= CHARLITERAL)||(LA118_2 >= DOUBLE && LA118_2 <= DOUBLELITERAL)||LA118_2==EQ||LA118_2==FALSE||(LA118_2 >= FLOAT && LA118_2 <= FLOATLITERAL)||LA118_2==IDENTIFIER||LA118_2==INT||LA118_2==INTLITERAL||(LA118_2 >= LONG && LA118_2 <= LPAREN)||(LA118_2 >= NEW && LA118_2 <= NULL)||LA118_2==PLUS||LA118_2==PLUSPLUS||LA118_2==SHORT||(LA118_2 >= STRINGLITERAL && LA118_2 <= SUB)||(LA118_2 >= SUBSUB && LA118_2 <= SUPER)||LA118_2==THIS||LA118_2==TILDE||LA118_2==TRUE||LA118_2==VOID) ) {
						alt118=1;
					}

				}
				else if ( (LA118_0==GT) ) {
					int LA118_3 = input.LA(2);
					if ( (LA118_3==BANG||LA118_3==BOOLEAN||LA118_3==BYTE||(LA118_3 >= CHAR && LA118_3 <= CHARLITERAL)||(LA118_3 >= DOUBLE && LA118_3 <= DOUBLELITERAL)||LA118_3==EQ||LA118_3==FALSE||(LA118_3 >= FLOAT && LA118_3 <= FLOATLITERAL)||LA118_3==IDENTIFIER||LA118_3==INT||LA118_3==INTLITERAL||(LA118_3 >= LONG && LA118_3 <= LPAREN)||(LA118_3 >= NEW && LA118_3 <= NULL)||LA118_3==PLUS||LA118_3==PLUSPLUS||LA118_3==SHORT||(LA118_3 >= STRINGLITERAL && LA118_3 <= SUB)||(LA118_3 >= SUBSUB && LA118_3 <= SUPER)||LA118_3==THIS||LA118_3==TILDE||LA118_3==TRUE||LA118_3==VOID) ) {
						alt118=1;
					}

				}

				switch (alt118) {
				case 1 :
					// Java.g:850:4: relationalOp ^ shiftExpression
					{
					pushFollow(FOLLOW_relationalOp_in_relationalExpression3922);
					relationalOp291=relationalOp();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(relationalOp291.getTree(), root_0);
					pushFollow(FOLLOW_shiftExpression_in_relationalExpression3925);
					shiftExpression292=shiftExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, shiftExpression292.getTree());

					}
					break;

				default :
					break loop118;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 80, relationalExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "relationalExpression"


	public static class relationalOp_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "relationalOp"
	// Java.g:854:1: relationalOp : ( '<' '=' -> LTEQ | '>' '=' -> GTEQ | '<' -> LT | '>' -> GT );
	public final JavaParser.relationalOp_return relationalOp() throws RecognitionException {
		JavaParser.relationalOp_return retval = new JavaParser.relationalOp_return();
		retval.start = input.LT(1);
		int relationalOp_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal293=null;
		Token char_literal294=null;
		Token char_literal295=null;
		Token char_literal296=null;
		Token char_literal297=null;
		Token char_literal298=null;

		CommonTree char_literal293_tree=null;
		CommonTree char_literal294_tree=null;
		CommonTree char_literal295_tree=null;
		CommonTree char_literal296_tree=null;
		CommonTree char_literal297_tree=null;
		CommonTree char_literal298_tree=null;
		RewriteRuleTokenStream stream_GT=new RewriteRuleTokenStream(adaptor,"token GT");
		RewriteRuleTokenStream stream_LT=new RewriteRuleTokenStream(adaptor,"token LT");
		RewriteRuleTokenStream stream_EQ=new RewriteRuleTokenStream(adaptor,"token EQ");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 81) ) { return retval; }

			// Java.g:855:2: ( '<' '=' -> LTEQ | '>' '=' -> GTEQ | '<' -> LT | '>' -> GT )
			int alt119=4;
			int LA119_0 = input.LA(1);
			if ( (LA119_0==LT) ) {
				int LA119_1 = input.LA(2);
				if ( (LA119_1==EQ) ) {
					alt119=1;
				}
				else if ( (LA119_1==BANG||LA119_1==BOOLEAN||LA119_1==BYTE||(LA119_1 >= CHAR && LA119_1 <= CHARLITERAL)||(LA119_1 >= DOUBLE && LA119_1 <= DOUBLELITERAL)||LA119_1==FALSE||(LA119_1 >= FLOAT && LA119_1 <= FLOATLITERAL)||LA119_1==IDENTIFIER||LA119_1==INT||LA119_1==INTLITERAL||(LA119_1 >= LONG && LA119_1 <= LPAREN)||(LA119_1 >= NEW && LA119_1 <= NULL)||LA119_1==PLUS||LA119_1==PLUSPLUS||LA119_1==SHORT||(LA119_1 >= STRINGLITERAL && LA119_1 <= SUB)||(LA119_1 >= SUBSUB && LA119_1 <= SUPER)||LA119_1==THIS||LA119_1==TILDE||LA119_1==TRUE||LA119_1==VOID) ) {
					alt119=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 119, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA119_0==GT) ) {
				int LA119_2 = input.LA(2);
				if ( (LA119_2==EQ) ) {
					alt119=2;
				}
				else if ( (LA119_2==BANG||LA119_2==BOOLEAN||LA119_2==BYTE||(LA119_2 >= CHAR && LA119_2 <= CHARLITERAL)||(LA119_2 >= DOUBLE && LA119_2 <= DOUBLELITERAL)||LA119_2==FALSE||(LA119_2 >= FLOAT && LA119_2 <= FLOATLITERAL)||LA119_2==IDENTIFIER||LA119_2==INT||LA119_2==INTLITERAL||(LA119_2 >= LONG && LA119_2 <= LPAREN)||(LA119_2 >= NEW && LA119_2 <= NULL)||LA119_2==PLUS||LA119_2==PLUSPLUS||LA119_2==SHORT||(LA119_2 >= STRINGLITERAL && LA119_2 <= SUB)||(LA119_2 >= SUBSUB && LA119_2 <= SUPER)||LA119_2==THIS||LA119_2==TILDE||LA119_2==TRUE||LA119_2==VOID) ) {
					alt119=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 119, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 119, 0, input);
				throw nvae;
			}

			switch (alt119) {
				case 1 :
					// Java.g:855:4: '<' '='
					{
					char_literal293=(Token)match(input,LT,FOLLOW_LT_in_relationalOp3939); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LT.add(char_literal293);

					char_literal294=(Token)match(input,EQ,FOLLOW_EQ_in_relationalOp3941); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EQ.add(char_literal294);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 855:12: -> LTEQ
					{
						adaptor.addChild(root_0, (CommonTree)adaptor.create(LTEQ, "LTEQ"));
					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:856:4: '>' '='
					{
					char_literal295=(Token)match(input,GT,FOLLOW_GT_in_relationalOp3950); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_GT.add(char_literal295);

					char_literal296=(Token)match(input,EQ,FOLLOW_EQ_in_relationalOp3952); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_EQ.add(char_literal296);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 856:12: -> GTEQ
					{
						adaptor.addChild(root_0, (CommonTree)adaptor.create(GTEQ, "GTEQ"));
					}


					retval.tree = root_0;
					}

					}
					break;
				case 3 :
					// Java.g:857:4: '<'
					{
					char_literal297=(Token)match(input,LT,FOLLOW_LT_in_relationalOp3961); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LT.add(char_literal297);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 857:9: -> LT
					{
						adaptor.addChild(root_0, (CommonTree)adaptor.create(LT, "LT"));
					}


					retval.tree = root_0;
					}

					}
					break;
				case 4 :
					// Java.g:858:4: '>'
					{
					char_literal298=(Token)match(input,GT,FOLLOW_GT_in_relationalOp3971); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_GT.add(char_literal298);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 858:9: -> GT
					{
						adaptor.addChild(root_0, (CommonTree)adaptor.create(GT, "GT"));
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 81, relationalOp_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "relationalOp"


	public static class shiftExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "shiftExpression"
	// Java.g:862:1: shiftExpression : additiveExpression ( shiftOp ^ additiveExpression )* ;
	public final JavaParser.shiftExpression_return shiftExpression() throws RecognitionException {
		JavaParser.shiftExpression_return retval = new JavaParser.shiftExpression_return();
		retval.start = input.LT(1);
		int shiftExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope additiveExpression299 =null;
		ParserRuleReturnScope shiftOp300 =null;
		ParserRuleReturnScope additiveExpression301 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 82) ) { return retval; }

			// Java.g:863:2: ( additiveExpression ( shiftOp ^ additiveExpression )* )
			// Java.g:863:4: additiveExpression ( shiftOp ^ additiveExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_additiveExpression_in_shiftExpression3988);
			additiveExpression299=additiveExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, additiveExpression299.getTree());

			// Java.g:864:3: ( shiftOp ^ additiveExpression )*
			loop120:
			while (true) {
				int alt120=2;
				int LA120_0 = input.LA(1);
				if ( (LA120_0==LT) ) {
					int LA120_1 = input.LA(2);
					if ( (LA120_1==LT) ) {
						int LA120_4 = input.LA(3);
						if ( (LA120_4==BANG||LA120_4==BOOLEAN||LA120_4==BYTE||(LA120_4 >= CHAR && LA120_4 <= CHARLITERAL)||(LA120_4 >= DOUBLE && LA120_4 <= DOUBLELITERAL)||LA120_4==FALSE||(LA120_4 >= FLOAT && LA120_4 <= FLOATLITERAL)||LA120_4==IDENTIFIER||LA120_4==INT||LA120_4==INTLITERAL||(LA120_4 >= LONG && LA120_4 <= LPAREN)||(LA120_4 >= NEW && LA120_4 <= NULL)||LA120_4==PLUS||LA120_4==PLUSPLUS||LA120_4==SHORT||(LA120_4 >= STRINGLITERAL && LA120_4 <= SUB)||(LA120_4 >= SUBSUB && LA120_4 <= SUPER)||LA120_4==THIS||LA120_4==TILDE||LA120_4==TRUE||LA120_4==VOID) ) {
							alt120=1;
						}

					}

				}
				else if ( (LA120_0==GT) ) {
					int LA120_2 = input.LA(2);
					if ( (LA120_2==GT) ) {
						int LA120_5 = input.LA(3);
						if ( (LA120_5==GT) ) {
							int LA120_7 = input.LA(4);
							if ( (LA120_7==BANG||LA120_7==BOOLEAN||LA120_7==BYTE||(LA120_7 >= CHAR && LA120_7 <= CHARLITERAL)||(LA120_7 >= DOUBLE && LA120_7 <= DOUBLELITERAL)||LA120_7==FALSE||(LA120_7 >= FLOAT && LA120_7 <= FLOATLITERAL)||LA120_7==IDENTIFIER||LA120_7==INT||LA120_7==INTLITERAL||(LA120_7 >= LONG && LA120_7 <= LPAREN)||(LA120_7 >= NEW && LA120_7 <= NULL)||LA120_7==PLUS||LA120_7==PLUSPLUS||LA120_7==SHORT||(LA120_7 >= STRINGLITERAL && LA120_7 <= SUB)||(LA120_7 >= SUBSUB && LA120_7 <= SUPER)||LA120_7==THIS||LA120_7==TILDE||LA120_7==TRUE||LA120_7==VOID) ) {
								alt120=1;
							}

						}
						else if ( (LA120_5==BANG||LA120_5==BOOLEAN||LA120_5==BYTE||(LA120_5 >= CHAR && LA120_5 <= CHARLITERAL)||(LA120_5 >= DOUBLE && LA120_5 <= DOUBLELITERAL)||LA120_5==FALSE||(LA120_5 >= FLOAT && LA120_5 <= FLOATLITERAL)||LA120_5==IDENTIFIER||LA120_5==INT||LA120_5==INTLITERAL||(LA120_5 >= LONG && LA120_5 <= LPAREN)||(LA120_5 >= NEW && LA120_5 <= NULL)||LA120_5==PLUS||LA120_5==PLUSPLUS||LA120_5==SHORT||(LA120_5 >= STRINGLITERAL && LA120_5 <= SUB)||(LA120_5 >= SUBSUB && LA120_5 <= SUPER)||LA120_5==THIS||LA120_5==TILDE||LA120_5==TRUE||LA120_5==VOID) ) {
							alt120=1;
						}

					}

				}

				switch (alt120) {
				case 1 :
					// Java.g:864:4: shiftOp ^ additiveExpression
					{
					pushFollow(FOLLOW_shiftOp_in_shiftExpression3993);
					shiftOp300=shiftOp();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot(shiftOp300.getTree(), root_0);
					pushFollow(FOLLOW_additiveExpression_in_shiftExpression3996);
					additiveExpression301=additiveExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, additiveExpression301.getTree());

					}
					break;

				default :
					break loop120;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 82, shiftExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "shiftExpression"


	public static class shiftOp_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "shiftOp"
	// Java.g:868:1: shiftOp : ( '<' '<' | '>' '>' '>' | '>' '>' );
	public final JavaParser.shiftOp_return shiftOp() throws RecognitionException {
		JavaParser.shiftOp_return retval = new JavaParser.shiftOp_return();
		retval.start = input.LT(1);
		int shiftOp_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal302=null;
		Token char_literal303=null;
		Token char_literal304=null;
		Token char_literal305=null;
		Token char_literal306=null;
		Token char_literal307=null;
		Token char_literal308=null;

		CommonTree char_literal302_tree=null;
		CommonTree char_literal303_tree=null;
		CommonTree char_literal304_tree=null;
		CommonTree char_literal305_tree=null;
		CommonTree char_literal306_tree=null;
		CommonTree char_literal307_tree=null;
		CommonTree char_literal308_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 83) ) { return retval; }

			// Java.g:869:2: ( '<' '<' | '>' '>' '>' | '>' '>' )
			int alt121=3;
			int LA121_0 = input.LA(1);
			if ( (LA121_0==LT) ) {
				alt121=1;
			}
			else if ( (LA121_0==GT) ) {
				int LA121_2 = input.LA(2);
				if ( (LA121_2==GT) ) {
					int LA121_3 = input.LA(3);
					if ( (LA121_3==GT) ) {
						alt121=2;
					}
					else if ( (LA121_3==BANG||LA121_3==BOOLEAN||LA121_3==BYTE||(LA121_3 >= CHAR && LA121_3 <= CHARLITERAL)||(LA121_3 >= DOUBLE && LA121_3 <= DOUBLELITERAL)||LA121_3==FALSE||(LA121_3 >= FLOAT && LA121_3 <= FLOATLITERAL)||LA121_3==IDENTIFIER||LA121_3==INT||LA121_3==INTLITERAL||(LA121_3 >= LONG && LA121_3 <= LPAREN)||(LA121_3 >= NEW && LA121_3 <= NULL)||LA121_3==PLUS||LA121_3==PLUSPLUS||LA121_3==SHORT||(LA121_3 >= STRINGLITERAL && LA121_3 <= SUB)||(LA121_3 >= SUBSUB && LA121_3 <= SUPER)||LA121_3==THIS||LA121_3==TILDE||LA121_3==TRUE||LA121_3==VOID) ) {
						alt121=3;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 121, 3, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 121, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 121, 0, input);
				throw nvae;
			}

			switch (alt121) {
				case 1 :
					// Java.g:869:4: '<' '<'
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal302=(Token)match(input,LT,FOLLOW_LT_in_shiftOp4010); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal302_tree = (CommonTree)adaptor.create(char_literal302);
					adaptor.addChild(root_0, char_literal302_tree);
					}

					char_literal303=(Token)match(input,LT,FOLLOW_LT_in_shiftOp4012); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal303_tree = (CommonTree)adaptor.create(char_literal303);
					adaptor.addChild(root_0, char_literal303_tree);
					}

					}
					break;
				case 2 :
					// Java.g:870:4: '>' '>' '>'
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal304=(Token)match(input,GT,FOLLOW_GT_in_shiftOp4017); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal304_tree = (CommonTree)adaptor.create(char_literal304);
					adaptor.addChild(root_0, char_literal304_tree);
					}

					char_literal305=(Token)match(input,GT,FOLLOW_GT_in_shiftOp4019); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal305_tree = (CommonTree)adaptor.create(char_literal305);
					adaptor.addChild(root_0, char_literal305_tree);
					}

					char_literal306=(Token)match(input,GT,FOLLOW_GT_in_shiftOp4021); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal306_tree = (CommonTree)adaptor.create(char_literal306);
					adaptor.addChild(root_0, char_literal306_tree);
					}

					}
					break;
				case 3 :
					// Java.g:871:4: '>' '>'
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal307=(Token)match(input,GT,FOLLOW_GT_in_shiftOp4026); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal307_tree = (CommonTree)adaptor.create(char_literal307);
					adaptor.addChild(root_0, char_literal307_tree);
					}

					char_literal308=(Token)match(input,GT,FOLLOW_GT_in_shiftOp4028); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal308_tree = (CommonTree)adaptor.create(char_literal308);
					adaptor.addChild(root_0, char_literal308_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 83, shiftOp_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "shiftOp"


	public static class additiveExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "additiveExpression"
	// Java.g:875:1: additiveExpression : multiplicativeExpression ( ( '+' | '-' ) ^ multiplicativeExpression )* ;
	public final JavaParser.additiveExpression_return additiveExpression() throws RecognitionException {
		JavaParser.additiveExpression_return retval = new JavaParser.additiveExpression_return();
		retval.start = input.LT(1);
		int additiveExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token set310=null;
		ParserRuleReturnScope multiplicativeExpression309 =null;
		ParserRuleReturnScope multiplicativeExpression311 =null;

		CommonTree set310_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 84) ) { return retval; }

			// Java.g:876:2: ( multiplicativeExpression ( ( '+' | '-' ) ^ multiplicativeExpression )* )
			// Java.g:876:4: multiplicativeExpression ( ( '+' | '-' ) ^ multiplicativeExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression4040);
			multiplicativeExpression309=multiplicativeExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, multiplicativeExpression309.getTree());

			// Java.g:877:3: ( ( '+' | '-' ) ^ multiplicativeExpression )*
			loop122:
			while (true) {
				int alt122=2;
				int LA122_0 = input.LA(1);
				if ( (LA122_0==PLUS||LA122_0==SUB) ) {
					alt122=1;
				}

				switch (alt122) {
				case 1 :
					// Java.g:878:4: ( '+' | '-' ) ^ multiplicativeExpression
					{
					set310=input.LT(1);
					set310=input.LT(1);
					if ( input.LA(1)==PLUS||input.LA(1)==SUB ) {
						input.consume();
						if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(set310), root_0);
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression4061);
					multiplicativeExpression311=multiplicativeExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, multiplicativeExpression311.getTree());

					}
					break;

				default :
					break loop122;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 84, additiveExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "additiveExpression"


	public static class multiplicativeExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "multiplicativeExpression"
	// Java.g:884:1: multiplicativeExpression : unaryExpression ( ( '*' | '/' | '%' ) ^ unaryExpression )* ;
	public final JavaParser.multiplicativeExpression_return multiplicativeExpression() throws RecognitionException {
		JavaParser.multiplicativeExpression_return retval = new JavaParser.multiplicativeExpression_return();
		retval.start = input.LT(1);
		int multiplicativeExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token set313=null;
		ParserRuleReturnScope unaryExpression312 =null;
		ParserRuleReturnScope unaryExpression314 =null;

		CommonTree set313_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 85) ) { return retval; }

			// Java.g:885:2: ( unaryExpression ( ( '*' | '/' | '%' ) ^ unaryExpression )* )
			// Java.g:885:4: unaryExpression ( ( '*' | '/' | '%' ) ^ unaryExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression4078);
			unaryExpression312=unaryExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression312.getTree());

			// Java.g:886:3: ( ( '*' | '/' | '%' ) ^ unaryExpression )*
			loop123:
			while (true) {
				int alt123=2;
				int LA123_0 = input.LA(1);
				if ( (LA123_0==PERCENT||LA123_0==SLASH||LA123_0==STAR) ) {
					alt123=1;
				}

				switch (alt123) {
				case 1 :
					// Java.g:887:4: ( '*' | '/' | '%' ) ^ unaryExpression
					{
					set313=input.LT(1);
					set313=input.LT(1);
					if ( input.LA(1)==PERCENT||input.LA(1)==SLASH||input.LA(1)==STAR ) {
						input.consume();
						if ( state.backtracking==0 ) root_0 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(set313), root_0);
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression4103);
					unaryExpression314=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression314.getTree());

					}
					break;

				default :
					break loop123;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 85, multiplicativeExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "multiplicativeExpression"


	public static class unaryExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "unaryExpression"
	// Java.g:897:1: unaryExpression : ( '+' ^ unaryExpression | '-' ^ unaryExpression | '++' ^ unaryExpression | '--' ^ unaryExpression | unaryExpressionNotPlusMinus );
	public final JavaParser.unaryExpression_return unaryExpression() throws RecognitionException {
		JavaParser.unaryExpression_return retval = new JavaParser.unaryExpression_return();
		retval.start = input.LT(1);
		int unaryExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal315=null;
		Token char_literal317=null;
		Token string_literal319=null;
		Token string_literal321=null;
		ParserRuleReturnScope unaryExpression316 =null;
		ParserRuleReturnScope unaryExpression318 =null;
		ParserRuleReturnScope unaryExpression320 =null;
		ParserRuleReturnScope unaryExpression322 =null;
		ParserRuleReturnScope unaryExpressionNotPlusMinus323 =null;

		CommonTree char_literal315_tree=null;
		CommonTree char_literal317_tree=null;
		CommonTree string_literal319_tree=null;
		CommonTree string_literal321_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 86) ) { return retval; }

			// Java.g:898:2: ( '+' ^ unaryExpression | '-' ^ unaryExpression | '++' ^ unaryExpression | '--' ^ unaryExpression | unaryExpressionNotPlusMinus )
			int alt124=5;
			switch ( input.LA(1) ) {
			case PLUS:
				{
				alt124=1;
				}
				break;
			case SUB:
				{
				alt124=2;
				}
				break;
			case PLUSPLUS:
				{
				alt124=3;
				}
				break;
			case SUBSUB:
				{
				alt124=4;
				}
				break;
			case BANG:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case CHARLITERAL:
			case DOUBLE:
			case DOUBLELITERAL:
			case FALSE:
			case FLOAT:
			case FLOATLITERAL:
			case IDENTIFIER:
			case INT:
			case INTLITERAL:
			case LONG:
			case LONGLITERAL:
			case LPAREN:
			case NEW:
			case NULL:
			case SHORT:
			case STRINGLITERAL:
			case SUPER:
			case THIS:
			case TILDE:
			case TRUE:
			case VOID:
				{
				alt124=5;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 124, 0, input);
				throw nvae;
			}
			switch (alt124) {
				case 1 :
					// Java.g:898:4: '+' ^ unaryExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal315=(Token)match(input,PLUS,FOLLOW_PLUS_in_unaryExpression4122); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal315_tree = (CommonTree)adaptor.create(char_literal315);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal315_tree, root_0);
					}

					pushFollow(FOLLOW_unaryExpression_in_unaryExpression4126);
					unaryExpression316=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression316.getTree());

					}
					break;
				case 2 :
					// Java.g:899:4: '-' ^ unaryExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal317=(Token)match(input,SUB,FOLLOW_SUB_in_unaryExpression4131); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal317_tree = (CommonTree)adaptor.create(char_literal317);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal317_tree, root_0);
					}

					pushFollow(FOLLOW_unaryExpression_in_unaryExpression4134);
					unaryExpression318=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression318.getTree());

					}
					break;
				case 3 :
					// Java.g:900:4: '++' ^ unaryExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal319=(Token)match(input,PLUSPLUS,FOLLOW_PLUSPLUS_in_unaryExpression4139); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal319_tree = (CommonTree)adaptor.create(string_literal319);
					root_0 = (CommonTree)adaptor.becomeRoot(string_literal319_tree, root_0);
					}

					pushFollow(FOLLOW_unaryExpression_in_unaryExpression4142);
					unaryExpression320=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression320.getTree());

					}
					break;
				case 4 :
					// Java.g:901:4: '--' ^ unaryExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal321=(Token)match(input,SUBSUB,FOLLOW_SUBSUB_in_unaryExpression4147); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal321_tree = (CommonTree)adaptor.create(string_literal321);
					root_0 = (CommonTree)adaptor.becomeRoot(string_literal321_tree, root_0);
					}

					pushFollow(FOLLOW_unaryExpression_in_unaryExpression4150);
					unaryExpression322=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression322.getTree());

					}
					break;
				case 5 :
					// Java.g:902:4: unaryExpressionNotPlusMinus
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_unaryExpressionNotPlusMinus_in_unaryExpression4155);
					unaryExpressionNotPlusMinus323=unaryExpressionNotPlusMinus();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpressionNotPlusMinus323.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 86, unaryExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "unaryExpression"


	public static class unaryExpressionNotPlusMinus_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "unaryExpressionNotPlusMinus"
	// Java.g:906:1: unaryExpressionNotPlusMinus : ( '~' ^ unaryExpression | '!' ^ unaryExpression | castExpression | primary ( selector )* ( '++' | '--' )? );
	public final JavaParser.unaryExpressionNotPlusMinus_return unaryExpressionNotPlusMinus() throws RecognitionException {
		JavaParser.unaryExpressionNotPlusMinus_return retval = new JavaParser.unaryExpressionNotPlusMinus_return();
		retval.start = input.LT(1);
		int unaryExpressionNotPlusMinus_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal324=null;
		Token char_literal326=null;
		Token set331=null;
		ParserRuleReturnScope unaryExpression325 =null;
		ParserRuleReturnScope unaryExpression327 =null;
		ParserRuleReturnScope castExpression328 =null;
		ParserRuleReturnScope primary329 =null;
		ParserRuleReturnScope selector330 =null;

		CommonTree char_literal324_tree=null;
		CommonTree char_literal326_tree=null;
		CommonTree set331_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 87) ) { return retval; }

			// Java.g:907:2: ( '~' ^ unaryExpression | '!' ^ unaryExpression | castExpression | primary ( selector )* ( '++' | '--' )? )
			int alt127=4;
			switch ( input.LA(1) ) {
			case TILDE:
				{
				alt127=1;
				}
				break;
			case BANG:
				{
				alt127=2;
				}
				break;
			case LPAREN:
				{
				int LA127_3 = input.LA(2);
				if ( (synpred199_Java()) ) {
					alt127=3;
				}
				else if ( (true) ) {
					alt127=4;
				}

				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case CHARLITERAL:
			case DOUBLE:
			case DOUBLELITERAL:
			case FALSE:
			case FLOAT:
			case FLOATLITERAL:
			case IDENTIFIER:
			case INT:
			case INTLITERAL:
			case LONG:
			case LONGLITERAL:
			case NEW:
			case NULL:
			case SHORT:
			case STRINGLITERAL:
			case SUPER:
			case THIS:
			case TRUE:
			case VOID:
				{
				alt127=4;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 127, 0, input);
				throw nvae;
			}
			switch (alt127) {
				case 1 :
					// Java.g:907:4: '~' ^ unaryExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal324=(Token)match(input,TILDE,FOLLOW_TILDE_in_unaryExpressionNotPlusMinus4167); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal324_tree = (CommonTree)adaptor.create(char_literal324);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal324_tree, root_0);
					}

					pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus4170);
					unaryExpression325=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression325.getTree());

					}
					break;
				case 2 :
					// Java.g:908:4: '!' ^ unaryExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal326=(Token)match(input,BANG,FOLLOW_BANG_in_unaryExpressionNotPlusMinus4175); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal326_tree = (CommonTree)adaptor.create(char_literal326);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal326_tree, root_0);
					}

					pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus4178);
					unaryExpression327=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression327.getTree());

					}
					break;
				case 3 :
					// Java.g:909:4: castExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_castExpression_in_unaryExpressionNotPlusMinus4183);
					castExpression328=castExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, castExpression328.getTree());

					}
					break;
				case 4 :
					// Java.g:910:4: primary ( selector )* ( '++' | '--' )?
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_primary_in_unaryExpressionNotPlusMinus4188);
					primary329=primary();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, primary329.getTree());

					// Java.g:911:3: ( selector )*
					loop125:
					while (true) {
						int alt125=2;
						int LA125_0 = input.LA(1);
						if ( (LA125_0==DOT||LA125_0==LBRACKET) ) {
							alt125=1;
						}

						switch (alt125) {
						case 1 :
							// Java.g:911:4: selector
							{
							pushFollow(FOLLOW_selector_in_unaryExpressionNotPlusMinus4193);
							selector330=selector();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, selector330.getTree());

							}
							break;

						default :
							break loop125;
						}
					}

					// Java.g:912:3: ( '++' | '--' )?
					int alt126=2;
					int LA126_0 = input.LA(1);
					if ( (LA126_0==PLUSPLUS||LA126_0==SUBSUB) ) {
						alt126=1;
					}
					switch (alt126) {
						case 1 :
							// Java.g:
							{
							set331=input.LT(1);
							if ( input.LA(1)==PLUSPLUS||input.LA(1)==SUBSUB ) {
								input.consume();
								if ( state.backtracking==0 ) adaptor.addChild(root_0, (CommonTree)adaptor.create(set331));
								state.errorRecovery=false;
								state.failed=false;
							}
							else {
								if (state.backtracking>0) {state.failed=true; return retval;}
								MismatchedSetException mse = new MismatchedSetException(null,input);
								throw mse;
							}
							}
							break;

					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 87, unaryExpressionNotPlusMinus_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "unaryExpressionNotPlusMinus"


	public static class castExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "castExpression"
	// Java.g:916:1: castExpression : ( '(' ! primitiveType ')' ! unaryExpression | '(' ! type ')' ! unaryExpressionNotPlusMinus );
	public final JavaParser.castExpression_return castExpression() throws RecognitionException {
		JavaParser.castExpression_return retval = new JavaParser.castExpression_return();
		retval.start = input.LT(1);
		int castExpression_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal332=null;
		Token char_literal334=null;
		Token char_literal336=null;
		Token char_literal338=null;
		ParserRuleReturnScope primitiveType333 =null;
		ParserRuleReturnScope unaryExpression335 =null;
		ParserRuleReturnScope type337 =null;
		ParserRuleReturnScope unaryExpressionNotPlusMinus339 =null;

		CommonTree char_literal332_tree=null;
		CommonTree char_literal334_tree=null;
		CommonTree char_literal336_tree=null;
		CommonTree char_literal338_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 88) ) { return retval; }

			// Java.g:917:2: ( '(' ! primitiveType ')' ! unaryExpression | '(' ! type ')' ! unaryExpressionNotPlusMinus )
			int alt128=2;
			int LA128_0 = input.LA(1);
			if ( (LA128_0==LPAREN) ) {
				int LA128_1 = input.LA(2);
				if ( (synpred203_Java()) ) {
					alt128=1;
				}
				else if ( (true) ) {
					alt128=2;
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 128, 0, input);
				throw nvae;
			}

			switch (alt128) {
				case 1 :
					// Java.g:917:4: '(' ! primitiveType ')' ! unaryExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal332=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_castExpression4218); if (state.failed) return retval;
					pushFollow(FOLLOW_primitiveType_in_castExpression4221);
					primitiveType333=primitiveType();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, primitiveType333.getTree());

					char_literal334=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_castExpression4223); if (state.failed) return retval;
					pushFollow(FOLLOW_unaryExpression_in_castExpression4228);
					unaryExpression335=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpression335.getTree());

					}
					break;
				case 2 :
					// Java.g:919:4: '(' ! type ')' ! unaryExpressionNotPlusMinus
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal336=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_castExpression4233); if (state.failed) return retval;
					pushFollow(FOLLOW_type_in_castExpression4236);
					type337=type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, type337.getTree());

					char_literal338=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_castExpression4238); if (state.failed) return retval;
					pushFollow(FOLLOW_unaryExpressionNotPlusMinus_in_castExpression4243);
					unaryExpressionNotPlusMinus339=unaryExpressionNotPlusMinus();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, unaryExpressionNotPlusMinus339.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 88, castExpression_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "castExpression"


	public static class primary_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "primary"
	// Java.g:927:1: primary : ( parExpression | call | thisIdentifierSequence | identifierSequence | 'super' superSuffix | literal | creator | primitiveType ( '[' ']' )* '.' ^ 'class' | 'void' '.' ^ 'class' );
	public final JavaParser.primary_return primary() throws RecognitionException {
		JavaParser.primary_return retval = new JavaParser.primary_return();
		retval.start = input.LT(1);
		int primary_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal344=null;
		Token char_literal349=null;
		Token char_literal350=null;
		Token char_literal351=null;
		Token string_literal352=null;
		Token string_literal353=null;
		Token char_literal354=null;
		Token string_literal355=null;
		ParserRuleReturnScope parExpression340 =null;
		ParserRuleReturnScope call341 =null;
		ParserRuleReturnScope thisIdentifierSequence342 =null;
		ParserRuleReturnScope identifierSequence343 =null;
		ParserRuleReturnScope superSuffix345 =null;
		ParserRuleReturnScope literal346 =null;
		ParserRuleReturnScope creator347 =null;
		ParserRuleReturnScope primitiveType348 =null;

		CommonTree string_literal344_tree=null;
		CommonTree char_literal349_tree=null;
		CommonTree char_literal350_tree=null;
		CommonTree char_literal351_tree=null;
		CommonTree string_literal352_tree=null;
		CommonTree string_literal353_tree=null;
		CommonTree char_literal354_tree=null;
		CommonTree string_literal355_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 89) ) { return retval; }

			// Java.g:928:2: ( parExpression | call | thisIdentifierSequence | identifierSequence | 'super' superSuffix | literal | creator | primitiveType ( '[' ']' )* '.' ^ 'class' | 'void' '.' ^ 'class' )
			int alt130=9;
			switch ( input.LA(1) ) {
			case LPAREN:
				{
				alt130=1;
				}
				break;
			case THIS:
				{
				int LA130_2 = input.LA(2);
				if ( (synpred205_Java()) ) {
					alt130=2;
				}
				else if ( (synpred206_Java()) ) {
					alt130=3;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 130, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case IDENTIFIER:
				{
				int LA130_3 = input.LA(2);
				if ( (synpred205_Java()) ) {
					alt130=2;
				}
				else if ( (synpred207_Java()) ) {
					alt130=4;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 130, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case SUPER:
				{
				alt130=5;
				}
				break;
			case CHARLITERAL:
			case DOUBLELITERAL:
			case FALSE:
			case FLOATLITERAL:
			case INTLITERAL:
			case LONGLITERAL:
			case NULL:
			case STRINGLITERAL:
			case TRUE:
				{
				alt130=6;
				}
				break;
			case NEW:
				{
				alt130=7;
				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				{
				alt130=8;
				}
				break;
			case VOID:
				{
				alt130=9;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 130, 0, input);
				throw nvae;
			}
			switch (alt130) {
				case 1 :
					// Java.g:928:4: parExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_parExpression_in_primary4257);
					parExpression340=parExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, parExpression340.getTree());

					}
					break;
				case 2 :
					// Java.g:929:4: call
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_call_in_primary4262);
					call341=call();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, call341.getTree());

					}
					break;
				case 3 :
					// Java.g:930:4: thisIdentifierSequence
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_thisIdentifierSequence_in_primary4267);
					thisIdentifierSequence342=thisIdentifierSequence();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, thisIdentifierSequence342.getTree());

					}
					break;
				case 4 :
					// Java.g:932:4: identifierSequence
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_identifierSequence_in_primary4273);
					identifierSequence343=identifierSequence();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, identifierSequence343.getTree());

					}
					break;
				case 5 :
					// Java.g:934:4: 'super' superSuffix
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal344=(Token)match(input,SUPER,FOLLOW_SUPER_in_primary4279); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal344_tree = (CommonTree)adaptor.create(string_literal344);
					adaptor.addChild(root_0, string_literal344_tree);
					}

					pushFollow(FOLLOW_superSuffix_in_primary4283);
					superSuffix345=superSuffix();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, superSuffix345.getTree());

					}
					break;
				case 6 :
					// Java.g:936:4: literal
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_literal_in_primary4288);
					literal346=literal();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, literal346.getTree());

					}
					break;
				case 7 :
					// Java.g:937:4: creator
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_creator_in_primary4293);
					creator347=creator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, creator347.getTree());

					}
					break;
				case 8 :
					// Java.g:938:4: primitiveType ( '[' ']' )* '.' ^ 'class'
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_primitiveType_in_primary4298);
					primitiveType348=primitiveType();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, primitiveType348.getTree());

					// Java.g:939:3: ( '[' ']' )*
					loop129:
					while (true) {
						int alt129=2;
						int LA129_0 = input.LA(1);
						if ( (LA129_0==LBRACKET) ) {
							alt129=1;
						}

						switch (alt129) {
						case 1 :
							// Java.g:939:4: '[' ']'
							{
							char_literal349=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_primary4303); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal349_tree = (CommonTree)adaptor.create(char_literal349);
							adaptor.addChild(root_0, char_literal349_tree);
							}

							char_literal350=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_primary4305); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal350_tree = (CommonTree)adaptor.create(char_literal350);
							adaptor.addChild(root_0, char_literal350_tree);
							}

							}
							break;

						default :
							break loop129;
						}
					}

					char_literal351=(Token)match(input,DOT,FOLLOW_DOT_in_primary4311); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal351_tree = (CommonTree)adaptor.create(char_literal351);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal351_tree, root_0);
					}

					string_literal352=(Token)match(input,CLASS,FOLLOW_CLASS_in_primary4314); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal352_tree = (CommonTree)adaptor.create(string_literal352);
					adaptor.addChild(root_0, string_literal352_tree);
					}

					}
					break;
				case 9 :
					// Java.g:941:4: 'void' '.' ^ 'class'
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal353=(Token)match(input,VOID,FOLLOW_VOID_in_primary4319); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal353_tree = (CommonTree)adaptor.create(string_literal353);
					adaptor.addChild(root_0, string_literal353_tree);
					}

					char_literal354=(Token)match(input,DOT,FOLLOW_DOT_in_primary4321); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal354_tree = (CommonTree)adaptor.create(char_literal354);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal354_tree, root_0);
					}

					string_literal355=(Token)match(input,CLASS,FOLLOW_CLASS_in_primary4324); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal355_tree = (CommonTree)adaptor.create(string_literal355);
					adaptor.addChild(root_0, string_literal355_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 89, primary_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "primary"


	public static class call_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "call"
	// Java.g:945:1: call : (a= thisIdentifierSequence b= identifierSuffix -> ^( CALL $a $b) |c= identifierSequence b= identifierSuffix -> ^( CALL $c $b) );
	public final JavaParser.call_return call() throws RecognitionException {
		JavaParser.call_return retval = new JavaParser.call_return();
		retval.start = input.LT(1);
		int call_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope c =null;

		RewriteRuleSubtreeStream stream_identifierSequence=new RewriteRuleSubtreeStream(adaptor,"rule identifierSequence");
		RewriteRuleSubtreeStream stream_thisIdentifierSequence=new RewriteRuleSubtreeStream(adaptor,"rule thisIdentifierSequence");
		RewriteRuleSubtreeStream stream_identifierSuffix=new RewriteRuleSubtreeStream(adaptor,"rule identifierSuffix");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 90) ) { return retval; }

			// Java.g:946:2: (a= thisIdentifierSequence b= identifierSuffix -> ^( CALL $a $b) |c= identifierSequence b= identifierSuffix -> ^( CALL $c $b) )
			int alt131=2;
			int LA131_0 = input.LA(1);
			if ( (LA131_0==THIS) ) {
				alt131=1;
			}
			else if ( (LA131_0==IDENTIFIER) ) {
				alt131=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 131, 0, input);
				throw nvae;
			}

			switch (alt131) {
				case 1 :
					// Java.g:946:4: a= thisIdentifierSequence b= identifierSuffix
					{
					pushFollow(FOLLOW_thisIdentifierSequence_in_call4338);
					a=thisIdentifierSequence();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_thisIdentifierSequence.add(a.getTree());
					pushFollow(FOLLOW_identifierSuffix_in_call4344);
					b=identifierSuffix();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_identifierSuffix.add(b.getTree());
					// AST REWRITE
					// elements: b, a
					// token labels: 
					// rule labels: retval, b, a
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);
					RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 948:3: -> ^( CALL $a $b)
					{
						// Java.g:948:6: ^( CALL $a $b)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CALL, "CALL"), root_1);
						adaptor.addChild(root_1, stream_a.nextTree());
						adaptor.addChild(root_1, stream_b.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:949:4: c= identifierSequence b= identifierSuffix
					{
					pushFollow(FOLLOW_identifierSequence_in_call4365);
					c=identifierSequence();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_identifierSequence.add(c.getTree());
					pushFollow(FOLLOW_identifierSuffix_in_call4371);
					b=identifierSuffix();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_identifierSuffix.add(b.getTree());
					// AST REWRITE
					// elements: c, b
					// token labels: 
					// rule labels: retval, b, c
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);
					RewriteRuleSubtreeStream stream_c=new RewriteRuleSubtreeStream(adaptor,"rule c",c!=null?c.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 951:3: -> ^( CALL $c $b)
					{
						// Java.g:951:6: ^( CALL $c $b)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CALL, "CALL"), root_1);
						adaptor.addChild(root_1, stream_c.nextTree());
						adaptor.addChild(root_1, stream_b.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 90, call_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "call"


	public static class thisIdentifierSequence_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "thisIdentifierSequence"
	// Java.g:955:1: thisIdentifierSequence : (a= 'this' b= (c= '.' d= identifierSequence ) -> ^( SEQUENCE[$c] $a $d) |a= 'this' -> $a);
	public final JavaParser.thisIdentifierSequence_return thisIdentifierSequence() throws RecognitionException {
		JavaParser.thisIdentifierSequence_return retval = new JavaParser.thisIdentifierSequence_return();
		retval.start = input.LT(1);
		int thisIdentifierSequence_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token c=null;
		Token b=null;
		ParserRuleReturnScope d =null;

		CommonTree a_tree=null;
		CommonTree c_tree=null;
		CommonTree b_tree=null;
		RewriteRuleTokenStream stream_DOT=new RewriteRuleTokenStream(adaptor,"token DOT");
		RewriteRuleTokenStream stream_THIS=new RewriteRuleTokenStream(adaptor,"token THIS");
		RewriteRuleSubtreeStream stream_identifierSequence=new RewriteRuleSubtreeStream(adaptor,"rule identifierSequence");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 91) ) { return retval; }

			// Java.g:956:2: (a= 'this' b= (c= '.' d= identifierSequence ) -> ^( SEQUENCE[$c] $a $d) |a= 'this' -> $a)
			int alt132=2;
			int LA132_0 = input.LA(1);
			if ( (LA132_0==THIS) ) {
				int LA132_1 = input.LA(2);
				if ( (LA132_1==DOT) ) {
					int LA132_2 = input.LA(3);
					if ( (LA132_2==IDENTIFIER) ) {
						int LA132_4 = input.LA(4);
						if ( (synpred214_Java()) ) {
							alt132=1;
						}
						else if ( (true) ) {
							alt132=2;
						}

					}
					else if ( (LA132_2==CLASS||LA132_2==LT||LA132_2==NEW||LA132_2==SUPER||LA132_2==THIS) ) {
						alt132=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 132, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA132_1==EOF||(LA132_1 >= AMP && LA132_1 <= AMPEQ)||(LA132_1 >= BANGEQ && LA132_1 <= BAREQ)||(LA132_1 >= CARET && LA132_1 <= CARETEQ)||(LA132_1 >= COLON && LA132_1 <= COMMA)||(LA132_1 >= EQ && LA132_1 <= EQEQ)||LA132_1==GT||LA132_1==INSTANCEOF||LA132_1==LBRACKET||(LA132_1 >= LPAREN && LA132_1 <= LT)||(LA132_1 >= PERCENT && LA132_1 <= PLUSPLUS)||(LA132_1 >= QUES && LA132_1 <= RBRACKET)||(LA132_1 >= RPAREN && LA132_1 <= SEMI)||(LA132_1 >= SLASH && LA132_1 <= STAREQ)||(LA132_1 >= SUB && LA132_1 <= SUBSUB)) ) {
					alt132=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 132, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 132, 0, input);
				throw nvae;
			}

			switch (alt132) {
				case 1 :
					// Java.g:956:4: a= 'this' b= (c= '.' d= identifierSequence )
					{
					a=(Token)match(input,THIS,FOLLOW_THIS_in_thisIdentifierSequence4399); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_THIS.add(a);

					// Java.g:957:5: (c= '.' d= identifierSequence )
					// Java.g:957:6: c= '.' d= identifierSequence
					{
					c=(Token)match(input,DOT,FOLLOW_DOT_in_thisIdentifierSequence4408); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOT.add(c);

					pushFollow(FOLLOW_identifierSequence_in_thisIdentifierSequence4412);
					d=identifierSequence();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_identifierSequence.add(d.getTree());
					}

					// AST REWRITE
					// elements: d, a
					// token labels: a
					// rule labels: retval, d
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_d=new RewriteRuleSubtreeStream(adaptor,"rule d",d!=null?d.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 958:3: -> ^( SEQUENCE[$c] $a $d)
					{
						// Java.g:958:6: ^( SEQUENCE[$c] $a $d)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(SEQUENCE, c), root_1);
						adaptor.addChild(root_1, stream_a.nextNode());
						adaptor.addChild(root_1, stream_d.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:959:4: a= 'this'
					{
					a=(Token)match(input,THIS,FOLLOW_THIS_in_thisIdentifierSequence4435); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_THIS.add(a);

					// AST REWRITE
					// elements: a
					// token labels: a
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 960:3: -> $a
					{
						adaptor.addChild(root_0, stream_a.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 91, thisIdentifierSequence_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "thisIdentifierSequence"


	public static class identifierSequence_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "identifierSequence"
	// Java.g:964:1: identifierSequence : (a= IDENTIFIER b= (c= '.' d= identifierSequence ) -> ^( SEQUENCE[$c] $a $d) |a= IDENTIFIER -> $a);
	public final JavaParser.identifierSequence_return identifierSequence() throws RecognitionException {
		JavaParser.identifierSequence_return retval = new JavaParser.identifierSequence_return();
		retval.start = input.LT(1);
		int identifierSequence_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token c=null;
		Token b=null;
		ParserRuleReturnScope d =null;

		CommonTree a_tree=null;
		CommonTree c_tree=null;
		CommonTree b_tree=null;
		RewriteRuleTokenStream stream_DOT=new RewriteRuleTokenStream(adaptor,"token DOT");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_identifierSequence=new RewriteRuleSubtreeStream(adaptor,"rule identifierSequence");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 92) ) { return retval; }

			// Java.g:965:2: (a= IDENTIFIER b= (c= '.' d= identifierSequence ) -> ^( SEQUENCE[$c] $a $d) |a= IDENTIFIER -> $a)
			int alt133=2;
			int LA133_0 = input.LA(1);
			if ( (LA133_0==IDENTIFIER) ) {
				int LA133_1 = input.LA(2);
				if ( (LA133_1==DOT) ) {
					int LA133_2 = input.LA(3);
					if ( (LA133_2==IDENTIFIER) ) {
						int LA133_4 = input.LA(4);
						if ( (synpred215_Java()) ) {
							alt133=1;
						}
						else if ( (true) ) {
							alt133=2;
						}

					}
					else if ( (LA133_2==CLASS||LA133_2==LT||LA133_2==NEW||LA133_2==SUPER||LA133_2==THIS) ) {
						alt133=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 133, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA133_1==EOF||(LA133_1 >= AMP && LA133_1 <= AMPEQ)||(LA133_1 >= BANGEQ && LA133_1 <= BAREQ)||(LA133_1 >= CARET && LA133_1 <= CARETEQ)||(LA133_1 >= COLON && LA133_1 <= COMMA)||(LA133_1 >= EQ && LA133_1 <= EQEQ)||LA133_1==GT||LA133_1==INSTANCEOF||LA133_1==LBRACKET||(LA133_1 >= LPAREN && LA133_1 <= LT)||(LA133_1 >= PERCENT && LA133_1 <= PLUSPLUS)||(LA133_1 >= QUES && LA133_1 <= RBRACKET)||(LA133_1 >= RPAREN && LA133_1 <= SEMI)||(LA133_1 >= SLASH && LA133_1 <= STAREQ)||(LA133_1 >= SUB && LA133_1 <= SUBSUB)) ) {
					alt133=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 133, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 133, 0, input);
				throw nvae;
			}

			switch (alt133) {
				case 1 :
					// Java.g:965:4: a= IDENTIFIER b= (c= '.' d= identifierSequence )
					{
					a=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_identifierSequence4456); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(a);

					// Java.g:966:5: (c= '.' d= identifierSequence )
					// Java.g:966:6: c= '.' d= identifierSequence
					{
					c=(Token)match(input,DOT,FOLLOW_DOT_in_identifierSequence4465); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOT.add(c);

					pushFollow(FOLLOW_identifierSequence_in_identifierSequence4469);
					d=identifierSequence();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_identifierSequence.add(d.getTree());
					}

					// AST REWRITE
					// elements: d, a
					// token labels: a
					// rule labels: retval, d
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_d=new RewriteRuleSubtreeStream(adaptor,"rule d",d!=null?d.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 967:3: -> ^( SEQUENCE[$c] $a $d)
					{
						// Java.g:967:6: ^( SEQUENCE[$c] $a $d)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(SEQUENCE, c), root_1);
						adaptor.addChild(root_1, stream_a.nextNode());
						adaptor.addChild(root_1, stream_d.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:968:4: a= IDENTIFIER
					{
					a=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_identifierSequence4492); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(a);

					// AST REWRITE
					// elements: a
					// token labels: a
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 969:3: -> $a
					{
						adaptor.addChild(root_0, stream_a.nextNode());
					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 92, identifierSequence_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "identifierSequence"


	public static class superSuffix_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "superSuffix"
	// Java.g:973:1: superSuffix : ( arguments | '.' ^ ( typeArguments )? IDENTIFIER ( arguments )? );
	public final JavaParser.superSuffix_return superSuffix() throws RecognitionException {
		JavaParser.superSuffix_return retval = new JavaParser.superSuffix_return();
		retval.start = input.LT(1);
		int superSuffix_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal357=null;
		Token IDENTIFIER359=null;
		ParserRuleReturnScope arguments356 =null;
		ParserRuleReturnScope typeArguments358 =null;
		ParserRuleReturnScope arguments360 =null;

		CommonTree char_literal357_tree=null;
		CommonTree IDENTIFIER359_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 93) ) { return retval; }

			// Java.g:974:2: ( arguments | '.' ^ ( typeArguments )? IDENTIFIER ( arguments )? )
			int alt136=2;
			int LA136_0 = input.LA(1);
			if ( (LA136_0==LPAREN) ) {
				alt136=1;
			}
			else if ( (LA136_0==DOT) ) {
				alt136=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 136, 0, input);
				throw nvae;
			}

			switch (alt136) {
				case 1 :
					// Java.g:974:4: arguments
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_arguments_in_superSuffix4511);
					arguments356=arguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arguments356.getTree());

					}
					break;
				case 2 :
					// Java.g:975:4: '.' ^ ( typeArguments )? IDENTIFIER ( arguments )?
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal357=(Token)match(input,DOT,FOLLOW_DOT_in_superSuffix4516); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal357_tree = (CommonTree)adaptor.create(char_literal357);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal357_tree, root_0);
					}

					// Java.g:976:3: ( typeArguments )?
					int alt134=2;
					int LA134_0 = input.LA(1);
					if ( (LA134_0==LT) ) {
						alt134=1;
					}
					switch (alt134) {
						case 1 :
							// Java.g:976:4: typeArguments
							{
							pushFollow(FOLLOW_typeArguments_in_superSuffix4522);
							typeArguments358=typeArguments();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, typeArguments358.getTree());

							}
							break;

					}

					IDENTIFIER359=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_superSuffix4528); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER359_tree = (CommonTree)adaptor.create(IDENTIFIER359);
					adaptor.addChild(root_0, IDENTIFIER359_tree);
					}

					// Java.g:978:3: ( arguments )?
					int alt135=2;
					int LA135_0 = input.LA(1);
					if ( (LA135_0==LPAREN) ) {
						alt135=1;
					}
					switch (alt135) {
						case 1 :
							// Java.g:978:4: arguments
							{
							pushFollow(FOLLOW_arguments_in_superSuffix4533);
							arguments360=arguments();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, arguments360.getTree());

							}
							break;

					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 93, superSuffix_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "superSuffix"


	public static class identifierSuffix_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "identifierSuffix"
	// Java.g:982:1: identifierSuffix : ( ( '[' ']' )+ '.' ^ 'class' | ( '[' ! expression ']' !)+ | arguments | '.' ^ 'class' | '.' ^ nonWildcardTypeArguments IDENTIFIER arguments | '.' ^ 'this' | '.' ^ 'super' arguments | innerCreator );
	public final JavaParser.identifierSuffix_return identifierSuffix() throws RecognitionException {
		JavaParser.identifierSuffix_return retval = new JavaParser.identifierSuffix_return();
		retval.start = input.LT(1);
		int identifierSuffix_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal361=null;
		Token char_literal362=null;
		Token char_literal363=null;
		Token string_literal364=null;
		Token char_literal365=null;
		Token char_literal367=null;
		Token char_literal369=null;
		Token string_literal370=null;
		Token char_literal371=null;
		Token IDENTIFIER373=null;
		Token char_literal375=null;
		Token string_literal376=null;
		Token char_literal377=null;
		Token string_literal378=null;
		ParserRuleReturnScope expression366 =null;
		ParserRuleReturnScope arguments368 =null;
		ParserRuleReturnScope nonWildcardTypeArguments372 =null;
		ParserRuleReturnScope arguments374 =null;
		ParserRuleReturnScope arguments379 =null;
		ParserRuleReturnScope innerCreator380 =null;

		CommonTree char_literal361_tree=null;
		CommonTree char_literal362_tree=null;
		CommonTree char_literal363_tree=null;
		CommonTree string_literal364_tree=null;
		CommonTree char_literal365_tree=null;
		CommonTree char_literal367_tree=null;
		CommonTree char_literal369_tree=null;
		CommonTree string_literal370_tree=null;
		CommonTree char_literal371_tree=null;
		CommonTree IDENTIFIER373_tree=null;
		CommonTree char_literal375_tree=null;
		CommonTree string_literal376_tree=null;
		CommonTree char_literal377_tree=null;
		CommonTree string_literal378_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 94) ) { return retval; }

			// Java.g:983:2: ( ( '[' ']' )+ '.' ^ 'class' | ( '[' ! expression ']' !)+ | arguments | '.' ^ 'class' | '.' ^ nonWildcardTypeArguments IDENTIFIER arguments | '.' ^ 'this' | '.' ^ 'super' arguments | innerCreator )
			int alt139=8;
			switch ( input.LA(1) ) {
			case LBRACKET:
				{
				int LA139_1 = input.LA(2);
				if ( (LA139_1==RBRACKET) ) {
					alt139=1;
				}
				else if ( (LA139_1==BANG||LA139_1==BOOLEAN||LA139_1==BYTE||(LA139_1 >= CHAR && LA139_1 <= CHARLITERAL)||(LA139_1 >= DOUBLE && LA139_1 <= DOUBLELITERAL)||LA139_1==FALSE||(LA139_1 >= FLOAT && LA139_1 <= FLOATLITERAL)||LA139_1==IDENTIFIER||LA139_1==INT||LA139_1==INTLITERAL||(LA139_1 >= LONG && LA139_1 <= LPAREN)||(LA139_1 >= NEW && LA139_1 <= NULL)||LA139_1==PLUS||LA139_1==PLUSPLUS||LA139_1==SHORT||(LA139_1 >= STRINGLITERAL && LA139_1 <= SUB)||(LA139_1 >= SUBSUB && LA139_1 <= SUPER)||LA139_1==THIS||LA139_1==TILDE||LA139_1==TRUE||LA139_1==VOID) ) {
					alt139=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 139, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case LPAREN:
				{
				alt139=3;
				}
				break;
			case DOT:
				{
				switch ( input.LA(2) ) {
				case CLASS:
					{
					alt139=4;
					}
					break;
				case THIS:
					{
					alt139=6;
					}
					break;
				case SUPER:
					{
					alt139=7;
					}
					break;
				case NEW:
					{
					alt139=8;
					}
					break;
				case LT:
					{
					alt139=5;
					}
					break;
				default:
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 139, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 139, 0, input);
				throw nvae;
			}
			switch (alt139) {
				case 1 :
					// Java.g:983:4: ( '[' ']' )+ '.' ^ 'class'
					{
					root_0 = (CommonTree)adaptor.nil();


					// Java.g:983:4: ( '[' ']' )+
					int cnt137=0;
					loop137:
					while (true) {
						int alt137=2;
						int LA137_0 = input.LA(1);
						if ( (LA137_0==LBRACKET) ) {
							alt137=1;
						}

						switch (alt137) {
						case 1 :
							// Java.g:983:5: '[' ']'
							{
							char_literal361=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_identifierSuffix4548); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal361_tree = (CommonTree)adaptor.create(char_literal361);
							adaptor.addChild(root_0, char_literal361_tree);
							}

							char_literal362=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_identifierSuffix4550); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal362_tree = (CommonTree)adaptor.create(char_literal362);
							adaptor.addChild(root_0, char_literal362_tree);
							}

							}
							break;

						default :
							if ( cnt137 >= 1 ) break loop137;
							if (state.backtracking>0) {state.failed=true; return retval;}
							EarlyExitException eee = new EarlyExitException(137, input);
							throw eee;
						}
						cnt137++;
					}

					char_literal363=(Token)match(input,DOT,FOLLOW_DOT_in_identifierSuffix4556); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal363_tree = (CommonTree)adaptor.create(char_literal363);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal363_tree, root_0);
					}

					string_literal364=(Token)match(input,CLASS,FOLLOW_CLASS_in_identifierSuffix4559); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal364_tree = (CommonTree)adaptor.create(string_literal364);
					adaptor.addChild(root_0, string_literal364_tree);
					}

					}
					break;
				case 2 :
					// Java.g:985:4: ( '[' ! expression ']' !)+
					{
					root_0 = (CommonTree)adaptor.nil();


					// Java.g:985:4: ( '[' ! expression ']' !)+
					int cnt138=0;
					loop138:
					while (true) {
						int alt138=2;
						int LA138_0 = input.LA(1);
						if ( (LA138_0==LBRACKET) ) {
							int LA138_2 = input.LA(2);
							if ( (synpred221_Java()) ) {
								alt138=1;
							}

						}

						switch (alt138) {
						case 1 :
							// Java.g:985:5: '[' ! expression ']' !
							{
							char_literal365=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_identifierSuffix4565); if (state.failed) return retval;
							pushFollow(FOLLOW_expression_in_identifierSuffix4568);
							expression366=expression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, expression366.getTree());

							char_literal367=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_identifierSuffix4570); if (state.failed) return retval;
							}
							break;

						default :
							if ( cnt138 >= 1 ) break loop138;
							if (state.backtracking>0) {state.failed=true; return retval;}
							EarlyExitException eee = new EarlyExitException(138, input);
							throw eee;
						}
						cnt138++;
					}

					}
					break;
				case 3 :
					// Java.g:986:4: arguments
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_arguments_in_identifierSuffix4578);
					arguments368=arguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arguments368.getTree());

					}
					break;
				case 4 :
					// Java.g:987:4: '.' ^ 'class'
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal369=(Token)match(input,DOT,FOLLOW_DOT_in_identifierSuffix4583); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal369_tree = (CommonTree)adaptor.create(char_literal369);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal369_tree, root_0);
					}

					string_literal370=(Token)match(input,CLASS,FOLLOW_CLASS_in_identifierSuffix4586); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal370_tree = (CommonTree)adaptor.create(string_literal370);
					adaptor.addChild(root_0, string_literal370_tree);
					}

					}
					break;
				case 5 :
					// Java.g:988:4: '.' ^ nonWildcardTypeArguments IDENTIFIER arguments
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal371=(Token)match(input,DOT,FOLLOW_DOT_in_identifierSuffix4591); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal371_tree = (CommonTree)adaptor.create(char_literal371);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal371_tree, root_0);
					}

					pushFollow(FOLLOW_nonWildcardTypeArguments_in_identifierSuffix4594);
					nonWildcardTypeArguments372=nonWildcardTypeArguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, nonWildcardTypeArguments372.getTree());

					IDENTIFIER373=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_identifierSuffix4596); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IDENTIFIER373_tree = (CommonTree)adaptor.create(IDENTIFIER373);
					adaptor.addChild(root_0, IDENTIFIER373_tree);
					}

					pushFollow(FOLLOW_arguments_in_identifierSuffix4598);
					arguments374=arguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arguments374.getTree());

					}
					break;
				case 6 :
					// Java.g:989:4: '.' ^ 'this'
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal375=(Token)match(input,DOT,FOLLOW_DOT_in_identifierSuffix4603); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal375_tree = (CommonTree)adaptor.create(char_literal375);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal375_tree, root_0);
					}

					string_literal376=(Token)match(input,THIS,FOLLOW_THIS_in_identifierSuffix4606); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal376_tree = (CommonTree)adaptor.create(string_literal376);
					adaptor.addChild(root_0, string_literal376_tree);
					}

					}
					break;
				case 7 :
					// Java.g:990:4: '.' ^ 'super' arguments
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal377=(Token)match(input,DOT,FOLLOW_DOT_in_identifierSuffix4611); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal377_tree = (CommonTree)adaptor.create(char_literal377);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal377_tree, root_0);
					}

					string_literal378=(Token)match(input,SUPER,FOLLOW_SUPER_in_identifierSuffix4614); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal378_tree = (CommonTree)adaptor.create(string_literal378);
					adaptor.addChild(root_0, string_literal378_tree);
					}

					pushFollow(FOLLOW_arguments_in_identifierSuffix4616);
					arguments379=arguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arguments379.getTree());

					}
					break;
				case 8 :
					// Java.g:991:4: innerCreator
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_innerCreator_in_identifierSuffix4621);
					innerCreator380=innerCreator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, innerCreator380.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 94, identifierSuffix_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "identifierSuffix"


	public static class selector_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "selector"
	// Java.g:995:1: selector : ( '.' a= IDENTIFIER (b= arguments )? -> ^( $a ( $b)? ) | '.' ^ 'this' | '.' ^ 'super' superSuffix | innerCreator | '[' ! expression ']' !);
	public final JavaParser.selector_return selector() throws RecognitionException {
		JavaParser.selector_return retval = new JavaParser.selector_return();
		retval.start = input.LT(1);
		int selector_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token char_literal381=null;
		Token char_literal382=null;
		Token string_literal383=null;
		Token char_literal384=null;
		Token string_literal385=null;
		Token char_literal388=null;
		Token char_literal390=null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope superSuffix386 =null;
		ParserRuleReturnScope innerCreator387 =null;
		ParserRuleReturnScope expression389 =null;

		CommonTree a_tree=null;
		CommonTree char_literal381_tree=null;
		CommonTree char_literal382_tree=null;
		CommonTree string_literal383_tree=null;
		CommonTree char_literal384_tree=null;
		CommonTree string_literal385_tree=null;
		CommonTree char_literal388_tree=null;
		CommonTree char_literal390_tree=null;
		RewriteRuleTokenStream stream_DOT=new RewriteRuleTokenStream(adaptor,"token DOT");
		RewriteRuleTokenStream stream_IDENTIFIER=new RewriteRuleTokenStream(adaptor,"token IDENTIFIER");
		RewriteRuleSubtreeStream stream_arguments=new RewriteRuleSubtreeStream(adaptor,"rule arguments");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 95) ) { return retval; }

			// Java.g:996:2: ( '.' a= IDENTIFIER (b= arguments )? -> ^( $a ( $b)? ) | '.' ^ 'this' | '.' ^ 'super' superSuffix | innerCreator | '[' ! expression ']' !)
			int alt141=5;
			int LA141_0 = input.LA(1);
			if ( (LA141_0==DOT) ) {
				switch ( input.LA(2) ) {
				case IDENTIFIER:
					{
					alt141=1;
					}
					break;
				case THIS:
					{
					alt141=2;
					}
					break;
				case SUPER:
					{
					alt141=3;
					}
					break;
				case NEW:
					{
					alt141=4;
					}
					break;
				default:
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 141, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
			}
			else if ( (LA141_0==LBRACKET) ) {
				alt141=5;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 141, 0, input);
				throw nvae;
			}

			switch (alt141) {
				case 1 :
					// Java.g:996:4: '.' a= IDENTIFIER (b= arguments )?
					{
					char_literal381=(Token)match(input,DOT,FOLLOW_DOT_in_selector4633); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_DOT.add(char_literal381);

					a=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_selector4637); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_IDENTIFIER.add(a);

					// Java.g:996:22: (b= arguments )?
					int alt140=2;
					int LA140_0 = input.LA(1);
					if ( (LA140_0==LPAREN) ) {
						alt140=1;
					}
					switch (alt140) {
						case 1 :
							// Java.g:996:22: b= arguments
							{
							pushFollow(FOLLOW_arguments_in_selector4641);
							b=arguments();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) stream_arguments.add(b.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: b, a
					// token labels: a
					// rule labels: retval, b
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 997:2: -> ^( $a ( $b)? )
					{
						// Java.g:997:5: ^( $a ( $b)? )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot(stream_a.nextNode(), root_1);
						// Java.g:997:11: ( $b)?
						if ( stream_b.hasNext() ) {
							adaptor.addChild(root_1, stream_b.nextTree());
						}
						stream_b.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:999:4: '.' ^ 'this'
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal382=(Token)match(input,DOT,FOLLOW_DOT_in_selector4661); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal382_tree = (CommonTree)adaptor.create(char_literal382);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal382_tree, root_0);
					}

					string_literal383=(Token)match(input,THIS,FOLLOW_THIS_in_selector4664); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal383_tree = (CommonTree)adaptor.create(string_literal383);
					adaptor.addChild(root_0, string_literal383_tree);
					}

					}
					break;
				case 3 :
					// Java.g:1000:4: '.' ^ 'super' superSuffix
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal384=(Token)match(input,DOT,FOLLOW_DOT_in_selector4669); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal384_tree = (CommonTree)adaptor.create(char_literal384);
					root_0 = (CommonTree)adaptor.becomeRoot(char_literal384_tree, root_0);
					}

					string_literal385=(Token)match(input,SUPER,FOLLOW_SUPER_in_selector4672); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal385_tree = (CommonTree)adaptor.create(string_literal385);
					adaptor.addChild(root_0, string_literal385_tree);
					}

					pushFollow(FOLLOW_superSuffix_in_selector4676);
					superSuffix386=superSuffix();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, superSuffix386.getTree());

					}
					break;
				case 4 :
					// Java.g:1002:4: innerCreator
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_innerCreator_in_selector4681);
					innerCreator387=innerCreator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, innerCreator387.getTree());

					}
					break;
				case 5 :
					// Java.g:1003:4: '[' ! expression ']' !
					{
					root_0 = (CommonTree)adaptor.nil();


					char_literal388=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_selector4686); if (state.failed) return retval;
					pushFollow(FOLLOW_expression_in_selector4689);
					expression389=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression389.getTree());

					char_literal390=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_selector4691); if (state.failed) return retval;
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 95, selector_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "selector"


	public static class creator_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "creator"
	// Java.g:1007:1: creator : ( 'new' nonWildcardTypeArguments classOrInterfaceType classCreatorRest | 'new' classOrInterfaceType classCreatorRest | arrayCreator );
	public final JavaParser.creator_return creator() throws RecognitionException {
		JavaParser.creator_return retval = new JavaParser.creator_return();
		retval.start = input.LT(1);
		int creator_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal391=null;
		Token string_literal395=null;
		ParserRuleReturnScope nonWildcardTypeArguments392 =null;
		ParserRuleReturnScope classOrInterfaceType393 =null;
		ParserRuleReturnScope classCreatorRest394 =null;
		ParserRuleReturnScope classOrInterfaceType396 =null;
		ParserRuleReturnScope classCreatorRest397 =null;
		ParserRuleReturnScope arrayCreator398 =null;

		CommonTree string_literal391_tree=null;
		CommonTree string_literal395_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 96) ) { return retval; }

			// Java.g:1008:2: ( 'new' nonWildcardTypeArguments classOrInterfaceType classCreatorRest | 'new' classOrInterfaceType classCreatorRest | arrayCreator )
			int alt142=3;
			int LA142_0 = input.LA(1);
			if ( (LA142_0==NEW) ) {
				int LA142_1 = input.LA(2);
				if ( (synpred233_Java()) ) {
					alt142=1;
				}
				else if ( (synpred234_Java()) ) {
					alt142=2;
				}
				else if ( (true) ) {
					alt142=3;
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 142, 0, input);
				throw nvae;
			}

			switch (alt142) {
				case 1 :
					// Java.g:1008:4: 'new' nonWildcardTypeArguments classOrInterfaceType classCreatorRest
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal391=(Token)match(input,NEW,FOLLOW_NEW_in_creator4704); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal391_tree = (CommonTree)adaptor.create(string_literal391);
					adaptor.addChild(root_0, string_literal391_tree);
					}

					pushFollow(FOLLOW_nonWildcardTypeArguments_in_creator4708);
					nonWildcardTypeArguments392=nonWildcardTypeArguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, nonWildcardTypeArguments392.getTree());

					pushFollow(FOLLOW_classOrInterfaceType_in_creator4712);
					classOrInterfaceType393=classOrInterfaceType();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classOrInterfaceType393.getTree());

					pushFollow(FOLLOW_classCreatorRest_in_creator4716);
					classCreatorRest394=classCreatorRest();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classCreatorRest394.getTree());

					}
					break;
				case 2 :
					// Java.g:1012:4: 'new' classOrInterfaceType classCreatorRest
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal395=(Token)match(input,NEW,FOLLOW_NEW_in_creator4721); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal395_tree = (CommonTree)adaptor.create(string_literal395);
					adaptor.addChild(root_0, string_literal395_tree);
					}

					pushFollow(FOLLOW_classOrInterfaceType_in_creator4725);
					classOrInterfaceType396=classOrInterfaceType();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classOrInterfaceType396.getTree());

					pushFollow(FOLLOW_classCreatorRest_in_creator4729);
					classCreatorRest397=classCreatorRest();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classCreatorRest397.getTree());

					}
					break;
				case 3 :
					// Java.g:1015:4: arrayCreator
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_arrayCreator_in_creator4734);
					arrayCreator398=arrayCreator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arrayCreator398.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 96, creator_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "creator"


	public static class arrayCreator_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "arrayCreator"
	// Java.g:1019:1: arrayCreator : ( 'new' createdName '[' ']' ( '[' ']' )* arrayInitializer | 'new' createdName '[' expression ']' ( '[' expression ']' )* ( '[' ']' )* );
	public final JavaParser.arrayCreator_return arrayCreator() throws RecognitionException {
		JavaParser.arrayCreator_return retval = new JavaParser.arrayCreator_return();
		retval.start = input.LT(1);
		int arrayCreator_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal399=null;
		Token char_literal401=null;
		Token char_literal402=null;
		Token char_literal403=null;
		Token char_literal404=null;
		Token string_literal406=null;
		Token char_literal408=null;
		Token char_literal410=null;
		Token char_literal411=null;
		Token char_literal413=null;
		Token char_literal414=null;
		Token char_literal415=null;
		ParserRuleReturnScope createdName400 =null;
		ParserRuleReturnScope arrayInitializer405 =null;
		ParserRuleReturnScope createdName407 =null;
		ParserRuleReturnScope expression409 =null;
		ParserRuleReturnScope expression412 =null;

		CommonTree string_literal399_tree=null;
		CommonTree char_literal401_tree=null;
		CommonTree char_literal402_tree=null;
		CommonTree char_literal403_tree=null;
		CommonTree char_literal404_tree=null;
		CommonTree string_literal406_tree=null;
		CommonTree char_literal408_tree=null;
		CommonTree char_literal410_tree=null;
		CommonTree char_literal411_tree=null;
		CommonTree char_literal413_tree=null;
		CommonTree char_literal414_tree=null;
		CommonTree char_literal415_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 97) ) { return retval; }

			// Java.g:1020:2: ( 'new' createdName '[' ']' ( '[' ']' )* arrayInitializer | 'new' createdName '[' expression ']' ( '[' expression ']' )* ( '[' ']' )* )
			int alt146=2;
			int LA146_0 = input.LA(1);
			if ( (LA146_0==NEW) ) {
				int LA146_1 = input.LA(2);
				if ( (synpred236_Java()) ) {
					alt146=1;
				}
				else if ( (true) ) {
					alt146=2;
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 146, 0, input);
				throw nvae;
			}

			switch (alt146) {
				case 1 :
					// Java.g:1020:4: 'new' createdName '[' ']' ( '[' ']' )* arrayInitializer
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal399=(Token)match(input,NEW,FOLLOW_NEW_in_arrayCreator4746); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal399_tree = (CommonTree)adaptor.create(string_literal399);
					adaptor.addChild(root_0, string_literal399_tree);
					}

					pushFollow(FOLLOW_createdName_in_arrayCreator4750);
					createdName400=createdName();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, createdName400.getTree());

					char_literal401=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_arrayCreator4754); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal401_tree = (CommonTree)adaptor.create(char_literal401);
					adaptor.addChild(root_0, char_literal401_tree);
					}

					char_literal402=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_arrayCreator4756); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal402_tree = (CommonTree)adaptor.create(char_literal402);
					adaptor.addChild(root_0, char_literal402_tree);
					}

					// Java.g:1023:3: ( '[' ']' )*
					loop143:
					while (true) {
						int alt143=2;
						int LA143_0 = input.LA(1);
						if ( (LA143_0==LBRACKET) ) {
							alt143=1;
						}

						switch (alt143) {
						case 1 :
							// Java.g:1023:4: '[' ']'
							{
							char_literal403=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_arrayCreator4761); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal403_tree = (CommonTree)adaptor.create(char_literal403);
							adaptor.addChild(root_0, char_literal403_tree);
							}

							char_literal404=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_arrayCreator4763); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal404_tree = (CommonTree)adaptor.create(char_literal404);
							adaptor.addChild(root_0, char_literal404_tree);
							}

							}
							break;

						default :
							break loop143;
						}
					}

					pushFollow(FOLLOW_arrayInitializer_in_arrayCreator4769);
					arrayInitializer405=arrayInitializer();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arrayInitializer405.getTree());

					}
					break;
				case 2 :
					// Java.g:1025:4: 'new' createdName '[' expression ']' ( '[' expression ']' )* ( '[' ']' )*
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal406=(Token)match(input,NEW,FOLLOW_NEW_in_arrayCreator4774); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal406_tree = (CommonTree)adaptor.create(string_literal406);
					adaptor.addChild(root_0, string_literal406_tree);
					}

					pushFollow(FOLLOW_createdName_in_arrayCreator4778);
					createdName407=createdName();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, createdName407.getTree());

					char_literal408=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_arrayCreator4782); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal408_tree = (CommonTree)adaptor.create(char_literal408);
					adaptor.addChild(root_0, char_literal408_tree);
					}

					pushFollow(FOLLOW_expression_in_arrayCreator4784);
					expression409=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression409.getTree());

					char_literal410=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_arrayCreator4786); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal410_tree = (CommonTree)adaptor.create(char_literal410);
					adaptor.addChild(root_0, char_literal410_tree);
					}

					// Java.g:1028:3: ( '[' expression ']' )*
					loop144:
					while (true) {
						int alt144=2;
						int LA144_0 = input.LA(1);
						if ( (LA144_0==LBRACKET) ) {
							int LA144_1 = input.LA(2);
							if ( (synpred237_Java()) ) {
								alt144=1;
							}

						}

						switch (alt144) {
						case 1 :
							// Java.g:1028:4: '[' expression ']'
							{
							char_literal411=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_arrayCreator4791); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal411_tree = (CommonTree)adaptor.create(char_literal411);
							adaptor.addChild(root_0, char_literal411_tree);
							}

							pushFollow(FOLLOW_expression_in_arrayCreator4793);
							expression412=expression();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, expression412.getTree());

							char_literal413=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_arrayCreator4795); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal413_tree = (CommonTree)adaptor.create(char_literal413);
							adaptor.addChild(root_0, char_literal413_tree);
							}

							}
							break;

						default :
							break loop144;
						}
					}

					// Java.g:1029:3: ( '[' ']' )*
					loop145:
					while (true) {
						int alt145=2;
						int LA145_0 = input.LA(1);
						if ( (LA145_0==LBRACKET) ) {
							int LA145_2 = input.LA(2);
							if ( (LA145_2==RBRACKET) ) {
								alt145=1;
							}

						}

						switch (alt145) {
						case 1 :
							// Java.g:1029:4: '[' ']'
							{
							char_literal414=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_arrayCreator4802); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal414_tree = (CommonTree)adaptor.create(char_literal414);
							adaptor.addChild(root_0, char_literal414_tree);
							}

							char_literal415=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_arrayCreator4804); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal415_tree = (CommonTree)adaptor.create(char_literal415);
							adaptor.addChild(root_0, char_literal415_tree);
							}

							}
							break;

						default :
							break loop145;
						}
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 97, arrayCreator_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "arrayCreator"


	public static class variableInitializer_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "variableInitializer"
	// Java.g:1033:1: variableInitializer : ( arrayInitializer | expression );
	public final JavaParser.variableInitializer_return variableInitializer() throws RecognitionException {
		JavaParser.variableInitializer_return retval = new JavaParser.variableInitializer_return();
		retval.start = input.LT(1);
		int variableInitializer_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope arrayInitializer416 =null;
		ParserRuleReturnScope expression417 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 98) ) { return retval; }

			// Java.g:1034:2: ( arrayInitializer | expression )
			int alt147=2;
			int LA147_0 = input.LA(1);
			if ( (LA147_0==LBRACE) ) {
				alt147=1;
			}
			else if ( (LA147_0==BANG||LA147_0==BOOLEAN||LA147_0==BYTE||(LA147_0 >= CHAR && LA147_0 <= CHARLITERAL)||(LA147_0 >= DOUBLE && LA147_0 <= DOUBLELITERAL)||LA147_0==FALSE||(LA147_0 >= FLOAT && LA147_0 <= FLOATLITERAL)||LA147_0==IDENTIFIER||LA147_0==INT||LA147_0==INTLITERAL||(LA147_0 >= LONG && LA147_0 <= LPAREN)||(LA147_0 >= NEW && LA147_0 <= NULL)||LA147_0==PLUS||LA147_0==PLUSPLUS||LA147_0==SHORT||(LA147_0 >= STRINGLITERAL && LA147_0 <= SUB)||(LA147_0 >= SUBSUB && LA147_0 <= SUPER)||LA147_0==THIS||LA147_0==TILDE||LA147_0==TRUE||LA147_0==VOID) ) {
				alt147=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 147, 0, input);
				throw nvae;
			}

			switch (alt147) {
				case 1 :
					// Java.g:1034:4: arrayInitializer
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_arrayInitializer_in_variableInitializer4818);
					arrayInitializer416=arrayInitializer();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arrayInitializer416.getTree());

					}
					break;
				case 2 :
					// Java.g:1035:4: expression
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_expression_in_variableInitializer4823);
					expression417=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression417.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 98, variableInitializer_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "variableInitializer"


	public static class arrayInitializer_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "arrayInitializer"
	// Java.g:1039:1: arrayInitializer : '{' ! ( variableInitializer ( ',' variableInitializer )* )? ( ',' )? '}' !;
	public final JavaParser.arrayInitializer_return arrayInitializer() throws RecognitionException {
		JavaParser.arrayInitializer_return retval = new JavaParser.arrayInitializer_return();
		retval.start = input.LT(1);
		int arrayInitializer_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal418=null;
		Token char_literal420=null;
		Token char_literal422=null;
		Token char_literal423=null;
		ParserRuleReturnScope variableInitializer419 =null;
		ParserRuleReturnScope variableInitializer421 =null;

		CommonTree char_literal418_tree=null;
		CommonTree char_literal420_tree=null;
		CommonTree char_literal422_tree=null;
		CommonTree char_literal423_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 99) ) { return retval; }

			// Java.g:1040:2: ( '{' ! ( variableInitializer ( ',' variableInitializer )* )? ( ',' )? '}' !)
			// Java.g:1040:4: '{' ! ( variableInitializer ( ',' variableInitializer )* )? ( ',' )? '}' !
			{
			root_0 = (CommonTree)adaptor.nil();


			char_literal418=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_arrayInitializer4835); if (state.failed) return retval;
			// Java.g:1041:3: ( variableInitializer ( ',' variableInitializer )* )?
			int alt149=2;
			int LA149_0 = input.LA(1);
			if ( (LA149_0==BANG||LA149_0==BOOLEAN||LA149_0==BYTE||(LA149_0 >= CHAR && LA149_0 <= CHARLITERAL)||(LA149_0 >= DOUBLE && LA149_0 <= DOUBLELITERAL)||LA149_0==FALSE||(LA149_0 >= FLOAT && LA149_0 <= FLOATLITERAL)||LA149_0==IDENTIFIER||LA149_0==INT||LA149_0==INTLITERAL||LA149_0==LBRACE||(LA149_0 >= LONG && LA149_0 <= LPAREN)||(LA149_0 >= NEW && LA149_0 <= NULL)||LA149_0==PLUS||LA149_0==PLUSPLUS||LA149_0==SHORT||(LA149_0 >= STRINGLITERAL && LA149_0 <= SUB)||(LA149_0 >= SUBSUB && LA149_0 <= SUPER)||LA149_0==THIS||LA149_0==TILDE||LA149_0==TRUE||LA149_0==VOID) ) {
				alt149=1;
			}
			switch (alt149) {
				case 1 :
					// Java.g:1042:4: variableInitializer ( ',' variableInitializer )*
					{
					pushFollow(FOLLOW_variableInitializer_in_arrayInitializer4845);
					variableInitializer419=variableInitializer();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, variableInitializer419.getTree());

					// Java.g:1043:4: ( ',' variableInitializer )*
					loop148:
					while (true) {
						int alt148=2;
						int LA148_0 = input.LA(1);
						if ( (LA148_0==COMMA) ) {
							int LA148_1 = input.LA(2);
							if ( (LA148_1==BANG||LA148_1==BOOLEAN||LA148_1==BYTE||(LA148_1 >= CHAR && LA148_1 <= CHARLITERAL)||(LA148_1 >= DOUBLE && LA148_1 <= DOUBLELITERAL)||LA148_1==FALSE||(LA148_1 >= FLOAT && LA148_1 <= FLOATLITERAL)||LA148_1==IDENTIFIER||LA148_1==INT||LA148_1==INTLITERAL||LA148_1==LBRACE||(LA148_1 >= LONG && LA148_1 <= LPAREN)||(LA148_1 >= NEW && LA148_1 <= NULL)||LA148_1==PLUS||LA148_1==PLUSPLUS||LA148_1==SHORT||(LA148_1 >= STRINGLITERAL && LA148_1 <= SUB)||(LA148_1 >= SUBSUB && LA148_1 <= SUPER)||LA148_1==THIS||LA148_1==TILDE||LA148_1==TRUE||LA148_1==VOID) ) {
								alt148=1;
							}

						}

						switch (alt148) {
						case 1 :
							// Java.g:1043:5: ',' variableInitializer
							{
							char_literal420=(Token)match(input,COMMA,FOLLOW_COMMA_in_arrayInitializer4851); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal420_tree = (CommonTree)adaptor.create(char_literal420);
							adaptor.addChild(root_0, char_literal420_tree);
							}

							pushFollow(FOLLOW_variableInitializer_in_arrayInitializer4853);
							variableInitializer421=variableInitializer();
							state._fsp--;
							if (state.failed) return retval;
							if ( state.backtracking==0 ) adaptor.addChild(root_0, variableInitializer421.getTree());

							}
							break;

						default :
							break loop148;
						}
					}

					}
					break;

			}

			// Java.g:1045:3: ( ',' )?
			int alt150=2;
			int LA150_0 = input.LA(1);
			if ( (LA150_0==COMMA) ) {
				alt150=1;
			}
			switch (alt150) {
				case 1 :
					// Java.g:1045:4: ','
					{
					char_literal422=(Token)match(input,COMMA,FOLLOW_COMMA_in_arrayInitializer4865); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal422_tree = (CommonTree)adaptor.create(char_literal422);
					adaptor.addChild(root_0, char_literal422_tree);
					}

					}
					break;

			}

			char_literal423=(Token)match(input,RBRACE,FOLLOW_RBRACE_in_arrayInitializer4871); if (state.failed) return retval;
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 99, arrayInitializer_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "arrayInitializer"


	public static class createdName_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "createdName"
	// Java.g:1050:1: createdName : ( classOrInterfaceType | primitiveType );
	public final JavaParser.createdName_return createdName() throws RecognitionException {
		JavaParser.createdName_return retval = new JavaParser.createdName_return();
		retval.start = input.LT(1);
		int createdName_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope classOrInterfaceType424 =null;
		ParserRuleReturnScope primitiveType425 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 100) ) { return retval; }

			// Java.g:1051:2: ( classOrInterfaceType | primitiveType )
			int alt151=2;
			int LA151_0 = input.LA(1);
			if ( (LA151_0==IDENTIFIER) ) {
				alt151=1;
			}
			else if ( (LA151_0==BOOLEAN||LA151_0==BYTE||LA151_0==CHAR||LA151_0==DOUBLE||LA151_0==FLOAT||LA151_0==INT||LA151_0==LONG||LA151_0==SHORT) ) {
				alt151=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 151, 0, input);
				throw nvae;
			}

			switch (alt151) {
				case 1 :
					// Java.g:1051:4: classOrInterfaceType
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_classOrInterfaceType_in_createdName4897);
					classOrInterfaceType424=classOrInterfaceType();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classOrInterfaceType424.getTree());

					}
					break;
				case 2 :
					// Java.g:1052:4: primitiveType
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_primitiveType_in_createdName4902);
					primitiveType425=primitiveType();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, primitiveType425.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 100, createdName_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "createdName"


	public static class innerCreator_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "innerCreator"
	// Java.g:1056:1: innerCreator : '.' ^ 'new' ( nonWildcardTypeArguments )? IDENTIFIER ( typeArguments )? classCreatorRest ;
	public final JavaParser.innerCreator_return innerCreator() throws RecognitionException {
		JavaParser.innerCreator_return retval = new JavaParser.innerCreator_return();
		retval.start = input.LT(1);
		int innerCreator_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal426=null;
		Token string_literal427=null;
		Token IDENTIFIER429=null;
		ParserRuleReturnScope nonWildcardTypeArguments428 =null;
		ParserRuleReturnScope typeArguments430 =null;
		ParserRuleReturnScope classCreatorRest431 =null;

		CommonTree char_literal426_tree=null;
		CommonTree string_literal427_tree=null;
		CommonTree IDENTIFIER429_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 101) ) { return retval; }

			// Java.g:1057:2: ( '.' ^ 'new' ( nonWildcardTypeArguments )? IDENTIFIER ( typeArguments )? classCreatorRest )
			// Java.g:1057:4: '.' ^ 'new' ( nonWildcardTypeArguments )? IDENTIFIER ( typeArguments )? classCreatorRest
			{
			root_0 = (CommonTree)adaptor.nil();


			char_literal426=(Token)match(input,DOT,FOLLOW_DOT_in_innerCreator4914); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal426_tree = (CommonTree)adaptor.create(char_literal426);
			root_0 = (CommonTree)adaptor.becomeRoot(char_literal426_tree, root_0);
			}

			string_literal427=(Token)match(input,NEW,FOLLOW_NEW_in_innerCreator4917); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal427_tree = (CommonTree)adaptor.create(string_literal427);
			adaptor.addChild(root_0, string_literal427_tree);
			}

			// Java.g:1058:3: ( nonWildcardTypeArguments )?
			int alt152=2;
			int LA152_0 = input.LA(1);
			if ( (LA152_0==LT) ) {
				alt152=1;
			}
			switch (alt152) {
				case 1 :
					// Java.g:1058:4: nonWildcardTypeArguments
					{
					pushFollow(FOLLOW_nonWildcardTypeArguments_in_innerCreator4922);
					nonWildcardTypeArguments428=nonWildcardTypeArguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, nonWildcardTypeArguments428.getTree());

					}
					break;

			}

			IDENTIFIER429=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_innerCreator4928); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER429_tree = (CommonTree)adaptor.create(IDENTIFIER429);
			adaptor.addChild(root_0, IDENTIFIER429_tree);
			}

			// Java.g:1060:3: ( typeArguments )?
			int alt153=2;
			int LA153_0 = input.LA(1);
			if ( (LA153_0==LT) ) {
				alt153=1;
			}
			switch (alt153) {
				case 1 :
					// Java.g:1060:4: typeArguments
					{
					pushFollow(FOLLOW_typeArguments_in_innerCreator4933);
					typeArguments430=typeArguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, typeArguments430.getTree());

					}
					break;

			}

			pushFollow(FOLLOW_classCreatorRest_in_innerCreator4939);
			classCreatorRest431=classCreatorRest();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, classCreatorRest431.getTree());

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 101, innerCreator_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "innerCreator"


	public static class classCreatorRest_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "classCreatorRest"
	// Java.g:1065:1: classCreatorRest : arguments ( classBody )? ;
	public final JavaParser.classCreatorRest_return classCreatorRest() throws RecognitionException {
		JavaParser.classCreatorRest_return retval = new JavaParser.classCreatorRest_return();
		retval.start = input.LT(1);
		int classCreatorRest_StartIndex = input.index();

		CommonTree root_0 = null;

		ParserRuleReturnScope arguments432 =null;
		ParserRuleReturnScope classBody433 =null;


		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 102) ) { return retval; }

			// Java.g:1066:2: ( arguments ( classBody )? )
			// Java.g:1066:4: arguments ( classBody )?
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_arguments_in_classCreatorRest4951);
			arguments432=arguments();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, arguments432.getTree());

			// Java.g:1067:3: ( classBody )?
			int alt154=2;
			int LA154_0 = input.LA(1);
			if ( (LA154_0==LBRACE) ) {
				alt154=1;
			}
			switch (alt154) {
				case 1 :
					// Java.g:1067:4: classBody
					{
					pushFollow(FOLLOW_classBody_in_classCreatorRest4956);
					classBody433=classBody();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classBody433.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 102, classCreatorRest_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "classCreatorRest"


	public static class nonWildcardTypeArguments_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "nonWildcardTypeArguments"
	// Java.g:1071:1: nonWildcardTypeArguments : '<' ! typeList '>' !;
	public final JavaParser.nonWildcardTypeArguments_return nonWildcardTypeArguments() throws RecognitionException {
		JavaParser.nonWildcardTypeArguments_return retval = new JavaParser.nonWildcardTypeArguments_return();
		retval.start = input.LT(1);
		int nonWildcardTypeArguments_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal434=null;
		Token char_literal436=null;
		ParserRuleReturnScope typeList435 =null;

		CommonTree char_literal434_tree=null;
		CommonTree char_literal436_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 103) ) { return retval; }

			// Java.g:1072:2: ( '<' ! typeList '>' !)
			// Java.g:1072:4: '<' ! typeList '>' !
			{
			root_0 = (CommonTree)adaptor.nil();


			char_literal434=(Token)match(input,LT,FOLLOW_LT_in_nonWildcardTypeArguments4970); if (state.failed) return retval;
			pushFollow(FOLLOW_typeList_in_nonWildcardTypeArguments4973);
			typeList435=typeList();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, typeList435.getTree());

			char_literal436=(Token)match(input,GT,FOLLOW_GT_in_nonWildcardTypeArguments4975); if (state.failed) return retval;
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 103, nonWildcardTypeArguments_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "nonWildcardTypeArguments"


	public static class arguments_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "arguments"
	// Java.g:1076:1: arguments : (a= '(' ')' -> ^( ARGUMENTS[$a,\"ARGUMENTS\"] ) |a= '(' b= expressionList ')' -> ^( ARGUMENTS[$a,\"ARGUMENTS\"] $b) );
	public final JavaParser.arguments_return arguments() throws RecognitionException {
		JavaParser.arguments_return retval = new JavaParser.arguments_return();
		retval.start = input.LT(1);
		int arguments_StartIndex = input.index();

		CommonTree root_0 = null;

		Token a=null;
		Token char_literal437=null;
		Token char_literal438=null;
		ParserRuleReturnScope b =null;

		CommonTree a_tree=null;
		CommonTree char_literal437_tree=null;
		CommonTree char_literal438_tree=null;
		RewriteRuleTokenStream stream_RPAREN=new RewriteRuleTokenStream(adaptor,"token RPAREN");
		RewriteRuleTokenStream stream_LPAREN=new RewriteRuleTokenStream(adaptor,"token LPAREN");
		RewriteRuleSubtreeStream stream_expressionList=new RewriteRuleSubtreeStream(adaptor,"rule expressionList");

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 104) ) { return retval; }

			// Java.g:1077:2: (a= '(' ')' -> ^( ARGUMENTS[$a,\"ARGUMENTS\"] ) |a= '(' b= expressionList ')' -> ^( ARGUMENTS[$a,\"ARGUMENTS\"] $b) )
			int alt155=2;
			int LA155_0 = input.LA(1);
			if ( (LA155_0==LPAREN) ) {
				int LA155_1 = input.LA(2);
				if ( (LA155_1==RPAREN) ) {
					alt155=1;
				}
				else if ( (LA155_1==BANG||LA155_1==BOOLEAN||LA155_1==BYTE||(LA155_1 >= CHAR && LA155_1 <= CHARLITERAL)||(LA155_1 >= DOUBLE && LA155_1 <= DOUBLELITERAL)||LA155_1==FALSE||(LA155_1 >= FLOAT && LA155_1 <= FLOATLITERAL)||LA155_1==IDENTIFIER||LA155_1==INT||LA155_1==INTLITERAL||(LA155_1 >= LONG && LA155_1 <= LPAREN)||(LA155_1 >= NEW && LA155_1 <= NULL)||LA155_1==PLUS||LA155_1==PLUSPLUS||LA155_1==SHORT||(LA155_1 >= STRINGLITERAL && LA155_1 <= SUB)||(LA155_1 >= SUBSUB && LA155_1 <= SUPER)||LA155_1==THIS||LA155_1==TILDE||LA155_1==TRUE||LA155_1==VOID) ) {
					alt155=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 155, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 155, 0, input);
				throw nvae;
			}

			switch (alt155) {
				case 1 :
					// Java.g:1077:4: a= '(' ')'
					{
					a=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_arguments4990); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(a);

					char_literal437=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_arguments4992); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(char_literal437);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 1078:2: -> ^( ARGUMENTS[$a,\"ARGUMENTS\"] )
					{
						// Java.g:1078:5: ^( ARGUMENTS[$a,\"ARGUMENTS\"] )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ARGUMENTS, a, "ARGUMENTS"), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;
				case 2 :
					// Java.g:1079:4: a= '(' b= expressionList ')'
					{
					a=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_arguments5008); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_LPAREN.add(a);

					pushFollow(FOLLOW_expressionList_in_arguments5012);
					b=expressionList();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) stream_expressionList.add(b.getTree());
					char_literal438=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_arguments5014); if (state.failed) return retval; 
					if ( state.backtracking==0 ) stream_RPAREN.add(char_literal438);

					// AST REWRITE
					// elements: b
					// token labels: 
					// rule labels: retval, b
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					if ( state.backtracking==0 ) {
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 1080:2: -> ^( ARGUMENTS[$a,\"ARGUMENTS\"] $b)
					{
						// Java.g:1080:5: ^( ARGUMENTS[$a,\"ARGUMENTS\"] $b)
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ARGUMENTS, a, "ARGUMENTS"), root_1);
						adaptor.addChild(root_1, stream_b.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 104, arguments_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "arguments"


	public static class literal_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "literal"
	// Java.g:1084:1: literal : ( INTLITERAL | LONGLITERAL | FLOATLITERAL | DOUBLELITERAL | CHARLITERAL | STRINGLITERAL | TRUE | FALSE | NULL );
	public final JavaParser.literal_return literal() throws RecognitionException {
		JavaParser.literal_return retval = new JavaParser.literal_return();
		retval.start = input.LT(1);
		int literal_StartIndex = input.index();

		CommonTree root_0 = null;

		Token set439=null;

		CommonTree set439_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 105) ) { return retval; }

			// Java.g:1085:2: ( INTLITERAL | LONGLITERAL | FLOATLITERAL | DOUBLELITERAL | CHARLITERAL | STRINGLITERAL | TRUE | FALSE | NULL )
			// Java.g:
			{
			root_0 = (CommonTree)adaptor.nil();


			set439=input.LT(1);
			if ( input.LA(1)==CHARLITERAL||input.LA(1)==DOUBLELITERAL||input.LA(1)==FALSE||input.LA(1)==FLOATLITERAL||input.LA(1)==INTLITERAL||input.LA(1)==LONGLITERAL||input.LA(1)==NULL||input.LA(1)==STRINGLITERAL||input.LA(1)==TRUE ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (CommonTree)adaptor.create(set439));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 105, literal_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "literal"


	public static class classHeader_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "classHeader"
	// Java.g:1101:1: classHeader : modifiers ! 'class' IDENTIFIER ;
	public final JavaParser.classHeader_return classHeader() throws RecognitionException {
		JavaParser.classHeader_return retval = new JavaParser.classHeader_return();
		retval.start = input.LT(1);
		int classHeader_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal441=null;
		Token IDENTIFIER442=null;
		ParserRuleReturnScope modifiers440 =null;

		CommonTree string_literal441_tree=null;
		CommonTree IDENTIFIER442_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 106) ) { return retval; }

			// Java.g:1102:2: ( modifiers ! 'class' IDENTIFIER )
			// Java.g:1102:4: modifiers ! 'class' IDENTIFIER
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_modifiers_in_classHeader5092);
			modifiers440=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			string_literal441=(Token)match(input,CLASS,FOLLOW_CLASS_in_classHeader5095); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal441_tree = (CommonTree)adaptor.create(string_literal441);
			adaptor.addChild(root_0, string_literal441_tree);
			}

			IDENTIFIER442=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_classHeader5097); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER442_tree = (CommonTree)adaptor.create(IDENTIFIER442);
			adaptor.addChild(root_0, IDENTIFIER442_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 106, classHeader_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "classHeader"


	public static class enumHeader_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "enumHeader"
	// Java.g:1105:1: enumHeader : modifiers ! ( 'enum' | IDENTIFIER ) IDENTIFIER ;
	public final JavaParser.enumHeader_return enumHeader() throws RecognitionException {
		JavaParser.enumHeader_return retval = new JavaParser.enumHeader_return();
		retval.start = input.LT(1);
		int enumHeader_StartIndex = input.index();

		CommonTree root_0 = null;

		Token set444=null;
		Token IDENTIFIER445=null;
		ParserRuleReturnScope modifiers443 =null;

		CommonTree set444_tree=null;
		CommonTree IDENTIFIER445_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 107) ) { return retval; }

			// Java.g:1106:2: ( modifiers ! ( 'enum' | IDENTIFIER ) IDENTIFIER )
			// Java.g:1106:4: modifiers ! ( 'enum' | IDENTIFIER ) IDENTIFIER
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_modifiers_in_enumHeader5108);
			modifiers443=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			set444=input.LT(1);
			if ( input.LA(1)==ENUM||input.LA(1)==IDENTIFIER ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (CommonTree)adaptor.create(set444));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			IDENTIFIER445=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_enumHeader5119); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER445_tree = (CommonTree)adaptor.create(IDENTIFIER445);
			adaptor.addChild(root_0, IDENTIFIER445_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 107, enumHeader_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "enumHeader"


	public static class interfaceHeader_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "interfaceHeader"
	// Java.g:1109:1: interfaceHeader : modifiers ! 'interface' IDENTIFIER ;
	public final JavaParser.interfaceHeader_return interfaceHeader() throws RecognitionException {
		JavaParser.interfaceHeader_return retval = new JavaParser.interfaceHeader_return();
		retval.start = input.LT(1);
		int interfaceHeader_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal447=null;
		Token IDENTIFIER448=null;
		ParserRuleReturnScope modifiers446 =null;

		CommonTree string_literal447_tree=null;
		CommonTree IDENTIFIER448_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 108) ) { return retval; }

			// Java.g:1110:2: ( modifiers ! 'interface' IDENTIFIER )
			// Java.g:1110:4: modifiers ! 'interface' IDENTIFIER
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_modifiers_in_interfaceHeader5130);
			modifiers446=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			string_literal447=(Token)match(input,INTERFACE,FOLLOW_INTERFACE_in_interfaceHeader5133); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal447_tree = (CommonTree)adaptor.create(string_literal447);
			adaptor.addChild(root_0, string_literal447_tree);
			}

			IDENTIFIER448=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_interfaceHeader5135); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER448_tree = (CommonTree)adaptor.create(IDENTIFIER448);
			adaptor.addChild(root_0, IDENTIFIER448_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 108, interfaceHeader_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "interfaceHeader"


	public static class annotationHeader_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "annotationHeader"
	// Java.g:1113:1: annotationHeader : modifiers ! '@' 'interface' IDENTIFIER ;
	public final JavaParser.annotationHeader_return annotationHeader() throws RecognitionException {
		JavaParser.annotationHeader_return retval = new JavaParser.annotationHeader_return();
		retval.start = input.LT(1);
		int annotationHeader_StartIndex = input.index();

		CommonTree root_0 = null;

		Token char_literal450=null;
		Token string_literal451=null;
		Token IDENTIFIER452=null;
		ParserRuleReturnScope modifiers449 =null;

		CommonTree char_literal450_tree=null;
		CommonTree string_literal451_tree=null;
		CommonTree IDENTIFIER452_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 109) ) { return retval; }

			// Java.g:1114:2: ( modifiers ! '@' 'interface' IDENTIFIER )
			// Java.g:1114:4: modifiers ! '@' 'interface' IDENTIFIER
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_modifiers_in_annotationHeader5146);
			modifiers449=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			char_literal450=(Token)match(input,MONKEYS_AT,FOLLOW_MONKEYS_AT_in_annotationHeader5149); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal450_tree = (CommonTree)adaptor.create(char_literal450);
			adaptor.addChild(root_0, char_literal450_tree);
			}

			string_literal451=(Token)match(input,INTERFACE,FOLLOW_INTERFACE_in_annotationHeader5151); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			string_literal451_tree = (CommonTree)adaptor.create(string_literal451);
			adaptor.addChild(root_0, string_literal451_tree);
			}

			IDENTIFIER452=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_annotationHeader5153); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER452_tree = (CommonTree)adaptor.create(IDENTIFIER452);
			adaptor.addChild(root_0, IDENTIFIER452_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 109, annotationHeader_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "annotationHeader"


	public static class typeHeader_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "typeHeader"
	// Java.g:1117:1: typeHeader : modifiers ! ( 'class' | 'enum' | ( ( '@' )? 'interface' ) ) IDENTIFIER ;
	public final JavaParser.typeHeader_return typeHeader() throws RecognitionException {
		JavaParser.typeHeader_return retval = new JavaParser.typeHeader_return();
		retval.start = input.LT(1);
		int typeHeader_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal454=null;
		Token string_literal455=null;
		Token char_literal456=null;
		Token string_literal457=null;
		Token IDENTIFIER458=null;
		ParserRuleReturnScope modifiers453 =null;

		CommonTree string_literal454_tree=null;
		CommonTree string_literal455_tree=null;
		CommonTree char_literal456_tree=null;
		CommonTree string_literal457_tree=null;
		CommonTree IDENTIFIER458_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 110) ) { return retval; }

			// Java.g:1118:2: ( modifiers ! ( 'class' | 'enum' | ( ( '@' )? 'interface' ) ) IDENTIFIER )
			// Java.g:1118:4: modifiers ! ( 'class' | 'enum' | ( ( '@' )? 'interface' ) ) IDENTIFIER
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_modifiers_in_typeHeader5164);
			modifiers453=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			// Java.g:1118:15: ( 'class' | 'enum' | ( ( '@' )? 'interface' ) )
			int alt157=3;
			switch ( input.LA(1) ) {
			case CLASS:
				{
				alt157=1;
				}
				break;
			case ENUM:
				{
				alt157=2;
				}
				break;
			case INTERFACE:
			case MONKEYS_AT:
				{
				alt157=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 157, 0, input);
				throw nvae;
			}
			switch (alt157) {
				case 1 :
					// Java.g:1118:16: 'class'
					{
					string_literal454=(Token)match(input,CLASS,FOLLOW_CLASS_in_typeHeader5168); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal454_tree = (CommonTree)adaptor.create(string_literal454);
					adaptor.addChild(root_0, string_literal454_tree);
					}

					}
					break;
				case 2 :
					// Java.g:1118:24: 'enum'
					{
					string_literal455=(Token)match(input,ENUM,FOLLOW_ENUM_in_typeHeader5170); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal455_tree = (CommonTree)adaptor.create(string_literal455);
					adaptor.addChild(root_0, string_literal455_tree);
					}

					}
					break;
				case 3 :
					// Java.g:1118:31: ( ( '@' )? 'interface' )
					{
					// Java.g:1118:31: ( ( '@' )? 'interface' )
					// Java.g:1118:32: ( '@' )? 'interface'
					{
					// Java.g:1118:32: ( '@' )?
					int alt156=2;
					int LA156_0 = input.LA(1);
					if ( (LA156_0==MONKEYS_AT) ) {
						alt156=1;
					}
					switch (alt156) {
						case 1 :
							// Java.g:1118:32: '@'
							{
							char_literal456=(Token)match(input,MONKEYS_AT,FOLLOW_MONKEYS_AT_in_typeHeader5173); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							char_literal456_tree = (CommonTree)adaptor.create(char_literal456);
							adaptor.addChild(root_0, char_literal456_tree);
							}

							}
							break;

					}

					string_literal457=(Token)match(input,INTERFACE,FOLLOW_INTERFACE_in_typeHeader5177); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal457_tree = (CommonTree)adaptor.create(string_literal457);
					adaptor.addChild(root_0, string_literal457_tree);
					}

					}

					}
					break;

			}

			IDENTIFIER458=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_typeHeader5181); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER458_tree = (CommonTree)adaptor.create(IDENTIFIER458);
			adaptor.addChild(root_0, IDENTIFIER458_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 110, typeHeader_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "typeHeader"


	public static class methodHeader_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "methodHeader"
	// Java.g:1121:1: methodHeader : modifiers ! ( typeParameters )? ( type | 'void' )? IDENTIFIER '(' ;
	public final JavaParser.methodHeader_return methodHeader() throws RecognitionException {
		JavaParser.methodHeader_return retval = new JavaParser.methodHeader_return();
		retval.start = input.LT(1);
		int methodHeader_StartIndex = input.index();

		CommonTree root_0 = null;

		Token string_literal462=null;
		Token IDENTIFIER463=null;
		Token char_literal464=null;
		ParserRuleReturnScope modifiers459 =null;
		ParserRuleReturnScope typeParameters460 =null;
		ParserRuleReturnScope type461 =null;

		CommonTree string_literal462_tree=null;
		CommonTree IDENTIFIER463_tree=null;
		CommonTree char_literal464_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 111) ) { return retval; }

			// Java.g:1122:2: ( modifiers ! ( typeParameters )? ( type | 'void' )? IDENTIFIER '(' )
			// Java.g:1122:4: modifiers ! ( typeParameters )? ( type | 'void' )? IDENTIFIER '('
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_modifiers_in_methodHeader5192);
			modifiers459=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			// Java.g:1122:15: ( typeParameters )?
			int alt158=2;
			int LA158_0 = input.LA(1);
			if ( (LA158_0==LT) ) {
				alt158=1;
			}
			switch (alt158) {
				case 1 :
					// Java.g:1122:15: typeParameters
					{
					pushFollow(FOLLOW_typeParameters_in_methodHeader5195);
					typeParameters460=typeParameters();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, typeParameters460.getTree());

					}
					break;

			}

			// Java.g:1122:31: ( type | 'void' )?
			int alt159=3;
			switch ( input.LA(1) ) {
				case IDENTIFIER:
					{
					int LA159_1 = input.LA(2);
					if ( (LA159_1==DOT||LA159_1==IDENTIFIER||LA159_1==LBRACKET||LA159_1==LT) ) {
						alt159=1;
					}
					}
					break;
				case BOOLEAN:
				case BYTE:
				case CHAR:
				case DOUBLE:
				case FLOAT:
				case INT:
				case LONG:
				case SHORT:
					{
					alt159=1;
					}
					break;
				case VOID:
					{
					alt159=2;
					}
					break;
			}
			switch (alt159) {
				case 1 :
					// Java.g:1122:32: type
					{
					pushFollow(FOLLOW_type_in_methodHeader5199);
					type461=type();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, type461.getTree());

					}
					break;
				case 2 :
					// Java.g:1122:39: 'void'
					{
					string_literal462=(Token)match(input,VOID,FOLLOW_VOID_in_methodHeader5203); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					string_literal462_tree = (CommonTree)adaptor.create(string_literal462);
					adaptor.addChild(root_0, string_literal462_tree);
					}

					}
					break;

			}

			IDENTIFIER463=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_methodHeader5207); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER463_tree = (CommonTree)adaptor.create(IDENTIFIER463);
			adaptor.addChild(root_0, IDENTIFIER463_tree);
			}

			char_literal464=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_methodHeader5209); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			char_literal464_tree = (CommonTree)adaptor.create(char_literal464);
			adaptor.addChild(root_0, char_literal464_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 111, methodHeader_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "methodHeader"


	public static class fieldHeader_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "fieldHeader"
	// Java.g:1125:1: fieldHeader : modifiers ! type IDENTIFIER ( '[' ']' )* ( '=' | ',' | ';' ) ;
	public final JavaParser.fieldHeader_return fieldHeader() throws RecognitionException {
		JavaParser.fieldHeader_return retval = new JavaParser.fieldHeader_return();
		retval.start = input.LT(1);
		int fieldHeader_StartIndex = input.index();

		CommonTree root_0 = null;

		Token IDENTIFIER467=null;
		Token char_literal468=null;
		Token char_literal469=null;
		Token set470=null;
		ParserRuleReturnScope modifiers465 =null;
		ParserRuleReturnScope type466 =null;

		CommonTree IDENTIFIER467_tree=null;
		CommonTree char_literal468_tree=null;
		CommonTree char_literal469_tree=null;
		CommonTree set470_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 112) ) { return retval; }

			// Java.g:1126:2: ( modifiers ! type IDENTIFIER ( '[' ']' )* ( '=' | ',' | ';' ) )
			// Java.g:1126:4: modifiers ! type IDENTIFIER ( '[' ']' )* ( '=' | ',' | ';' )
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_modifiers_in_fieldHeader5220);
			modifiers465=modifiers();
			state._fsp--;
			if (state.failed) return retval;
			pushFollow(FOLLOW_type_in_fieldHeader5223);
			type466=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, type466.getTree());

			IDENTIFIER467=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_fieldHeader5225); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER467_tree = (CommonTree)adaptor.create(IDENTIFIER467);
			adaptor.addChild(root_0, IDENTIFIER467_tree);
			}

			// Java.g:1126:31: ( '[' ']' )*
			loop160:
			while (true) {
				int alt160=2;
				int LA160_0 = input.LA(1);
				if ( (LA160_0==LBRACKET) ) {
					alt160=1;
				}

				switch (alt160) {
				case 1 :
					// Java.g:1126:32: '[' ']'
					{
					char_literal468=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_fieldHeader5228); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal468_tree = (CommonTree)adaptor.create(char_literal468);
					adaptor.addChild(root_0, char_literal468_tree);
					}

					char_literal469=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_fieldHeader5229); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal469_tree = (CommonTree)adaptor.create(char_literal469);
					adaptor.addChild(root_0, char_literal469_tree);
					}

					}
					break;

				default :
					break loop160;
				}
			}

			set470=input.LT(1);
			if ( input.LA(1)==COMMA||input.LA(1)==EQ||input.LA(1)==SEMI ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (CommonTree)adaptor.create(set470));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 112, fieldHeader_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "fieldHeader"


	public static class localVariableHeader_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "localVariableHeader"
	// Java.g:1129:1: localVariableHeader : variableModifiers type IDENTIFIER ( '[' ']' )* ( '=' | ',' | ';' ) ;
	public final JavaParser.localVariableHeader_return localVariableHeader() throws RecognitionException {
		JavaParser.localVariableHeader_return retval = new JavaParser.localVariableHeader_return();
		retval.start = input.LT(1);
		int localVariableHeader_StartIndex = input.index();

		CommonTree root_0 = null;

		Token IDENTIFIER473=null;
		Token char_literal474=null;
		Token char_literal475=null;
		Token set476=null;
		ParserRuleReturnScope variableModifiers471 =null;
		ParserRuleReturnScope type472 =null;

		CommonTree IDENTIFIER473_tree=null;
		CommonTree char_literal474_tree=null;
		CommonTree char_literal475_tree=null;
		CommonTree set476_tree=null;

		try {
			if ( state.backtracking>0 && alreadyParsedRule(input, 113) ) { return retval; }

			// Java.g:1130:2: ( variableModifiers type IDENTIFIER ( '[' ']' )* ( '=' | ',' | ';' ) )
			// Java.g:1130:4: variableModifiers type IDENTIFIER ( '[' ']' )* ( '=' | ',' | ';' )
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_variableModifiers_in_localVariableHeader5250);
			variableModifiers471=variableModifiers();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, variableModifiers471.getTree());

			pushFollow(FOLLOW_type_in_localVariableHeader5252);
			type472=type();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, type472.getTree());

			IDENTIFIER473=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_localVariableHeader5254); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IDENTIFIER473_tree = (CommonTree)adaptor.create(IDENTIFIER473);
			adaptor.addChild(root_0, IDENTIFIER473_tree);
			}

			// Java.g:1130:38: ( '[' ']' )*
			loop161:
			while (true) {
				int alt161=2;
				int LA161_0 = input.LA(1);
				if ( (LA161_0==LBRACKET) ) {
					alt161=1;
				}

				switch (alt161) {
				case 1 :
					// Java.g:1130:39: '[' ']'
					{
					char_literal474=(Token)match(input,LBRACKET,FOLLOW_LBRACKET_in_localVariableHeader5257); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal474_tree = (CommonTree)adaptor.create(char_literal474);
					adaptor.addChild(root_0, char_literal474_tree);
					}

					char_literal475=(Token)match(input,RBRACKET,FOLLOW_RBRACKET_in_localVariableHeader5258); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					char_literal475_tree = (CommonTree)adaptor.create(char_literal475);
					adaptor.addChild(root_0, char_literal475_tree);
					}

					}
					break;

				default :
					break loop161;
				}
			}

			set476=input.LT(1);
			if ( input.LA(1)==COMMA||input.LA(1)==EQ||input.LA(1)==SEMI ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (CommonTree)adaptor.create(set476));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			if ( state.backtracking>0 ) { memoize(input, 113, localVariableHeader_StartIndex); }

		}
		return retval;
	}
	// $ANTLR end "localVariableHeader"

	// $ANTLR start synpred1_Java
	public final void synpred1_Java_fragment() throws RecognitionException {
		// Java.g:90:4: ( packageDeclaration )
		// Java.g:90:4: packageDeclaration
		{
		pushFollow(FOLLOW_packageDeclaration_in_synpred1_Java196);
		packageDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred1_Java

	// $ANTLR start synpred7_Java
	public final void synpred7_Java_fragment() throws RecognitionException {
		// Java.g:121:4: ( classDeclaration )
		// Java.g:121:4: classDeclaration
		{
		pushFollow(FOLLOW_classDeclaration_in_synpred7_Java333);
		classDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred7_Java

	// $ANTLR start synpred22_Java
	public final void synpred22_Java_fragment() throws RecognitionException {
		// Java.g:151:4: ( normalClassDeclaration )
		// Java.g:151:4: normalClassDeclaration
		{
		pushFollow(FOLLOW_normalClassDeclaration_in_synpred22_Java475);
		normalClassDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred22_Java

	// $ANTLR start synpred38_Java
	public final void synpred38_Java_fragment() throws RecognitionException {
		// Java.g:235:4: ( normalInterfaceDeclaration )
		// Java.g:235:4: normalInterfaceDeclaration
		{
		pushFollow(FOLLOW_normalInterfaceDeclaration_in_synpred38_Java874);
		normalInterfaceDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred38_Java

	// $ANTLR start synpred47_Java
	public final void synpred47_Java_fragment() throws RecognitionException {
		// Java.g:284:4: ( fieldDeclaration )
		// Java.g:284:4: fieldDeclaration
		{
		pushFollow(FOLLOW_fieldDeclaration_in_synpred47_Java1098);
		fieldDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred47_Java

	// $ANTLR start synpred48_Java
	public final void synpred48_Java_fragment() throws RecognitionException {
		// Java.g:285:4: ( methodDeclaration )
		// Java.g:285:4: methodDeclaration
		{
		pushFollow(FOLLOW_methodDeclaration_in_synpred48_Java1103);
		methodDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred48_Java

	// $ANTLR start synpred49_Java
	public final void synpred49_Java_fragment() throws RecognitionException {
		// Java.g:286:4: ( classDeclaration )
		// Java.g:286:4: classDeclaration
		{
		pushFollow(FOLLOW_classDeclaration_in_synpred49_Java1108);
		classDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred49_Java

	// $ANTLR start synpred52_Java
	public final void synpred52_Java_fragment() throws RecognitionException {
		ParserRuleReturnScope f =null;


		// Java.g:299:4: (f= explicitConstructorInvocation )
		// Java.g:299:4: f= explicitConstructorInvocation
		{
		pushFollow(FOLLOW_explicitConstructorInvocation_in_synpred52_Java1171);
		f=explicitConstructorInvocation();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred52_Java

	// $ANTLR start synpred54_Java
	public final void synpred54_Java_fragment() throws RecognitionException {
		Token c=null;
		Token e=null;
		List<Object> list_g=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope d =null;
		ParserRuleReturnScope f =null;
		RuleReturnScope g = null;

		// Java.g:293:3: (a= modifiers (b= typeParameters )? c= IDENTIFIER d= formalParameters ( 'throws' qualifiedNameList )? e= '{' (f= explicitConstructorInvocation )? (g+= blockStatement )* '}' )
		// Java.g:293:3: a= modifiers (b= typeParameters )? c= IDENTIFIER d= formalParameters ( 'throws' qualifiedNameList )? e= '{' (f= explicitConstructorInvocation )? (g+= blockStatement )* '}'
		{
		pushFollow(FOLLOW_modifiers_in_synpred54_Java1131);
		a=modifiers();
		state._fsp--;
		if (state.failed) return;

		// Java.g:294:4: (b= typeParameters )?
		int alt163=2;
		int LA163_0 = input.LA(1);
		if ( (LA163_0==LT) ) {
			alt163=1;
		}
		switch (alt163) {
			case 1 :
				// Java.g:294:4: b= typeParameters
				{
				pushFollow(FOLLOW_typeParameters_in_synpred54_Java1137);
				b=typeParameters();
				state._fsp--;
				if (state.failed) return;

				}
				break;

		}

		c=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred54_Java1144); if (state.failed) return;

		pushFollow(FOLLOW_formalParameters_in_synpred54_Java1150);
		d=formalParameters();
		state._fsp--;
		if (state.failed) return;

		// Java.g:297:3: ( 'throws' qualifiedNameList )?
		int alt164=2;
		int LA164_0 = input.LA(1);
		if ( (LA164_0==THROWS) ) {
			alt164=1;
		}
		switch (alt164) {
			case 1 :
				// Java.g:297:4: 'throws' qualifiedNameList
				{
				match(input,THROWS,FOLLOW_THROWS_in_synpred54_Java1155); if (state.failed) return;

				pushFollow(FOLLOW_qualifiedNameList_in_synpred54_Java1157);
				qualifiedNameList();
				state._fsp--;
				if (state.failed) return;

				}
				break;

		}

		e=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_synpred54_Java1165); if (state.failed) return;

		// Java.g:299:4: (f= explicitConstructorInvocation )?
		int alt165=2;
		switch ( input.LA(1) ) {
			case LT:
				{
				alt165=1;
				}
				break;
			case THIS:
				{
				int LA165_2 = input.LA(2);
				if ( (synpred52_Java()) ) {
					alt165=1;
				}
				}
				break;
			case LPAREN:
				{
				int LA165_3 = input.LA(2);
				if ( (synpred52_Java()) ) {
					alt165=1;
				}
				}
				break;
			case SUPER:
				{
				int LA165_4 = input.LA(2);
				if ( (synpred52_Java()) ) {
					alt165=1;
				}
				}
				break;
			case IDENTIFIER:
				{
				int LA165_5 = input.LA(2);
				if ( (synpred52_Java()) ) {
					alt165=1;
				}
				}
				break;
			case CHARLITERAL:
			case DOUBLELITERAL:
			case FALSE:
			case FLOATLITERAL:
			case INTLITERAL:
			case LONGLITERAL:
			case NULL:
			case STRINGLITERAL:
			case TRUE:
				{
				int LA165_6 = input.LA(2);
				if ( (synpred52_Java()) ) {
					alt165=1;
				}
				}
				break;
			case NEW:
				{
				int LA165_7 = input.LA(2);
				if ( (synpred52_Java()) ) {
					alt165=1;
				}
				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				{
				int LA165_8 = input.LA(2);
				if ( (synpred52_Java()) ) {
					alt165=1;
				}
				}
				break;
			case VOID:
				{
				int LA165_9 = input.LA(2);
				if ( (synpred52_Java()) ) {
					alt165=1;
				}
				}
				break;
		}
		switch (alt165) {
			case 1 :
				// Java.g:299:4: f= explicitConstructorInvocation
				{
				pushFollow(FOLLOW_explicitConstructorInvocation_in_synpred54_Java1171);
				f=explicitConstructorInvocation();
				state._fsp--;
				if (state.failed) return;

				}
				break;

		}

		// Java.g:300:4: (g+= blockStatement )*
		loop166:
		while (true) {
			int alt166=2;
			int LA166_0 = input.LA(1);
			if ( (LA166_0==ABSTRACT||(LA166_0 >= ASSERT && LA166_0 <= BANG)||(LA166_0 >= BOOLEAN && LA166_0 <= BYTE)||(LA166_0 >= CHAR && LA166_0 <= CLASS)||LA166_0==CONTINUE||LA166_0==DO||(LA166_0 >= DOUBLE && LA166_0 <= DOUBLELITERAL)||LA166_0==ENUM||LA166_0==FALSE||LA166_0==FINAL||(LA166_0 >= FLOAT && LA166_0 <= FOR)||(LA166_0 >= IDENTIFIER && LA166_0 <= IF)||(LA166_0 >= INT && LA166_0 <= INTLITERAL)||LA166_0==LBRACE||(LA166_0 >= LONG && LA166_0 <= LT)||(LA166_0 >= MONKEYS_AT && LA166_0 <= NULL)||LA166_0==PLUS||(LA166_0 >= PLUSPLUS && LA166_0 <= PUBLIC)||LA166_0==RETURN||LA166_0==SEMI||LA166_0==SHORT||(LA166_0 >= STATIC && LA166_0 <= SUB)||(LA166_0 >= SUBSUB && LA166_0 <= SYNCHRONIZED)||(LA166_0 >= THIS && LA166_0 <= THROW)||(LA166_0 >= TILDE && LA166_0 <= TRY)||(LA166_0 >= VOID && LA166_0 <= WHILE)) ) {
				alt166=1;
			}

			switch (alt166) {
			case 1 :
				// Java.g:300:4: g+= blockStatement
				{
				pushFollow(FOLLOW_blockStatement_in_synpred54_Java1178);
				g=blockStatement();
				state._fsp--;
				if (state.failed) return;

				}
				break;

			default :
				break loop166;
			}
		}

		match(input,RBRACE,FOLLOW_RBRACE_in_synpred54_Java1183); if (state.failed) return;

		}

	}
	// $ANTLR end synpred54_Java

	// $ANTLR start synpred63_Java
	public final void synpred63_Java_fragment() throws RecognitionException {
		// Java.g:339:4: ( interfaceFieldDeclaration )
		// Java.g:339:4: interfaceFieldDeclaration
		{
		pushFollow(FOLLOW_interfaceFieldDeclaration_in_synpred63_Java1439);
		interfaceFieldDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred63_Java

	// $ANTLR start synpred64_Java
	public final void synpred64_Java_fragment() throws RecognitionException {
		// Java.g:340:4: ( interfaceMethodDeclaration )
		// Java.g:340:4: interfaceMethodDeclaration
		{
		pushFollow(FOLLOW_interfaceMethodDeclaration_in_synpred64_Java1444);
		interfaceMethodDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred64_Java

	// $ANTLR start synpred65_Java
	public final void synpred65_Java_fragment() throws RecognitionException {
		// Java.g:341:4: ( interfaceDeclaration )
		// Java.g:341:4: interfaceDeclaration
		{
		pushFollow(FOLLOW_interfaceDeclaration_in_synpred65_Java1449);
		interfaceDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred65_Java

	// $ANTLR start synpred66_Java
	public final void synpred66_Java_fragment() throws RecognitionException {
		// Java.g:342:4: ( classDeclaration )
		// Java.g:342:4: classDeclaration
		{
		pushFollow(FOLLOW_classDeclaration_in_synpred66_Java1454);
		classDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred66_Java

	// $ANTLR start synpred91_Java
	public final void synpred91_Java_fragment() throws RecognitionException {
		ParserRuleReturnScope b =null;


		// Java.g:435:4: (b= ellipsisParameterDecl )
		// Java.g:435:4: b= ellipsisParameterDecl
		{
		pushFollow(FOLLOW_ellipsisParameterDecl_in_synpred91_Java1888);
		b=ellipsisParameterDecl();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred91_Java

	// $ANTLR start synpred93_Java
	public final void synpred93_Java_fragment() throws RecognitionException {
		List<Object> list_a=null;
		RuleReturnScope a = null;

		// Java.g:437:4: (a+= normalParameterDecl ( ',' a+= normalParameterDecl )* )
		// Java.g:437:4: a+= normalParameterDecl ( ',' a+= normalParameterDecl )*
		{
		pushFollow(FOLLOW_normalParameterDecl_in_synpred93_Java1906);
		a=normalParameterDecl();
		state._fsp--;
		if (state.failed) return;

		// Java.g:438:3: ( ',' a+= normalParameterDecl )*
		loop169:
		while (true) {
			int alt169=2;
			int LA169_0 = input.LA(1);
			if ( (LA169_0==COMMA) ) {
				alt169=1;
			}

			switch (alt169) {
			case 1 :
				// Java.g:438:4: ',' a+= normalParameterDecl
				{
				match(input,COMMA,FOLLOW_COMMA_in_synpred93_Java1911); if (state.failed) return;

				pushFollow(FOLLOW_normalParameterDecl_in_synpred93_Java1915);
				a=normalParameterDecl();
				state._fsp--;
				if (state.failed) return;

				}
				break;

			default :
				break loop169;
			}
		}

		}

	}
	// $ANTLR end synpred93_Java

	// $ANTLR start synpred94_Java
	public final void synpred94_Java_fragment() throws RecognitionException {
		List<Object> list_a=null;
		RuleReturnScope a = null;

		// Java.g:440:5: (a+= normalParameterDecl ',' )
		// Java.g:440:5: a+= normalParameterDecl ','
		{
		pushFollow(FOLLOW_normalParameterDecl_in_synpred94_Java1937);
		a=normalParameterDecl();
		state._fsp--;
		if (state.failed) return;

		match(input,COMMA,FOLLOW_COMMA_in_synpred94_Java1941); if (state.failed) return;

		}

	}
	// $ANTLR end synpred94_Java

	// $ANTLR start synpred98_Java
	public final void synpred98_Java_fragment() throws RecognitionException {
		// Java.g:466:4: ( ( nonWildcardTypeArguments )? ( 'this' | 'super' ) arguments ';' )
		// Java.g:466:4: ( nonWildcardTypeArguments )? ( 'this' | 'super' ) arguments ';'
		{
		// Java.g:466:4: ( nonWildcardTypeArguments )?
		int alt170=2;
		int LA170_0 = input.LA(1);
		if ( (LA170_0==LT) ) {
			alt170=1;
		}
		switch (alt170) {
			case 1 :
				// Java.g:466:5: nonWildcardTypeArguments
				{
				pushFollow(FOLLOW_nonWildcardTypeArguments_in_synpred98_Java2064);
				nonWildcardTypeArguments();
				state._fsp--;
				if (state.failed) return;

				}
				break;

		}

		if ( input.LA(1)==SUPER||input.LA(1)==THIS ) {
			input.consume();
			state.errorRecovery=false;
			state.failed=false;
		}
		else {
			if (state.backtracking>0) {state.failed=true; return;}
			MismatchedSetException mse = new MismatchedSetException(null,input);
			throw mse;
		}
		pushFollow(FOLLOW_arguments_in_synpred98_Java2088);
		arguments();
		state._fsp--;
		if (state.failed) return;

		match(input,SEMI,FOLLOW_SEMI_in_synpred98_Java2090); if (state.failed) return;

		}

	}
	// $ANTLR end synpred98_Java

	// $ANTLR start synpred114_Java
	public final void synpred114_Java_fragment() throws RecognitionException {
		// Java.g:565:4: ( annotationMethodDeclaration )
		// Java.g:565:4: annotationMethodDeclaration
		{
		pushFollow(FOLLOW_annotationMethodDeclaration_in_synpred114_Java2484);
		annotationMethodDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred114_Java

	// $ANTLR start synpred115_Java
	public final void synpred115_Java_fragment() throws RecognitionException {
		// Java.g:566:4: ( interfaceFieldDeclaration )
		// Java.g:566:4: interfaceFieldDeclaration
		{
		pushFollow(FOLLOW_interfaceFieldDeclaration_in_synpred115_Java2489);
		interfaceFieldDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred115_Java

	// $ANTLR start synpred116_Java
	public final void synpred116_Java_fragment() throws RecognitionException {
		// Java.g:567:4: ( normalClassDeclaration )
		// Java.g:567:4: normalClassDeclaration
		{
		pushFollow(FOLLOW_normalClassDeclaration_in_synpred116_Java2494);
		normalClassDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred116_Java

	// $ANTLR start synpred117_Java
	public final void synpred117_Java_fragment() throws RecognitionException {
		// Java.g:568:4: ( normalInterfaceDeclaration )
		// Java.g:568:4: normalInterfaceDeclaration
		{
		pushFollow(FOLLOW_normalInterfaceDeclaration_in_synpred117_Java2499);
		normalInterfaceDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred117_Java

	// $ANTLR start synpred118_Java
	public final void synpred118_Java_fragment() throws RecognitionException {
		// Java.g:569:4: ( enumDeclaration )
		// Java.g:569:4: enumDeclaration
		{
		pushFollow(FOLLOW_enumDeclaration_in_synpred118_Java2504);
		enumDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred118_Java

	// $ANTLR start synpred119_Java
	public final void synpred119_Java_fragment() throws RecognitionException {
		// Java.g:570:4: ( annotationTypeDeclaration )
		// Java.g:570:4: annotationTypeDeclaration
		{
		pushFollow(FOLLOW_annotationTypeDeclaration_in_synpred119_Java2509);
		annotationTypeDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred119_Java

	// $ANTLR start synpred122_Java
	public final void synpred122_Java_fragment() throws RecognitionException {
		// Java.g:621:4: ( localVariableDeclarationStatement )
		// Java.g:621:4: localVariableDeclarationStatement
		{
		pushFollow(FOLLOW_localVariableDeclarationStatement_in_synpred122_Java2625);
		localVariableDeclarationStatement();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred122_Java

	// $ANTLR start synpred123_Java
	public final void synpred123_Java_fragment() throws RecognitionException {
		// Java.g:622:4: ( classOrInterfaceDeclaration )
		// Java.g:622:4: classOrInterfaceDeclaration
		{
		pushFollow(FOLLOW_classOrInterfaceDeclaration_in_synpred123_Java2630);
		classOrInterfaceDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred123_Java

	// $ANTLR start synpred128_Java
	public final void synpred128_Java_fragment() throws RecognitionException {
		ParserRuleReturnScope h =null;


		// Java.g:644:42: ( 'else' h= statement )
		// Java.g:644:42: 'else' h= statement
		{
		match(input,ELSE,FOLLOW_ELSE_in_synpred128_Java2769); if (state.failed) return;

		pushFollow(FOLLOW_statement_in_synpred128_Java2773);
		h=statement();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred128_Java

	// $ANTLR start synpred149_Java
	public final void synpred149_Java_fragment() throws RecognitionException {
		// Java.g:684:5: ( catches 'finally' block )
		// Java.g:684:5: catches 'finally' block
		{
		pushFollow(FOLLOW_catches_in_synpred149_Java3193);
		catches();
		state._fsp--;
		if (state.failed) return;

		match(input,FINALLY,FOLLOW_FINALLY_in_synpred149_Java3195); if (state.failed) return;

		pushFollow(FOLLOW_block_in_synpred149_Java3197);
		block();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred149_Java

	// $ANTLR start synpred150_Java
	public final void synpred150_Java_fragment() throws RecognitionException {
		// Java.g:685:5: ( catches )
		// Java.g:685:5: catches
		{
		pushFollow(FOLLOW_catches_in_synpred150_Java3203);
		catches();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred150_Java

	// $ANTLR start synpred153_Java
	public final void synpred153_Java_fragment() throws RecognitionException {
		Token a=null;


		// Java.g:713:4: (a= 'for' '(' variableModifiers type IDENTIFIER ':' expression ')' statement )
		// Java.g:713:4: a= 'for' '(' variableModifiers type IDENTIFIER ':' expression ')' statement
		{
		a=(Token)match(input,FOR,FOLLOW_FOR_in_synpred153_Java3301); if (state.failed) return;

		match(input,LPAREN,FOLLOW_LPAREN_in_synpred153_Java3303); if (state.failed) return;

		pushFollow(FOLLOW_variableModifiers_in_synpred153_Java3309);
		variableModifiers();
		state._fsp--;
		if (state.failed) return;

		pushFollow(FOLLOW_type_in_synpred153_Java3314);
		type();
		state._fsp--;
		if (state.failed) return;

		match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred153_Java3319); if (state.failed) return;

		match(input,COLON,FOLLOW_COLON_in_synpred153_Java3324); if (state.failed) return;

		pushFollow(FOLLOW_expression_in_synpred153_Java3329);
		expression();
		state._fsp--;
		if (state.failed) return;

		match(input,RPAREN,FOLLOW_RPAREN_in_synpred153_Java3333); if (state.failed) return;

		pushFollow(FOLLOW_statement_in_synpred153_Java3337);
		statement();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred153_Java

	// $ANTLR start synpred157_Java
	public final void synpred157_Java_fragment() throws RecognitionException {
		// Java.g:735:4: ( localVariableDeclaration )
		// Java.g:735:4: localVariableDeclaration
		{
		pushFollow(FOLLOW_localVariableDeclaration_in_synpred157_Java3412);
		localVariableDeclaration();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred157_Java

	// $ANTLR start synpred159_Java
	public final void synpred159_Java_fragment() throws RecognitionException {
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope c =null;


		// Java.g:763:4: (a= conditionalExpression b= assignmentOperator c= subExpression )
		// Java.g:763:4: a= conditionalExpression b= assignmentOperator c= subExpression
		{
		pushFollow(FOLLOW_conditionalExpression_in_synpred159_Java3481);
		a=conditionalExpression();
		state._fsp--;
		if (state.failed) return;

		pushFollow(FOLLOW_assignmentOperator_in_synpred159_Java3485);
		b=assignmentOperator();
		state._fsp--;
		if (state.failed) return;

		pushFollow(FOLLOW_subExpression_in_synpred159_Java3489);
		c=subExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred159_Java

	// $ANTLR start synpred160_Java
	public final void synpred160_Java_fragment() throws RecognitionException {
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope c =null;


		// Java.g:770:4: (a= conditionalExpression b= assignmentOperator c= subExpression )
		// Java.g:770:4: a= conditionalExpression b= assignmentOperator c= subExpression
		{
		pushFollow(FOLLOW_conditionalExpression_in_synpred160_Java3537);
		a=conditionalExpression();
		state._fsp--;
		if (state.failed) return;

		pushFollow(FOLLOW_assignmentOperator_in_synpred160_Java3541);
		b=assignmentOperator();
		state._fsp--;
		if (state.failed) return;

		pushFollow(FOLLOW_subExpression_in_synpred160_Java3545);
		c=subExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred160_Java

	// $ANTLR start synpred199_Java
	public final void synpred199_Java_fragment() throws RecognitionException {
		// Java.g:909:4: ( castExpression )
		// Java.g:909:4: castExpression
		{
		pushFollow(FOLLOW_castExpression_in_synpred199_Java4183);
		castExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred199_Java

	// $ANTLR start synpred203_Java
	public final void synpred203_Java_fragment() throws RecognitionException {
		// Java.g:917:4: ( '(' primitiveType ')' unaryExpression )
		// Java.g:917:4: '(' primitiveType ')' unaryExpression
		{
		match(input,LPAREN,FOLLOW_LPAREN_in_synpred203_Java4218); if (state.failed) return;

		pushFollow(FOLLOW_primitiveType_in_synpred203_Java4221);
		primitiveType();
		state._fsp--;
		if (state.failed) return;

		match(input,RPAREN,FOLLOW_RPAREN_in_synpred203_Java4223); if (state.failed) return;

		pushFollow(FOLLOW_unaryExpression_in_synpred203_Java4228);
		unaryExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred203_Java

	// $ANTLR start synpred205_Java
	public final void synpred205_Java_fragment() throws RecognitionException {
		// Java.g:929:4: ( call )
		// Java.g:929:4: call
		{
		pushFollow(FOLLOW_call_in_synpred205_Java4262);
		call();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred205_Java

	// $ANTLR start synpred206_Java
	public final void synpred206_Java_fragment() throws RecognitionException {
		// Java.g:930:4: ( thisIdentifierSequence )
		// Java.g:930:4: thisIdentifierSequence
		{
		pushFollow(FOLLOW_thisIdentifierSequence_in_synpred206_Java4267);
		thisIdentifierSequence();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred206_Java

	// $ANTLR start synpred207_Java
	public final void synpred207_Java_fragment() throws RecognitionException {
		// Java.g:932:4: ( identifierSequence )
		// Java.g:932:4: identifierSequence
		{
		pushFollow(FOLLOW_identifierSequence_in_synpred207_Java4273);
		identifierSequence();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred207_Java

	// $ANTLR start synpred214_Java
	public final void synpred214_Java_fragment() throws RecognitionException {
		Token a=null;
		Token c=null;
		Token b=null;
		ParserRuleReturnScope d =null;


		// Java.g:956:4: (a= 'this' b= (c= '.' d= identifierSequence ) )
		// Java.g:956:4: a= 'this' b= (c= '.' d= identifierSequence )
		{
		a=(Token)match(input,THIS,FOLLOW_THIS_in_synpred214_Java4399); if (state.failed) return;

		// Java.g:957:5: (c= '.' d= identifierSequence )
		// Java.g:957:6: c= '.' d= identifierSequence
		{
		c=(Token)match(input,DOT,FOLLOW_DOT_in_synpred214_Java4408); if (state.failed) return;

		pushFollow(FOLLOW_identifierSequence_in_synpred214_Java4412);
		d=identifierSequence();
		state._fsp--;
		if (state.failed) return;

		}

		}

	}
	// $ANTLR end synpred214_Java

	// $ANTLR start synpred215_Java
	public final void synpred215_Java_fragment() throws RecognitionException {
		Token a=null;
		Token c=null;
		Token b=null;
		ParserRuleReturnScope d =null;


		// Java.g:965:4: (a= IDENTIFIER b= (c= '.' d= identifierSequence ) )
		// Java.g:965:4: a= IDENTIFIER b= (c= '.' d= identifierSequence )
		{
		a=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_synpred215_Java4456); if (state.failed) return;

		// Java.g:966:5: (c= '.' d= identifierSequence )
		// Java.g:966:6: c= '.' d= identifierSequence
		{
		c=(Token)match(input,DOT,FOLLOW_DOT_in_synpred215_Java4465); if (state.failed) return;

		pushFollow(FOLLOW_identifierSequence_in_synpred215_Java4469);
		d=identifierSequence();
		state._fsp--;
		if (state.failed) return;

		}

		}

	}
	// $ANTLR end synpred215_Java

	// $ANTLR start synpred221_Java
	public final void synpred221_Java_fragment() throws RecognitionException {
		// Java.g:985:5: ( '[' expression ']' )
		// Java.g:985:5: '[' expression ']'
		{
		match(input,LBRACKET,FOLLOW_LBRACKET_in_synpred221_Java4565); if (state.failed) return;

		pushFollow(FOLLOW_expression_in_synpred221_Java4568);
		expression();
		state._fsp--;
		if (state.failed) return;

		match(input,RBRACKET,FOLLOW_RBRACKET_in_synpred221_Java4570); if (state.failed) return;

		}

	}
	// $ANTLR end synpred221_Java

	// $ANTLR start synpred233_Java
	public final void synpred233_Java_fragment() throws RecognitionException {
		// Java.g:1008:4: ( 'new' nonWildcardTypeArguments classOrInterfaceType classCreatorRest )
		// Java.g:1008:4: 'new' nonWildcardTypeArguments classOrInterfaceType classCreatorRest
		{
		match(input,NEW,FOLLOW_NEW_in_synpred233_Java4704); if (state.failed) return;

		pushFollow(FOLLOW_nonWildcardTypeArguments_in_synpred233_Java4708);
		nonWildcardTypeArguments();
		state._fsp--;
		if (state.failed) return;

		pushFollow(FOLLOW_classOrInterfaceType_in_synpred233_Java4712);
		classOrInterfaceType();
		state._fsp--;
		if (state.failed) return;

		pushFollow(FOLLOW_classCreatorRest_in_synpred233_Java4716);
		classCreatorRest();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred233_Java

	// $ANTLR start synpred234_Java
	public final void synpred234_Java_fragment() throws RecognitionException {
		// Java.g:1012:4: ( 'new' classOrInterfaceType classCreatorRest )
		// Java.g:1012:4: 'new' classOrInterfaceType classCreatorRest
		{
		match(input,NEW,FOLLOW_NEW_in_synpred234_Java4721); if (state.failed) return;

		pushFollow(FOLLOW_classOrInterfaceType_in_synpred234_Java4725);
		classOrInterfaceType();
		state._fsp--;
		if (state.failed) return;

		pushFollow(FOLLOW_classCreatorRest_in_synpred234_Java4729);
		classCreatorRest();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred234_Java

	// $ANTLR start synpred236_Java
	public final void synpred236_Java_fragment() throws RecognitionException {
		// Java.g:1020:4: ( 'new' createdName '[' ']' ( '[' ']' )* arrayInitializer )
		// Java.g:1020:4: 'new' createdName '[' ']' ( '[' ']' )* arrayInitializer
		{
		match(input,NEW,FOLLOW_NEW_in_synpred236_Java4746); if (state.failed) return;

		pushFollow(FOLLOW_createdName_in_synpred236_Java4750);
		createdName();
		state._fsp--;
		if (state.failed) return;

		match(input,LBRACKET,FOLLOW_LBRACKET_in_synpred236_Java4754); if (state.failed) return;

		match(input,RBRACKET,FOLLOW_RBRACKET_in_synpred236_Java4756); if (state.failed) return;

		// Java.g:1023:3: ( '[' ']' )*
		loop183:
		while (true) {
			int alt183=2;
			int LA183_0 = input.LA(1);
			if ( (LA183_0==LBRACKET) ) {
				alt183=1;
			}

			switch (alt183) {
			case 1 :
				// Java.g:1023:4: '[' ']'
				{
				match(input,LBRACKET,FOLLOW_LBRACKET_in_synpred236_Java4761); if (state.failed) return;

				match(input,RBRACKET,FOLLOW_RBRACKET_in_synpred236_Java4763); if (state.failed) return;

				}
				break;

			default :
				break loop183;
			}
		}

		pushFollow(FOLLOW_arrayInitializer_in_synpred236_Java4769);
		arrayInitializer();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred236_Java

	// $ANTLR start synpred237_Java
	public final void synpred237_Java_fragment() throws RecognitionException {
		// Java.g:1028:4: ( '[' expression ']' )
		// Java.g:1028:4: '[' expression ']'
		{
		match(input,LBRACKET,FOLLOW_LBRACKET_in_synpred237_Java4791); if (state.failed) return;

		pushFollow(FOLLOW_expression_in_synpred237_Java4793);
		expression();
		state._fsp--;
		if (state.failed) return;

		match(input,RBRACKET,FOLLOW_RBRACKET_in_synpred237_Java4795); if (state.failed) return;

		}

	}
	// $ANTLR end synpred237_Java

	// Delegated rules

	public final boolean synpred98_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred98_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred157_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred157_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred207_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred207_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred63_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred63_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred221_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred221_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred214_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred214_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred114_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred114_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred116_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred116_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred1_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred1_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred49_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred49_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred119_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred119_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred215_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred215_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred54_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred54_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred205_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred205_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred117_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred117_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred94_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred94_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred65_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred65_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred91_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred91_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred38_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred38_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred48_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred48_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred199_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred199_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred66_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred66_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred52_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred52_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred236_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred236_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred123_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred123_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred233_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred233_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred149_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred149_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred64_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred64_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred47_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred47_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred122_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred122_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred150_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred150_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred206_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred206_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred93_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred93_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred159_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred159_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred128_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred128_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred153_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred153_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred203_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred203_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred234_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred234_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred160_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred160_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred22_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred22_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred7_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred7_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred237_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred237_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred115_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred115_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred118_Java() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred118_Java_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}



	public static final BitSet FOLLOW_packageDeclaration_in_compilationUnit196 = new BitSet(new long[]{0x0020100008000012L,0x40C041C006000120L,0x0000000000000420L});
	public static final BitSet FOLLOW_importDeclaration_in_compilationUnit201 = new BitSet(new long[]{0x0020100008000012L,0x40C041C006000120L,0x0000000000000420L});
	public static final BitSet FOLLOW_typeDeclaration_in_compilationUnit206 = new BitSet(new long[]{0x0020100008000012L,0x40C041C006000100L,0x0000000000000420L});
	public static final BitSet FOLLOW_annotations_in_packageDeclaration231 = new BitSet(new long[]{0x0000000000000000L,0x0000000040000000L});
	public static final BitSet FOLLOW_PACKAGE_in_packageDeclaration238 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_qualifiedName_in_packageDeclaration244 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_packageDeclaration248 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IMPORT_in_importDeclaration273 = new BitSet(new long[]{0x0000000000000000L,0x0048000000000004L});
	public static final BitSet FOLLOW_STATIC_in_importDeclaration279 = new BitSet(new long[]{0x0000000000000000L,0x0008000000000004L});
	public static final BitSet FOLLOW_qualifiedImportName_in_importDeclaration286 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_importDeclaration290 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classOrInterfaceDeclaration_in_typeDeclaration316 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SEMI_in_typeDeclaration321 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classDeclaration_in_classOrInterfaceDeclaration333 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_interfaceDeclaration_in_classOrInterfaceDeclaration338 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_annotation_in_modifiers353 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_PUBLIC_in_modifiers360 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_PROTECTED_in_modifiers367 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_PRIVATE_in_modifiers374 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_STATIC_in_modifiers381 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_ABSTRACT_in_modifiers388 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_FINAL_in_modifiers395 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_NATIVE_in_modifiers402 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_SYNCHRONIZED_in_modifiers409 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_TRANSIENT_in_modifiers416 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_VOLATILE_in_modifiers423 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_STRICTFP_in_modifiers430 = new BitSet(new long[]{0x0020000000000012L,0x40C001C006000000L,0x0000000000000420L});
	public static final BitSet FOLLOW_FINAL_in_variableModifiers459 = new BitSet(new long[]{0x0020000000000002L,0x0000000002000000L});
	public static final BitSet FOLLOW_annotation_in_variableModifiers461 = new BitSet(new long[]{0x0020000000000002L,0x0000000002000000L});
	public static final BitSet FOLLOW_normalClassDeclaration_in_classDeclaration475 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_enumDeclaration_in_classDeclaration480 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_normalClassDeclaration494 = new BitSet(new long[]{0x0000000008000000L});
	public static final BitSet FOLLOW_CLASS_in_normalClassDeclaration500 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_normalClassDeclaration506 = new BitSet(new long[]{0x0001000000000000L,0x0000000000082010L});
	public static final BitSet FOLLOW_typeParameters_in_normalClassDeclaration512 = new BitSet(new long[]{0x0001000000000000L,0x0000000000002010L});
	public static final BitSet FOLLOW_EXTENDS_in_normalClassDeclaration520 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_normalClassDeclaration524 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002010L});
	public static final BitSet FOLLOW_IMPLEMENTS_in_normalClassDeclaration533 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_typeList_in_normalClassDeclaration537 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_classBody_in_normalClassDeclaration545 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LT_in_typeParameters592 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_typeParameter_in_typeParameters596 = new BitSet(new long[]{0x1000000020000000L});
	public static final BitSet FOLLOW_COMMA_in_typeParameters601 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_typeParameter_in_typeParameters603 = new BitSet(new long[]{0x1000000020000000L});
	public static final BitSet FOLLOW_GT_in_typeParameters609 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_typeParameter634 = new BitSet(new long[]{0x0001000000000002L});
	public static final BitSet FOLLOW_EXTENDS_in_typeParameter641 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_typeBound_in_typeParameter645 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_type_in_typeBound678 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_AMP_in_typeBound681 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_typeBound683 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_modifiers_in_enumDeclaration705 = new BitSet(new long[]{0x0000100000000000L});
	public static final BitSet FOLLOW_ENUM_in_enumDeclaration711 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_enumDeclaration717 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002010L});
	public static final BitSet FOLLOW_IMPLEMENTS_in_enumDeclaration724 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_typeList_in_enumDeclaration728 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_enumBody_in_enumDeclaration736 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACE_in_enumBody764 = new BitSet(new long[]{0x0000000020000000L,0x0000440002000004L});
	public static final BitSet FOLLOW_enumConstants_in_enumBody769 = new BitSet(new long[]{0x0000000020000000L,0x0000440000000000L});
	public static final BitSet FOLLOW_COMMA_in_enumBody775 = new BitSet(new long[]{0x0000000000000000L,0x0000440000000000L});
	public static final BitSet FOLLOW_enumBodyDeclarations_in_enumBody781 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
	public static final BitSet FOLLOW_RBRACE_in_enumBody787 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_enumConstant_in_enumConstants799 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_COMMA_in_enumConstants802 = new BitSet(new long[]{0x0000000000000000L,0x0000000002000004L});
	public static final BitSet FOLLOW_enumConstant_in_enumConstants804 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_annotations_in_enumConstant825 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_enumConstant830 = new BitSet(new long[]{0x0000000000000002L,0x0000000000042000L});
	public static final BitSet FOLLOW_arguments_in_enumConstant834 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
	public static final BitSet FOLLOW_classBody_in_enumConstant839 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SEMI_in_enumBodyDeclarations856 = new BitSet(new long[]{0x00A010800A0A0012L,0x40C141C006092184L,0x0000000000000620L});
	public static final BitSet FOLLOW_classBodyDeclaration_in_enumBodyDeclarations861 = new BitSet(new long[]{0x00A010800A0A0012L,0x40C141C006092184L,0x0000000000000620L});
	public static final BitSet FOLLOW_normalInterfaceDeclaration_in_interfaceDeclaration874 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_annotationTypeDeclaration_in_interfaceDeclaration879 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_normalInterfaceDeclaration893 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
	public static final BitSet FOLLOW_INTERFACE_in_normalInterfaceDeclaration899 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_normalInterfaceDeclaration905 = new BitSet(new long[]{0x0001000000000000L,0x0000000000082000L});
	public static final BitSet FOLLOW_typeParameters_in_normalInterfaceDeclaration912 = new BitSet(new long[]{0x0001000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_EXTENDS_in_normalInterfaceDeclaration921 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_typeList_in_normalInterfaceDeclaration925 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_interfaceBody_in_normalInterfaceDeclaration933 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_type_in_typeList970 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_COMMA_in_typeList973 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_typeList975 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_LBRACE_in_classBody997 = new BitSet(new long[]{0x00A010800A0A0010L,0x40C145C006092184L,0x0000000000000620L});
	public static final BitSet FOLLOW_classBodyDeclaration_in_classBody1003 = new BitSet(new long[]{0x00A010800A0A0010L,0x40C145C006092184L,0x0000000000000620L});
	public static final BitSet FOLLOW_RBRACE_in_classBody1008 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACE_in_interfaceBody1032 = new BitSet(new long[]{0x00A010800A0A0010L,0x40C145C006090184L,0x0000000000000620L});
	public static final BitSet FOLLOW_interfaceBodyDeclaration_in_interfaceBody1037 = new BitSet(new long[]{0x00A010800A0A0010L,0x40C145C006090184L,0x0000000000000620L});
	public static final BitSet FOLLOW_RBRACE_in_interfaceBody1043 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SEMI_in_classBodyDeclaration1061 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STATIC_in_classBodyDeclaration1070 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_block_in_classBodyDeclaration1076 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_memberDecl_in_classBodyDeclaration1086 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_fieldDeclaration_in_memberDecl1098 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_methodDeclaration_in_memberDecl1103 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classDeclaration_in_memberDecl1108 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_interfaceDeclaration_in_memberDecl1113 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_methodDeclaration1131 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080004L});
	public static final BitSet FOLLOW_typeParameters_in_methodDeclaration1137 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_methodDeclaration1144 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_formalParameters_in_methodDeclaration1150 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L,0x0000000000000008L});
	public static final BitSet FOLLOW_THROWS_in_methodDeclaration1155 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_qualifiedNameList_in_methodDeclaration1157 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_LBRACE_in_methodDeclaration1165 = new BitSet(new long[]{0x03A811A40E0E0C10L,0x7BC155E81E0F238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_explicitConstructorInvocation_in_methodDeclaration1171 = new BitSet(new long[]{0x03A811A40E0E0C10L,0x7BC155E81E07238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_blockStatement_in_methodDeclaration1178 = new BitSet(new long[]{0x03A811A40E0E0C10L,0x7BC155E81E07238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_RBRACE_in_methodDeclaration1183 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_methodDeclaration1219 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000090084L,0x0000000000000200L});
	public static final BitSet FOLLOW_typeParameters_in_methodDeclaration1225 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L,0x0000000000000200L});
	public static final BitSet FOLLOW_type_in_methodDeclaration1233 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_VOID_in_methodDeclaration1237 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_methodDeclaration1244 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_formalParameters_in_methodDeclaration1250 = new BitSet(new long[]{0x0000000000000000L,0x0000400000006000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LBRACKET_in_methodDeclaration1255 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_methodDeclaration1257 = new BitSet(new long[]{0x0000000000000000L,0x0000400000006000L,0x0000000000000008L});
	public static final BitSet FOLLOW_THROWS_in_methodDeclaration1264 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_qualifiedNameList_in_methodDeclaration1266 = new BitSet(new long[]{0x0000000000000000L,0x0000400000002000L});
	public static final BitSet FOLLOW_block_in_methodDeclaration1275 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SEMI_in_methodDeclaration1279 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_fieldDeclaration1318 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_fieldDeclaration1324 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_variableDeclarator_in_fieldDeclaration1330 = new BitSet(new long[]{0x0000000020000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_COMMA_in_fieldDeclaration1335 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_variableDeclarator_in_fieldDeclaration1339 = new BitSet(new long[]{0x0000000020000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_fieldDeclaration1345 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_variableDeclarator1372 = new BitSet(new long[]{0x0000200000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_variableDeclarator1377 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_variableDeclarator1379 = new BitSet(new long[]{0x0000200000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_EQ_in_variableDeclarator1388 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818072284L,0x0000000000000252L});
	public static final BitSet FOLLOW_variableInitializer_in_variableDeclarator1392 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_interfaceFieldDeclaration_in_interfaceBodyDeclaration1439 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_interfaceMethodDeclaration_in_interfaceBodyDeclaration1444 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_interfaceDeclaration_in_interfaceBodyDeclaration1449 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classDeclaration_in_interfaceBodyDeclaration1454 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SEMI_in_interfaceBodyDeclaration1459 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_interfaceMethodDeclaration1473 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000090084L,0x0000000000000200L});
	public static final BitSet FOLLOW_typeParameters_in_interfaceMethodDeclaration1479 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L,0x0000000000000200L});
	public static final BitSet FOLLOW_type_in_interfaceMethodDeclaration1489 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_VOID_in_interfaceMethodDeclaration1495 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_interfaceMethodDeclaration1502 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_formalParameters_in_interfaceMethodDeclaration1508 = new BitSet(new long[]{0x0000000000000000L,0x0000400000004000L,0x0000000000000008L});
	public static final BitSet FOLLOW_LBRACKET_in_interfaceMethodDeclaration1513 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_interfaceMethodDeclaration1515 = new BitSet(new long[]{0x0000000000000000L,0x0000400000004000L,0x0000000000000008L});
	public static final BitSet FOLLOW_THROWS_in_interfaceMethodDeclaration1522 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_qualifiedNameList_in_interfaceMethodDeclaration1526 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_interfaceMethodDeclaration1532 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_interfaceFieldDeclaration1589 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_interfaceFieldDeclaration1595 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_variableDeclarator_in_interfaceFieldDeclaration1601 = new BitSet(new long[]{0x0000000020000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_COMMA_in_interfaceFieldDeclaration1606 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_variableDeclarator_in_interfaceFieldDeclaration1610 = new BitSet(new long[]{0x0000000020000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_interfaceFieldDeclaration1616 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_type1643 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_type1646 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_type1648 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_primitiveType_in_type1655 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_type1658 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_type1660 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_IDENTIFIER_in_classOrInterfaceType1674 = new BitSet(new long[]{0x0000004000000002L,0x0000000000080000L});
	public static final BitSet FOLLOW_typeArguments_in_classOrInterfaceType1678 = new BitSet(new long[]{0x0000004000000002L});
	public static final BitSet FOLLOW_DOT_in_classOrInterfaceType1684 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_classOrInterfaceType1686 = new BitSet(new long[]{0x0000004000000002L,0x0000000000080000L});
	public static final BitSet FOLLOW_typeArguments_in_classOrInterfaceType1691 = new BitSet(new long[]{0x0000004000000002L});
	public static final BitSet FOLLOW_LT_in_typeArguments1756 = new BitSet(new long[]{0x00800080020A0000L,0x0001020000010084L});
	public static final BitSet FOLLOW_typeArgument_in_typeArguments1758 = new BitSet(new long[]{0x1000000020000000L});
	public static final BitSet FOLLOW_COMMA_in_typeArguments1763 = new BitSet(new long[]{0x00800080020A0000L,0x0001020000010084L});
	public static final BitSet FOLLOW_typeArgument_in_typeArguments1765 = new BitSet(new long[]{0x1000000020000000L});
	public static final BitSet FOLLOW_GT_in_typeArguments1771 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_type_in_typeArgument1789 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_QUES_in_typeArgument1794 = new BitSet(new long[]{0x0001000000000002L,0x1000000000000000L});
	public static final BitSet FOLLOW_set_in_typeArgument1803 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_typeArgument1814 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_qualifiedName_in_qualifiedNameList1831 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_COMMA_in_qualifiedNameList1836 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_qualifiedName_in_qualifiedNameList1838 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_LPAREN_in_formalParameters1858 = new BitSet(new long[]{0x00A00080020A0000L,0x0001200002010084L});
	public static final BitSet FOLLOW_formalParameterDecls_in_formalParameters1862 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_formalParameters1867 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ellipsisParameterDecl_in_formalParameterDecls1888 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_normalParameterDecl_in_formalParameterDecls1906 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_COMMA_in_formalParameterDecls1911 = new BitSet(new long[]{0x00A00080020A0000L,0x0001000002010084L});
	public static final BitSet FOLLOW_normalParameterDecl_in_formalParameterDecls1915 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_normalParameterDecl_in_formalParameterDecls1937 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_COMMA_in_formalParameterDecls1941 = new BitSet(new long[]{0x00A00080020A0000L,0x0001000002010084L});
	public static final BitSet FOLLOW_ellipsisParameterDecl_in_formalParameterDecls1949 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variableModifiers_in_normalParameterDecl1976 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_normalParameterDecl1982 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_normalParameterDecl1988 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_normalParameterDecl1993 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_normalParameterDecl1995 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_variableModifiers_in_ellipsisParameterDecl2021 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_ellipsisParameterDecl2027 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_ELLIPSIS_in_ellipsisParameterDecl2031 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_ellipsisParameterDecl2037 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_nonWildcardTypeArguments_in_explicitConstructorInvocation2064 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_set_in_explicitConstructorInvocation2078 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_arguments_in_explicitConstructorInvocation2088 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_explicitConstructorInvocation2090 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primary_in_explicitConstructorInvocation2096 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_DOT_in_explicitConstructorInvocation2100 = new BitSet(new long[]{0x0000000000000000L,0x1000000000080000L});
	public static final BitSet FOLLOW_nonWildcardTypeArguments_in_explicitConstructorInvocation2105 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000000L});
	public static final BitSet FOLLOW_SUPER_in_explicitConstructorInvocation2111 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_arguments_in_explicitConstructorInvocation2115 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_explicitConstructorInvocation2117 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_qualifiedName2132 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_DOT_in_qualifiedName2136 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_qualifiedName_in_qualifiedName2140 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_qualifiedName2161 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_qualifiedImportName2181 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_DOT_in_qualifiedImportName2185 = new BitSet(new long[]{0x0000000000000000L,0x0008000000000004L});
	public static final BitSet FOLLOW_qualifiedImportName_in_qualifiedImportName2189 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_qualifiedImportName2210 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STAR_in_qualifiedImportName2221 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_annotation_in_annotations2234 = new BitSet(new long[]{0x0000000000000002L,0x0000000002000000L});
	public static final BitSet FOLLOW_MONKEYS_AT_in_annotation2260 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_qualifiedName_in_annotation2262 = new BitSet(new long[]{0x0000000000000002L,0x0000000000040000L});
	public static final BitSet FOLLOW_LPAREN_in_annotation2267 = new BitSet(new long[]{0x01880180060A0800L,0x1B0120281A072284L,0x0000000000000252L});
	public static final BitSet FOLLOW_elementValuePairs_in_annotation2274 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_elementValue_in_annotation2281 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_annotation2288 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_elementValuePair_in_elementValuePairs2311 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_COMMA_in_elementValuePairs2316 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_elementValuePair_in_elementValuePairs2318 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_elementValuePair2338 = new BitSet(new long[]{0x0000200000000000L});
	public static final BitSet FOLLOW_EQ_in_elementValuePair2340 = new BitSet(new long[]{0x01880180060A0800L,0x1B0100281A072284L,0x0000000000000252L});
	public static final BitSet FOLLOW_elementValue_in_elementValuePair2343 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalExpression_in_elementValue2355 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_annotation_in_elementValue2360 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_elementValueArrayInitializer_in_elementValue2365 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACE_in_elementValueArrayInitializer2377 = new BitSet(new long[]{0x01880180260A0800L,0x1B0104281A072284L,0x0000000000000252L});
	public static final BitSet FOLLOW_elementValue_in_elementValueArrayInitializer2382 = new BitSet(new long[]{0x0000000020000000L,0x0000040000000000L});
	public static final BitSet FOLLOW_COMMA_in_elementValueArrayInitializer2388 = new BitSet(new long[]{0x01880180060A0800L,0x1B0100281A072284L,0x0000000000000252L});
	public static final BitSet FOLLOW_elementValue_in_elementValueArrayInitializer2390 = new BitSet(new long[]{0x0000000020000000L,0x0000040000000000L});
	public static final BitSet FOLLOW_COMMA_in_elementValueArrayInitializer2400 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
	public static final BitSet FOLLOW_RBRACE_in_elementValueArrayInitializer2404 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_annotationTypeDeclaration2418 = new BitSet(new long[]{0x0000000000000000L,0x0000000002000000L});
	public static final BitSet FOLLOW_MONKEYS_AT_in_annotationTypeDeclaration2421 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
	public static final BitSet FOLLOW_INTERFACE_in_annotationTypeDeclaration2425 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_annotationTypeDeclaration2429 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_annotationTypeBody_in_annotationTypeDeclaration2433 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACE_in_annotationTypeBody2447 = new BitSet(new long[]{0x00A010800A0A0010L,0x40C145C006010184L,0x0000000000000420L});
	public static final BitSet FOLLOW_annotationTypeElementDeclaration_in_annotationTypeBody2453 = new BitSet(new long[]{0x00A010800A0A0010L,0x40C145C006010184L,0x0000000000000420L});
	public static final BitSet FOLLOW_RBRACE_in_annotationTypeBody2458 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_annotationMethodDeclaration_in_annotationTypeElementDeclaration2484 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_interfaceFieldDeclaration_in_annotationTypeElementDeclaration2489 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_normalClassDeclaration_in_annotationTypeElementDeclaration2494 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_normalInterfaceDeclaration_in_annotationTypeElementDeclaration2499 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_enumDeclaration_in_annotationTypeElementDeclaration2504 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_annotationTypeDeclaration_in_annotationTypeElementDeclaration2509 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SEMI_in_annotationTypeElementDeclaration2514 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_annotationMethodDeclaration2527 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_annotationMethodDeclaration2531 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_annotationMethodDeclaration2535 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_LPAREN_in_annotationMethodDeclaration2537 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_annotationMethodDeclaration2539 = new BitSet(new long[]{0x0000001000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_DEFAULT_in_annotationMethodDeclaration2548 = new BitSet(new long[]{0x01880180060A0800L,0x1B0100281A072284L,0x0000000000000252L});
	public static final BitSet FOLLOW_elementValue_in_annotationMethodDeclaration2553 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_annotationMethodDeclaration2562 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACE_in_block2588 = new BitSet(new long[]{0x03A811A40E0E0C10L,0x7BC155E81E07238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_blockStatement_in_block2594 = new BitSet(new long[]{0x03A811A40E0E0C10L,0x7BC155E81E07238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_RBRACE_in_block2599 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_localVariableDeclarationStatement_in_blockStatement2625 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classOrInterfaceDeclaration_in_blockStatement2630 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statement_in_blockStatement2635 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_localVariableDeclaration_in_localVariableDeclarationStatement2647 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_localVariableDeclarationStatement2649 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variableModifiers_in_localVariableDeclaration2664 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_localVariableDeclaration2670 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_variableDeclarator_in_localVariableDeclaration2676 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_COMMA_in_localVariableDeclaration2681 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_variableDeclarator_in_localVariableDeclaration2685 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_block_in_statement2712 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ASSERT_in_statement2719 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_statement2723 = new BitSet(new long[]{0x0000000010000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_COLON_in_statement2726 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_statement2730 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_statement2734 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_statement2756 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_parExpression_in_statement2760 = new BitSet(new long[]{0x038801A4060E0C00L,0x7B0150281807228CL,0x0000000000000AD6L});
	public static final BitSet FOLLOW_statement_in_statement2764 = new BitSet(new long[]{0x0000080000000002L});
	public static final BitSet FOLLOW_ELSE_in_statement2769 = new BitSet(new long[]{0x038801A4060E0C00L,0x7B0150281807228CL,0x0000000000000AD6L});
	public static final BitSet FOLLOW_statement_in_statement2773 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_forstatement_in_statement2833 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHILE_in_statement2840 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_parExpression_in_statement2844 = new BitSet(new long[]{0x038801A4060E0C00L,0x7B0150281807228CL,0x0000000000000AD6L});
	public static final BitSet FOLLOW_statement_in_statement2848 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DO_in_statement2877 = new BitSet(new long[]{0x038801A4060E0C00L,0x7B0150281807228CL,0x0000000000000AD6L});
	public static final BitSet FOLLOW_statement_in_statement2881 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
	public static final BitSet FOLLOW_WHILE_in_statement2883 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_parExpression_in_statement2887 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_statement2889 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_trystatement_in_statement2916 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SWITCH_in_statement2923 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_parExpression_in_statement2927 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_LBRACE_in_statement2929 = new BitSet(new long[]{0x0000001000800000L,0x0000040000000000L});
	public static final BitSet FOLLOW_switchBlockStatementGroups_in_statement2933 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
	public static final BitSet FOLLOW_RBRACE_in_statement2935 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SYNCHRONIZED_in_statement2963 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_parExpression_in_statement2965 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_block_in_statement2967 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_RETURN_in_statement2974 = new BitSet(new long[]{0x01880180060A0800L,0x1B01402818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_statement2979 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_statement2983 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_THROW_in_statement3000 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_statement3002 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_statement3004 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BREAK_in_statement3012 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_statement3016 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_statement3019 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CONTINUE_in_statement3038 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_statement3042 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_statement3045 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_statement3062 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_statement3065 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_statement3071 = new BitSet(new long[]{0x0000000010000000L});
	public static final BitSet FOLLOW_COLON_in_statement3073 = new BitSet(new long[]{0x038801A4060E0C00L,0x7B0150281807228CL,0x0000000000000AD6L});
	public static final BitSet FOLLOW_statement_in_statement3076 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SEMI_in_statement3081 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_switchBlockStatementGroup_in_switchBlockStatementGroups3095 = new BitSet(new long[]{0x0000001000800002L});
	public static final BitSet FOLLOW_CASE_in_switchBlockStatementGroup3111 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_switchBlockStatementGroup3115 = new BitSet(new long[]{0x0000000010000000L});
	public static final BitSet FOLLOW_COLON_in_switchBlockStatementGroup3117 = new BitSet(new long[]{0x03A811A40E0E0C12L,0x7BC151E81E07238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_blockStatement_in_switchBlockStatementGroup3121 = new BitSet(new long[]{0x03A811A40E0E0C12L,0x7BC151E81E07238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_DEFAULT_in_switchBlockStatementGroup3151 = new BitSet(new long[]{0x0000000010000000L});
	public static final BitSet FOLLOW_COLON_in_switchBlockStatementGroup3153 = new BitSet(new long[]{0x03A811A40E0E0C12L,0x7BC151E81E07238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_blockStatement_in_switchBlockStatementGroup3157 = new BitSet(new long[]{0x03A811A40E0E0C12L,0x7BC151E81E07238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_TRY_in_trystatement3185 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_block_in_trystatement3187 = new BitSet(new long[]{0x0040000001000000L});
	public static final BitSet FOLLOW_catches_in_trystatement3193 = new BitSet(new long[]{0x0040000000000000L});
	public static final BitSet FOLLOW_FINALLY_in_trystatement3195 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_block_in_trystatement3197 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_catches_in_trystatement3203 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FINALLY_in_trystatement3209 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_block_in_trystatement3211 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_catchClause_in_catches3227 = new BitSet(new long[]{0x0000000001000002L});
	public static final BitSet FOLLOW_catchClause_in_catches3232 = new BitSet(new long[]{0x0000000001000002L});
	public static final BitSet FOLLOW_CATCH_in_catchClause3246 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_LPAREN_in_catchClause3250 = new BitSet(new long[]{0x00A00080020A0000L,0x0001000002010084L});
	public static final BitSet FOLLOW_formalParameter_in_catchClause3252 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_catchClause3254 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_block_in_catchClause3258 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variableModifiers_in_formalParameter3270 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_formalParameter3274 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_formalParameter3278 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_formalParameter3283 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_formalParameter3285 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_FOR_in_forstatement3301 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_LPAREN_in_forstatement3303 = new BitSet(new long[]{0x00A00080020A0000L,0x0001000002010084L});
	public static final BitSet FOLLOW_variableModifiers_in_forstatement3309 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_forstatement3314 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_forstatement3319 = new BitSet(new long[]{0x0000000010000000L});
	public static final BitSet FOLLOW_COLON_in_forstatement3324 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_forstatement3329 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_forstatement3333 = new BitSet(new long[]{0x038801A4060E0C00L,0x7B0150281807228CL,0x0000000000000AD6L});
	public static final BitSet FOLLOW_statement_in_forstatement3337 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FOR_in_forstatement3353 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_LPAREN_in_forstatement3355 = new BitSet(new long[]{0x01A80180060A0800L,0x1B0140281A070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_forInit_in_forstatement3360 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_forstatement3366 = new BitSet(new long[]{0x01880180060A0800L,0x1B01402818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_forstatement3371 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_forstatement3377 = new BitSet(new long[]{0x01880180060A0800L,0x1B01202818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expressionList_in_forstatement3382 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_forstatement3387 = new BitSet(new long[]{0x038801A4060E0C00L,0x7B0150281807228CL,0x0000000000000AD6L});
	public static final BitSet FOLLOW_statement_in_forstatement3391 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_localVariableDeclaration_in_forInit3412 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expressionList_in_forInit3417 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_parExpression3429 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_parExpression3431 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_parExpression3433 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_expressionList3450 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_COMMA_in_expressionList3455 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_expressionList3457 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_conditionalExpression_in_expression3481 = new BitSet(new long[]{0x1000200000408080L,0x0414001400080000L});
	public static final BitSet FOLLOW_assignmentOperator_in_expression3485 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_subExpression_in_expression3489 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalExpression_in_expression3514 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalExpression_in_subExpression3537 = new BitSet(new long[]{0x1000200000408080L,0x0414001400080000L});
	public static final BitSet FOLLOW_assignmentOperator_in_subExpression3541 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_subExpression_in_subExpression3545 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalExpression_in_subExpression3566 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EQ_in_assignmentOperator3584 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PLUSEQ_in_assignmentOperator3593 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SUBEQ_in_assignmentOperator3602 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STAREQ_in_assignmentOperator3611 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SLASHEQ_in_assignmentOperator3620 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_AMPEQ_in_assignmentOperator3629 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BAREQ_in_assignmentOperator3638 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CARETEQ_in_assignmentOperator3647 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PERCENTEQ_in_assignmentOperator3656 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LT_in_assignmentOperator3665 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
	public static final BitSet FOLLOW_LT_in_assignmentOperator3667 = new BitSet(new long[]{0x0000200000000000L});
	public static final BitSet FOLLOW_EQ_in_assignmentOperator3669 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GT_in_assignmentOperator3678 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_GT_in_assignmentOperator3680 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_GT_in_assignmentOperator3682 = new BitSet(new long[]{0x0000200000000000L});
	public static final BitSet FOLLOW_EQ_in_assignmentOperator3684 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GT_in_assignmentOperator3693 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_GT_in_assignmentOperator3695 = new BitSet(new long[]{0x0000200000000000L});
	public static final BitSet FOLLOW_EQ_in_assignmentOperator3697 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalOrExpression_in_conditionalExpression3713 = new BitSet(new long[]{0x0000000000000002L,0x0000020000000000L});
	public static final BitSet FOLLOW_QUES_in_conditionalExpression3718 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_conditionalExpression3722 = new BitSet(new long[]{0x0000000010000000L});
	public static final BitSet FOLLOW_COLON_in_conditionalExpression3726 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_conditionalExpression_in_conditionalExpression3730 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalAndExpression_in_conditionalOrExpression3747 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_BARBAR_in_conditionalOrExpression3752 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_conditionalAndExpression_in_conditionalOrExpression3755 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_inclusiveOrExpression_in_conditionalAndExpression3769 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_AMPAMP_in_conditionalAndExpression3774 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_inclusiveOrExpression_in_conditionalAndExpression3777 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression3791 = new BitSet(new long[]{0x0000000000002002L});
	public static final BitSet FOLLOW_BAR_in_inclusiveOrExpression3796 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression3799 = new BitSet(new long[]{0x0000000000002002L});
	public static final BitSet FOLLOW_andExpression_in_exclusiveOrExpression3813 = new BitSet(new long[]{0x0000000000200002L});
	public static final BitSet FOLLOW_CARET_in_exclusiveOrExpression3818 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_andExpression_in_exclusiveOrExpression3821 = new BitSet(new long[]{0x0000000000200002L});
	public static final BitSet FOLLOW_equalityExpression_in_andExpression3835 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_AMP_in_andExpression3840 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_equalityExpression_in_andExpression3843 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_instanceOfExpression_in_equalityExpression3857 = new BitSet(new long[]{0x0000400000001002L});
	public static final BitSet FOLLOW_set_in_equalityExpression3866 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_instanceOfExpression_in_equalityExpression3878 = new BitSet(new long[]{0x0000400000001002L});
	public static final BitSet FOLLOW_relationalExpression_in_instanceOfExpression3895 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000040L});
	public static final BitSet FOLLOW_INSTANCEOF_in_instanceOfExpression3900 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_instanceOfExpression3903 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_shiftExpression_in_relationalExpression3917 = new BitSet(new long[]{0x1000000000000002L,0x0000000000080000L});
	public static final BitSet FOLLOW_relationalOp_in_relationalExpression3922 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_shiftExpression_in_relationalExpression3925 = new BitSet(new long[]{0x1000000000000002L,0x0000000000080000L});
	public static final BitSet FOLLOW_LT_in_relationalOp3939 = new BitSet(new long[]{0x0000200000000000L});
	public static final BitSet FOLLOW_EQ_in_relationalOp3941 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GT_in_relationalOp3950 = new BitSet(new long[]{0x0000200000000000L});
	public static final BitSet FOLLOW_EQ_in_relationalOp3952 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LT_in_relationalOp3961 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GT_in_relationalOp3971 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_additiveExpression_in_shiftExpression3988 = new BitSet(new long[]{0x1000000000000002L,0x0000000000080000L});
	public static final BitSet FOLLOW_shiftOp_in_shiftExpression3993 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_additiveExpression_in_shiftExpression3996 = new BitSet(new long[]{0x1000000000000002L,0x0000000000080000L});
	public static final BitSet FOLLOW_LT_in_shiftOp4010 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
	public static final BitSet FOLLOW_LT_in_shiftOp4012 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GT_in_shiftOp4017 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_GT_in_shiftOp4019 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_GT_in_shiftOp4021 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GT_in_shiftOp4026 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_GT_in_shiftOp4028 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression4040 = new BitSet(new long[]{0x0000000000000002L,0x0200000800000000L});
	public static final BitSet FOLLOW_set_in_additiveExpression4049 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression4061 = new BitSet(new long[]{0x0000000000000002L,0x0200000800000000L});
	public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression4078 = new BitSet(new long[]{0x0000000000000002L,0x000A000200000000L});
	public static final BitSet FOLLOW_set_in_multiplicativeExpression4087 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression4103 = new BitSet(new long[]{0x0000000000000002L,0x000A000200000000L});
	public static final BitSet FOLLOW_PLUS_in_unaryExpression4122 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpression4126 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SUB_in_unaryExpression4131 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpression4134 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PLUSPLUS_in_unaryExpression4139 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpression4142 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SUBSUB_in_unaryExpression4147 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpression4150 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_unaryExpressionNotPlusMinus_in_unaryExpression4155 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TILDE_in_unaryExpressionNotPlusMinus4167 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus4170 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BANG_in_unaryExpressionNotPlusMinus4175 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus4178 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_castExpression_in_unaryExpressionNotPlusMinus4183 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primary_in_unaryExpressionNotPlusMinus4188 = new BitSet(new long[]{0x0000004000000002L,0x0800002000004000L});
	public static final BitSet FOLLOW_selector_in_unaryExpressionNotPlusMinus4193 = new BitSet(new long[]{0x0000004000000002L,0x0800002000004000L});
	public static final BitSet FOLLOW_LPAREN_in_castExpression4218 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010080L});
	public static final BitSet FOLLOW_primitiveType_in_castExpression4221 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_castExpression4223 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_unaryExpression_in_castExpression4228 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_castExpression4233 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_castExpression4236 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_castExpression4238 = new BitSet(new long[]{0x01880180060A0800L,0x1101000018070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_unaryExpressionNotPlusMinus_in_castExpression4243 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_parExpression_in_primary4257 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_call_in_primary4262 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_thisIdentifierSequence_in_primary4267 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_identifierSequence_in_primary4273 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SUPER_in_primary4279 = new BitSet(new long[]{0x0000004000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_superSuffix_in_primary4283 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_literal_in_primary4288 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_creator_in_primary4293 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primitiveType_in_primary4298 = new BitSet(new long[]{0x0000004000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_primary4303 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_primary4305 = new BitSet(new long[]{0x0000004000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_DOT_in_primary4311 = new BitSet(new long[]{0x0000000008000000L});
	public static final BitSet FOLLOW_CLASS_in_primary4314 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VOID_in_primary4319 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_DOT_in_primary4321 = new BitSet(new long[]{0x0000000008000000L});
	public static final BitSet FOLLOW_CLASS_in_primary4324 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_thisIdentifierSequence_in_call4338 = new BitSet(new long[]{0x0000004000000000L,0x0000000000044000L});
	public static final BitSet FOLLOW_identifierSuffix_in_call4344 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_identifierSequence_in_call4365 = new BitSet(new long[]{0x0000004000000000L,0x0000000000044000L});
	public static final BitSet FOLLOW_identifierSuffix_in_call4371 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_THIS_in_thisIdentifierSequence4399 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_DOT_in_thisIdentifierSequence4408 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_identifierSequence_in_thisIdentifierSequence4412 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_THIS_in_thisIdentifierSequence4435 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_identifierSequence4456 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_DOT_in_identifierSequence4465 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_identifierSequence_in_identifierSequence4469 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_identifierSequence4492 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arguments_in_superSuffix4511 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_superSuffix4516 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080004L});
	public static final BitSet FOLLOW_typeArguments_in_superSuffix4522 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_superSuffix4528 = new BitSet(new long[]{0x0000000000000002L,0x0000000000040000L});
	public static final BitSet FOLLOW_arguments_in_superSuffix4533 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACKET_in_identifierSuffix4548 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_identifierSuffix4550 = new BitSet(new long[]{0x0000004000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_DOT_in_identifierSuffix4556 = new BitSet(new long[]{0x0000000008000000L});
	public static final BitSet FOLLOW_CLASS_in_identifierSuffix4559 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACKET_in_identifierSuffix4565 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_identifierSuffix4568 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_identifierSuffix4570 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_arguments_in_identifierSuffix4578 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_identifierSuffix4583 = new BitSet(new long[]{0x0000000008000000L});
	public static final BitSet FOLLOW_CLASS_in_identifierSuffix4586 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_identifierSuffix4591 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
	public static final BitSet FOLLOW_nonWildcardTypeArguments_in_identifierSuffix4594 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_identifierSuffix4596 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_arguments_in_identifierSuffix4598 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_identifierSuffix4603 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_THIS_in_identifierSuffix4606 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_identifierSuffix4611 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000000L});
	public static final BitSet FOLLOW_SUPER_in_identifierSuffix4614 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_arguments_in_identifierSuffix4616 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_innerCreator_in_identifierSuffix4621 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_selector4633 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_selector4637 = new BitSet(new long[]{0x0000000000000002L,0x0000000000040000L});
	public static final BitSet FOLLOW_arguments_in_selector4641 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_selector4661 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_THIS_in_selector4664 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_selector4669 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000000L});
	public static final BitSet FOLLOW_SUPER_in_selector4672 = new BitSet(new long[]{0x0000004000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_superSuffix_in_selector4676 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_innerCreator_in_selector4681 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACKET_in_selector4686 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_selector4689 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_selector4691 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_creator4704 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
	public static final BitSet FOLLOW_nonWildcardTypeArguments_in_creator4708 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_creator4712 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_classCreatorRest_in_creator4716 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_creator4721 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_creator4725 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_classCreatorRest_in_creator4729 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arrayCreator_in_creator4734 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_arrayCreator4746 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_createdName_in_arrayCreator4750 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_arrayCreator4754 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_arrayCreator4756 = new BitSet(new long[]{0x0000000000000000L,0x0000000000006000L});
	public static final BitSet FOLLOW_LBRACKET_in_arrayCreator4761 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_arrayCreator4763 = new BitSet(new long[]{0x0000000000000000L,0x0000000000006000L});
	public static final BitSet FOLLOW_arrayInitializer_in_arrayCreator4769 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_arrayCreator4774 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_createdName_in_arrayCreator4778 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_arrayCreator4782 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_arrayCreator4784 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_arrayCreator4786 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_arrayCreator4791 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_arrayCreator4793 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_arrayCreator4795 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_arrayCreator4802 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_arrayCreator4804 = new BitSet(new long[]{0x0000000000000002L,0x0000000000004000L});
	public static final BitSet FOLLOW_arrayInitializer_in_variableInitializer4818 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_variableInitializer4823 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACE_in_arrayInitializer4835 = new BitSet(new long[]{0x01880180260A0800L,0x1B01042818072284L,0x0000000000000252L});
	public static final BitSet FOLLOW_variableInitializer_in_arrayInitializer4845 = new BitSet(new long[]{0x0000000020000000L,0x0000040000000000L});
	public static final BitSet FOLLOW_COMMA_in_arrayInitializer4851 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818072284L,0x0000000000000252L});
	public static final BitSet FOLLOW_variableInitializer_in_arrayInitializer4853 = new BitSet(new long[]{0x0000000020000000L,0x0000040000000000L});
	public static final BitSet FOLLOW_COMMA_in_arrayInitializer4865 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
	public static final BitSet FOLLOW_RBRACE_in_arrayInitializer4871 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_createdName4897 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primitiveType_in_createdName4902 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_innerCreator4914 = new BitSet(new long[]{0x0000000000000000L,0x0000000008000000L});
	public static final BitSet FOLLOW_NEW_in_innerCreator4917 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080004L});
	public static final BitSet FOLLOW_nonWildcardTypeArguments_in_innerCreator4922 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_innerCreator4928 = new BitSet(new long[]{0x0000000000000000L,0x00000000000C0000L});
	public static final BitSet FOLLOW_typeArguments_in_innerCreator4933 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_classCreatorRest_in_innerCreator4939 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arguments_in_classCreatorRest4951 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
	public static final BitSet FOLLOW_classBody_in_classCreatorRest4956 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LT_in_nonWildcardTypeArguments4970 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_typeList_in_nonWildcardTypeArguments4973 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_GT_in_nonWildcardTypeArguments4975 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_arguments4990 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_arguments4992 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_arguments5008 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expressionList_in_arguments5012 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_arguments5014 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_classHeader5092 = new BitSet(new long[]{0x0000000008000000L});
	public static final BitSet FOLLOW_CLASS_in_classHeader5095 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_classHeader5097 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_enumHeader5108 = new BitSet(new long[]{0x0000100000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_set_in_enumHeader5111 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_enumHeader5119 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_interfaceHeader5130 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
	public static final BitSet FOLLOW_INTERFACE_in_interfaceHeader5133 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_interfaceHeader5135 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_annotationHeader5146 = new BitSet(new long[]{0x0000000000000000L,0x0000000002000000L});
	public static final BitSet FOLLOW_MONKEYS_AT_in_annotationHeader5149 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
	public static final BitSet FOLLOW_INTERFACE_in_annotationHeader5151 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_annotationHeader5153 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_typeHeader5164 = new BitSet(new long[]{0x0000100008000000L,0x0000000002000100L});
	public static final BitSet FOLLOW_CLASS_in_typeHeader5168 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_ENUM_in_typeHeader5170 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_MONKEYS_AT_in_typeHeader5173 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
	public static final BitSet FOLLOW_INTERFACE_in_typeHeader5177 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_typeHeader5181 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_methodHeader5192 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000090084L,0x0000000000000200L});
	public static final BitSet FOLLOW_typeParameters_in_methodHeader5195 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L,0x0000000000000200L});
	public static final BitSet FOLLOW_type_in_methodHeader5199 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_VOID_in_methodHeader5203 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_methodHeader5207 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_LPAREN_in_methodHeader5209 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_fieldHeader5220 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_fieldHeader5223 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_fieldHeader5225 = new BitSet(new long[]{0x0000200020000000L,0x0000400000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_fieldHeader5228 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_fieldHeader5229 = new BitSet(new long[]{0x0000200020000000L,0x0000400000004000L});
	public static final BitSet FOLLOW_set_in_fieldHeader5233 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variableModifiers_in_localVariableHeader5250 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_localVariableHeader5252 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_localVariableHeader5254 = new BitSet(new long[]{0x0000200020000000L,0x0000400000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_localVariableHeader5257 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_localVariableHeader5258 = new BitSet(new long[]{0x0000200020000000L,0x0000400000004000L});
	public static final BitSet FOLLOW_set_in_localVariableHeader5262 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_packageDeclaration_in_synpred1_Java196 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classDeclaration_in_synpred7_Java333 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_normalClassDeclaration_in_synpred22_Java475 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_normalInterfaceDeclaration_in_synpred38_Java874 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_fieldDeclaration_in_synpred47_Java1098 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_methodDeclaration_in_synpred48_Java1103 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classDeclaration_in_synpred49_Java1108 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_explicitConstructorInvocation_in_synpred52_Java1171 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_modifiers_in_synpred54_Java1131 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080004L});
	public static final BitSet FOLLOW_typeParameters_in_synpred54_Java1137 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred54_Java1144 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_formalParameters_in_synpred54_Java1150 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L,0x0000000000000008L});
	public static final BitSet FOLLOW_THROWS_in_synpred54_Java1155 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_qualifiedNameList_in_synpred54_Java1157 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_LBRACE_in_synpred54_Java1165 = new BitSet(new long[]{0x03A811A40E0E0C10L,0x7BC155E81E0F238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_explicitConstructorInvocation_in_synpred54_Java1171 = new BitSet(new long[]{0x03A811A40E0E0C10L,0x7BC155E81E07238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_blockStatement_in_synpred54_Java1178 = new BitSet(new long[]{0x03A811A40E0E0C10L,0x7BC155E81E07238CL,0x0000000000000EF6L});
	public static final BitSet FOLLOW_RBRACE_in_synpred54_Java1183 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_interfaceFieldDeclaration_in_synpred63_Java1439 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_interfaceMethodDeclaration_in_synpred64_Java1444 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_interfaceDeclaration_in_synpred65_Java1449 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classDeclaration_in_synpred66_Java1454 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ellipsisParameterDecl_in_synpred91_Java1888 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_normalParameterDecl_in_synpred93_Java1906 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_COMMA_in_synpred93_Java1911 = new BitSet(new long[]{0x00A00080020A0000L,0x0001000002010084L});
	public static final BitSet FOLLOW_normalParameterDecl_in_synpred93_Java1915 = new BitSet(new long[]{0x0000000020000002L});
	public static final BitSet FOLLOW_normalParameterDecl_in_synpred94_Java1937 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_COMMA_in_synpred94_Java1941 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_nonWildcardTypeArguments_in_synpred98_Java2064 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_set_in_synpred98_Java2078 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_arguments_in_synpred98_Java2088 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
	public static final BitSet FOLLOW_SEMI_in_synpred98_Java2090 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_annotationMethodDeclaration_in_synpred114_Java2484 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_interfaceFieldDeclaration_in_synpred115_Java2489 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_normalClassDeclaration_in_synpred116_Java2494 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_normalInterfaceDeclaration_in_synpred117_Java2499 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_enumDeclaration_in_synpred118_Java2504 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_annotationTypeDeclaration_in_synpred119_Java2509 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_localVariableDeclarationStatement_in_synpred122_Java2625 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classOrInterfaceDeclaration_in_synpred123_Java2630 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ELSE_in_synpred128_Java2769 = new BitSet(new long[]{0x038801A4060E0C00L,0x7B0150281807228CL,0x0000000000000AD6L});
	public static final BitSet FOLLOW_statement_in_synpred128_Java2773 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_catches_in_synpred149_Java3193 = new BitSet(new long[]{0x0040000000000000L});
	public static final BitSet FOLLOW_FINALLY_in_synpred149_Java3195 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
	public static final BitSet FOLLOW_block_in_synpred149_Java3197 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_catches_in_synpred150_Java3203 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FOR_in_synpred153_Java3301 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_LPAREN_in_synpred153_Java3303 = new BitSet(new long[]{0x00A00080020A0000L,0x0001000002010084L});
	public static final BitSet FOLLOW_variableModifiers_in_synpred153_Java3309 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_type_in_synpred153_Java3314 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred153_Java3319 = new BitSet(new long[]{0x0000000010000000L});
	public static final BitSet FOLLOW_COLON_in_synpred153_Java3324 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_synpred153_Java3329 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_synpred153_Java3333 = new BitSet(new long[]{0x038801A4060E0C00L,0x7B0150281807228CL,0x0000000000000AD6L});
	public static final BitSet FOLLOW_statement_in_synpred153_Java3337 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_localVariableDeclaration_in_synpred157_Java3412 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalExpression_in_synpred159_Java3481 = new BitSet(new long[]{0x1000200000408080L,0x0414001400080000L});
	public static final BitSet FOLLOW_assignmentOperator_in_synpred159_Java3485 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_subExpression_in_synpred159_Java3489 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_conditionalExpression_in_synpred160_Java3537 = new BitSet(new long[]{0x1000200000408080L,0x0414001400080000L});
	public static final BitSet FOLLOW_assignmentOperator_in_synpred160_Java3541 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_subExpression_in_synpred160_Java3545 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_castExpression_in_synpred199_Java4183 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_synpred203_Java4218 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010080L});
	public static final BitSet FOLLOW_primitiveType_in_synpred203_Java4221 = new BitSet(new long[]{0x0000000000000000L,0x0000200000000000L});
	public static final BitSet FOLLOW_RPAREN_in_synpred203_Java4223 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_unaryExpression_in_synpred203_Java4228 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_call_in_synpred205_Java4262 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_thisIdentifierSequence_in_synpred206_Java4267 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_identifierSequence_in_synpred207_Java4273 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_THIS_in_synpred214_Java4399 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_DOT_in_synpred214_Java4408 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_identifierSequence_in_synpred214_Java4412 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDENTIFIER_in_synpred215_Java4456 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_DOT_in_synpred215_Java4465 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_identifierSequence_in_synpred215_Java4469 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACKET_in_synpred221_Java4565 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_synpred221_Java4568 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_synpred221_Java4570 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_synpred233_Java4704 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
	public static final BitSet FOLLOW_nonWildcardTypeArguments_in_synpred233_Java4708 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_synpred233_Java4712 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_classCreatorRest_in_synpred233_Java4716 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_synpred234_Java4721 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_classOrInterfaceType_in_synpred234_Java4725 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
	public static final BitSet FOLLOW_classCreatorRest_in_synpred234_Java4729 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_synpred236_Java4746 = new BitSet(new long[]{0x00800080020A0000L,0x0001000000010084L});
	public static final BitSet FOLLOW_createdName_in_synpred236_Java4750 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
	public static final BitSet FOLLOW_LBRACKET_in_synpred236_Java4754 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_synpred236_Java4756 = new BitSet(new long[]{0x0000000000000000L,0x0000000000006000L});
	public static final BitSet FOLLOW_LBRACKET_in_synpred236_Java4761 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_synpred236_Java4763 = new BitSet(new long[]{0x0000000000000000L,0x0000000000006000L});
	public static final BitSet FOLLOW_arrayInitializer_in_synpred236_Java4769 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LBRACKET_in_synpred237_Java4791 = new BitSet(new long[]{0x01880180060A0800L,0x1B01002818070284L,0x0000000000000252L});
	public static final BitSet FOLLOW_expression_in_synpred237_Java4793 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
	public static final BitSet FOLLOW_RBRACKET_in_synpred237_Java4795 = new BitSet(new long[]{0x0000000000000002L});
}
