package lang.java;

/*
Copyright (c) 2015, Vítor T. Martins
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import lang.Nexus;
import lang.Suspect;
import lang.TokenGroup;
import lang.TokenType;

import lang.java.*;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import org.antlr.stringtemplate.StringTemplate;

/**
 * A connection between an ANTLR Java grammar and Spector
 * @author  Vítor T. Martins
 */
public class JavaNexus extends Nexus {
    private final static boolean debug = false;      // Toggle debugging information for this module

    // Add groups of tokens (from the JavaLexer.java file)
    public JavaNexus() {
        super();

        // Add the Token Groups

        HashSet<Integer> typeList = new HashSet<>();

        typeList.add(JavaLexer.PACKAGE);
        groups.put(TokenGroup.Package, (HashSet<Integer>) typeList.clone());

        typeList.clear();
        typeList.add(JavaLexer.IMPORT);
        groups.put(TokenGroup.Import, (HashSet<Integer>) typeList.clone());

        typeList.clear();
        typeList.add(JavaLexer.COMMENT);
        typeList.add(JavaLexer.LINE_COMMENT);
        groups.put(TokenGroup.Comment, (HashSet<Integer>) typeList.clone());

        typeList.clear();
        typeList.add(JavaLexer.CLASS);
        typeList.add(JavaLexer.METHOD);
        typeList.add(JavaLexer.IDENTIFIER);
        typeList.add(JavaLexer.SEQUENCE);
        groups.put(TokenGroup.Identifier, (HashSet<Integer>) typeList.clone());

        typeList.clear();
        typeList.add(JavaLexer.SEQUENCE);
        groups.put(TokenGroup.Sequence, (HashSet<Integer>) typeList.clone());

        typeList.clear();
        typeList.add(JavaLexer.EXPRESSION);
        groups.put(TokenGroup.Expression, (HashSet<Integer>) typeList.clone());

        typeList.clear();
        typeList.add(JavaLexer.CONDITIONAL);
        typeList.add(JavaLexer.CASE);
        groups.put(TokenGroup.Conditional, (HashSet<Integer>) typeList.clone());

        typeList.clear();
        typeList.add(JavaLexer.CONDITION);
        groups.put(TokenGroup.Condition, (HashSet<Integer>) typeList.clone());

        typeList.clear();
        typeList.add(JavaLexer.CASE);
        groups.put(TokenGroup.Case, (HashSet<Integer>) typeList.clone());

        typeList.clear();
        typeList.add(JavaLexer.BLOCK);
        groups.put(TokenGroup.Block, (HashSet<Integer>) typeList.clone());

        typeList.clear();
        typeList.add(JavaLexer.CALL);
        groups.put(TokenGroup.Call, (HashSet<Integer>) typeList.clone());

        // Tokens

        token.put(TokenType.Equals, JavaLexer.EQEQ);
        token.put(TokenType.Call, JavaLexer.CALL);
        token.put(TokenType.Arguments, JavaLexer.ARGUMENTS);
        token.put(TokenType.Identifier, JavaLexer.IDENTIFIER);

        // Add the valid extensions

        extensions.add("java");

        // List of groups to ignore

        ignore = new HashSet<Integer>();

        // Comments
        ignore.add(JavaLexer.COMMENT);
        ignore.add(JavaLexer.LINE_COMMENT);

        // Abstract Nodes
        ignore.add(JavaLexer.ARGUMENTS);
        ignore.add(JavaLexer.EXPRESSION);
        ignore.add(JavaLexer.CONDITION);
        ignore.add(JavaLexer.BLOCK);
        ignore.add(JavaLexer.PARAMETERS);
    }


    /**
     * Checks if a file has a valid extension for this Nexus
     * @param name      The file name
     * @return Whether the file has a valid extension
     */
    @Override
    public boolean validExtension(String name) {
        // Check if the file name ends with a valid extension
        boolean valid = false;

        for(String extension : extensions) {
            if (name.endsWith(extension)) {
                valid = true;
                break;
            }
        }

        return valid;
    }


    /**
     * Parses an input file and returns a Suspect
     * @param   file    source file
     * @return  A Suspect object, containing metrics, the source code File and its AST
     */
    @Override
    public Suspect parse(File file) {
        try {
            // Make sure the file is defined and is indeed a file
            if (file == null) {
                if (debug) { System.out.println("JavaNexus.parse(null): Invalid argument."); }
                return null;
            } else if (!file.isFile()) {
                if (debug) { System.out.println("JavaNexus.parse(File): Invalid argument (doesn't lead to a file)."); }
                return null;
            }

            // Check if the file name ends with a valid extension
            String name = file.getName();
            if(!validExtension(name)) {
                if (debug) { System.out.println("JavaNexus.parse("+name+"): Invalid extension."); }
                return null;
            }

            // Make input streams to read the file
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            ANTLRInputStream ais = new ANTLRInputStream(fis);

            // Lex the file and get its tokens
            JavaLexer lexer = new JavaLexer(ais);
            CommonTokenStream tokens = new CommonTokenStream(lexer);

            // Parse the file
            JavaParser parser = new JavaParser(tokens);
            JavaParser.compilationUnit_return result = parser.compilationUnit();

            // Extract the resulting AST
            CommonTree tree = result.getTree();

            // Build the suspects database
            Suspect s = new Suspect(tokens.size(), file, tree);
            if(debug) { System.out.println("JavaNexus.parse("+file.getName()+"): Successfully parsed the file, got "+tokens.size()+" tokens."); }

            return s;
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(file != null) {
                System.out.print("(" + file.getName() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());

            return null;
        }
    }

    @Override
    public int getType(TokenType type) {
        return token.get(type);
    }

    @Override
    public boolean hasType(int nodeType, TokenType type) {
        if(nodeType == token.get(type)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if a Token belongs to a Group
     * @param   token   Token value
     * @param   group   Token group name
     * @return  Whether the Token is part of the group
     */
    @Override
    public boolean inGroup(int token, TokenGroup group) {
        try {
            HashSet<Integer> tokens = groups.get(group);
            for(int candidate : tokens) {
                if(token == candidate) {
                    return true;
                }
            }

            //throw new Exception("Unknown group of tokens.");
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println("(" + token + ", " + group + ") @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return false;
    }

    @Override
    public boolean isIgnored(int token) {
        try {
            for(int type : ignore) {
                if(token == type) {
                    return true;
                }
            }

            //throw new Exception("Unknown group of tokens.");
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());
            System.out.println("(" + token + ") @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return false;
    }

    /**
     * Produce and save an AST from a Suspect
     * @param   input    File to be parsed
     * @return  Success of the attempt
     */
    @Override
    public String toDOT(Suspect input) {
        String name = null;

        try {
            name = input.getFile().getName();
            CommonTree tree = input.getTree();
            DOTTreeGenerator dotg = new DOTTreeGenerator();
            StringTemplate st = dotg.toDOT(tree);

            if(debug) {
                System.out.print("JavaNexus.parse(");
                if(name != null) {
                    System.out.println(name + ")");
                }
                System.out.println(": Successfully generated the AST.");
            }

            return st.toString();
        } catch(Exception x) {
            StackTraceElement[] trace = x.getStackTrace();
            StackTraceElement ste = trace[trace.length - 1];

            System.out.print(x.getClass().getName() + " @ " + ste.getClassName() + "." + ste.getMethodName());

            if(input != null) {
                System.out.print(input.getName() + ")");
            }

            System.out.println(" @ " + ste.getLineNumber() + ": " + x.getMessage());
        }

        return null;
    }
}
