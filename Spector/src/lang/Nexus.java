package lang;

/*
Copyright (c) 2015, Vítor T. Martins
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import org.antlr.runtime.tree.CommonTree;

/**
 * An interface for connections between ANTLR grammars and Spector
 * @author  Vítor T. Martins
 */
public abstract class Nexus {
    // Map of Token Groups/values
    protected HashMap<TokenGroup, HashSet<Integer>> groups = null;
    // List of Tokens
    protected HashMap<TokenType, Integer> token = null;
    // List of valid Extensions
    protected HashSet<String> extensions = null;
    // List of ignored types
    protected HashSet<Integer> ignore = null;

    public Nexus() {
        groups = new HashMap<TokenGroup, HashSet<Integer>>();
        token = new HashMap<TokenType, Integer>();
        extensions = new HashSet<String>();
        ignore = new HashSet<Integer>();
    }

    /**
     * Checks if a file has a valid extension for this Nexus
     * @param name      The file name
     * @return Whether the file has a valid extension
     */
    public abstract boolean validExtension(String name);

    /**
     * Checks if a Token belongs to a Group
     * @param   token   Token value
     * @param   group   Token group name
     * @return  Whether the Token is part of the group
     */
    public abstract boolean inGroup(int token, TokenGroup group);

    public abstract boolean isIgnored(int token);

    public abstract int getType(TokenType type);

    public abstract boolean hasType(int token, TokenType type);

    /**
     * Parses an input file and returns a Suspect
     * @param   file    source file
     * @return  A Suspect object, containing metrics, the source code File and its AST
     */
    public abstract Suspect parse(File file);

    /**
     * Produce and save an AST from a Suspect
     * @param   input    File to be parsed
     * @return  String with DOT tree
     */
    public abstract String toDOT(Suspect input);
}