package lang;

/*
Copyright (c) 2015, Vítor T. Martins
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import java.io.File;
import org.antlr.runtime.tree.CommonTree;

/**
 * A suspect object with lots of information relative to the Source Code
 * @author  Vítor T. Martins
 */
public class Suspect {
    // Metrics
    private int tokens = 0;         // Number of tokens parsed
    private File file = null;       // Source Code File
    private CommonTree tree = null; // Abstract Syntax Tree

    // Constructors

    public Suspect(File file) {
        this.file = file;
    }

    public Suspect(File file, CommonTree tree) {
        this.file = file;
        this.tree = tree;
    }

    public Suspect(int tokens, File file, CommonTree tree) {
        this.tokens = tokens;
        this.file = file;
        this.tree = tree;
    }

    // Gets

    public String getName() {
        String name = null;

        if(file != null) {
            name = file.getName();
        }

        return name;
    }

    public File getFile() {
        return file;
    }

    public CommonTree getTree() {
        return tree;
    }

    public int getTokens() {
        return tokens;
    }
}